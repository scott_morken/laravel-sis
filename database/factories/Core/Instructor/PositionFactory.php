<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Instructor;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Instructor\Position;

class PositionFactory extends FactoryWithHelper
{
    protected array $incrementKeys = ['getStudentId'];

    protected $model = Position::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $studentId = $h->getStudentId($this->count > 1);

        return [
            'EMPLID' => $studentId,
            'EMPL_RCD' => $this->faker->randomNumber(1),
            'DEPTID' => $this->faker->randomNumber(3),
            'JOBCODE' => 'JOB'.$this->faker->randomNumber(2),
            'POSITION_NBR' => $this->faker->randomNumber(1),
            'DESCR' => $this->faker->words(3, true),
        ];
    }
}
