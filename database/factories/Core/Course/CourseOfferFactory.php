<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Course;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Course\CourseOffer;

class CourseOfferFactory extends FactoryWithHelper
{
    protected $model = CourseOffer::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $effdt = date('Y-m-d', strtotime('-1 year'));

        return [
            'COFFR_ACAD_CAREER' => 'CRED',
            'COFFR_ACAD_GROUP' => 'AO',
            'COFFR_ACAD_ORG' => 'AO-1',
            'COFFR_ACAD_ORG_LDESC' => 'Academic Org 1',
            'COFFR_CAMPUS' => '',
            'COFFR_CATALOG_NBR' => ' 101',
            'COFFR_CATALOG_PRINT' => 'Y',
            'COFFR_CLASS_SID' => '1',
            'COFFR_COURSE_APPROVED' => 'A',
            'COFFR_CRSE_EFFDT' => date('Y-m-d', strtotime('-1 year')),
            'COFFR_CRSE_ID' => $h->getCourseId(),
            'COFFR_CRSE_OFFER_NBR' => $h->getCourseOfferNumber(),
            'COFFR_INSTITUTION' => $h->getCollegeId(),
            'COFFR_REC_KEY' => sprintf('%s+%s+%s', $h->getCourseId(), $h->getCourseOfferNumber(), $effdt),
            'COFFR_SCHEDULE_PRINT' => 'Y',
            'COFFR_SUBJECT' => 'ABC',
            'CRSE_CATALOG_SID' => $this->faker->randomNumber(4),
        ];
    }
}
