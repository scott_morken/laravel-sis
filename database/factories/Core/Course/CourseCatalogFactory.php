<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Course;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Course\CourseCatalog;

class CourseCatalogFactory extends FactoryWithHelper
{
    protected $model = CourseCatalog::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $effdt = date('Y-m-d', strtotime('-1 year'));

        return [
            'CRSE_REC_KEY' => sprintf('%s+%s+%s', $h->getCourseId(), $h->getCourseOfferNumber(), $effdt),
            'CRSE_CATALOG_SID' => $h->getSequence(),
            'CRSE_CRSE_ID' => $h->getCourseId(),
            'CRSE_DESCR' => 'Course '.$h->getCourseId(),
            'CRSE_CONSENT' => 'N',
            'CRSE_UNITS_MINIMUM' => 3, //credits
            'CRSE_UNITS_MAXIMUM' => 3,
            'CRSE_GRADING_BASIS' => 'GRD',
            'CRSE_COMPONENT' => 'LEC',
            'CRSE_COURSE_NOTES' => '',
            'CRSE_GOV_BD_APPROVAL_DATE' => $effdt,
            'CRSE_EFFDT' => $effdt,
        ];
    }
}
