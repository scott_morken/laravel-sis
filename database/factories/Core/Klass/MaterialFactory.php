<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Klass;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Klass\Material;

class MaterialFactory extends FactoryWithHelper
{
    protected array $incrementKeys = ['getSequence'];

    protected $model = Material::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $mh = $h->getMaterialHelper();
        $sequence = $h->getSequence($this->count > 1);

        return [
            'TBMAT_SID' => $h->getTermId().$h->getClassNumber(),
            'TBMAT_SEQ_NBR' => $sequence,
            'TBMAT_TYPE_CD' => $mh->typeCode,
            'TBMAT_TYPE_CD_LDESC' => $mh->typeDesc,
            'TBMAT_STATUS_CD' => $mh->statusCode,
            'TBMAT_STATUS_LDESC' => $mh->statusDesc,
            'TBMAT_TITLE' => $this->faker->words(3, true),
            'TBMAT_ISBN_NBR' => $this->faker->isbn13(),
            'TBMAT_AUTHOR' => $this->faker->name(),
            'TBMAT_PUBLISHER' => $this->faker->company(),
            'TBMAT_EDITION' => $this->faker->randomNumber(1),
            'TBMAT_YEAR_PUBLISHED' => $this->faker->date('Y', '-2 years'),
            'TBMAT_PRICE' => $this->faker->randomFloat(2, 1, 100),
            'TBMAT_NOTES' => $this->faker->words(3, true),
        ];
    }
}
