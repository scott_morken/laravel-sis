<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Klass;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Klass\Klass;
use Smorken\Sis\Enums\Klass\SessionCodes;
use Smorken\Sis\Enums\Klass\TypeCodes;
use Smorken\Sis\Enums\Shared\GradingBases;

class KlassFactory extends FactoryWithHelper
{
    protected $model = Klass::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $kh = $h->getKlassHelper();
        $catalogNbr = '101';
        $classSubject = 'ABC';

        return [
            'CASSC_GRADING_BASIS' => GradingBases::PASS_FAIL,
            'CASSC_GRADING_BASIS_LDESC' => GradingBases::toArray()[GradingBases::PASS_FAIL],
            'CASSC_HONORS_YN' => 'N',
            'CASSC_UNITS_MINIMUM' => '3',
            'CLASS_ACAD_CAREER' => 'CRED',
            'CLASS_ACAD_ORG' => 'AO-1',
            'CLASS_ACAD_ORG_LDESC' => 'Org 1',
            'CLASS_ASSOCIATED_CLASS' => '9999',
            'CLASS_CAMPUS_CD' => 'CAMP01',
            'CLASS_CAMPUS_LDESC' => 'Campus 1',
            'CLASS_CANCEL_DATE' => null,
            'CLASS_CATALOG_NBR' => ' '.$catalogNbr,
            'CLASS_CLASS_NAME' => $classSubject.$catalogNbr,
            'CLASS_CLASS_NBR' => $h->getClassNumber(),
            'CLASS_CLASS_ROLL_YN' => 'Y',
            'CLASS_CLASS_STAT' => $kh->status,
            'CLASS_CLASS_STAT_LDESC' => $kh->statusDesc,
            'CLASS_COMBINED_ID' => '0010',
            'CLASS_COMBINED_DESCR' => 'ABC102-11111, 103',
            'CLASS_COMBINED_MAJOR_YN' => 'N',
            'CLASS_COMBINED_SECTION' => ' ', // C or space
            'CLASS_COMPONENT_CD' => $kh->componentCode,
            'CLASS_COMPONENT_LDESC' => $kh->componentDesc,
            'CLASS_CONSENT' => $kh->consent,
            'CLASS_CONSENT_LDESC' => $kh->consentDesc,
            'CLASS_COURSE_ID' => $h->getCourseId(),
            'CLASS_COURSE_OFFER_NBR' => $h->getCourseOfferNumber(),
            'CLASS_DAY_EVE' => 'D',
            'CLASS_DAY_EVE_LDESC' => 'Day',
            'CLASS_DEPTID_EXPENSE_CD' => '100-12345',
            'CLASS_DESCR' => $classSubject.$catalogNbr.' Class',
            'CLASS_ENRL_CAP' => '30',
            'CLASS_ENRL_STAT' => $kh->enrollmentStatus,
            'CLASS_ENRL_STAT_LDESC' => $kh->enrollmentStatusDesc,
            'CLASS_ENRL_TOT' => '15',
            'CLASS_HS_DUAL_ENROLL_YN' => 'N',
            'CLASS_HS_DUAL_SPONSOR' => '',
            'CLASS_HIGH_SCHOOL_LDESC' => '',
            'CLASS_HIGH_SCHOOL_SDESC' => '',
            'CLASS_HONORS' => 'N',
            'CLASS_INSTITUTION_CD' => $h->getCollegeId(),
            'CLASS_INSTRUCTION_MODE' => $kh->instructionMode,
            'CLASS_INSTRUCTION_MODE_LDESC' => $kh->instructionModeDesc,
            'CLASS_INSTRUCTOR_EMPLID' => $h->getStudentId(),
            'CLASS_INSTRUCTOR_NAME' => 'Last,First M',
            'CLASS_LOAD_CALC_CODE' => 'S',
            'CLASS_LOAD_CALC_SDESC' => 'Standard',
            'CLASS_LOCATION_CD' => 'PC Main',
            'CLASS_LOCATION_LDESC' => 'Phoenix College',
            'CLASS_MAJOR_CLASS_NAME' => $classSubject.$catalogNbr,
            'CLASS_MAJOR_CLASS_NBR' => '12344',
            'CLASS_SCHEDULE_PRINT' => 'Y',
            'CLASS_SECTION' => $h->getClassSection(),
            'CLASS_SESSION_CD' => SessionCodes::DYNAMIC_DATED,
            'CLASS_SESSION_LDESC' => SessionCodes::toArray()[SessionCodes::DYNAMIC_DATED],
            'CLASS_SID' => $h->getTermId().$h->getClassNumber(),
            'CLASS_SUBJECT_CD' => $classSubject,
            'CLASS_TERM_CD' => $h->getTermId(),
            'CLASS_TYPE_CD' => $kh->componentCode === 'LAB' ? TypeCodes::N : TypeCodes::E,
            'CLASS_TYPE_LDESC' => $kh->componentCode === 'LAB' ? TypeCodes::toArray()[TypeCodes::N] : TypeCodes::toArray()[TypeCodes::E],
            'COFFR_CLASS_SID' => $this->faker->randomNumber(4),
            'COMP_WEEK_WORKLOAD_HRS' => '2.4',
            'CLASS_TOT_WORKLOAD_HRS' => '2.4',
            'CLASS_START_DATE' => date('Y-m-d', strtotime('-1 week')),
            'CLASS_END_DATE' => date('Y-m-d', strtotime('+1 month')),
        ];
    }
}
