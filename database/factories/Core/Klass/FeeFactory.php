<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Klass;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Klass\Fee;

class FeeFactory extends FactoryWithHelper
{
    protected $model = Fee::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $sdesc = $this->faker->randomNumber(8);

        return [
            'CLSF_SID' => $this->faker->randomNumber(5),
            'CLASS_SID' => $h->getTermId().$h->getClassNumber(),
            'CLSBF_SETID' => $h->getCollegeId(), //college id
            'CLSBF_CRSE_ID' => $h->getCourseId(),
            'CLSBF_CRSE_OFFER_NBR' => $h->getCourseOfferNumber(),
            'CLSBF_STRM' => $h->getTermId(),
            'CLSBF_SESSION_CODE' => 'DD',
            'CLSBF_CLASS_SECTION' => $h->getClassSection(),
            'CLSBF_COMPONENT' => 'LEC',
            'CLSBF_ACCOUNT_TYPE_SF' => 'ACT',
            'CLSBF_ITEM_TYPE' => '10000001111111',
            'CLSBF_ITEM_TYPE_SDESC' => $sdesc,
            'CLSBF_ITEM_TYPE_LDESC' => 'Fee for '.$sdesc,
            'CLSBF_FLAT_AMT' => $this->faker->numberBetween(5, 250),
            'CLSBF_CURRENCY_CD' => 'USD',
        ];
    }
}
