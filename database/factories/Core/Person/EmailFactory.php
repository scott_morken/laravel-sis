<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Person;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Person\Email;
use Smorken\Sis\Enums\Person\EmailTypes;

class EmailFactory extends FactoryWithHelper
{
    protected $model = Email::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $studentId = $h->getStudentId($this->count > 1);
        $meid = $h->getMeid();
        $email = "${meid}@email.edu";

        return [
            'EMAIL_REC_KEY' => $h->getSequence().'-'.$email,
            'EMAIL_EMPLID' => $studentId,
            'EMAIL_ADDR_TYPE' => EmailTypes::MCCD,
            'EMAIL_ADDR_TYPE_LDESC' => EmailTypes::toArray()[EmailTypes::MCCD],
            'EMAIL_PREF_EMAIL_FLAG' => 'Y',
            'EMAIL_EMAIL_ADDR' => $email,
        ];
    }
}
