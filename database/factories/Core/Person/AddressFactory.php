<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Person;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Person\Address;
use Smorken\Sis\Oci\Constants\Person\AddressType;

class AddressFactory extends FactoryWithHelper
{
    protected $model = Address::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $studentId = $h->getStudentId($this->count > 1);

        return [
            'ADDR_REC_KEY' => $h->getSequence().'-'.$studentId,
            'ADDR_EMPLID' => $studentId,
            'ADDR_ADDRESS_TYPE' => AddressType::HOME->value,
            'ADDR_COUNTRY' => 'US',
            'ADDR_COUNTRY_LDESC' => 'United States',
            'ADDR_ADDRESS1' => $this->faker->streetAddress(),
            'ADDR_ADDRESS2' => null,
            'ADDR_ADDRESS3' => null,
            'ADDR_CITY' => $this->faker->city(),
            'ADDR_COUNTY' => null,
            'ADDR_STATE' => 'AZ',
            'ADDR_STATE_DESC' => null,
            'ADDR_POSTAL' => $this->faker->postcode(),
            'ADDR_GEO_CODE' => null,
            'ADDR_PREF_EMAIL_FLAG' => 'Y',
            'ADDR_EMAIL_ADDR' => $this->faker->safeEmail(),
            'ADDR_PHONE_COUNTRY_CD' => null,
            'ADDR_PHONE_NUMBER' => $this->faker->phoneNumber(),
            'ADDR_PHONE_EXTENSION' => null,
        ];
    }
}
