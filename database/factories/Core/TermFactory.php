<?php

namespace Database\Factories\Smorken\Sis\Db\Core;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Term;

class TermFactory extends FactoryWithHelper
{
    protected array $incrementKeys = ['getTermId'];

    protected $model = Term::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $termId = $h->getTermId($this->count > 1);

        return [
            'STRM' => $termId,
            'DESCR' => 'Term '.$termId,
            'TERM_BEGIN_DT' => \Carbon\Carbon::instance($this->faker->dateTimeBetween('-1 month', '-1 week')),
            'TERM_END_DT' => \Carbon\Carbon::instance($this->faker->dateTimeBetween('+1 week', '+1 month')),
            'INSTITUTION' => $h->getCollegeId(),
            'ACAD_CAREER' => 'CRED',
        ];
    }
}
