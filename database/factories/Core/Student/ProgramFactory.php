<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Student;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Student\Program;

class ProgramFactory extends FactoryWithHelper
{
    protected $model = Program::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $currentTerm = $h->getTermId();
        $studentId = $h->getStudentId($this->count > 1);

        return [
            'STRUC_EMPLID' => $studentId,
            'STRUC_INSTITUTION' => $h->getCollegeId(),
            'STRUC_ACAD_PROG_LDESC' => 'Degree and Cert Programs',
            'STRUC_ACAD_PROG_STATUS' => 'AC',
            'STRUC_ACAD_PROG_STATUS_LDESC' => 'Active',
            'STRUC_PROG_REASON_LDESC' => 'Active in Program',
            'STRUC_PROG_ADMIT_TERM' => $currentTerm - 1000,
            'STRUC_ACAD_PLAN_LDESC' => 'Associate in FooBar',
            'STRUC_PLAN_TYPE_LDESC' => 'Degree',
            'STRUC_ACAD_PLAN_DEGREE' => 'AAS',
            'STRUC_EXP_GRAD_TERM' => $currentTerm + 3000,
            'STRUC_ACAD_INTEREST' => null,
            'STRUC_ACAD_INTEREST_LDESC' => null,
            'STRUC_DEGR_CHECKOUT_STAT' => 'AW',
            'STRUC_DEGR_CHECKOUT_LDESC' => 'Awarded',
        ];
    }
}
