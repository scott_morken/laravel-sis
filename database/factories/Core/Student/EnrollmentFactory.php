<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Student;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Student\Enrollment;
use Smorken\Sis\Enums\Shared\GradingBases;

class EnrollmentFactory extends FactoryWithHelper
{
    protected array $incrementKeys = ['getStudentId'];

    protected $model = Enrollment::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $eh = $h->getEnrollmentHelper();
        $studentId = $h->getStudentId($this->count > 1);

        return [
            'CLASS_SID' => $this->faker->randomNumber(9),
            'EMPLID' => $studentId,
            'ENRL_INSTITUTION_CD' => $h->getCollegeId(),
            'ENRL_TERM_CD' => $h->getTermId(),
            'ENRL_CLASS_NBR' => $h->getClassNumber(),
            'ENRL_ACAD_CAREER' => 'CRED',
            'ENRL_GRADING_BASIS' => GradingBases::GRADED,
            'ENRL_OFFICIAL_GRADE' => $eh->isEnrolled() ? 'A' : null,
            'ENRL_GRADE_DATE' => $eh->isEnrolled() ? $this->faker->optional(0.1)->date('Y-m-d', '-1 day') : null,
            'ENRL_GRADE_POINTS' => $eh->isEnrolled() ? 4 : null,
            'ENRL_UNITS_TAKEN' => $eh->isEnrolled() ? 3 : 0,
            'ENRL_EARNED_CREDIT' => 'Y',
            'ENRL_STATUS_REASON_CD' => $eh->statusCode,
            'ENRL_STATUS_REASON_LDESC' => $eh->statusDesc,
            'ENRL_ACTN_RSN_LAST_CD' => $eh->lastActionCode,
            'ENRL_ACTN_RSN_LAST_LDESC' => $eh->lastActionDesc,
            'ENRL_INCLUDE_IN_GPA' => $eh->isEnrolled() ? 'Y' : '',
            'ENRL_OEE_START_DATE' => '',
            'ENRL_OEE_END_DATE' => '',
        ];
    }
}
