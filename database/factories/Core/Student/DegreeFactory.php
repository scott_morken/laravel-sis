<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Student;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Student\Degree;

class DegreeFactory extends FactoryWithHelper
{
    protected $model = Degree::class;

    public function definition(): array
    {
        $h = $this->getHelper();

        return [
            'INSTITUTION' => $h->getCollegeId(),
            'ACAD_PROG' => 'CRED',
            'ACAD_PLAN_TYPE' => 'DEG',
            'ACAD_PLAN' => $this->faker->randomNumber(4),
            'EFFDT' => $this->faker->dateTimeBetween('-5 years', '-1 year'),
            'EFF_STATUS' => 'A',
            'FIRST_TERM_VALID' => 1012,
            'DESCR' => $this->faker->words(3, true),
            'DEGREE' => 'AAS',
            'DEGREE_DESCR' => 'Associate in Applied Science',
            'DIPLOMA_DESCR' => $this->faker->words(3, true),
            'TRNSCR_DESCR' => $this->faker->words(3, true),
            'MIN_UNITS_REQD' => 60,
            'PLAN_DESCRIPTION' => $this->faker->paragraphs(2, true),
        ];
    }
}
