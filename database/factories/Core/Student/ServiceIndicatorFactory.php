<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Student;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Student\ServiceIndicator;

class ServiceIndicatorFactory extends FactoryWithHelper
{
    protected array $incrementKeys = ['getStudentId'];

    protected $model = ServiceIndicator::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $sh = $h->getServiceIndicatorHelper();
        $termId = $h->getTermId();
        $studentId = $h->getStudentId($this->count > 1);
        $meid = $h->getMeid();

        return [
            'SRVC_EMPLID' => $studentId,
            'SRVC_IND_DATETIME' => $this->faker->dateTimeBetween('-1 year', '-1 day'),
            'SRVC_OPRID' => $meid,
            'SRVC_INSTITUTION' => $h->getCollegeId(),
            'SRVC_SERVICE_IND_CD' => $sh->code,
            'SRVC_SERVICE_IND_LDESC' => $sh->description,
            'SRVC_SERVICE_IND_REASON' => $sh->reasonCode,
            'SRVC_SERVICE_IND_REASON_LDESC' => $sh->reasonDesc,
            'SRVC_SERVICE_IND_ACT_TERM' => $termId,
            'SRVC_SERVICE_IND_ACTIVE_DT' => $this->faker->dateTimeBetween('-1 year', '-1 day'),
            'SRVC_POSITIVE_SRVC_INDICATOR' => $sh->positive,
            'SRVC_AMOUNT' => $this->faker->randomFloat(2, 0, 100),
            'SRVC_COMM_COMMENTS' => $this->faker->words(5, true),
            'SRVC_CONTACT' => $this->faker->name(),
            'SRVC_DEPARTMENT' => 'D'.$this->faker->numberBetween(100, 999),
            'SRVC_DEPARTMENT_LDESC' => $h->getCollegeId().' Department',
        ];
    }
}
