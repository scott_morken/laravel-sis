<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Student;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Student\StudentTerm;

class StudentTermFactory extends FactoryWithHelper
{
    protected array $incrementKeys = ['getTermId', 'getStudentId'];

    protected $model = StudentTerm::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $sth = $h->getStudentTermHelper();
        $termId = $h->getTermId($this->count > 1);
        $studentId = $h->getStudentId($this->count > 1);

        return [
            'EMPLID' => $studentId,
            'CTERM_TERM_CD' => $termId,
            'CTERM_TERM_SDESC' => 'Term '.$termId,
            'CTERM_INSTITUTION_CODE' => $h->getCollegeId(),
            'CTERM_ACAD_CAREER' => 'CRED',
            'STFACT_CUM_GPA' => $this->faker->randomFloat(2, 1, 4),
            'STFACT_TERM_GPA' => $this->faker->randomFloat(2, 1, 4),
            'STFACT_MCCD_CUM_AHRS' => rand(10, 60),
            'STFACT_MCCD_CUM_EHRS' => rand(10, 60),
            'STFACT_MCCD_CUM_QHRS' => rand(10, 60),
            'STFACT_MCCD_CUM_QPTS' => $this->faker->randomFloat(2, 1, 4),
            'STFACT_MCCD_CUM_CLOCK_HRS' => $this->faker->randomFloat(2, 1, 64),
            'STFACT_MCCD_TERM_AHRS' => rand(1, 15),
            'STFACT_MCCD_TERM_EHRS' => rand(1, 15),
            'STFACT_MCCD_TERM_QHRS' => rand(1, 15),
            'STFACT_MCCD_TERM_QPTS' => $this->faker->randomFloat(2, 1, 4),
            'STFACT_MCCD_TERM_CLOCK_HRS' => $this->faker->randomFloat(2, 1, 15),
            'CTERM_ACAD_STNDNG_ACTN' => $sth->standingCode,
            'CTERM_ACAD_STDNG_LDESC' => $sth->standingDesc,
            'CTERM_ACAD_LOAD_CODE' => $sth->loadCode,
            'CTERM_ACAD_LOAD_LDESC' => $sth->loadDesc,
            'STFACT_TOT_INPROG_GPA' => $this->faker->randomFloat(1, 0, 4),
            'STFACT_TOT_INPROG_NOGPA' => $this->faker->randomFloat(1, 0, 4),
        ];
    }
}
