<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Student;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Illuminate\Support\Str;
use Smorken\Sis\Db\Core\Student\Group;

class GroupFactory extends FactoryWithHelper
{
    protected $model = Group::class;

    public function definition(): array
    {
        $groupName = strtoupper(Str::random(5));
        $h = $this->getHelper();
        $studentId = $h->getStudentId($this->count > 1);

        return [
            'STDGR_INSTITUTION' => $h->getCollegeId(),
            'STDGR_EMPLID' => $studentId,
            'STDGR_STUDENT_GROUP' => $groupName,
            'STDGR_STDNT_GROUP_LDESC' => 'Group description for '.$groupName,
            'STDGR_START_DATE' => $this->faker->dateTimeBetween('-2 years', '-6 months'),
            'STDGR_END_DATE' => $this->faker->date('Y-m-d', '+1 week'),
            'STDGR_STUDENT_STATUS' => 'A',
            'STDGR_STUDENT_STATUS_LDESC' => 'Active',
            'STDGR_COMMENTS' => $this->faker->words(5, true),
        ];
    }
}
