<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Student;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Core\Student\Athlete;

class AthleteFactory extends FactoryWithHelper
{
    protected array $incrementKeys = ['getStudentId'];

    protected $model = Athlete::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $studentId = $h->getStudentId($this->count > 1);

        return [
            'ATHL_INSTITUTION' => $h->getCollegeId(),
            'ATHL_EMPLID' => $studentId,
            'ATHL_SPORT' => $this->faker->randomElement(['FB', 'BSK', 'MSC']),
            'ATHL_SPORT_LDESC' => $this->faker->text(32),
            'ATHL_ATHLETIC_PARTIC_CD' => 'ACTIV',
            'ATHL_ATHLETIC_PARTIC_LDESC' => 'Active',
            'ATHL_NCAA_ELIGIBLE' => 'Y',
            'ATHL_CURRENT_PARTICIPANT' => 'Y',
            'ATHL_START_DATE' => date('Y-m-d', strtotime('-1 month')),
            'ATHL_END_DATE' => null,
        ];
    }
}
