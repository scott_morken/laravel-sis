<?php

namespace Database\Factories\Smorken\Sis\Db\Core\Physical;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Smorken\Sis\Db\Core\Physical\Facility;

class FacilityFactory extends Factory
{
    protected $model = Facility::class;

    public function definition(): array
    {
        $building = strtoupper(Str::random(1));
        $roomNumber = $this->faker->numberBetween(100, 399);

        return [
            'FACILITY_SETID' => 'PCC01',
            'FACILITY_ID' => $building.$roomNumber,
            'FACILITY_EFF_STATUS' => 'A',
            'FACILITY_BLDG_CD' => 'A '.$building.' BLDG',
            'FACILITY_ROOM' => $roomNumber,
            'FACILITY_DESCR' => $building.$roomNumber.' Room',
            'FACILITY_DESCRSHORT' => $building.$roomNumber,
            'FACILITY_GROUP' => 'CLAS',
            'FACILITY_LOCATION' => 'PC MAIN',
            'FACILITY_CAPACITY' => '25',
            'FACILITY_ACAD_ORG' => 'AO-1',
        ];
    }
}
