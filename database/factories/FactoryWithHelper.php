<?php

namespace Database\Factories\Smorken\Sis\Db;

use Database\Factories\Smorken\Sis\Db\Concerns\HasHelper;
use Illuminate\Database\Eloquent\Factories\Factory;

abstract class FactoryWithHelper extends Factory
{
    use HasHelper;
}
