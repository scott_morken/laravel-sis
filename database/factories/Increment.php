<?php

namespace Database\Factories\Smorken\Sis\Db;

class Increment
{
    protected static array $increment = [];

    protected array $defaultIncrement = [
        'classNumber' => 10000,
        'classSection' => 10,
        'courseId' => 100000,
        'courseOfferNumber' => 1,
        'hrmsId' => 10000000,
        'studentId' => 30000000,
        'sequence' => 1,
        'termId' => 1202,
    ];

    public function __construct(array $increment = [])
    {
        $this->initDefaults();
        if (empty(self::$increment)) {
            self::$increment = $this->defaultIncrement;
        }
        foreach ($increment as $k => $v) {
            $this->setIncrementing($k, $v);
        }
    }

    public static function resetAll(): void
    {
        self::$increment = [];
        new static;
    }

    public function getIncrementing(string $key, int $incrementBy = 0): int
    {
        $value = self::$increment[$key] ?? 1;
        if ($incrementBy > 0) {
            $this->increment($key, $incrementBy);
        }

        return $value;
    }

    public function increment(string $key, int $incrementBy = 1): int
    {
        $next = (self::$increment[$key] ?? 1) + $incrementBy;
        self::$increment[$key] = $next;

        return $next;
    }

    public function reset(?string $key = null): void
    {
        if ($key) {
            $this->setIncrementing($key, $this->defaultIncrement[$key] ?? 1);

            return;
        }
        self::$increment = [];
        foreach ($this->defaultIncrement as $k => $v) {
            $this->setIncrementing($k, $v);
        }
    }

    public function setIncrementing(string $key, int $value): static
    {
        self::$increment[$key] = $value;

        return $this;
    }

    protected function initDefaults(): void
    {
        $termId = (int) sprintf('1%d2', date('y'));
        $this->defaultIncrement['termId'] = $termId;
    }
}
