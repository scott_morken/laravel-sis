<?php

namespace Database\Factories\Smorken\Sis\Db\Traits;

trait FromTo
{
    abstract protected function getMap(): array;

    protected function convertTo(array $otherDef): array
    {
        $map = $this->getMap();
        $to = [];
        foreach ($map as $toKey => $fromKey) {
            $fromKey = $fromKey ?? $toKey;
            $to[$toKey] = $otherDef[$fromKey] ?? null;
        }

        return $to;
    }
}
