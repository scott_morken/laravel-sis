<?php

namespace Database\Factories\Smorken\Sis\Db\Helpers;

/**
 * @property string $typeCode
 * @property string $typeDesc
 * @property string $statusCode
 * @property string $statusDesc
 */
class MaterialHelper extends BaseHelper
{
    const STATUS_CHOICE = 'CH';

    const STATUS_CHOICE_RECOMMENDED = 'CRM';

    const STATUS_RECOMMENDED = 'REC';

    const STATUS_REQUIRED = 'REQ';

    const TYPE_CRS_PACKET = 'CRS PACKET';

    const TYPE_EBOOK = 'EBOOK';

    const TYPE_PACKAGE = 'PACKAGE';

    const TYPE_SUPPLY = 'SUPPLY';

    const TYPE_TEXTBOOK = 'TEXTBOOK';

    protected array $map = [
        self::STATUS_CHOICE => [
            'statusCode' => self::STATUS_CHOICE,
            'statusDesc' => 'Choice',
        ],
        self::STATUS_CHOICE_RECOMMENDED => [
            'statusCode' => self::STATUS_CHOICE_RECOMMENDED,
            'statusDesc' => 'Choice Component Recommended',
        ],
        self::STATUS_RECOMMENDED => [
            'statusCode' => self::STATUS_RECOMMENDED,
            'statusDesc' => 'Recommended',
        ],
        self::STATUS_REQUIRED => [
            'statusCode' => self::STATUS_REQUIRED,
            'statusDesc' => 'Required',
        ],
        self::TYPE_CRS_PACKET => [
            'typeCode' => self::TYPE_CRS_PACKET,
            'typeDesc' => 'Course Packet',
        ],
        self::TYPE_EBOOK => [
            'typeCode' => self::TYPE_EBOOK,
            'typeDesc' => 'eBook',
        ],
        self::TYPE_PACKAGE => [
            'typeCode' => self::TYPE_PACKAGE,
            'typeDesc' => 'Package',
        ],
        self::TYPE_SUPPLY => [
            'typeCode' => self::TYPE_SUPPLY,
            'typeDesc' => 'Supply',
        ],
        self::TYPE_TEXTBOOK => [
            'typeCode' => self::TYPE_TEXTBOOK,
            'typeDesc' => 'Textbook',
        ],
    ];

    public function get(string $status = self::STATUS_REQUIRED, string $type = self::TYPE_TEXTBOOK): static
    {
        $this->fromMap($status);
        $this->fromMap($type);

        return $this;
    }
}
