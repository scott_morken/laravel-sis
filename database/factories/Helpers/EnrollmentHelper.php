<?php

namespace Database\Factories\Smorken\Sis\Db\Helpers;

/**
 * @property string $lastActionCode
 * @property string $lastActionDesc
 * @property string $statusCode
 * @property string $statusDesc
 */
class EnrollmentHelper extends BaseHelper
{
    const CANCELLED = 'CANC';

    const DROPPED = 'DROP';

    const DROPWAITLIST = 'DRWL';

    const ENROLLED = 'ENRL';

    const ENROLLWAITLIST = 'EWAT';

    const FULL = 'FULL';

    const WITHDRAW = 'WDRW';

    public array $map = [
        self::CANCELLED => [
            'statusCode' => self::CANCELLED,
            'statusDesc' => 'Dropped (class cancelled)',
        ],
        self::DROPPED => [
            'statusCode' => self::DROPPED,
            'statusDesc' => 'Dropped (was enrolled)',
        ],
        self::DROPWAITLIST => [
            'statusCode' => self::DROPWAITLIST,
            'statusDesc' => 'Dropped (was wait listed)',
        ],
        self::ENROLLED => [
            'statusCode' => self::ENROLLED,
            'statusDesc' => 'Enrolled',
        ],
        self::FULL => [
            'statusCode' => self::FULL,
            'statusDesc' => 'Section is Full',
        ],
        self::ENROLLWAITLIST => [
            'statusCode' => self::ENROLLWAITLIST,
            'statusDesc' => 'Enrolled from Wait List',
        ],
        self::WITHDRAW => [
            'statusCode' => self::WITHDRAW,
            'statusDesc' => 'Withdrawn from Class',
        ],
    ];

    public function get(string $status = self::ENROLLED): static
    {
        $this->fromMap($status);
        $this->attributes['statusCode'] = $status;
        if (! $this->isEnrolled()) {
            $this->attributes['lastActionCode'] = $this->faker->randomNumber(2);
            $this->attributes['lastActionDesc'] = $this->faker->words(3, true);
        }

        return $this;
    }

    public function isEnrolled(): bool
    {
        $enrolled = [self::ENROLLED, self::ENROLLWAITLIST];

        return in_array($this->statusCode, $enrolled);
    }
}
