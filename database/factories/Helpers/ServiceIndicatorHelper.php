<?php

namespace Database\Factories\Smorken\Sis\Db\Helpers;

/**
 * @property string $code
 * @property string $description
 * @property string $positive
 * @property string $reasonCode
 * @property string $reasonDesc
 */
class ServiceIndicatorHelper extends BaseHelper
{
    const ADM = 'ADM';

    const ADX = 'ADX';

    const ATH = 'ATH';

    const DBT = 'DBT';

    const ENH = 'ENH';

    const FAD = 'FAD';

    const HON = 'HON';

    const TRS = 'TRS';

    const VA = 'VA';

    const VET = 'VET';

    protected array $map = [
        self::ADM => [
            'code' => self::ADM,
            'description' => 'Administrative Hold',
            'positive' => 'N',
        ],
        self::ADX => [
            'code' => self::ADX,
            'description' => 'District Wide Admin Hold',
            'positive' => 'N',
        ],
        self::ATH => [
            'code' => self::ATH,
            'description' => 'Athletic Eligibility',
            'positive' => 'N',
        ],
        self::DBT => [
            'code' => self::DBT,
            'description' => 'MCCCD Debt Owed',
            'positive' => 'N',
        ],
        self::ENH => [
            'code' => self::ENH,
            'description' => 'Enrollment Hold',
            'positive' => 'N',
        ],
        self::FAD => [
            'code' => self::FAD,
            'description' => 'Financial Aid Disbursement Hld',
            'positive' => 'N',
        ],
        self::HON => [
            'code' => self::HON,
            'description' => 'Honors',
            'positive' => 'Y',
        ],
        self::TRS => [
            'code' => self::TRS,
            'description' => 'Hard Copy Transcript',
            'positive' => 'Y',
        ],
        self::VA => [
            'code' => self::VA,
            'description' => 'Veterans Department',
            'positive' => 'Y',
        ],
        self::VET => [
            'code' => self::VET,
            'description' => 'Veteran Benefits Certified',
            'positive' => 'N',
        ],
    ];

    public function get(string $code = self::ADM): static
    {
        $this->fromMap($code);
        $this->setReasons();

        return $this;
    }

    protected function setReasons(): void
    {
        if (! isset($this->attributes['reasonCode'])) {
            $this->attributes['reasonCode'] = 'R'.$this->code;
        }
        if (! isset($this->attributes['reasonDesc'])) {
            $this->attributes['reasonDesc'] = 'R: '.$this->description;
        }
    }
}
