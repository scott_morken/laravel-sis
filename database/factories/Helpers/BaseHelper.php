<?php

namespace Database\Factories\Smorken\Sis\Db\Helpers;

use Faker\Generator;

class BaseHelper
{
    protected array $attributes = [];

    protected array $map = [];

    public function __construct(protected Generator $faker) {}

    public function __get(string $key): ?string
    {
        return $this->attributes[$key] ?? null;
    }

    protected function fromMap(string $key): void
    {
        $this->attributes = array_merge($this->map[$key] ?? [], $this->attributes);
    }
}
