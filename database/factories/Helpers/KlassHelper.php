<?php

namespace Database\Factories\Smorken\Sis\Db\Helpers;

/**
 * @property string $componentCode
 * @property string $componentDesc
 * @property string $consent
 * @property string $consentDesc
 * @property string $enrollmentStatus
 * @property string $enrollmentStatusDesc
 * @property string $instructionMode
 * @property string $instructionModeDesc
 * @property string $status
 * @property string $statusDesc
 */
class KlassHelper extends BaseHelper
{
    const COMPONENT_LAB = 'LAB';

    const COMPONENT_LECTURE = 'LEC';

    const COMPONENT_LECTURE_LAB = 'L+L';

    const CONSENT_DEPARTMENT = 'D';

    const CONSENT_INSTRUCTOR = 'I';

    const CONSENT_NONE = 'N';

    const ENROLLMENT_CLOSED = 'C';

    const ENROLLMENT_OPEN = 'O';

    const INSTRUCTION_FIELD = 'FB';

    const INSTRUCTION_HYBRID = 'HY';

    const INSTRUCTION_HYBRID_VIRTUAL = 'HV';

    const INSTRUCTION_INDEPENDENT = 'IS';

    const INSTRUCTION_IN_PERSON = 'P';

    const INSTRUCTION_LIVE_ONLINE = 'LO';

    const INSTRUCTION_ONLINE = 'IN';

    const INSTRUCTION_PARTNERSHIP = 'ES';

    const INSTRUCTION_PRIVATE = 'PI';

    const KEY_COMPONENT = 'component';

    const KEY_CONSENT = 'consent';

    const KEY_ENROLLMENT = 'enrollment';

    const KEY_INSTRUCTION = 'instruction';

    const KEY_STATUS = 'status';

    const STATUS_ACTIVE = 'A';

    const STATUS_CANCELLED = 'X';

    const STATUS_STOP = 'S';

    const STATUS_TENTATIVE = 'T';

    protected array $map = [
        self::KEY_COMPONENT => [
            self::COMPONENT_LAB => [
                'componentCode' => self::COMPONENT_LAB,
                'componentDesc' => 'Laboratory',
            ],
            self::COMPONENT_LECTURE => [
                'componentCode' => self::COMPONENT_LECTURE,
                'componentDesc' => 'Lecture',
            ],
            self::COMPONENT_LECTURE_LAB => [
                'componentCode' => self::COMPONENT_LECTURE_LAB,
                'componentDesc' => 'Lecture & Lab',
            ],
        ],
        self::KEY_CONSENT => [
            self::CONSENT_DEPARTMENT => [
                'consent' => self::CONSENT_DEPARTMENT,
                'consentDesc' => 'Department Consent Required',
            ],
            self::CONSENT_INSTRUCTOR => [
                'consent' => self::CONSENT_INSTRUCTOR,
                'consentDesc' => 'Instructor Consent Required',
            ],
            self::CONSENT_NONE => [
                'consent' => self::CONSENT_NONE,
                'consentDesc' => 'No Special Consent Required',
            ],
        ],
        self::KEY_ENROLLMENT => [
            self::ENROLLMENT_CLOSED => [
                'enrollmentStatus' => self::ENROLLMENT_CLOSED,
                'enrollmentStatusDesc' => 'Closed',
            ],
            self::ENROLLMENT_OPEN => [
                'enrollmentStatus' => self::ENROLLMENT_OPEN,
                'enrollmentStatusDesc' => 'Open',
            ],
        ],
        self::KEY_INSTRUCTION => [
            self::INSTRUCTION_FIELD => [
                'instructionMode' => self::INSTRUCTION_FIELD,
                'instructionModeDesc' => 'Field Based',
            ],
            self::INSTRUCTION_HYBRID => [
                'instructionMode' => self::INSTRUCTION_HYBRID,
                'instructionModeDesc' => 'Hybrid',
            ],
            self::INSTRUCTION_HYBRID_VIRTUAL => [
                'instructionMode' => self::INSTRUCTION_HYBRID_VIRTUAL,
                'instructionModeDesc' => 'Hybrid Virtual',
            ],
            self::INSTRUCTION_IN_PERSON => [
                'instructionMode' => self::INSTRUCTION_IN_PERSON,
                'instructionModeDesc' => 'In Person',
            ],
            self::INSTRUCTION_INDEPENDENT => [
                'instructionMode' => self::INSTRUCTION_INDEPENDENT,
                'instructionModeDesc' => 'Independent Study',
            ],
            self::INSTRUCTION_LIVE_ONLINE => [
                'instructionMode' => self::INSTRUCTION_LIVE_ONLINE,
                'instructionModeDesc' => 'Live Online',
            ],
            self::INSTRUCTION_ONLINE => [
                'instructionMode' => self::INSTRUCTION_ONLINE,
                'instructionModeDesc' => 'Online',
            ],
            self::INSTRUCTION_PARTNERSHIP => [
                'instructionMode' => self::INSTRUCTION_PARTNERSHIP,
                'instructionModeDesc' => 'Educational Serv Partnership',
            ],
            self::INSTRUCTION_PRIVATE => [
                'instructionMode' => self::INSTRUCTION_PRIVATE,
                'instructionModeDesc' => 'Private Instruction',
            ],
        ],
        self::KEY_STATUS => [
            self::STATUS_ACTIVE => [
                'status' => self::STATUS_ACTIVE,
                'statusDesc' => 'Active',
            ],
            self::STATUS_CANCELLED => [
                'status' => self::STATUS_CANCELLED,
                'statusDesc' => 'Cancelled Section',
            ],
            self::STATUS_STOP => [
                'status' => self::STATUS_STOP,
                'statusDesc' => 'Stop Further Enrollment',
            ],
            self::STATUS_TENTATIVE => [
                'status' => self::STATUS_TENTATIVE,
                'statusDesc' => 'Tentative Section',
            ],
        ],
    ];

    public function get(array $properties = []): static
    {
        $properties = $this->makeProperties($properties);
        foreach ($properties as $key => $default) {
            $this->setByKeyAndItem($key, $default);
        }

        return $this;
    }

    protected function getDefaults(): array
    {
        return [
            self::KEY_COMPONENT => self::COMPONENT_LECTURE,
            self::KEY_CONSENT => self::CONSENT_NONE,
            self::KEY_ENROLLMENT => self::ENROLLMENT_OPEN,
            self::KEY_INSTRUCTION => self::INSTRUCTION_IN_PERSON,
            self::KEY_STATUS => self::STATUS_ACTIVE,
        ];
    }

    protected function makeProperties(array $properties): array
    {
        return array_merge($this->getDefaults(), $properties);
    }

    protected function setByKeyAndItem(string $key, string $item): void
    {
        $items = $this->map[$key] ?? [];
        $values = $items[$item] ?? [];
        foreach ($values as $k => $v) {
            $this->attributes[$k] = $v;
        }
    }
}
