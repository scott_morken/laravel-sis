<?php

namespace Database\Factories\Smorken\Sis\Db\Helpers;

/**
 * @property string $standingCode
 * @property string $standingDesc
 * @property string $loadCode
 * @property string $loadDesc
 */
class StudentTermHelper extends BaseHelper
{
    const LOAD_FULLTIME = 'F';

    const LOAD_HALFTIME = ' H';

    const LOAD_LESS_THAN_HALFTIME = 'L';

    const LOAD_NONE = 'N';

    const LOAD_THREEQUARTER_TIME = 'T';

    const STANDING_ACCEPTABLE = 'ACPT';

    const STANDING_CONTINUED_PROBATION = 'CONT';

    const STANDING_PROBATION = 'PROB';

    protected array $map = [
        self::STANDING_ACCEPTABLE => [
            'standingCode' => self::STANDING_ACCEPTABLE,
            'standingDesc' => 'Acceptable',
        ],
        self::STANDING_PROBATION => [
            'standingCode' => self::STANDING_PROBATION,
            'standingDesc' => 'Probation',
        ],
        self::STANDING_CONTINUED_PROBATION => [
            'standingCode' => self::STANDING_CONTINUED_PROBATION,
            'standingDesc' => 'Continued Probation',
        ],
        self::LOAD_NONE => [
            'loadCode' => self::LOAD_NONE,
            'loadDesc' => 'No Unit Load',
        ],
        self::LOAD_FULLTIME => [
            'loadCode' => self::LOAD_FULLTIME,
            'loadDesc' => 'Enrolled Full-Time',
        ],
        self::LOAD_LESS_THAN_HALFTIME => [
            'loadCode' => self::LOAD_LESS_THAN_HALFTIME,
            'loadDesc' => 'Less than Half-Time',
        ],
        self::LOAD_HALFTIME => [
            'loadCode' => self::LOAD_HALFTIME,
            'loadDesc' => 'Enrolled Half-Time',
        ],
        self::LOAD_THREEQUARTER_TIME => [
            'loadCode' => self::LOAD_THREEQUARTER_TIME,
            'loadDesc' => 'Enrolled Half-Time',
        ],
    ];

    public function get(string $standing = self::STANDING_ACCEPTABLE, string $load = self::LOAD_FULLTIME): static
    {
        $this->fromMap($standing);
        $this->fromMap($load);

        return $this;
    }
}
