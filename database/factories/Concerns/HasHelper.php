<?php

namespace Database\Factories\Smorken\Sis\Db\Concerns;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Database\Factories\Smorken\Sis\Db\Helper;

trait HasHelper
{
    protected ?Helper $helper = null;

    public function getHelper(): Helper
    {
        if (! $this->helper) {
            $this->helper = new Helper($this->faker);
        }

        return $this->helper;
    }

    public function setHelper(Helper $helper): void
    {
        $this->helper = $helper;
    }

    public function increment(string|array|null $key = null): FactoryWithHelper
    {
        if ($key) {
            $key = (array) $key;
            foreach ($key as $k) {
                $this->incrementKey($k);
            }

            return $this;
        }
        if (property_exists($this, 'incrementKeys')) {
            foreach ($this->incrementKeys as $key) {
                $this->incrementKey($key);
            }
        }

        return $this;
    }

    protected function incrementKey(string $key): int
    {
        if (method_exists($this->getHelper(), $key)) {
            return $this->getHelper()->$key(true);
        }

        return $this->getHelper()->getIncrementing($key);
    }
}
