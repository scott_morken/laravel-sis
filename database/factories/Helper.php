<?php

namespace Database\Factories\Smorken\Sis\Db;

use Database\Factories\Smorken\Sis\Db\Helpers\EnrollmentHelper;
use Database\Factories\Smorken\Sis\Db\Helpers\KlassHelper;
use Database\Factories\Smorken\Sis\Db\Helpers\MaterialHelper;
use Database\Factories\Smorken\Sis\Db\Helpers\ServiceIndicatorHelper;
use Database\Factories\Smorken\Sis\Db\Helpers\StudentTermHelper;
use Faker\Generator;
use Illuminate\Support\Str;

class Helper
{
    protected array $defaults = [
        'collegeId' => 'COLL01',
    ];

    protected Increment $increment;

    public function __construct(protected Generator $faker, ?Increment $increment = null, array $defaults = [])
    {
        if (is_null($increment)) {
            $increment = new Increment;
        }
        $this->increment = $increment;
        foreach ($defaults as $k => $v) {
            $this->setByKey($k, $v);
        }
    }

    public function getBirthdate(): string
    {
        $min = strtotime('50 years ago');
        $max = strtotime('18 years ago');
        $time = mt_rand($min, $max);

        return date('Y-m-d', $time);
    }

    public function getByKey(string $key, mixed $default = null): mixed
    {
        return $this->defaults[$key] ?? $default;
    }

    public function getClassNumber(bool $increment = false): int
    {
        return $this->getIncrementing('classNumber', $increment ? 1 : 0);
    }

    public function getClassSection(bool $increment = false): int
    {
        return $this->getIncrementing('classSection', $increment ? 1 : 0);
    }

    public function getCollegeId(): string
    {
        return $this->getByKey('collegeId', 'COLL01');
    }

    public function getCourseId(bool $increment = false): int
    {
        return $this->getIncrementing('courseId', $increment ? 1 : 0);
    }

    public function getCourseOfferNumber(bool $increment = false): int
    {
        return $this->getIncrementing('courseOfferNumber', $increment ? 1 : 0);
    }

    public function getEnrollmentHelper(string $status = EnrollmentHelper::ENROLLED): EnrollmentHelper
    {
        $eh = new EnrollmentHelper($this->faker);

        return $eh->get($status);
    }

    public function getHrmsId(bool $increment = false): int
    {
        return $this->getIncrementing('hrmsId', $increment ? 1 : 0);
    }

    public function getIncrement(): Increment
    {
        return $this->increment;
    }

    public function getIncrementing(string $key, int $incrementBy = 1): int
    {
        return $this->getIncrement()->getIncrementing($key, $incrementBy);
    }

    public function getKlassHelper(array $properties = []): KlassHelper
    {
        $kh = new KlassHelper($this->faker);

        return $kh->get($properties);
    }

    public function getMaterialHelper(
        string $status = MaterialHelper::STATUS_REQUIRED,
        string $type = MaterialHelper::TYPE_TEXTBOOK
    ): MaterialHelper {
        $mh = new MaterialHelper($this->faker);

        return $mh->get($status, $type);
    }

    public function getMeid(): string
    {
        $counts = [3, 5];
        $count = $counts[array_rand($counts)];
        $str = strtoupper(Str::random($count));
        $digitCount = 10 - strlen($str);

        return $str.substr($this->getStudentId(), $digitCount * -1);
    }

    public function getSequence(bool $increment = false): int
    {
        return $this->getIncrementing('sequence', $increment ? 1 : 0);
    }

    public function getServiceIndicatorHelper(string $code = ServiceIndicatorHelper::ADM): ServiceIndicatorHelper
    {
        $sh = new ServiceIndicatorHelper($this->faker);

        return $sh->get($code);
    }

    public function getStudentId(bool $increment = false): int
    {
        return $this->getIncrementing('studentId', $increment ? 1 : 0);
    }

    public function getStudentTermHelper(
        string $standing = StudentTermHelper::STANDING_ACCEPTABLE,
        string $load = StudentTermHelper::LOAD_FULLTIME
    ): StudentTermHelper {
        $sth = new StudentTermHelper($this->faker);

        return $sth->get($standing, $load);
    }

    public function getTermId(bool $increment = false): int
    {
        return $this->getIncrementing('termId', $increment ? 1000 : 0);
    }

    public function setByKey(string $key, mixed $value): static
    {
        $this->defaults[$key] = $value;

        return $this;
    }
}
