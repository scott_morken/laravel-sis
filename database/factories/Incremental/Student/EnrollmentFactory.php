<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental\Student;

use Smorken\Sis\Db\Incremental\Student\Enrollment;

class EnrollmentFactory extends \Database\Factories\Smorken\Sis\Db\Core\Student\EnrollmentFactory
{
    protected $model = Enrollment::class;

    public function definition(): array
    {
        $def = parent::definition();

        return $this->getCdsValues($def);
    }

    protected function getCdsValues(array $rdsDef): array
    {
        $map = [
            'ENRL_EMPLID' => 'EMPLID',
            'ENRL_INSTITUTION_CD' => null,
            'ENRL_TERM_CD' => null,
            'ENRL_CLASS_NBR' => null,
            'ENRL_ACAD_CAREER' => null,
            'ENRL_GRADING_BASIS' => null,
            'ENRL_OFFICIAL_GRADE' => null,
            'ENRL_EARNED_CREDIT' => null,
            'ENRL_INCLUDE_IN_GPA' => null,
            'ENRL_GRADE_DATE' => null,
            'ENRL_GRADE_POINTS' => null,
            'ENRL_UNITS_TAKEN' => null,
            'ENRL_STATUS_REASON_CD' => null,
            'ENRL_ACTN_RSN_LAST_CD' => null,
            'ENRL_ACTN_RSN_LAST_LDESC' => null,
            'ENRL_OEE_START_DATE' => null,
            'ENRL_OEE_END_DATE' => null,
        ];
        $cds = [];
        foreach ($map as $cdsKey => $rdsKey) {
            $rdsKey = $rdsKey ?? $cdsKey;
            $cds[$cdsKey] = $rdsDef[$rdsKey] ?? null;
        }

        return $cds;
    }
}
