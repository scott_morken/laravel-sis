<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental\Student;

use Database\Factories\Smorken\Sis\Db\Traits\FromTo;
use Smorken\Sis\Db\Incremental\Student\StudentTerm;

class StudentTermFactory extends \Database\Factories\Smorken\Sis\Db\Core\Student\StudentTermFactory
{
    use FromTo;

    protected $model = StudentTerm::class;

    public function definition(): array
    {
        $rdsDef = parent::definition();

        return $this->convertTo($rdsDef);
    }

    protected function getMap(): array
    {
        return [
            'CTERM_EMPLID' => 'EMPLID',
            'CTERM_TERM_CD' => null,
            'CTERM_INSTITUTION_CODE' => null,
            'CTERM_ACAD_CAREER' => null,
            'STFACT_MCCD_TERM_AHRS' => null,
            'STFACT_MCCD_TERM_EHRS' => null,
            'STFACT_MCCD_TERM_QHRS' => null,
            'STFACT_MCCD_TERM_QPTS' => null,
            'STFACT_MCCD_TERM_CLOCK_HOURS' => null,
            'STFACT_MCCD_CUM_AHRS' => null,
            'STFACT_MCCD_CUM_EHRS' => null,
            'STFACT_MCCD_CUM_QHRS' => null,
            'STFACT_MCCD_CUM_QPTS' => null,
            'STFACT_MCCD_CUM_CLOCK_HOURS' => null,
            'CTERM_ACAD_STNDNG_ACTN' => null,
            'CTERM_ACAD_LOAD_CODE' => null,
        ];
    }
}
