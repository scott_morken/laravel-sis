<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental\Person;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Incremental\Person\Person;

class PersonFactory extends FactoryWithHelper
{
    protected array $incrementKeys = ['getStudentId', 'getHrmsId'];

    protected $model = Person::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $studentId = $h->getStudentId($this->count > 1);
        $hrmsId = $h->getHrmsId($this->count > 1);

        return [
            'PERS_EMPLID' => $studentId,
            'PERS_OPRID' => $h->getMeid(),
            'PERS_PREFERRED_FIRST_NAME' => '',
            'PERS_PREFERRED_LAST_NAME' => '',
            'PERS_PRIMARY_FIRST_NAME' => $this->faker->firstName(),
            'PERS_PRIMARY_LAST_NAME' => $this->faker->lastName(),
            'PERS_EMAIL_ADDR' => $this->faker->safeEmail(),
            'PERS_HOME_PHONE_NBR' => $this->faker->phoneNumber(),
            'PERS_BIRTHDATE' => $h->getBirthdate(),
            'HRMS_EMPLID' => $hrmsId,
        ];
    }
}
