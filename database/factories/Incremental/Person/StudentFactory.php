<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental\Person;

use Smorken\Sis\Db\Incremental\Person\Student;

class StudentFactory extends PersonFactory
{
    protected $model = Student::class;
}
