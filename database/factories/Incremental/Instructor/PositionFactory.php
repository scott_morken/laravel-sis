<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental\Instructor;

use Smorken\Sis\Db\Cds\Instructor\Position;

class PositionFactory extends \Database\Factories\Smorken\Sis\Db\Core\Instructor\PositionFactory
{
    protected $model = Position::class;
}
