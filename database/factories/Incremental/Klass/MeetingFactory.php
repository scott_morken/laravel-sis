<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental\Klass;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Illuminate\Support\Str;
use Smorken\Sis\Db\Incremental\Klass\Meeting;

class MeetingFactory extends FactoryWithHelper
{
    protected $model = Meeting::class;

    public function definition()
    {
        $h = $this->getHelper();
        $building = strtoupper(Str::random(1));
        $room = $this->faker->numberBetween(100, 399);

        return [
            'CLASS_COURSE_ID' => $h->getCourseId(),
            'CLASS_COURSE_OFFER_NBR' => $h->getCourseOfferNumber(),
            'CLASS_SECTION' => $h->getClassSection(),
            'CLASS_SESSION_CD' => 'DD',
            'CLASS_TERM_CD' => $h->getTermId(),
            'CLASSM_BLDG_CD' => $building.' BLD',
            'CLASSM_CLASS_MTG_NBR' => $h->getSequence(),
            'CLASSM_DAYS_PATTERN' => 'MWF',
            'CLASSM_END_DATE' => \Carbon\Carbon::instance($this->faker->dateTimeBetween('+1 week', '+1 month')),
            'CLASSM_FACILITY_ID' => $building.'_FAC_ID',
            'CLASSM_LOCATION' => $h->getCollegeId(),
            'CLASSM_MEETING_TIME_END' => '9:00 AM',
            'CLASSM_MEETING_TIME_START' => '9:50 AM',
            'CLASSM_ROOM' => $room,
            'CLASSM_START_DATE' => \Carbon\Carbon::instance($this->faker->dateTimeBetween('-1 month', '-1 week')),
            'CLASSM_COURSE_TOPIC_DESCR' => '',
            'CLASS_INSTITUTION_CD' => $h->getCollegeId(),
        ];
    }
}
