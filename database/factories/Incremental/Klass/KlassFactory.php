<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental\Klass;

use Database\Factories\Smorken\Sis\Db\Traits\FromTo;
use Smorken\Sis\Db\Incremental\Klass\Klass;

class KlassFactory extends \Database\Factories\Smorken\Sis\Db\Core\Klass\KlassFactory
{
    use FromTo;

    protected $model = Klass::class;

    public function definition(): array
    {
        $rdsDef = parent::definition();

        return $this->convertTo($rdsDef);
    }

    protected function getMap(): array
    {
        return [
            'CASSC_GRADING_BASIS' => null,
            'CASSC_HONORS_YN' => null,
            'CASSC_UNITS_MINIMUM' => null,
            'CLASS_ACAD_CAREER' => null,
            'CLASS_ACAD_ORG' => null,
            'CLASS_ACAD_ORG_LDESC' => null,
            'CLASS_ASSOCIATED_CLASS' => null,
            'CLASS_CANCEL_DATE' => null,
            'CLASS_CATALOG_NBR' => null,
            'CLASS_CLASS_NAME' => null,
            'CLASS_CLASS_NBR' => null,
            'CLASS_CLASS_STAT' => null,
            'CLASS_COMBINED_MAJOR_YN' => null,
            'CLASS_COMPONENT_CD' => null,
            'CLASS_COURSE_ID' => null,
            'CLASS_COURSE_OFFER_NBR' => null,
            'CLASS_DAY_EVE' => null,
            'CLASS_DESCR' => null,
            'CLASS_END_DATE' => null,
            'CLASS_ENRL_CAP' => null,
            'CLASS_ENRL_STAT' => null,
            'CLASS_ENRL_TOT' => null,
            'CLASS_INSTITUTION_CD' => null,
            'CLASS_INSTRUCTION_MODE' => null,
            'CLASS_LOCATION_CD' => null,
            'CLASS_SECTION' => null,
            'CLASS_SESSION_CD' => null,
            'CLASS_START_DATE' => null,
            'CLASS_SUBJECT_CD' => null,
            'CLASS_TERM_CD' => null,
            'CLASS_TYPE_CD' => null,
            'CLASS_CAMPUS_CD' => null,
        ];
    }
}
