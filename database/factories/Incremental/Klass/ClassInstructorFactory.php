<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental\Klass;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Incremental\Klass\ClassInstructor;

class ClassInstructorFactory extends FactoryWithHelper
{
    protected $model = ClassInstructor::class;

    public function definition(): array
    {
        $h = $this->getHelper();
        $firstName = $this->faker->firstName();
        $lastName = $this->faker->lastName();

        return [
            'CLASS_COURSE_ID' => $h->getCourseId(),
            'CLASS_COURSE_OFFER_NBR' => $h->getCourseOfferNumber(),
            'CLASS_SECTION' => $h->getClassSection(),
            'CLASS_SESSION_CD' => 'DD',
            'CLASS_TERM_CD' => '1234',
            'CLASS_INSTITUTION_CD' => 'COLL01',
            'CLASSM_ASSIGN_TYPE' => '',
            'CLASSM_CLASS_MTG_NBR' => $h->getSequence(),
            'CLASSM_INSTR_ASSIGN_SEQ' => $h->getSequence(),
            'CLASSM_INSTR_LOAD_FACTOR' => '',
            'CLASSM_WEEK_WORKLOAD_HRS' => '',
            'CLASSM_INSTR_NAME_FLAST' => substr($firstName, 0, 1).'. '.$lastName,
            'CLASSM_INSTR_TYPE' => $this->faker->randomElement(['ADJ', 'RES']),
            'CLASSM_INSTRUCTOR_EMPLID' => $h->getStudentId(),
            'CLASSM_INSTRUCTOR_NAME' => $firstName.' '.$lastName,
            'CLASSM_INSTRUCTOR_ROLE' => $this->faker->randomElement(['PI', 'SI']),
        ];
    }
}
