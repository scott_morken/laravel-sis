<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental\Course;

use Database\Factories\Smorken\Sis\Db\FactoryWithHelper;
use Smorken\Sis\Db\Incremental\Course\CourseNote;

class CourseNoteFactory extends FactoryWithHelper
{
    protected $model = CourseNote::class;

    public function definition(): array
    {
        $h = $this->getHelper();

        return [
            'CLASS_COURSE_ID' => $h->getCourseId(),
            'CLASS_COURSE_OFFER_NBR' => $h->getCourseOfferNumber(),
            'CLASS_TERM_CD' => $h->getTermId(),
            'NOTES_SEQ_NBR' => $h->getSequence(),
            'NOTES_NOTE_NBR' => $this->faker->randomNumber(2),
            'NOTES_TYPE_PUBL_STAF' => 'Public',
            'NOTES_DESCR' => $this->faker->words(2, true),
            'NOTES_NOTE_TEXT' => $this->faker->sentence(),
            'CLASS_INSTITUTION_CD' => $h->getCollegeId(),
        ];
    }
}
