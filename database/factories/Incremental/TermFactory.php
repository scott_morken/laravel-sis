<?php

namespace Database\Factories\Smorken\Sis\Db\Incremental;

use Smorken\Sis\Db\Cds\Term;

class TermFactory extends \Database\Factories\Smorken\Sis\Db\Core\TermFactory
{
    protected $model = Term::class;
}
