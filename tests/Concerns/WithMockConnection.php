<?php

namespace Tests\Smorken\Sis\Concerns;

use Aws\Athena\AthenaClient;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Model;
use Mockery as m;
use Smorken\Athena\Database\AthenaConnection;
use Tests\Smorken\Model\Stubs\PDOStub;

trait WithMockConnection
{
    protected \PDOStatement|m\MockInterface|null $statement = null;

    protected ?AthenaClient $client = null;

    protected function getAthenaClient(): AthenaClient
    {
        if (! $this->client) {
            $this->client = m::mock(AthenaClient::class);
        }

        return $this->client;
    }

    protected function getMockAthenaConnection(
        string $connectionClass = AthenaConnection::class,
        array $config = [],
        array $methods = []
    ): ConnectionInterface {
        if (empty($config)) {
            $config = ['database' => 'athena_db'];
        }
        $connection = $this->getMockBuilder($connectionClass)
            ->onlyMethods($methods)
            ->setConstructorArgs([$config])
            ->getMock();
        $connection->enableQueryLog();

        return $connection;
    }

    protected function initAthenaConnection(string $name = 'athena'): void
    {
        AthenaConnection::addAthenaClient($this->getAthenaClient(), $name);
        $cr = new ConnectionResolver([$name => $this->getMockAthenaConnection()]);
        $cr->setDefaultConnection($name);
        Model::setConnectionResolver($cr);
    }

    protected function getMockConnection(
        string $connectionClass,
        array $config = [],
        array $methods = []
    ): ConnectionInterface {
        $this->statement = null;
        $this->statement = m::mock(\PDOStatement::class);
        $this->statement->shouldReceive('setFetchMode');
        $pdo = m::mock(PDOStub::class);
        $connection = $this->getMockBuilder($connectionClass)
            ->onlyMethods($methods)
            ->setConstructorArgs([$pdo, '', '', $config])
            ->getMock();
        $connection->enableQueryLog();

        return $connection;
    }

    protected function getMySqlConnectionConfig(): array
    {
        return [
            'driver' => 'sqlsrv',
            'url' => null,
            'host' => '127.0.0.1',
            'port' => '1433',
            'database' => null,
            'username' => '',
            'password' => '',
            'unix_socket' => '',
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ];
    }
}
