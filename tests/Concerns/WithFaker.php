<?php

namespace Tests\Smorken\Sis\Concerns;

use Database\Factories\Smorken\Sis\Db\Increment;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Container\Container;

trait WithFaker
{
    protected function initFaker(): void
    {
        Increment::resetAll();
        if (! Container::getInstance()->bound(\Generator::class)) {
            Container::getInstance()->bind(Generator::class, fn () => Factory::create('en'));
        }
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initFaker();
    }
}
