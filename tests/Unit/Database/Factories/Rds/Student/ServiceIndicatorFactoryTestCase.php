<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories\Rds\Student;

use Smorken\Sis\Db\Core\Student\ServiceIndicator;
use Tests\Smorken\Sis\Unit\Database\Factories\BaseFactoryTestCase;

class ServiceIndicatorFactoryTestCase extends BaseFactoryTestCase
{
    public function testMakeMultiple(): void
    {
        $sis = ServiceIndicator::factory(3)->make();
        $expected = [
            [
                'SRVC_EMPLID' => 30000000,
                'SRVC_INSTITUTION' => 'COLL01',
                'SRVC_SERVICE_IND_CD' => 'ADM',
                'SRVC_SERVICE_IND_REASON' => 'RADM',
                'SRVC_SERVICE_IND_ACT_TERM' => $sis[0]->SRVC_SERVICE_IND_ACT_TERM,
            ],
            [
                'SRVC_EMPLID' => 30000001,
                'SRVC_INSTITUTION' => 'COLL01',
                'SRVC_SERVICE_IND_CD' => 'ADM',
                'SRVC_SERVICE_IND_REASON' => 'RADM',
                'SRVC_SERVICE_IND_ACT_TERM' => $sis[1]->SRVC_SERVICE_IND_ACT_TERM,
            ],
            [
                'SRVC_EMPLID' => 30000002,
                'SRVC_INSTITUTION' => 'COLL01',
                'SRVC_SERVICE_IND_CD' => 'ADM',
                'SRVC_SERVICE_IND_REASON' => 'RADM',
                'SRVC_SERVICE_IND_ACT_TERM' => $sis[2]->SRVC_SERVICE_IND_ACT_TERM,
            ],
        ];
        foreach ($expected as $i => $values) {
            $si = $sis[$i];
            foreach ($values as $k => $v) {
                $this->assertEquals($v, $si->getAttribute($k));
            }
        }
    }

    public function testMakeOne(): void
    {
        $si = ServiceIndicator::factory()->make();
        $expected = [
            'SRVC_EMPLID' => 30000000,
            'SRVC_INSTITUTION' => 'COLL01',
            'SRVC_SERVICE_IND_CD' => 'ADM',
            'SRVC_SERVICE_IND_REASON' => 'RADM',
            'SRVC_SERVICE_IND_ACT_TERM' => $si->SRVC_SERVICE_IND_ACT_TERM,
        ];
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $si->getAttribute($k));
        }
    }
}
