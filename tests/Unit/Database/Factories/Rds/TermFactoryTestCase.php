<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories\Rds;

use Carbon\Carbon;
use Database\Factories\Smorken\Sis\Db\Increment;
use Smorken\Sis\Db\Core\Term;
use Tests\Smorken\Sis\Unit\Database\Factories\BaseFactoryTestCase;

class TermFactoryTestCase extends BaseFactoryTestCase
{
    public function testCreateAndIncrementMany(): void
    {
        $base = date('y').'2';
        $term = Term::factory()->make();
        $expected = [
            'STRM' => '1'.$base,
            'DESCR' => 'Term 1'.$base,
            'INSTITUTION' => 'COLL01',
            'ACAD_CAREER' => 'CRED',
        ];
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $term->getAttribute($k));
        }
        $this->assertInstanceOf(Carbon::class, $term->TERM_BEGIN_DT);
        $this->assertInstanceOf(Carbon::class, $term->TERM_END_DT);
        $term = Term::factory()->increment()->make();
        $expected = [
            'STRM' => '2'.$base,
            'DESCR' => 'Term 2'.$base,
            'INSTITUTION' => 'COLL01',
            'ACAD_CAREER' => 'CRED',
        ];
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $term->getAttribute($k));
        }
        $this->assertInstanceOf(Carbon::class, $term->TERM_BEGIN_DT);
        $this->assertInstanceOf(Carbon::class, $term->TERM_END_DT);
        $term = Term::factory()->increment()->make();
        $expected = [
            'STRM' => '3'.$base,
            'DESCR' => 'Term 3'.$base,
            'INSTITUTION' => 'COLL01',
            'ACAD_CAREER' => 'CRED',
        ];
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $term->getAttribute($k));
        }
        $this->assertInstanceOf(Carbon::class, $term->TERM_BEGIN_DT);
        $this->assertInstanceOf(Carbon::class, $term->TERM_END_DT);
    }

    public function testCreateMultiple(): void
    {
        $base = date('y').'2';
        $terms = Term::factory(3)->make();
        $expected = [
            [
                'STRM' => '1'.$base,
                'DESCR' => 'Term 1'.$base,
                'INSTITUTION' => 'COLL01',
                'ACAD_CAREER' => 'CRED',
            ],
            [
                'STRM' => '2'.$base,
                'DESCR' => 'Term 2'.$base,
                'INSTITUTION' => 'COLL01',
                'ACAD_CAREER' => 'CRED',
            ],
            [
                'STRM' => '3'.$base,
                'DESCR' => 'Term 3'.$base,
                'INSTITUTION' => 'COLL01',
                'ACAD_CAREER' => 'CRED',
            ],
        ];
        foreach ($expected as $i => $values) {
            $term = $terms[$i];
            foreach ($values as $k => $v) {
                $this->assertEquals($v, $term->getAttribute($k));
            }
            $this->assertInstanceOf(Carbon::class, $term->TERM_BEGIN_DT);
            $this->assertInstanceOf(Carbon::class, $term->TERM_END_DT);
        }
    }

    public function testCreateOne(): void
    {
        $base = date('y').'2';
        $term = Term::factory()->make();
        $expected = [
            'STRM' => '1'.$base,
            'DESCR' => 'Term 1'.$base,
            'INSTITUTION' => 'COLL01',
            'ACAD_CAREER' => 'CRED',
        ];
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $term->getAttribute($k));
        }
        $this->assertInstanceOf(Carbon::class, $term->TERM_BEGIN_DT);
        $this->assertInstanceOf(Carbon::class, $term->TERM_END_DT);
    }

    protected function setUp(): void
    {
        parent::setUp();
        Increment::resetAll();
    }
}
