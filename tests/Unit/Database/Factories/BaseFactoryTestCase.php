<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories;

use Tests\Smorken\Sis\Concerns\WithFaker;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

abstract class BaseFactoryTestCase extends TestCaseWithMockConnectionResolver
{
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->initFaker();
    }
}
