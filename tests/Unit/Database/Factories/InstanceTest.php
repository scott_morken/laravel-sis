<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories;

use Smorken\Sis\Db\Core\Course\CourseCatalog;
use Smorken\Sis\Db\Core\Course\CourseOffer;
use Smorken\Sis\Db\Core\Instructor\Position;
use Smorken\Sis\Db\Core\Klass\Fee;
use Smorken\Sis\Db\Core\Klass\Material;
use Smorken\Sis\Db\Core\Physical\Facility;
use Smorken\Sis\Db\Core\Student\Athlete;
use Smorken\Sis\Db\Core\Student\Degree;
use Smorken\Sis\Db\Core\Student\Group;
use Smorken\Sis\Db\Core\Student\Program;
use Smorken\Sis\Db\Core\Student\ServiceIndicator;
use Smorken\Sis\Db\Core\Term;
use Smorken\Sis\Db\Incremental\Course\CourseNote;
use Smorken\Sis\Db\Incremental\Klass\ClassInstructor;
use Smorken\Sis\Db\Incremental\Klass\ClassNote;
use Smorken\Sis\Db\Incremental\Klass\Klass;
use Smorken\Sis\Db\Incremental\Klass\Meeting;
use Smorken\Sis\Db\Incremental\Person\Person;
use Smorken\Sis\Db\Incremental\Student\Enrollment;
use Smorken\Sis\Db\Incremental\Student\StudentTerm;
use Tests\Smorken\Sis\Concerns\WithFaker;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class InstanceTest extends TestCaseWithMockConnectionResolver
{
    use WithFaker;

    public function testCdsCourseNoteFactory(): void
    {
        $this->testModelInstanceOf(CourseNote::class);
    }

    public function testCdsEnrollmentFactory(): void
    {
        $this->testModelInstanceOf(Enrollment::class);
    }

    public function testCdsFeeFactory(): void
    {
        $this->testModelInstanceOf(Fee::class);
    }

    public function testCdsKlassFactory(): void
    {
        $this->testModelInstanceOf(Klass::class);
    }

    public function testCdsKlassInstructorFactory(): void
    {
        $this->testModelInstanceOf(ClassInstructor::class);
    }

    public function testCdsKlassNoteFactory(): void
    {
        $this->testModelInstanceOf(ClassNote::class);
    }

    public function testCdsMeetingFactory(): void
    {
        $this->testModelInstanceOf(Meeting::class);
    }

    public function testCdsPersonFactory(): void
    {
        $this->testModelInstanceOf(Person::class);
    }

    public function testCdsPositionFactory(): void
    {
        $this->testModelInstanceOf(Position::class);
    }

    public function testCdsStudentTermFactory(): void
    {
        $this->testModelInstanceOf(StudentTerm::class);
    }

    public function testCdsTermFactory(): void
    {
        $this->testModelInstanceOf(Term::class);
    }

    public function testRdsAthleteFactory(): void
    {
        $this->testModelInstanceOf(Athlete::class);
    }

    public function testRdsCourseCatalogFactory(): void
    {
        $this->testModelInstanceOf(CourseCatalog::class);
    }

    public function testRdsCourseOfferFactory(): void
    {
        $this->testModelInstanceOf(CourseOffer::class);
    }

    public function testRdsDegreeFactory(): void
    {
        $this->testModelInstanceOf(Degree::class);
    }

    public function testRdsEnrollmentFactory(): void
    {
        $this->testModelInstanceOf(\Smorken\Sis\Db\Core\Student\Enrollment::class);
    }

    public function testRdsFacilityFactory(): void
    {
        $this->testModelInstanceOf(Facility::class);
    }

    public function testRdsFeeFactory(): void
    {
        $this->testModelInstanceOf(Fee::class);
    }

    public function testRdsGroupFactory(): void
    {
        $this->testModelInstanceOf(Group::class);
    }

    public function testRdsKlassFactory(): void
    {
        $this->testModelInstanceOf(\Smorken\Sis\Db\Core\Klass\Klass::class);
    }

    public function testRdsKlassInstructorFactory(): void
    {
        $this->testModelInstanceOf(ClassInstructor::class);
    }

    public function testRdsMaterialFactory(): void
    {
        $this->testModelInstanceOf(Material::class);
    }

    public function testRdsPersonFactory(): void
    {
        $this->testModelInstanceOf(\Smorken\Sis\Db\Core\Person\Person::class);
    }

    public function testRdsPositionFactory(): void
    {
        $this->testModelInstanceOf(Position::class);
    }

    public function testRdsProgramFactory(): void
    {
        $this->testModelInstanceOf(Program::class);
    }

    public function testRdsServiceIndicatorFactory(): void
    {
        $this->testModelInstanceOf(ServiceIndicator::class);
    }

    public function testRdsStudentTermFactory(): void
    {
        $this->testModelInstanceOf(\Smorken\Sis\Db\Core\Student\StudentTerm::class);
    }

    public function testRdsTermFactory(): void
    {
        $this->testModelInstanceOf(Term::class);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initFaker();
    }

    protected function testModelInstanceOf(string $modelClass): void
    {
        $m = $modelClass::factory()->make();
        $this->assertInstanceOf($modelClass, $m);
    }
}
