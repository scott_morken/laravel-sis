<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories;

use Database\Factories\Smorken\Sis\Db\Helper;
use Database\Factories\Smorken\Sis\Db\Increment;
use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class HelperTest extends TestCase
{
    public function testDefaultsCanOverrideDefaults(): void
    {
        $sut = $this->getSut(['collegeId' => 'PCC01']);
        $this->assertEquals('PCC01', $sut->getByKey('collegeId'));
    }

    public function testIncrementCanOverrideIncrement(): void
    {
        $sut = $this->getSut([], new Increment(['classNumber' => 90000]));
        $this->assertEquals(90000, $sut->getIncrementing('classNumber'));
        $sut->getIncrement()->reset();
    }

    public function testResetWithKey(): void
    {
        $sut = $this->getSut();
        $this->assertEquals(10000, $sut->getIncrementing('classNumber', true));
        $this->assertEquals(10001, $sut->getIncrementing('classNumber', true));
        $this->assertEquals(10, $sut->getIncrementing('classSection', true));
        $sut->getIncrement()->reset('classNumber');
        $this->assertEquals(10000, $sut->getIncrementing('classNumber'));
        $this->assertEquals(11, $sut->getIncrementing('classSection'));
    }

    public function testTermIncrement(): void
    {
        $sut = $this->getSut();
        $expected = (int) sprintf('1%d2', date('y'));
        $this->assertTrue($expected > 1202 && $expected < 1992); // if this is still being used in 2099, we have a problem
        $this->assertEquals($expected, $sut->getTermId(true));
        $this->assertEquals($expected + 1000, $sut->getTermId(true));
        $sut->getIncrement()->reset();
    }

    protected function getFaker(): Generator
    {
        return Factory::create();
    }

    protected function getSut(array $defaults = [], ?Increment $increment = null): Helper
    {
        return new Helper($this->getFaker(), $increment, $defaults);
    }
}
