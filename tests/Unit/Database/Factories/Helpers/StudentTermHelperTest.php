<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories\Helpers;

use Database\Factories\Smorken\Sis\Db\Helpers\StudentTermHelper;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class StudentTermHelperTest extends TestCase
{
    public function testDefaults(): void
    {
        $sut = $this->getSut();
        $sut->get();
        $this->assertEquals(StudentTermHelper::STANDING_ACCEPTABLE, $sut->standingCode);
        $this->assertEquals('Acceptable', $sut->standingDesc);
        $this->assertEquals(StudentTermHelper::LOAD_FULLTIME, $sut->loadCode);
        $this->assertEquals('Enrolled Full-Time', $sut->loadDesc);
    }

    public function testOverrides(): void
    {
        $sut = $this->getSut();
        $sut->get(StudentTermHelper::STANDING_PROBATION, StudentTermHelper::LOAD_HALFTIME);
        $this->assertEquals(StudentTermHelper::STANDING_PROBATION, $sut->standingCode);
        $this->assertEquals('Probation', $sut->standingDesc);
        $this->assertEquals(StudentTermHelper::LOAD_HALFTIME, $sut->loadCode);
        $this->assertEquals('Enrolled Half-Time', $sut->loadDesc);
    }

    protected function getSut(): StudentTermHelper
    {
        return new StudentTermHelper(Factory::create());
    }
}
