<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories\Helpers;

use Database\Factories\Smorken\Sis\Db\Helpers\MaterialHelper;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class MaterialHelperTest extends TestCase
{
    public function testDefaults(): void
    {
        $sut = $this->getSut();
        $sut->get();
        $this->assertEquals(MaterialHelper::STATUS_REQUIRED, $sut->statusCode);
        $this->assertEquals('Required', $sut->statusDesc);
        $this->assertEquals(MaterialHelper::TYPE_TEXTBOOK, $sut->typeCode);
        $this->assertEquals('Textbook', $sut->typeDesc);
    }

    public function testOverrides(): void
    {
        $sut = $this->getSut();
        $sut->get(MaterialHelper::STATUS_RECOMMENDED, MaterialHelper::TYPE_EBOOK);
        $this->assertEquals(MaterialHelper::STATUS_RECOMMENDED, $sut->statusCode);
        $this->assertEquals('Recommended', $sut->statusDesc);
        $this->assertEquals(MaterialHelper::TYPE_EBOOK, $sut->typeCode);
        $this->assertEquals('eBook', $sut->typeDesc);
    }

    protected function getSut(): MaterialHelper
    {
        return new MaterialHelper(Factory::create());
    }
}
