<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories\Helpers;

use Database\Factories\Smorken\Sis\Db\Helpers\KlassHelper;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class KlassHelperTest extends TestCase
{
    public function testDefaults(): void
    {
        $sut = $this->getSut();
        $sut->get();
        $this->assertEquals(KlassHelper::COMPONENT_LECTURE, $sut->componentCode);
        $this->assertEquals('Lecture', $sut->componentDesc);
        $this->assertEquals(KlassHelper::CONSENT_NONE, $sut->consent);
        $this->assertEquals('No Special Consent Required', $sut->consentDesc);
        $this->assertEquals(KlassHelper::ENROLLMENT_OPEN, $sut->enrollmentStatus);
        $this->assertEquals('Open', $sut->enrollmentStatusDesc);
        $this->assertEquals(KlassHelper::INSTRUCTION_IN_PERSON, $sut->instructionMode);
        $this->assertEquals('In Person', $sut->instructionModeDesc);
        $this->assertEquals(KlassHelper::STATUS_ACTIVE, $sut->status);
        $this->assertEquals('Active', $sut->statusDesc);
    }

    public function testOverrides(): void
    {
        $sut = $this->getSut();
        $sut->get([
            KlassHelper::KEY_COMPONENT => KlassHelper::COMPONENT_LECTURE_LAB,
            KlassHelper::KEY_CONSENT => KlassHelper::CONSENT_DEPARTMENT,
            KlassHelper::KEY_ENROLLMENT => KlassHelper::ENROLLMENT_CLOSED,
            KlassHelper::KEY_INSTRUCTION => KlassHelper::INSTRUCTION_ONLINE,
        ]);
        $this->assertEquals(KlassHelper::COMPONENT_LECTURE_LAB, $sut->componentCode);
        $this->assertEquals('Lecture & Lab', $sut->componentDesc);
        $this->assertEquals(KlassHelper::CONSENT_DEPARTMENT, $sut->consent);
        $this->assertEquals('Department Consent Required', $sut->consentDesc);
        $this->assertEquals(KlassHelper::ENROLLMENT_CLOSED, $sut->enrollmentStatus);
        $this->assertEquals('Closed', $sut->enrollmentStatusDesc);
        $this->assertEquals(KlassHelper::INSTRUCTION_ONLINE, $sut->instructionMode);
        $this->assertEquals('Online', $sut->instructionModeDesc);
        $this->assertEquals(KlassHelper::STATUS_ACTIVE, $sut->status);
        $this->assertEquals('Active', $sut->statusDesc);
    }

    protected function getSut(): KlassHelper
    {
        return new KlassHelper(Factory::create());
    }
}
