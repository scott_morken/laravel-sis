<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories\Helpers;

use Database\Factories\Smorken\Sis\Db\Helpers\EnrollmentHelper;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class EnrollmentHelperTest extends TestCase
{
    public function testWithEnrolledStatus(): void
    {
        $sut = $this->getSut();
        $sut->get(EnrollmentHelper::ENROLLED);
        $this->assertEquals(EnrollmentHelper::ENROLLED, $sut->statusCode);
        $this->assertEquals('Enrolled', $sut->statusDesc);
        $this->assertNull($sut->lastActionCode);
        $this->assertNull($sut->lastActionDesc);
    }

    public function testWithNotEnrolledStatus(): void
    {
        $sut = $this->getSut();
        $sut->get(EnrollmentHelper::DROPPED);
        $this->assertEquals(EnrollmentHelper::DROPPED, $sut->statusCode);
        $this->assertEquals('Dropped (was enrolled)', $sut->statusDesc);
        $this->assertNotNull($sut->lastActionCode);
        $this->assertNotNull($sut->lastActionDesc);
    }

    protected function getSut(): EnrollmentHelper
    {
        return new EnrollmentHelper(Factory::create());
    }
}
