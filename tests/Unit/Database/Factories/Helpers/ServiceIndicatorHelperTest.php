<?php

namespace Tests\Smorken\Sis\Unit\Database\Factories\Helpers;

use Database\Factories\Smorken\Sis\Db\Helpers\ServiceIndicatorHelper;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class ServiceIndicatorHelperTest extends TestCase
{
    public function testDefaults(): void
    {
        $sut = $this->getSut();
        $sut->get();
        $this->assertEquals(ServiceIndicatorHelper::ADM, $sut->code);
        $this->assertEquals('Administrative Hold', $sut->description);
        $this->assertEquals('R'.ServiceIndicatorHelper::ADM, $sut->reasonCode);
        $this->assertEquals('R: Administrative Hold', $sut->reasonDesc);
    }

    public function testOverrides(): void
    {
        $sut = $this->getSut();
        $sut->get(ServiceIndicatorHelper::DBT);
        $this->assertEquals(ServiceIndicatorHelper::DBT, $sut->code);
        $this->assertEquals('MCCCD Debt Owed', $sut->description);
        $this->assertEquals('R'.ServiceIndicatorHelper::DBT, $sut->reasonCode);
        $this->assertEquals('R: MCCCD Debt Owed', $sut->reasonDesc);
    }

    protected function getSut(): ServiceIndicatorHelper
    {
        return new ServiceIndicatorHelper(Factory::create());
    }
}
