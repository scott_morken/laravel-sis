<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\TranslateItem;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class TranslateItemTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_max_effective_date_subquery_query(): void
    {
        $sut = (new TranslateItem)->newQuery()->joinOnMaxEffectiveDateSubquery(Carbon::parse('2023-10-12 10:00:00'));
        $this->assertEquals('select * from "psxlatitem" inner join (select fieldname, fieldvalue, MAX(effdt) as max_date from "psxlatitem" where "eff_status" = ? and "effdt" <= from_iso8601_timestamp(?) group by "fieldname", "fieldvalue") as "sub_eff_date" on "psxlatitem"."fieldname" = "sub_eff_date"."fieldname" and "psxlatitem"."effdt" = "sub_eff_date"."max_date" and "psxlatitem"."fieldvalue" = "sub_eff_date"."fieldvalue"',
            $sut->toSql());
        $this->assertEquals(['A', '2023-10-12T10:00:00+00:00'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_fieldname_query(): void
    {
        $sut = (new TranslateItem)->newQuery()->fieldNameIs('foo');
        $this->assertEquals('select * from "psxlatitem" where "psxlatitem"."fieldname" = ?',
            $sut->toSql());
        $this->assertEquals(['FOO'], $sut->getBindings());
    }
}
