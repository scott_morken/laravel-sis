<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\ItemType;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class ItemTypeTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_common_flat_amount_and_effective_date_query(): void
    {
        $sut = (new ItemType)->newQuery()
            ->joinCommonAmountFromSubFees()
            ->joinOnMaxEffectiveDateSubquery(Carbon::parse('2023-10-12 10:00:00'));
        $this->assertEquals('select "ps_item_type_tbl".*, "cfa"."common_amt" from "ps_item_type_tbl" left join (select "item_type", "setid", "flat_amt" as "common_amt", count(*) as fee_count from (select ps_class_sbfee_tbl.*, row_number() over (partition by setid, item_type order by strm desc) as rn from "ps_class_sbfee_tbl") as "cax" where "cax"."rn" <= ? group by "cax"."flat_amt", "cax"."item_type", "cax"."setid" order by "fee_count" desc) as "cfa" on "ps_item_type_tbl"."item_type" = "cfa"."item_type" and "ps_item_type_tbl"."setid" = "cfa"."setid" inner join (select item_type, setid, MAX(effdt) as max_date from "ps_item_type_tbl" where "eff_status" = ? and "effdt" <= from_iso8601_timestamp(?) group by "item_type", "setid") as "sub_eff_date" on "ps_item_type_tbl"."item_type" = "sub_eff_date"."item_type" and "ps_item_type_tbl"."effdt" = "sub_eff_date"."max_date" and "ps_item_type_tbl"."setid" = "sub_eff_date"."setid"',
            $sut->toSql());
        $this->assertEquals([
            '50',
            'A',
            '2023-10-12T10:00:00+00:00',
        ], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_common_flat_amount_query(): void
    {
        $sut = (new ItemType)->newQuery()->joinCommonAmountFromSubFees();
        $this->assertEquals('select "ps_item_type_tbl".*, "cfa"."common_amt" from "ps_item_type_tbl" left join (select "item_type", "setid", "flat_amt" as "common_amt", count(*) as fee_count from (select ps_class_sbfee_tbl.*, row_number() over (partition by setid, item_type order by strm desc) as rn from "ps_class_sbfee_tbl") as "cax" where "cax"."rn" <= ? group by "cax"."flat_amt", "cax"."item_type", "cax"."setid" order by "fee_count" desc) as "cfa" on "ps_item_type_tbl"."item_type" = "cfa"."item_type" and "ps_item_type_tbl"."setid" = "cfa"."setid"',
            $sut->toSql());
        $this->assertEquals(['50'], $sut->getBindings());
    }
}
