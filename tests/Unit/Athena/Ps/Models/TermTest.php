<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\Term;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class TermTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_college_id_query(): void
    {
        $sut = (new Term)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from "ps_term_tbl" where "institution" = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_active_and_upcoming_query(): void
    {
        Carbon::setTestNow(Carbon::parse('2023-10-12 10:00:00'));
        $sut = (new Term)->newQuery()->activeAndUpcoming();
        $this->assertEquals('select * from "ps_term_tbl" where "term_end_dt" >= from_iso8601_timestamp(?)',
            $sut->toSql());
        $this->assertEquals('2023-10-12T00:00:00+00:00', $sut->getBindings()[0]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Carbon::setTestNow();
    }
}
