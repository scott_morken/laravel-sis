<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models\Physical;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\Physical\ExternalOrg;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class ExternalOrgTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_college_id_query(): void
    {
        $sut = (new ExternalOrg)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from "ps_ext_org_tbl" where "setid" = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_max_effective_date_subquery_query(): void
    {
        $sut = (new ExternalOrg)->newQuery()->joinOnMaxEffectiveDateSubquery(Carbon::parse('2023-10-12 10:00:00'));
        $this->assertEquals('select * from "ps_ext_org_tbl" inner join (select ext_org_id, setid, MAX(effdt) as max_date from "ps_ext_org_tbl" where "eff_status" = ? and "effdt" <= from_iso8601_timestamp(?) group by "ext_org_id", "setid") as "sub_eff_date" on "ps_ext_org_tbl"."ext_org_id" = "sub_eff_date"."ext_org_id" and "ps_ext_org_tbl"."effdt" = "sub_eff_date"."max_date" and "ps_ext_org_tbl"."setid" = "sub_eff_date"."setid"',
            $sut->toSql());
        $this->assertEquals(['A', '2023-10-12T10:00:00+00:00'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_sponsor_only_query(): void
    {
        $sut = (new ExternalOrg)->newQuery()->hasSponsorGTETermId('4186');
        $this->assertEquals('select * from "ps_ext_org_tbl" where exists (select 1 from "ps_mc_sr_class_tbl" inner join "ps_class_tbl" on "ps_mc_sr_class_tbl"."crse_id" = "ps_class_tbl"."crse_id" and "ps_mc_sr_class_tbl"."crse_offer_nbr" = "ps_class_tbl"."crse_offer_nbr" and "ps_mc_sr_class_tbl"."strm" = "ps_class_tbl"."strm" and "ps_mc_sr_class_tbl"."session_code" = "ps_class_tbl"."session_code" and "ps_mc_sr_class_tbl"."class_section" = "ps_class_tbl"."class_section" where "ps_class_tbl"."cancel_dt" is null and "ps_class_tbl"."class_stat" <> ? and "ps_class_tbl"."strm" >= ? and "ps_ext_org_tbl"."ext_org_id" = "ps_mc_sr_class_tbl"."mc_sr_hs_sponsor")',
            $sut->toSql());
        $this->assertEquals(['X', "'4186'"], $sut->getBindings());
    }
}
