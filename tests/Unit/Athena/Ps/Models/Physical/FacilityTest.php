<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models\Physical;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\Physical\Facility;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class FacilityTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_college_id_query(): void
    {
        $sut = (new Facility)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from "ps_facility_tbl" where "setid" = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_max_effective_date_subquery_query(): void
    {
        $sut = (new Facility)->newQuery()->joinOnMaxEffectiveDateSubquery(Carbon::parse('2023-10-12 10:00:00'));
        $this->assertEquals('select * from "ps_facility_tbl" inner join (select facility_id, setid, MAX(effdt) as max_date from "ps_facility_tbl" where "eff_status" = ? and "effdt" <= from_iso8601_timestamp(?) group by "facility_id", "setid") as "sub_eff_date" on "ps_facility_tbl"."facility_id" = "sub_eff_date"."facility_id" and "ps_facility_tbl"."effdt" = "sub_eff_date"."max_date" and "ps_facility_tbl"."setid" = "sub_eff_date"."setid"',
            $sut->toSql());
        $this->assertEquals(['A', '2023-10-12T10:00:00+00:00'], $sut->getBindings());
    }
}
