<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\Note;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class NoteTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_college_id_query(): void
    {
        $sut = (new Note)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from "ps_class_notes_tbl" where "institution" = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_max_effective_date_subquery_query(): void
    {
        $sut = (new Note)->newQuery()->joinOnMaxEffectiveDateSubquery(Carbon::parse('2023-10-12 10:00:00'));
        $this->assertEquals('select * from "ps_class_notes_tbl" inner join (select class_note_nbr, institution, MAX(effdt) as max_date from "ps_class_notes_tbl" where "eff_status" = ? and "effdt" <= from_iso8601_timestamp(?) group by "class_note_nbr", "institution") as "sub_eff_date" on "ps_class_notes_tbl"."class_note_nbr" = "sub_eff_date"."class_note_nbr" and "ps_class_notes_tbl"."effdt" = "sub_eff_date"."max_date" and "ps_class_notes_tbl"."institution" = "sub_eff_date"."institution"',
            $sut->toSql());
        $this->assertEquals(['A', '2023-10-12T10:00:00+00:00'], $sut->getBindings());
    }
}
