<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models\Klass;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\Klass\SubFee;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class SubFeeTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_class_join_query(): void
    {
        $sut = (new SubFee)->newQuery()->joinClass();
        $this->assertEquals('select * from "ps_class_sbfee_tbl" inner join "ps_class_tbl" on "ps_class_sbfee_tbl"."crse_id" = "ps_class_tbl"."crse_id" and "ps_class_sbfee_tbl"."crse_offer_nbr" = "ps_class_tbl"."crse_offer_nbr" and "ps_class_sbfee_tbl"."strm" = "ps_class_tbl"."strm" and "ps_class_sbfee_tbl"."session_code" = "ps_class_tbl"."session_code" and "ps_class_sbfee_tbl"."class_section" = "ps_class_tbl"."class_section"',
            $sut->toSql());
        $this->assertEquals([], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_common_flat_amount_query(): void
    {
        $sut = (new SubFee)->newQuery()->commonFlatAmount();
        $this->assertEquals('select "item_type", "setid", "flat_amt" as "common_amt", count(*) as fee_count from (select ps_class_sbfee_tbl.*, row_number() over (partition by setid, item_type order by strm desc) as rn from "ps_class_sbfee_tbl") as "cax" where "cax"."rn" <= ? group by "cax"."flat_amt", "cax"."item_type", "cax"."setid" order by "fee_count" desc',
            $sut->toSql());
        $this->assertEquals(['50'], $sut->getBindings());
    }
}
