<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models\Klass;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\Klass\AncillaryInfo;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class AncillaryInfoTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_class_join_query(): void
    {
        $sut = (new AncillaryInfo)->newQuery()->joinClass();
        $this->assertEquals('select * from "ps_mc_sr_class_tbl" inner join "ps_class_tbl" on "ps_mc_sr_class_tbl"."crse_id" = "ps_class_tbl"."crse_id" and "ps_mc_sr_class_tbl"."crse_offer_nbr" = "ps_class_tbl"."crse_offer_nbr" and "ps_mc_sr_class_tbl"."strm" = "ps_class_tbl"."strm" and "ps_mc_sr_class_tbl"."session_code" = "ps_class_tbl"."session_code" and "ps_mc_sr_class_tbl"."class_section" = "ps_class_tbl"."class_section"',
            $sut->toSql());
        $this->assertEquals([], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_class_join_query_with_a_term_bound_param(): void
    {
        $sut = (new AncillaryInfo)->newQuery()->joinClass()->joinedClassTermIdGTE('4186');
        $this->assertEquals('select * from "ps_mc_sr_class_tbl" inner join "ps_class_tbl" on "ps_mc_sr_class_tbl"."crse_id" = "ps_class_tbl"."crse_id" and "ps_mc_sr_class_tbl"."crse_offer_nbr" = "ps_class_tbl"."crse_offer_nbr" and "ps_mc_sr_class_tbl"."strm" = "ps_class_tbl"."strm" and "ps_mc_sr_class_tbl"."session_code" = "ps_class_tbl"."session_code" and "ps_mc_sr_class_tbl"."class_section" = "ps_class_tbl"."class_section" where "ps_class_tbl"."strm" >= ?',
            $sut->toSql());
        $this->assertCount(1, $sut->getBindings());
        $param = $sut->getBindings()[0];
        $this->assertEquals("'4186'", $param);
    }
}
