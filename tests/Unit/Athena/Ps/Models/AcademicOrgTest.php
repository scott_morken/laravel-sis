<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\AcademicOrg;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class AcademicOrgTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_college_id_query(): void
    {
        $sut = (new AcademicOrg)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from "ps_acad_org_tbl" where "institution" = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_max_effective_date_subquery_query(): void
    {
        $sut = (new AcademicOrg)->newQuery()->joinOnMaxEffectiveDateSubquery(Carbon::parse('2023-10-12 10:00:00'));
        $this->assertEquals('select * from "ps_acad_org_tbl" inner join (select acad_org, institution, MAX(effdt) as max_date from "ps_acad_org_tbl" where "eff_status" = ? and "effdt" <= from_iso8601_timestamp(?) group by "acad_org", "institution") as "sub_eff_date" on "ps_acad_org_tbl"."acad_org" = "sub_eff_date"."acad_org" and "ps_acad_org_tbl"."effdt" = "sub_eff_date"."max_date" and "ps_acad_org_tbl"."institution" = "sub_eff_date"."institution"',
            $sut->toSql());
        $this->assertEquals(['A', '2023-10-12T10:00:00+00:00'], $sut->getBindings());
    }
}
