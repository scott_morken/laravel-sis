<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models\Course;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\Course\Offer;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class OfferTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_college_id_query(): void
    {
        $sut = (new Offer)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from "ps_crse_offer" where "institution" = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_joined_query_for_course_catalog(): void
    {
        $date = Carbon::parse('2023-10-12 10:00:00');
        $sut = (new Offer)->newQuery()->joinOnMaxEffectiveDateSubquery($date)->joinMaxEffectiveCatalog($date);
        $this->assertEquals('select * from "ps_crse_offer" inner join (select subject, catalog_nbr, acad_org, institution, MAX(effdt) as max_date from "ps_crse_offer" where "course_approved" = ? and "effdt" <= from_iso8601_timestamp(?) group by "subject", "catalog_nbr", "acad_org", "institution") as "sub_eff_date" on "ps_crse_offer"."subject" = "sub_eff_date"."subject" and "ps_crse_offer"."effdt" = "sub_eff_date"."max_date" and "ps_crse_offer"."catalog_nbr" = "sub_eff_date"."catalog_nbr" and "ps_crse_offer"."acad_org" = "sub_eff_date"."acad_org" and "ps_crse_offer"."institution" = "sub_eff_date"."institution" inner join (select "ps_crse_catalog"."crse_id", "ps_crse_catalog"."descr", "ps_crse_catalog"."descrlong" from "ps_crse_catalog" inner join (select crse_id, MAX(effdt) as max_date from "ps_crse_catalog" where "eff_status" = ? and "effdt" <= from_iso8601_timestamp(?) group by "crse_id") as "sub_eff_date" on "ps_crse_catalog"."crse_id" = "sub_eff_date"."crse_id" and "ps_crse_catalog"."effdt" = "sub_eff_date"."max_date") as "cat" on "ps_crse_offer"."crse_id" = "cat"."crse_id"',
            $sut->toSql());
        $this->assertEquals(['A', '2023-10-12T10:00:00+00:00', 'A', '2023-10-12T10:00:00+00:00'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_max_effective_date_subquery_query(): void
    {
        $sut = (new Offer)->newQuery()->joinOnMaxEffectiveDateSubquery(Carbon::parse('2023-10-12 10:00:00'));
        $this->assertEquals('select * from "ps_crse_offer" inner join (select subject, catalog_nbr, acad_org, institution, MAX(effdt) as max_date from "ps_crse_offer" where "course_approved" = ? and "effdt" <= from_iso8601_timestamp(?) group by "subject", "catalog_nbr", "acad_org", "institution") as "sub_eff_date" on "ps_crse_offer"."subject" = "sub_eff_date"."subject" and "ps_crse_offer"."effdt" = "sub_eff_date"."max_date" and "ps_crse_offer"."catalog_nbr" = "sub_eff_date"."catalog_nbr" and "ps_crse_offer"."acad_org" = "sub_eff_date"."acad_org" and "ps_crse_offer"."institution" = "sub_eff_date"."institution"',
            $sut->toSql());
        $this->assertEquals(['A', '2023-10-12T10:00:00+00:00'], $sut->getBindings());
    }
}
