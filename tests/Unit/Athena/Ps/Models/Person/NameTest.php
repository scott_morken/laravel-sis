<?php

declare(strict_types=1);

namespace Tests\Smorken\Sis\Unit\Athena\Ps\Models\Person;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Athena\Ps\Models\Person\Name;
use Tests\Smorken\Sis\Unit\TestCaseWithAthenaMockConnectionResolver;

class NameTest extends TestCaseWithAthenaMockConnectionResolver
{
    #[Test]
    public function it_creates_a_max_effective_date_subquery_query(): void
    {
        $sut = (new Name)->newQuery()->joinOnMaxEffectiveDateSubquery(Carbon::parse('2023-10-12 10:00:00'));
        $this->assertEquals('select * from "ps_names" inner join (select emplid, MAX(effdt) as max_date from "ps_names" where "eff_status" = ? and "effdt" <= from_iso8601_timestamp(?) group by "emplid") as "sub_eff_date" on "ps_names"."emplid" = "sub_eff_date"."emplid" and "ps_names"."effdt" = "sub_eff_date"."max_date"',
            $sut->toSql());
        $this->assertEquals(['A', '2023-10-12T10:00:00+00:00'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_student_id_query(): void
    {
        $sut = (new Name)->newQuery()->studentIdIs('12345');
        $this->assertEquals('select * from "ps_names" where "emplid" = ?', $sut->toSql());
        $this->assertEquals(['12345'], $sut->getBindings());
    }

    #[Test]
    public function it_gets_a_preferred_first_name(): void
    {
        $sut = (new Name)->forceFill(['first_name' => 'Foo', 'pref_first_name' => 'Bar']);
        $this->assertEquals('Bar', $sut->firstName);
    }

    #[Test]
    public function it_gets_a_preferred_last_name(): void
    {
        $sut = (new Name)->forceFill(['last_name' => 'Foo', 'pref_last_name' => 'Bar']);
        $this->assertEquals('Bar', $sut->lastName);
    }

    #[Test]
    public function it_gets_a_primary_first_name(): void
    {
        $sut = (new Name)->forceFill(['first_name' => 'Foo', 'pref_first_name' => ' ']);
        $this->assertEquals('Foo', $sut->firstName);
    }

    #[Test]
    public function it_gets_a_primary_last_name(): void
    {
        $sut = (new Name)->forceFill(['last_name' => 'Foo', 'pref_last_name' => ' ']);
        $this->assertEquals('Foo', $sut->lastName);
    }
}
