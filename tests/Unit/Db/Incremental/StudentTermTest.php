<?php

namespace Tests\Smorken\Sis\Unit\Db\Incremental;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Incremental\Student\StudentTerm;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\StudentTermModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class StudentTermTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new StudentTerm)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [CDS_STU_TERM_TBL] where [CTERM_INSTITUTION_CODE] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_max_hours(): void
    {
        $sut = (new StudentTerm)->newQuery()->maxByHours(64);
        $expected = 'select * from [CDS_STU_TERM_TBL] inner join (select MAX(CTERM_TERM_CD) AS MTERM_CD,
                            EMPLID AS MEMPLID, CTERM_INSTITUTION_CODE as MINSTITUTION_CODE,
                            CTERM_ACAD_CAREER AS MACAD_CAREER from [CDS_STU_TERM_TBL] group by [EMPLID], [CTERM_INSTITUTION_CODE], [CTERM_ACAD_CAREER]) stm on [stm].[MTERM_CD] = RDS_STU_TERM_VW.CTERM_TERM_CD and [stm].[MEMPLID] = RDS_STU_TERM_VW.EMPLID and [stm].[MINSTITUTION_CODE] = RDS_STU_TERM_VW.CTERM_INSTITUTION_CODE and [stm].[MACAD_CAREER] = RDS_STU_TERM_VW.CTERM_ACAD_CAREER where [STFACT_MCCD_CUM_EHRS] >= ?';

        $this->assertEquals($expected, $sut->toSql());
        $this->assertEquals(64, $sut->getBindings()[0]);
    }

    #[Test]
    public function it_creates_a_query_for_student_id(): void
    {
        $sut = (new StudentTerm)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [CDS_STU_TERM_TBL] where [CTERM_EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_term_id(): void
    {
        $sut = (new StudentTerm)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [CDS_STU_TERM_TBL] where [CTERM_TERM_CD] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = StudentTermModels::get();
        $this->assertEquals([
            'studentId' => 30000000,
            'termId' => '232',
            'collegeId' => 'COLL01',
            'academicCareer' => 'CRED',
            'cumulativeGpa' => 0.043,
            'cumulativeAttemptedHours' => 23.0,
            'cumulativeEarnedHours' => 48.0,
            'cumulativeGpaHours' => 58.0,
            'cumulativeGpaPoints' => 2.49,
            'cumulativeClockHours' => 23.0,
            'termGpa' => 0.483,
            'termAttemptedHours' => 12.0,
            'termEarnedHours' => 3.0,
            'termGpaHours' => 4.0,
            'termGpaPoints' => 1.93,
            'termClockHours' => 12.0,
            'academicStandingCode' => 'ACPT',
            'academicStandingDescription' => 'Acceptable',
            'loadCode' => 'F',
            'loadDescription' => 'Full time',
        ], $sut->toArray());
    }
}
