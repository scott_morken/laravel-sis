<?php

namespace Tests\Smorken\Sis\Unit\Db\Incremental;

use PHPUnit\Framework\Attributes\Test;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\ClassNoteModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class ClassNoteTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = ClassNoteModels::get();
        $this->assertEquals([
            'sequenceNumber' => 1,
            'noteNumber' => 30,
            'type' => 'Public',
            'description' => 'Class Note 30',
            'text' => 'Text for Class Note 30',
            'collegeId' => 'COLL01',
            'courseId' => 100000,
            'offeringNumber' => 1,
            'termId' => 1232,
            'sessionCode' => 'DD',
            'section' => 10,
        ], $sut->toArray());
    }
}
