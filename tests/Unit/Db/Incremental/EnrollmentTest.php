<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:33 PM
 */

namespace Tests\Smorken\Sis\Unit\Db\Incremental;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Incremental\Student\Enrollment;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\EnrollmentModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class EnrollmentTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_an_array_using_mapper(): void
    {
        $sut = EnrollmentModels::get();
        $this->assertInstanceOf(Carbon::class, $sut->oeeStartDate);
        $this->assertInstanceOf(Carbon::class, $sut->oeeEndDate);
        $this->assertEquals([
            'studentId' => 30000000,
            'collegeId' => 'COLL01',
            'termId' => 1232,
            'classNumber' => 10000,
            'academicCareer' => 'CRED',
            'gradingBasisCode' => 'GRD',
            'gradingBasisDescription' => 'Graded (standard)',
            'grade' => 'A',
            'gradeDate' => null,
            'unitsTaken' => 3.0,
            'gradePoints' => 4.0,
            'earnedCredit' => true,
            'includeInGpa' => true,
            'statusCode' => 'ENRL',
            'statusDescription' => 'Enrolled',
            'lastActionCode' => null,
            'lastActionDescription' => null,
            'oeeStartDate' => $sut->oeeStartDate,
            'oeeEndDate' => $sut->oeeEndDate,
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_query_for_active_by_class(): void
    {
        $sut = (new Enrollment)->newQuery()->activeByClass();
        $this->assertEquals(
            'select * from [CDS_STU_CLASS_TBL] where exists (select * from [CDS_CLASS_TBL] where [CDS_STU_CLASS_TBL].[ENRL_CLASS_NBR] = [CDS_CLASS_TBL].[CLASS_CLASS_NBR] and [CDS_STU_CLASS_TBL].[ENRL_TERM_CD] = [CDS_CLASS_TBL].[CLASS_TERM_CD] and [CLASS_CANCEL_DATE] is null and [CLASS_CLASS_STAT] <> ?)',
            $sut->toSql()
        );
        $this->assertEquals(['X'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new Enrollment)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [CDS_STU_CLASS_TBL] where [ENRL_INSTITUTION_CD] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_student_id(): void
    {
        $sut = (new Enrollment)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [CDS_STU_CLASS_TBL] where [ENRL_EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_term_id(): void
    {
        $sut = (new Enrollment)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [CDS_STU_CLASS_TBL] where [ENRL_TERM_CD] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }

    #[Test]
    public function test_it_creates_a_query_for_current_and_upcoming_term(): void
    {
        $sut = (new Enrollment)->newQuery()->currentAndUpcomingByTerm();
        $this->assertEquals(
            'select * from [CDS_STU_CLASS_TBL] where exists (select * from [RDS_TERM_VW] where [CDS_STU_CLASS_TBL].[ENRL_TERM_CD] = [RDS_TERM_VW].[STRM] and [CDS_STU_CLASS_TBL].[ENRL_INSTITUTION_CD] = [RDS_TERM_VW].[INSTITUTION] and [CDS_STU_CLASS_TBL].[ENRL_ACAD_CAREER] = [RDS_TERM_VW].[ACAD_CAREER] and [TERM_END_DT] >= ?)',
            $sut->toSql()
        );
        $this->assertEquals(Carbon::now()->startOfDay()->toString(), $sut->getBindings()[0]->toString());
    }
}
