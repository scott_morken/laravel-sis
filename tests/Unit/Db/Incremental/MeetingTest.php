<?php

namespace Tests\Smorken\Sis\Unit\Db\Incremental;

use Illuminate\Support\Collection;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Incremental\Klass\Meeting;
use Smorken\Sis\Enums\Relations;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\ClassInstructorModels;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\MeetingModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class MeetingTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_term_id(): void
    {
        $sut = (new Meeting)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [CDS_CLASS_MTG_TBL] where [CLASS_TERM_CD] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_with_college_id(): void
    {
        $sut = (new Meeting)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [CDS_CLASS_MTG_TBL] where [CLASS_INSTITUTION_CD] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = MeetingModels::get();
        $this->assertEquals([
            'sequenceNumber' => 1,
            'collegeId' => 'COLL01',
            'buildingCode' => 'C BLD',
            'locationCode' => 'COLL01',
            'facilityId' => 'C_FAC_ID',
            'room' => 303,
            'freeFormatTopic' => '',
            'startDate' => $sut->startDate,
            'endDate' => $sut->endDate,
            'startTime' => $sut->startTime,
            'endTime' => $sut->endTime,
            'daysPattern' => 'MWF',
            'days' => [
                '1',
                '3',
                '5',
            ],
            'daysRaw' => ['M', 'W', 'F'],
            'daysToString' => 'MWF',
            'courseId' => 100000,
            'offeringNumber' => 1,
            'section' => 10,
            'sessionCode' => 'DD',
            'termId' => 1232,
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_an_array_using_mapper(): void
    {
        $sut = MeetingModels::get();
        $classInstructor = ClassInstructorModels::fromMeeting($sut);
        $sut->setRelation(Relations::CLASS_INSTRUCTORS, new Collection([$classInstructor]));
        $this->assertEquals([
            'sequenceNumber' => 1,
            'collegeId' => 'COLL01',
            'buildingCode' => 'C BLD',
            'locationCode' => 'COLL01',
            'facilityId' => 'C_FAC_ID',
            'room' => 303,
            'freeFormatTopic' => '',
            'startDate' => $sut->startDate,
            'endDate' => $sut->endDate,
            'startTime' => $sut->startTime,
            'endTime' => $sut->endTime,
            'daysPattern' => 'MWF',
            'days' => [
                '1',
                '3',
                '5',
            ],
            'daysRaw' => ['M', 'W', 'F'],
            'daysToString' => 'MWF',
            'courseId' => 100000,
            'offeringNumber' => 1,
            'section' => 10,
            'sessionCode' => 'DD',
            'termId' => 1232,
            'class_instructors' => [
                [
                    'meetingSequenceNumber' => 1,
                    'sequenceNumber' => 1,
                    'instructorId' => 30000000,
                    'fullName' => 'Emie Simonis',
                    'shortName' => 'E. Simonis',
                    'typeCode' => 'ADJ',
                    'typeDescription' => 'Adjunct',
                    'roleCode' => 'SI',
                    'roleDescription' => 'Secondary',
                    'loadFactor' => 0.0,
                    'workloadHours' => 0.0,
                    'assignmentTypeCode' => '',
                    'collegeId' => 'COLL01',
                    'courseId' => 100000,
                    'offeringNumber' => 1,
                    'section' => 10,
                    'sessionCode' => 'DD',
                    'termId' => 1232,
                ],
            ],
        ], $sut->toArray());
        $this->assertEquals([
            'meetingSequenceNumber' => 1,
            'sequenceNumber' => 1,
            'instructorId' => 30000000,
            'fullName' => 'Emie Simonis',
            'shortName' => 'E. Simonis',
            'typeCode' => 'ADJ',
            'typeDescription' => 'Adjunct',
            'roleCode' => 'SI',
            'roleDescription' => 'Secondary',
            'loadFactor' => 0.0,
            'workloadHours' => 0.0,
            'assignmentTypeCode' => '',
            'collegeId' => 'COLL01',
            'courseId' => 100000,
            'offeringNumber' => 1,
            'section' => 10,
            'sessionCode' => 'DD',
            'termId' => 1232,

        ], $sut->primaryClassInstructor->toArray());
    }
}
