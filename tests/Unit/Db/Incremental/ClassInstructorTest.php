<?php

namespace Tests\Smorken\Sis\Unit\Db\Incremental;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Incremental\Klass\ClassInstructor;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\ClassInstructorModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class ClassInstructorTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new ClassInstructor)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [CDS_CLASS_INSTR_TBL] where [CLASS_INSTITUTION_CD] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = ClassInstructorModels::get();
        $this->assertEquals([
            'meetingSequenceNumber' => 1,
            'sequenceNumber' => 1,
            'instructorId' => 30000000,
            'fullName' => 'Emie Simonis',
            'shortName' => 'E. Simonis',
            'typeCode' => 'ADJ',
            'typeDescription' => 'Adjunct',
            'roleCode' => 'SI',
            'roleDescription' => 'Secondary',
            'loadFactor' => 0.0,
            'workloadHours' => 0.0,
            'assignmentTypeCode' => '',
            'collegeId' => 'COLL01',
            'courseId' => 100000,
            'offeringNumber' => 1,
            'section' => 10,
            'sessionCode' => 'DD',
            'termId' => '1234',
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_query_for_student_id(): void
    {
        $sut = (new ClassInstructor)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [CDS_CLASS_INSTR_TBL] where [CLASSM_INSTRUCTOR_EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }
}
