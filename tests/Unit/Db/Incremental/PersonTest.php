<?php

namespace Tests\Smorken\Sis\Unit\Db\Incremental;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Incremental\Person\Person;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\PersonModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class PersonTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_student_id(): void
    {
        $sut = (new Person)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [CDS_PERSON_TBL] where [PERS_EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }

    #[Test]
    public function it_returns_the_preferred_first_name(): void
    {
        $person = (new Person)->forceFill(['PERS_PREFERRED_FIRST_NAME' => 'Foo', 'PERS_PRIMARY_FIRST_NAME' => 'Bar']);
        $this->assertEquals('Foo', $person->getFirstName());
    }

    #[Test]
    public function it_returns_the_preferred_last_name(): void
    {
        $person = (new Person)->forceFill(['PERS_PREFERRED_LAST_NAME' => 'Foo', 'PERS_PRIMARY_LAST_NAME' => 'Bar']);
        $this->assertEquals('Foo', $person->getLastName());
    }

    #[Test]
    public function it_returns_the_primary_first_name(): void
    {
        $person = (new Person)->forceFill(['PERS_PRIMARY_FIRST_NAME' => 'Bar']);
        $this->assertEquals('Bar', $person->getFirstName());
    }

    #[Test]
    public function it_returns_the_primary_last_name(): void
    {
        $person = (new Person)->forceFill(['PERS_PRIMARY_LAST_NAME' => 'Bar']);
        $this->assertEquals('Bar', $person->getLastName());
    }

    #[Test]
    public function it_uses_the_mapper_to_create_an_array(): void
    {
        $sut = PersonModels::get();
        $this->assertEquals('Louisa', $sut->firstName);
        $this->assertInstanceOf(Carbon::class, $sut->dob);
        $this->assertEquals([
            'id' => 30000000,
            'altId' => '2QDGF00000',
            'PERS_PREFERRED_FIRST_NAME' => '',
            'PERS_PREFERRED_LAST_NAME' => '',
            'PERS_PRIMARY_FIRST_NAME' => 'Louisa',
            'PERS_PRIMARY_LAST_NAME' => 'Wuckert',
            'email' => 'xlowe@example.org',
            'phone' => '+1.847.640.3470',
            'dob' => $sut->dob,
            'hcmId' => 10000000,
            'firstName' => 'Louisa',
            'lastName' => 'Wuckert',
        ], $sut->toArray());
    }
}
