<?php

namespace Tests\Smorken\Sis\Unit\Db\Incremental;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Incremental\Klass\Klass;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\ClassInstructorModels;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\ClassModels;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\EnrollmentModels;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\MeetingModels;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\PersonModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class ClassTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_in_session_classes(): void
    {
        $today = Carbon::now();
        $sut = (new Klass)->newQuery()->inSession();
        $this->assertEquals('select * from [CDS_CLASS_TBL] where ([CLASS_START_DATE] <= ? and [CLASS_END_DATE] >= ?)',
            $sut->toSql());
        $this->assertEquals([(string) $today->copy()->endOfDay(), (string) $today->copy()->startOfDay()],
            array_map(static fn (Carbon $v) => (string) $v, $sut->getBindings()));
    }

    #[Test]
    public function it_creates_a_query_for_other_class(): void
    {
        DB::partialMock();
        $model = (new Klass)->forceFill([
            'CLASS_COURSE_ID' => 1,
            'CLASS_COURSE_OFFER_NBR' => 10,
            'CLASS_TERM_CD' => 1234,
            'CLASS_SESSION_CD' => 'DD',
            'CLASS_SECTION' => 2,
        ]);
        $sut = $model->other();
        $this->assertStringEndsWith('from [RDS_CLASS_VW] where [RDS_CLASS_VW].[CLASS_COURSE_ID] = ? and [RDS_CLASS_VW].[CLASS_COURSE_OFFER_NBR] = ? and [RDS_CLASS_VW].[CLASS_TERM_CD] = ? and [RDS_CLASS_VW].[CLASS_SESSION_CD] = ? and [RDS_CLASS_VW].[CLASS_SECTION] = ?',
            $sut->toSql());
        $this->assertEquals([
            1,
            10,
            1234,
            'DD',
            2,
        ], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_the_term_id(): void
    {
        $sut = (new Klass)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [CDS_CLASS_TBL] where [CLASS_TERM_CD] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_with_college_id(): void
    {
        $sut = (new Klass)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [CDS_CLASS_TBL] where [CLASS_INSTITUTION_CD] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = ClassModels::get();
        $this->assertInstanceOf(Carbon::class, $sut->endDate);
        $this->assertInstanceOf(Carbon::class, $sut->startDate);
        $this->assertEquals([
            'academicCareer' => 'CRED',
            'academicOrgId' => 'AO-1',
            'academicOrgDescription' => 'Org 1',
            'associatedClassNumber' => '9999',
            'campusCode' => 'CAMP01',
            'cancelDate' => null,
            'catalogNumber' => '101',
            'className' => 'ABC101',
            'classNumber' => 10000,
            'collegeId' => 'COLL01',
            'combinedMajor' => false,
            'componentCode' => 'LEC',
            'componentDescription' => 'Lecture',
            'courseId' => 100000,
            'offeringNumber' => 1,
            'dayEveningCode' => 'D',
            'dayEveningDescription' => 'Day',
            'description' => 'ABC101 Class',
            'friendlyDescription' => 'ABC101 Class',
            'endDate' => $sut->endDate,
            'enrolled' => 15,
            'enrolledCap' => 30,
            'enrollmentStatusCode' => 'O',
            'enrollmentStatusDescription' => 'Open',
            'gradingBasisCode' => '',
            'gradingBasisDescription' => '',
            'honors' => false,
            'instructionModeCode' => 'P',
            'instructionModeDescription' => 'In Person',
            'locationCode' => 'PC Main',
            'section' => 10,
            'sessionCode' => 'DD',
            'sessionDescription' => 'Dynamic Dated (normal)',
            'startDate' => $sut->startDate,
            'statusCode' => 'A',
            'statusDescription' => 'Active',
            'subject' => 'ABC',
            'termId' => 1232,
            'typeCode' => 'E',
            'typeDescription' => 'Enrollable',
            'units' => 3.0,
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_an_array_from_mapper_with_relations(): void
    {
        $sut = ClassModels::get();
        $meeting = MeetingModels::fromClass($sut);
        $sut->setRelation('meetings', new Collection([$meeting]));
        $classInstructor = ClassInstructorModels::fromMeeting($meeting);
        $meeting->setRelation('classInstructors', new Collection([$classInstructor]));
        $student = PersonModels::get();
        $enrollment = EnrollmentModels::fromClass($sut, ['ENRL_EMPLID' => $student->PERS_EMPLID]);
        $enrollment->setRelation('student', $student);
        $sut->setRelation('enrollments', new Collection([$enrollment]));
        $this->assertEquals([
            'academicCareer' => 'CRED',
            'academicOrgId' => 'AO-1',
            'academicOrgDescription' => 'Org 1',
            'associatedClassNumber' => '9999',
            'campusCode' => 'CAMP01',
            'cancelDate' => null,
            'catalogNumber' => '101',
            'className' => 'ABC101',
            'classNumber' => 10000,
            'collegeId' => 'COLL01',
            'combinedMajor' => false,
            'componentCode' => 'LEC',
            'componentDescription' => 'Lecture',
            'courseId' => 100000,
            'offeringNumber' => 1,
            'dayEveningCode' => 'D',
            'dayEveningDescription' => 'Day',
            'description' => 'ABC101 Class',
            'friendlyDescription' => 'ABC101 Class',
            'endDate' => $sut->endDate,
            'enrolled' => 15,
            'enrolledCap' => 30,
            'enrollmentStatusCode' => 'O',
            'enrollmentStatusDescription' => 'Open',
            'gradingBasisCode' => '',
            'gradingBasisDescription' => '',
            'honors' => false,
            'instructionModeCode' => 'P',
            'instructionModeDescription' => 'In Person',
            'locationCode' => 'PC Main',
            'section' => 10,
            'sessionCode' => 'DD',
            'sessionDescription' => 'Dynamic Dated (normal)',
            'startDate' => $sut->startDate,
            'statusCode' => 'A',
            'statusDescription' => 'Active',
            'subject' => 'ABC',
            'termId' => 1232,
            'typeCode' => 'E',
            'typeDescription' => 'Enrollable',
            'units' => 3.0,
            'meetings' => [
                [
                    'sequenceNumber' => 1,
                    'collegeId' => 'COLL01',
                    'buildingCode' => 'C BLD',
                    'locationCode' => 'COLL01',
                    'facilityId' => 'C_FAC_ID',
                    'room' => 303,
                    'freeFormatTopic' => '',
                    'startDate' => $meeting->startDate,
                    'endDate' => $meeting->endDate,
                    'startTime' => $meeting->startTime,
                    'endTime' => $meeting->endTime,
                    'daysPattern' => 'MWF',
                    'days' => [
                        '1',
                        '3',
                        '5',
                    ],
                    'daysRaw' => ['M', 'W', 'F'],
                    'daysToString' => 'MWF',
                    'courseId' => 100000,
                    'offeringNumber' => 1,
                    'section' => 10,
                    'sessionCode' => 'DD',
                    'termId' => 1232,
                    'class_instructors' => [
                        [
                            'meetingSequenceNumber' => 1,
                            'sequenceNumber' => 1,
                            'instructorId' => 30000000,
                            'fullName' => 'Emie Simonis',
                            'shortName' => 'E. Simonis',
                            'typeCode' => 'ADJ',
                            'typeDescription' => 'Adjunct',
                            'roleCode' => 'SI',
                            'roleDescription' => 'Secondary',
                            'loadFactor' => 0.0,
                            'workloadHours' => 0.0,
                            'assignmentTypeCode' => '',
                            'collegeId' => 'COLL01',
                            'courseId' => 100000,
                            'offeringNumber' => 1,
                            'section' => 10,
                            'sessionCode' => 'DD',
                            'termId' => 1232,
                        ],
                    ],
                ],
            ],
            'enrollments' => [
                [
                    'studentId' => 30000000,
                    'collegeId' => 'COLL01',
                    'termId' => 1232,
                    'classNumber' => 10000,
                    'academicCareer' => 'CRED',
                    'gradingBasisCode' => 'GRD',
                    'gradingBasisDescription' => 'Graded (standard)',
                    'grade' => 'A',
                    'gradeDate' => null,
                    'unitsTaken' => 3.0,
                    'gradePoints' => 4.0,
                    'earnedCredit' => true,
                    'includeInGpa' => true,
                    'statusCode' => 'ENRL',
                    'statusDescription' => 'Enrolled',
                    'lastActionCode' => null,
                    'lastActionDescription' => null,
                    'oeeStartDate' => $enrollment->oeeStartDate,
                    'oeeEndDate' => $enrollment->oeeEndDate,
                    'student' => [
                        'id' => 30000000,
                        'altId' => '2QDGF00000',
                        'firstName' => 'Louisa',
                        'lastName' => 'Wuckert',
                        'email' => 'xlowe@example.org',
                        'phone' => '+1.847.640.3470',
                        'dob' => $student->dob,
                        'hcmId' => 10000000,
                        'PERS_PREFERRED_FIRST_NAME' => '',
                        'PERS_PREFERRED_LAST_NAME' => '',
                        'PERS_PRIMARY_FIRST_NAME' => 'Louisa',
                        'PERS_PRIMARY_LAST_NAME' => 'Wuckert',
                    ],
                ],
            ],
        ], $sut->toArray());
    }
}
