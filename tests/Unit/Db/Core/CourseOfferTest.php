<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Course\CourseOffer;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\CourseCatalogModels;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\CourseOfferModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class CourseOfferTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_catalog_relation(): void
    {
        $model = (new CourseOffer)->forceFill([
            'CRSE_CATALOG_SID' => '1',
            'COFFR_CRSE_ID' => 100000,
        ]);
        $sut = $model->catalog();
        $this->assertEquals('select [REC_CRSE_CATALOG_VW].[CRSE_REC_KEY], [REC_CRSE_CATALOG_VW].[CRSE_CATALOG_SID], [REC_CRSE_CATALOG_VW].[CRSE_CRSE_ID], [REC_CRSE_CATALOG_VW].[CRSE_DESCR], [REC_CRSE_CATALOG_VW].[CRSE_CONSENT], [REC_CRSE_CATALOG_VW].[CRSE_UNITS_MINIMUM], [REC_CRSE_CATALOG_VW].[CRSE_UNITS_MAXIMUM], [REC_CRSE_CATALOG_VW].[CRSE_GRADING_BASIS], [REC_CRSE_CATALOG_VW].[CRSE_COMPONENT], [REC_CRSE_CATALOG_VW].[CRSE_COURSE_NOTES], [REC_CRSE_CATALOG_VW].[CRSE_GOV_BD_APPROVAL_DATE], [REC_CRSE_CATALOG_VW].[CRSE_EFFDT] from [REC_CRSE_CATALOG_VW] where [REC_CRSE_CATALOG_VW].[CRSE_CATALOG_SID] = ? and [REC_CRSE_CATALOG_VW].[CRSE_CRSE_ID] = ?',
            $sut->toSql());
        $this->assertEquals(['1', 100000], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new CourseOffer)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [REC_CRSE_OFFER_VW] where [COFFR_INSTITUTION] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_joining_catalog(): void
    {
        $sut = new CourseOffer;
        $query = $sut->newQuery()->joinCourseCatalog();
        $this->assertEquals(
            'select [REC_CRSE_OFFER_VW].*, [CRSE_REC_KEY], [CRSE_CRSE_ID], [CRSE_DESCR], [CRSE_CONSENT], [CRSE_UNITS_MINIMUM], [CRSE_UNITS_MAXIMUM], [CRSE_GRADING_BASIS], [CRSE_COMPONENT], [CRSE_COURSE_NOTES], [CRSE_GOV_BD_APPROVAL_DATE], [CRSE_EFFDT] from [REC_CRSE_OFFER_VW] inner join [REC_CRSE_CATALOG_VW] on [REC_CRSE_CATALOG_VW].[CRSE_CRSE_ID] = [REC_CRSE_OFFER_VW].[COFFR_CRSE_ID] and [REC_CRSE_CATALOG_VW].[CRSE_CATALOG_SID] = [REC_CRSE_OFFER_VW].[CRSE_CATALOG_SID] and [REC_CRSE_CATALOG_VW].[CRSE_EFFDT] = [REC_CRSE_OFFER_VW].[COFFR_CRSE_EFFDT]',
            $query->toSql()
        );
    }

    #[Test]
    public function it_creates_a_query_for_lifted_catalog(): void
    {
        $sut = new CourseOffer;
        $query = $sut->joinCourseCatalog()->newQuery();
        $this->assertEquals(
            'select [REC_CRSE_OFFER_VW].*, [CRSE_REC_KEY], [CRSE_CRSE_ID], [CRSE_DESCR], [CRSE_CONSENT], [CRSE_UNITS_MINIMUM], [CRSE_UNITS_MAXIMUM], [CRSE_GRADING_BASIS], [CRSE_COMPONENT], [CRSE_COURSE_NOTES], [CRSE_GOV_BD_APPROVAL_DATE], [CRSE_EFFDT] from [REC_CRSE_OFFER_VW] inner join [REC_CRSE_CATALOG_VW] on [REC_CRSE_CATALOG_VW].[CRSE_CRSE_ID] = [REC_CRSE_OFFER_VW].[COFFR_CRSE_ID] and [REC_CRSE_CATALOG_VW].[CRSE_CATALOG_SID] = [REC_CRSE_OFFER_VW].[CRSE_CATALOG_SID] and [REC_CRSE_CATALOG_VW].[CRSE_EFFDT] = [REC_CRSE_OFFER_VW].[COFFR_CRSE_EFFDT]',
            $query->toSql()
        );
    }

    #[Test]
    public function it_creates_a_query_for_max_effective_date(): void
    {
        $sut = (new CourseOffer)->newQuery()->joinOnMaxEffectiveDateSubquery();
        $this->assertEquals('select * from [REC_CRSE_OFFER_VW] inner join (select COFFR_CRSE_ID, MAX(COFFR_CRSE_EFFDT) as max_date from [REC_CRSE_OFFER_VW] where [COFFR_COURSE_APPROVED] = ? group by [COFFR_CRSE_ID]) as [sub_eff_date] on [REC_CRSE_OFFER_VW].[COFFR_CRSE_ID] = [sub_eff_date].[COFFR_CRSE_ID] and [REC_CRSE_OFFER_VW].[COFFR_CRSE_EFFDT] = [sub_eff_date].[max_date]',
            $sut->toSql());
        $this->assertEquals(['A'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_mapper_with_catalog_relation(): void
    {
        $model = CourseOfferModels::get();
        $model->setRelation('catalog', CourseCatalogModels::fromCourseOffer($model));
        $this->assertEquals([
            'id' => '100000+1+2022-03-25',
            'catalogId' => 5286,
            'classId' => 1,
            'offeringNumber' => 1,
            'academicCareer' => 'CRED',
            'academicGroup' => 'AO',
            'academicOrgId' => 'AO-1',
            'approved' => 'A',
            'campusCode' => 'CAMP01',
            'catalogNumber' => '101',
            'catalogPrint' => true,
            'collegeId' => 'COLL01',
            'effectiveDate' => $model->effectiveDate,
            'schedulePrint' => true,
            'subject' => 'ABC',
            'courseId' => 100000,
            'catalog' => [
                'id' => '100000+1+2022-03-25',
                'catalogId' => 5286,
                'courseId' => 100000,
                'description' => 'Course 100000',
                'consentCode' => 'N',
                'componentCode' => 'LEC',
                'units' => 3.0,
                'unitsMax' => 3.0,
                'gradingBasisCode' => 'GRD',
                'notes' => '',
                'approvalDate' => $model->catalog->approvalDate,
                'effectiveDate' => $model->catalog->effectiveDate,
            ],
        ], $model->toArray());
    }
}
