<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Student\Enrollment;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\EnrollmentModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class EnrollmentTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new Enrollment)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [RDS_STU_CLASS_VW] where [ENRL_INSTITUTION_CD] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_the_mapper(): void
    {
        $sut = EnrollmentModels::get();
        $this->assertInstanceOf(Carbon::class, $sut->oeeStartDate);
        $this->assertInstanceOf(Carbon::class, $sut->oeeEndDate);
        $this->assertEquals([
            'studentId' => 30000000,
            'collegeId' => 'COLL01',
            'termId' => 1232,
            'classNumber' => 10000,
            'academicCareer' => 'CRED',
            'gradingBasisCode' => 'GRD',
            'gradingBasisDescription' => 'Graded (standard)',
            'grade' => 'A',
            'gradeDate' => null,
            'unitsTaken' => 3.0,
            'gradePoints' => 4.0,
            'earnedCredit' => true,
            'includeInGpa' => true,
            'statusCode' => 'ENRL',
            'statusDescription' => 'Enrolled',
            'lastActionCode' => null,
            'lastActionDescription' => null,
            'oeeStartDate' => $sut->oeeStartDate,
            'oeeEndDate' => $sut->oeeEndDate,
            'CLASS_SID' => 175848596,
            'ENRL_GRADING_BASIS_LDESC' => 'Graded (standard)',
        ], $sut->toArray());
    }

    public function testStudentIdQuery(): void
    {
        $sut = (new Enrollment)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [RDS_STU_CLASS_VW] where [EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }

    public function testTermIdQuery(): void
    {
        $sut = (new Enrollment)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [RDS_STU_CLASS_VW] where [ENRL_TERM_CD] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }
}
