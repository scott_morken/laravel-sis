<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Klass\Klass;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\ClassModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class ClassTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new Klass)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [RDS_CLASS_VW] where [CLASS_INSTITUTION_CD] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_combined_sections_with_the_combined_id(): void
    {
        $sut = (new Klass)->forceFill([
            'CLASS_COMBINED_ID' => '0001',
            'CLASS_TERM_CD' => '1234',
            'CLASS_INSTITUTION_CD' => 'COLL01',
            'CLASS_SESSION_CD' => 'DD',
        ])->combinedSections();
        $this->assertStringEndsWith('from [RDS_CLASS_VW] where [RDS_CLASS_VW].[CLASS_TERM_CD] = ? and [RDS_CLASS_VW].[CLASS_INSTITUTION_CD] = ? and [RDS_CLASS_VW].[CLASS_SESSION_CD] = ? and [RDS_CLASS_VW].[CLASS_COMBINED_ID] = ? and ([CLASS_COMBINED_ID] is not null and [CLASS_COMBINED_ID] <> ? and [CLASS_CANCEL_DATE] is null and [CLASS_CLASS_STAT] <> ?)',
            $sut->toSql());
        $this->assertEquals([
            '1234',
            'COLL01',
            'DD',
            '0001',
            '',
            'X',
        ], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_combined_sections_without_the_combined_id(): void
    {
        $sut = (new Klass)->forceFill([
            'CLASS_COMBINED_ID' => '',
            'CLASS_TERM_CD' => '1234',
            'CLASS_INSTITUTION_CD' => 'COLL01',
            'CLASS_SESSION_CD' => 'DD',
        ])->combinedSections();
        $this->assertStringEndsWith('from [RDS_CLASS_VW] where [RDS_CLASS_VW].[CLASS_TERM_CD] = ? and [RDS_CLASS_VW].[CLASS_INSTITUTION_CD] = ? and [RDS_CLASS_VW].[CLASS_SESSION_CD] = ? and [RDS_CLASS_VW].[CLASS_COMBINED_ID] = ? and ([CLASS_COMBINED_ID] is not null and [CLASS_COMBINED_ID] <> ? and [CLASS_CANCEL_DATE] is null and [CLASS_CLASS_STAT] <> ?)',
            $sut->toSql());
        $this->assertEquals([
            '1234',
            'COLL01',
            'DD',
            '',
            '',
            'X',
        ], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = ClassModels::get();
        $this->assertEquals([
            'academicCareer' => 'CRED',
            'academicOrgId' => 'AO-1',
            'academicOrgDescription' => 'Org 1',
            'associatedClassNumber' => '9999',
            'campusCode' => 'CAMP01',
            'cancelDate' => null,
            'catalogNumber' => '101',
            'className' => 'ABC101',
            'classNumber' => 10000,
            'collegeId' => 'COLL01',
            'combinedId' => '0010',
            'combinedMajor' => false,
            'componentCode' => 'LEC',
            'componentDescription' => 'Lecture',
            'courseId' => 100000,
            'offeringNumber' => 1,
            'dayEveningCode' => 'D',
            'dayEveningDescription' => 'Day',
            'description' => 'ABC101 Class',
            'friendlyDescription' => 'ABC101 Class',
            'endDate' => $sut->endDate,
            'enrolled' => 15,
            'enrolledCap' => 30,
            'enrollmentStatusCode' => 'O',
            'enrollmentStatusDescription' => 'Open',
            'gradingBasisCode' => 'PZ',
            'gradingBasisDescription' => 'Pass/Fail',
            'honors' => false,
            'instructionModeCode' => 'P',
            'instructionModeDescription' => 'In Person',
            'locationCode' => 'PC Main',
            'section' => 10,
            'sessionCode' => 'DD',
            'sessionDescription' => 'Dynamic Dated (normal)',
            'startDate' => $sut->startDate,
            'statusCode' => 'A',
            'statusDescription' => 'Active',
            'subject' => 'ABC',
            'termId' => 1232,
            'typeCode' => 'E',
            'typeDescription' => 'Enrollable',
            'units' => 3.0,
            'campusDescription' => 'Campus 1',
            'combinedSection' => false,
            'consentCode' => 'N',
            'consentDescription' => 'No Special Consent Required',
            'departmentId' => '100-12345',
            'dualEnroll' => false,
            'dualEnrollSponsorCode' => '',
            'dualEnrollSponsorDescription' => '',
            'dualEnrollSponsorShortDescription' => '',
            'loadCalculationCode' => 'S',
            'loadCalculationDescription' => 'Standard',
            'locationDescription' => 'Phoenix College',
            'majorClassName' => 'ABC101',
            'majorClassNumber' => '12344',
            'primaryInstructorId' => 30000000,
            'primaryInstructorName' => 'Last,First M',
            'rollover' => true,
            'schedulePrint' => true,
            'weekWorkloadHours' => 2.4,
            'totalWorkloadHours' => 2.4,
            'majorClass' => 'ABC101 12344',
            'combinedClassesString' => 'ABC102 11111, 103',
            'CLASS_COMBINED_DESCR' => 'ABC102-11111, 103',
            'CLASS_HONORS' => 'N',
            'CLASS_SID' => '123210000',
            'COFFR_CLASS_SID' => 7279,
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_query_for_in_session_classes(): void
    {
        $today = Carbon::now();
        $sut = (new Klass)->newQuery()->inSession();
        $this->assertEquals('select * from [RDS_CLASS_VW] where ([CLASS_START_DATE] <= ? and [CLASS_END_DATE] >= ?)',
            $sut->toSql());
        $this->assertEquals([(string) $today->copy()->endOfDay(), (string) $today->copy()->startOfDay()],
            array_map(static fn (Carbon $v) => (string) $v, $sut->getBindings()));
    }

    #[Test]
    public function it_creates_a_query_for_term_id(): void
    {
        $sut = (new Klass)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [RDS_CLASS_VW] where [CLASS_TERM_CD] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }
}
