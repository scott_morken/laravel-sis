<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\CourseCatalogModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class CourseCatalogTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = CourseCatalogModels::get();
        $this->assertEquals([
            'id' => '100000+1+2022-03-25',
            'catalogId' => 1,
            'courseId' => '100000',
            'description' => 'Course 100000',
            'consentCode' => 'N',
            'componentCode' => 'LEC',
            'units' => 3.0,
            'unitsMax' => 3.0,
            'gradingBasisCode' => 'GRD',
            'notes' => '',
            'approvalDate' => $sut->approvalDate,
            'effectiveDate' => $sut->effectiveDate,
        ], $sut->toArray());
    }
}
