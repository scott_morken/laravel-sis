<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Student\ServiceIndicator;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\ServiceIndicatorModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class ServiceIndicatorTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new ServiceIndicator)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [RDS_SERV_IND_VW] where [SRVC_INSTITUTION] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_student_id(): void
    {
        $sut = (new ServiceIndicator)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [RDS_SERV_IND_VW] where [SRVC_EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_term_id(): void
    {
        $sut = (new ServiceIndicator)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [RDS_SERV_IND_VW] where [SRVC_SERVICE_IND_ACT_TERM] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_for_mapper(): void
    {
        $sut = ServiceIndicatorModels::get();
        $this->assertEquals([
            'studentId' => 30000000,
            'collegeId' => 'COLL01',
            'createdAt' => $sut->createdAt,
            'createdBy' => 'HCS0000000',
            'code' => 'ADM',
            'description' => 'Administrative Hold',
            'reasonCode' => 'RADM',
            'reasonDescription' => 'R: Administrative Hold',
            'activeTermId' => 1232,
            'activeDate' => $sut->activeDate,
            'positive' => false,
            'amount' => 91.68,
            'comments' => 'enim illum minima animi dolorem',
            'contact' => 'Phyllis Olson',
            'departmentCode' => 'D800',
            'departmentDescription' => 'COLL01 Department',
            'dueDate' => null,
        ], $sut->toArray());
    }
}
