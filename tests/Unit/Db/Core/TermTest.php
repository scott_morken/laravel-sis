<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Term;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\TermModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class TermTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_calls_the_active_scope_from_the_query_builder(): void
    {
        $sut = (new Term)->newQuery()->active();
        $now = new Carbon;
        $this->assertEquals('select * from [RDS_TERM_VW] where ([TERM_BEGIN_DT] <= ? and [TERM_END_DT] >= ?)',
            $sut->toSql());
        $this->assertEquals([
            $now->copy()->endOfDay()->toString(),
            $now->copy()->startOfDay()->toString(),
        ], array_map(fn (Carbon $v) => $v->toString(), $sut->getBindings()));
    }

    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new Term)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [RDS_TERM_VW] where [INSTITUTION] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_term_id(): void
    {
        $sut = (new Term)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [RDS_TERM_VW] where [STRM] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_for_mapper(): void
    {
        $sut = TermModels::get();
        $this->assertEquals([
            'id' => 'T1234',
            'description' => 'Term 1234',
            'academicCareer' => 'CRED',
            'startDate' => $sut->startDate,
            'endDate' => $sut->endDate,
            'collegeId' => 'COLL01',
        ], $sut->toArray());
    }
}
