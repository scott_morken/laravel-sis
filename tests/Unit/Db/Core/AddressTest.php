<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Person\Address;
use Smorken\Sis\Db\Core\Person\Email;
use Smorken\Sis\Oci\Constants\Person\AddressType;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\EmailModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class AddressTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_type(): void
    {
        $sut = (new Address)->newQuery()->typeIs(AddressType::HOME);
        $this->assertEquals('select * from [CC_ADDRESSES_VW] where [ADDR_ADDRESS_TYPE] = ?', $sut->toSql());
        $this->assertEquals(['HOME'], $sut->getBindings());
    }
}
