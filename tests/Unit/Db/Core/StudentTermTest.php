<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Student\StudentTerm;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\StudentTermModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class StudentTermTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new StudentTerm)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [RDS_STU_TERM_VW] where [CTERM_INSTITUTION_CODE] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_student_id(): void
    {
        $sut = (new StudentTerm)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [RDS_STU_TERM_VW] where [EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_term_id(): void
    {
        $sut = (new StudentTerm)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [RDS_STU_TERM_VW] where [CTERM_TERM_CD] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_for_mapper(): void
    {
        $sut = StudentTermModels::get();
        $this->assertEquals([
            'studentId' => 30000000,
            'termId' => 1232,
            'collegeId' => 'COLL01',
            'academicCareer' => 'CRED',
            'cumulativeGpa' => 2.71,
            'cumulativeAttemptedHours' => 34.0,
            'cumulativeEarnedHours' => 11.0,
            'cumulativeGpaHours' => 22.0,
            'cumulativeGpaPoints' => 1.48,
            'cumulativeClockHours' => 52.48,
            'termGpa' => 1.57,
            'termAttemptedHours' => 13.0,
            'termEarnedHours' => 8.0,
            'termGpaHours' => 7.0,
            'termGpaPoints' => 3.83,
            'termClockHours' => 12.52,
            'academicStandingCode' => 'ACPT',
            'academicStandingDescription' => 'Acceptable',
            'loadCode' => 'F',
            'loadDescription' => 'Enrolled Full-Time',
            'inProgressGpa' => 2.9,
            'inProgressNoGpa' => 1.1,
            'CTERM_TERM_SDESC' => 'Term 1232',
        ], $sut->toArray());
    }
}
