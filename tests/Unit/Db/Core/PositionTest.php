<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Instructor\Position;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\PositionModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class PositionTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_student_id(): void
    {
        $sut = (new Position)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [PS_MC_INSTR_POS_VW] where [EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = PositionModels::get();
        $this->assertEquals([
            'studentId' => '30000000',
            'recordNumber' => '3',
            'departmentId' => '141',
            'jobCode' => 'JOB39',
            'positionNumber' => '5',
            'positionDescription' => 'et similique maxime',
        ], $sut->toArray());
    }
}
