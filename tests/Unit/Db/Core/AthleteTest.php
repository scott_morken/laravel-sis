<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use Carbon\Carbon;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Student\Athlete;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\AthleteModels;
use Tests\Smorken\Sis\Stubs\StaticModels\Incremental\PersonModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class AthleteTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_college_id_query(): void
    {
        $sut = (new Athlete)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [REC_ATHLETIC_PARTIC_VW] where [ATHL_INSTITUTION] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_student_id_query(): void
    {
        $sut = (new Athlete)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [REC_ATHLETIC_PARTIC_VW] where [ATHL_EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }

    #[Test]
    public function it_uses_camel_mapper_for_toArray(): void
    {
        $sut = AthleteModels::get();
        $this->assertInstanceOf(Carbon::class, $sut->startDate);
        $this->assertEquals([
            'studentId' => 30000000,
            'collegeId' => 'COLL01',
            'sportCode' => 'BSK',
            'sportDescription' => 'Dicta in soluta similique aut.',
            'statusCode' => 'ACTIV',
            'statusDescription' => 'Active',
            'ncaaEligible' => true,
            'currentParticipant' => true,
            'startDate' => $sut->startDate,
            'endDate' => null,
        ], $sut->toArray());
    }

    public function testGetDataWithStudentRelation(): void
    {
        $student = PersonModels::get();
        $sut = AthleteModels::fromPerson($student);
        $sut->setRelation('student', $student);
        $this->assertEquals([
            'studentId' => 30000000,
            'collegeId' => 'COLL01',
            'sportCode' => 'BSK',
            'sportDescription' => 'Dicta in soluta similique aut.',
            'statusCode' => 'ACTIV',
            'statusDescription' => 'Active',
            'ncaaEligible' => true,
            'currentParticipant' => true,
            'startDate' => $sut->startDate,
            'endDate' => null,
            'student' => [
                'id' => 30000000,
                'altId' => '2QDGF00000',
                'firstName' => 'Louisa',
                'lastName' => 'Wuckert',
                'email' => 'xlowe@example.org',
                'phone' => '+1.847.640.3470',
                'dob' => $student->dob,
                'hcmId' => 10000000,
                'PERS_PREFERRED_FIRST_NAME' => '',
                'PERS_PREFERRED_LAST_NAME' => '',
                'PERS_PRIMARY_FIRST_NAME' => 'Louisa',
                'PERS_PRIMARY_LAST_NAME' => 'Wuckert',
            ],
        ], $sut->toArray());
    }
}
