<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Person\Person;
use Smorken\Sis\Enums\Relations;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\EnrollmentModels;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\PersonModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class PersonTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_student_id(): void
    {
        $sut = (new Person)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [RDS_PERSON_VW] where [EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }

    #[Test]
    public function it_maps_data_to_an_array(): void
    {
        $sut = PersonModels::get();
        $this->assertInstanceOf(Carbon::class, $sut->dob);
        $this->assertEquals([
            'id' => 30000000,
            'altId' => '2QDGF00000',
            'firstName' => 'Louisa',
            'lastName' => 'Wuckert',
            'email' => 'xlowe@example.org',
            'phone' => '+1.847.640.3470',
            'dob' => $sut->dob,
            'hcmId' => 10000000,
            'PERS_PREFERRED_FIRST_NAME' => '',
            'PERS_PREFERRED_LAST_NAME' => '',
            'PERS_PRIMARY_FIRST_NAME' => 'Louisa',
            'PERS_PRIMARY_LAST_NAME' => 'Wuckert',
        ], $sut->toArray());
    }

    #[Test]
    public function it_maps_data_to_an_array_with_enrollments(): void
    {
        $sut = PersonModels::get();
        $enrollment = EnrollmentModels::fromPerson($sut);
        $sut->setRelation(Relations::ENROLLMENTS, new Collection([$enrollment]));
        $this->assertEquals([
            'id' => 30000000,
            'altId' => '2QDGF00000',
            'firstName' => 'Louisa',
            'lastName' => 'Wuckert',
            'email' => 'xlowe@example.org',
            'phone' => '+1.847.640.3470',
            'dob' => $sut->dob,
            'hcmId' => 10000000,
            'PERS_PREFERRED_FIRST_NAME' => '',
            'PERS_PREFERRED_LAST_NAME' => '',
            'PERS_PRIMARY_FIRST_NAME' => 'Louisa',
            'PERS_PRIMARY_LAST_NAME' => 'Wuckert',
            'enrollments' => [
                [
                    'studentId' => 30000000,
                    'collegeId' => 'COLL01',
                    'termId' => 1232,
                    'classNumber' => 10000,
                    'academicCareer' => 'CRED',
                    'gradingBasisCode' => 'GRD',
                    'gradingBasisDescription' => 'Graded (standard)',
                    'grade' => 'A',
                    'gradeDate' => null,
                    'unitsTaken' => 3.0,
                    'gradePoints' => 4.0,
                    'earnedCredit' => true,
                    'includeInGpa' => true,
                    'statusCode' => 'ENRL',
                    'statusDescription' => 'Enrolled',
                    'lastActionCode' => null,
                    'lastActionDescription' => null,
                    'oeeStartDate' => $enrollment->oeeStartDate,
                    'oeeEndDate' => $enrollment->oeeEndDate,
                    'CLASS_SID' => 175848596,
                    'ENRL_GRADING_BASIS_LDESC' => 'Graded (standard)',
                ],
            ],
        ], $sut->toArray());
    }
}
