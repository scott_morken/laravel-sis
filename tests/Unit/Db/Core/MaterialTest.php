<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\MaterialModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class MaterialTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = MaterialModels::get();
        $this->assertEquals([
            'id' => '123210000',
            'sequenceNumber' => 1,
            'typeCode' => 'TEXTBOOK',
            'typeDescription' => 'Textbook',
            'statusCode' => 'REQ',
            'statusDescription' => 'Required',
            'title' => 'placeat vitae aliquam',
            'isbn' => '9793808992950',
            'author' => 'Gonzalo Zulauf',
            'publisher' => 'Considine-Conroy',
            'edition' => 6,
            'yearPublished' => '1977',
            'price' => 14.14,
            'notes' => 'excepturi iste cupiditate',
        ], $sut->toArray());
    }
}
