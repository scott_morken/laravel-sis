<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Person\Email;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\EmailModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class EmailTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = EmailModels::get();
        $this->assertEquals([
            'id' => '1-ABC1234567@email.edu',
            'studentId' => 30000000,
            'typeCode' => 'MCCD',
            'typeDescription' => 'MCCD Official',
            'email' => 'ABC1234567@email.edu',
            'preferred' => true,
        ], $sut->toArray());
    }

    #[Test]
    public function it_creates_a_query_for_student_id(): void
    {
        $sut = (new Email)->newQuery()->studentIdIs('30000000');
        $this->assertEquals('select * from [CC_EMAIL_ADDRESSES_VW] where [EMAIL_EMPLID] = ?', $sut->toSql());
        $this->assertEquals(['30000000'], $sut->getBindings());
    }
}
