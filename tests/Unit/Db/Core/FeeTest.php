<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Klass\Fee;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\FeeModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class FeeTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new Fee)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [RDS_CLASS_SUBFEE_VW] where [CLSBF_SETID] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_a_query_for_term_id(): void
    {
        $sut = (new Fee)->newQuery()->termIdIs('1234');
        $this->assertEquals('select * from [RDS_CLASS_SUBFEE_VW] where [CLSBF_STRM] = ?', $sut->toSql());
        $this->assertEquals(['1234'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = FeeModels::get();
        $this->assertEquals([
            'collegeId' => 'COLL01',
            'termId' => 1232,
            'accountType' => 'ACT',
            'itemType' => '10000001111111',
            'description' => 'Fee for 16933502',
            'shortDescription' => 16933502,
            'amount' => 84.0,
            'currency' => 'USD',
            'id' => 51655,
            'classId' => '123210000',
            'courseId' => 100000,
            'offeringNumber' => 1,
            'sessionCode' => 'DD',
            'section' => 10,
            'component' => 'LEC',
        ], $sut->toArray());
    }
}
