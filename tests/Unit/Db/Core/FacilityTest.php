<?php

namespace Tests\Smorken\Sis\Unit\Db\Core;

use PHPUnit\Framework\Attributes\Test;
use Smorken\Sis\Db\Core\Physical\Facility;
use Tests\Smorken\Sis\Stubs\StaticModels\Core\FacilityModels;
use Tests\Smorken\Sis\Unit\TestCaseWithMockConnectionResolver;

class FacilityTest extends TestCaseWithMockConnectionResolver
{
    #[Test]
    public function it_creates_a_query_for_college_id(): void
    {
        $sut = (new Facility)->newQuery()->collegeIdIs('COLL01');
        $this->assertEquals('select * from [REC_FACILITY_VW] where [FACILITY_SETID] = ?', $sut->toSql());
        $this->assertEquals(['COLL01'], $sut->getBindings());
    }

    #[Test]
    public function it_creates_an_array_from_mapper(): void
    {
        $sut = FacilityModels::get();
        $this->assertEquals([
            'id' => 'N207',
            'collegeId' => 'PCC01',
            'status' => 'A',
            'buildingCode' => 'A N BLDG',
            'room' => 207,
            'description' => 'N207 Room',
            'shortDescription' => 'N207',
            'groupCode' => 'CLAS',
            'groupDescription' => 'Classroom',
            'locationCode' => 'PC MAIN',
            'locationDescription' => 'Main Campus',
            'capacity' => 25,
            'academicOrgId' => 'AO-1',
        ], $sut->toArray());
    }
}
