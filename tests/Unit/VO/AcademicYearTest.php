<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:31 PM
 */

namespace Tests\Smorken\Sis\Unit\VO;

use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Smorken\Sis\VO\AcademicYear;

class AcademicYearTest extends TestCase
{
    public function testEndingAfterIsAfterStart(): void
    {
        $sut = new AcademicYear;
        $coll = $sut->endingAfter('2017-08-01');
        $this->assertInstanceOf(Collection::class, $coll);
        $this->assertEquals('2018', $coll->first()->id);
        $this->assertEquals('2019', $coll->last()->id);
    }

    public function testEndingAfterIsBeforeStart(): void
    {
        $sut = new AcademicYear;
        $coll = $sut->endingAfter('2017-01-01');
        $this->assertInstanceOf(Collection::class, $coll);
        $this->assertEquals('2017', $coll->first()->id);
        $this->assertEquals('2018', $coll->last()->id);
    }

    public function testGetById(): void
    {
        $sut = new AcademicYear;
        $m = $sut->find('2017');
        $this->assertEquals('2017', $m->id);
        $this->assertEquals('2016 - 2017', $m->description);
        $this->assertEquals('Fall 2016 - Spring 2017', $m->description_long);
        $this->assertEquals('2016-05-01 00:00:00', $m->start_date->toDateTimeString());
        $this->assertEquals('2017-04-30 23:59:59', $m->end_date->toDateTimeString());
    }
}
