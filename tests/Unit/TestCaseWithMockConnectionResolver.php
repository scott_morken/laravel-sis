<?php

namespace Tests\Smorken\Sis\Unit;

use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\SqlServerConnection;
use Mockery as m;
use Orchestra\Testbench\TestCase;
use Tests\Smorken\Sis\Concerns\WithMockConnection;

class TestCaseWithMockConnectionResolver extends TestCase
{
    use WithMockConnection;

    protected function getPackageProviders($app): array
    {
        return [];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $cr = new ConnectionResolver(['sis' => $this->getMockConnection(SqlServerConnection::class)]);
        $cr->setDefaultConnection('sis');
        Model::setConnectionResolver($cr);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
