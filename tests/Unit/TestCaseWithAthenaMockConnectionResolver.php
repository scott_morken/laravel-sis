<?php

namespace Tests\Smorken\Sis\Unit;

use Mockery as m;
use Orchestra\Testbench\TestCase;
use Tests\Smorken\Sis\Concerns\WithMockConnection;

class TestCaseWithAthenaMockConnectionResolver extends TestCase
{
    use WithMockConnection;

    protected function getPackageProviders($app): array
    {
        return [
            \Smorken\Athena\ServiceProvider::class,
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initAthenaConnection();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
