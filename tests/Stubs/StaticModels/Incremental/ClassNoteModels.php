<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Incremental;

use Smorken\Sis\Contracts\Base\Note;
use Smorken\Sis\Db\Core\Klass\Klass;
use Smorken\Sis\Db\Incremental\Klass\ClassNote;

class ClassNoteModels
{
    public static function fromClass(Klass $class): Note
    {
        return self::get([
            'CLASS_COURSE_ID' => $class->CLASS_COURSE_ID,
            'CLASS_COURSE_OFFER_NBR' => $class->CLASS_COURSE_OFFER_NBR,
            'CLASS_SECTION' => $class->CLASS_SECTION,
            'CLASS_SESSION_CD' => $class->CLASS_SESSION_CD,
            'CLASS_TERM_CD' => $class->CLASS_TERM_CD,
        ]);
    }

    public static function get(array $attributes = []): Note
    {
        return (new ClassNote)->forceFill([
            'CLASS_COURSE_ID' => 100000,
            'CLASS_COURSE_OFFER_NBR' => 1,
            'CLASS_TERM_CD' => 1232,
            'CLASS_SESSION_CD' => 'DD',
            'CLASS_SECTION' => 10,
            'NOTES_SEQ_NBR' => 1,
            'NOTES_NOTE_NBR' => 30,
            'NOTES_TYPE_PUBL_STAF' => 'Public',
            'NOTES_DESCR' => 'Class Note 30',
            'NOTES_NOTE_TEXT' => 'Text for Class Note 30',
            'CLASS_INSTITUTION_CD' => 'COLL01',
            ...$attributes,
        ]);
    }
}
