<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Incremental;

use Smorken\Sis\Contracts\Base\Note;
use Smorken\Sis\Db\Core\Klass\Klass;
use Smorken\Sis\Db\Incremental\Course\CourseNote;

class CourseNoteModels
{
    public static function fromClass(Klass $class): Note
    {
        return self::get([
            'CLASS_COURSE_ID' => $class->CLASS_COURSE_ID,
            'CLASS_COURSE_OFFER_NBR' => $class->CLASS_COURSE_OFFER_NBR,
            'CLASS_TERM_CD' => $class->CLASS_TERM_CD,
        ]);
    }

    public static function get(array $attributes = []): Note
    {
        return (new CourseNote)->forceFill([
            'CLASS_COURSE_ID' => 100000,
            'CLASS_COURSE_OFFER_NBR' => 1,
            'CLASS_TERM_CD' => 1232,
            'NOTES_SEQ_NBR' => 1,
            'NOTES_NOTE_NBR' => 66,
            'NOTES_TYPE_PUBL_STAF' => 'Public',
            'NOTES_DESCR' => 'Course Note 66',
            'NOTES_NOTE_TEXT' => 'Text for Course Note 66',
            'CLASS_INSTITUTION_CD' => 'COLL01',
        ]);
    }
}
