<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Incremental;

use Smorken\Sis\Contracts\Base\Klass\ClassInstructor;
use Smorken\Sis\Contracts\Base\Klass\Meeting;
use Smorken\Sis\Contracts\Base\Person\Person;

class ClassInstructorModels
{
    public static function get(array $attributes = []): ClassInstructor
    {
        return (new \Smorken\Sis\Db\Incremental\Klass\ClassInstructor)->forceFill([
            'CLASS_COURSE_ID' => 100000,
            'CLASS_COURSE_OFFER_NBR' => 1,
            'CLASS_SECTION' => 10,
            'CLASS_SESSION_CD' => 'DD',
            'CLASS_TERM_CD' => '1234',
            'CLASS_INSTITUTION_CD' => 'COLL01',
            'CLASSM_ASSIGN_TYPE' => '',
            'CLASSM_CLASS_MTG_NBR' => 1,
            'CLASSM_INSTR_ASSIGN_SEQ' => 1,
            'CLASSM_INSTR_LOAD_FACTOR' => 0.0,
            'CLASSM_WEEK_WORKLOAD_HRS' => 0.0,
            'CLASSM_INSTR_NAME_FLAST' => 'E. Simonis',
            'CLASSM_INSTR_TYPE' => 'ADJ',
            'CLASSM_INSTRUCTOR_EMPLID' => 30000000,
            'CLASSM_INSTRUCTOR_NAME' => 'Emie Simonis',
            'CLASSM_INSTRUCTOR_ROLE' => 'SI',
            ...$attributes,
        ]);
    }

    public static function fromMeeting(Meeting $meeting, array $attributes = []): ClassInstructor
    {
        return self::get([
            'CLASS_COURSE_ID' => $meeting->CLASS_COURSE_ID,
            'CLASS_COURSE_OFFER_NBR' => $meeting->CLASS_COURSE_OFFER_NBR,
            'CLASS_SECTION' => $meeting->CLASS_SECTION,
            'CLASS_SESSION_CD' => $meeting->CLASS_SESSION_CD,
            'CLASS_TERM_CD' => $meeting->CLASS_TERM_CD,
            'CLASSM_CLASS_MTG_NBR' => $meeting->CLASSM_CLASS_MTG_NBR,
            ...$attributes,
        ]);
    }

    public static function fromPerson(Person $person, array $attributes = []): ClassInstructor
    {
        return self::get([
            'CLASSM_INSTRUCTOR_EMPLID' => $person->PERS_EMPLID ?? $person->EMPLID,
            'CLASSM_INSTRUCTOR_NAME' => $person->getFirstName().' '.$person->getLastName(),
            ...$attributes,
        ]);
    }
}
