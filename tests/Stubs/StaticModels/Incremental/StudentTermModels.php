<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Incremental;

use Smorken\Sis\Contracts\Base\Student\StudentTerm;

class StudentTermModels
{
    public static function get(array $attributes = []): StudentTerm
    {
        return (new \Smorken\Sis\Db\Incremental\Student\StudentTerm)->forceFill([
            'CTERM_EMPLID' => 30000000,
            'CTERM_TERM_CD' => 232,
            'CTERM_INSTITUTION_CODE' => 'COLL01',
            'CTERM_ACAD_CAREER' => 'CRED',
            'STFACT_MCCD_TERM_AHRS' => 12.0,
            'STFACT_MCCD_TERM_EHRS' => 3.0,
            'STFACT_MCCD_TERM_QHRS' => 4.0,
            'STFACT_MCCD_TERM_QPTS' => 1.93,
            'STFACT_MCCD_TERM_CLOCK_HRS' => 12.0,
            'STFACT_MCCD_CUM_AHRS' => 23.0,
            'STFACT_MCCD_CUM_EHRS' => 48.0,
            'STFACT_MCCD_CUM_QHRS' => 58.0,
            'STFACT_MCCD_CUM_QPTS' => 2.49,
            'STFACT_MCCD_CUM_CLOCK_HRS' => 23.0,
            'CTERM_ACAD_STNDNG_ACTN' => 'ACPT',
            'CTERM_ACAD_LOAD_CODE' => 'F',
        ]);
    }
}
