<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Incremental;

use Smorken\Sis\Contracts\Base\Klass\Klass;
use Smorken\Sis\Contracts\Base\Klass\Meeting;

class MeetingModels
{
    public static function fromClass(Klass $class): Meeting
    {
        return self::get([
            'CLASS_COURSE_ID' => $class->CLASS_COURSE_ID,
            'CLASS_COURSE_OFFER_NBR' => $class->CLASS_COURSE_OFFER_NBR,
            'CLASS_SECTION' => $class->CLASS_SECTION,
            'CLASS_SESSION_CD' => $class->CLASS_SESSION_CD,
            'CLASS_TERM_CD' => $class->CLASS_TERM_CD,
        ]);
    }

    public static function get(array $attributes = []): Meeting
    {
        return (new \Smorken\Sis\Db\Incremental\Klass\Meeting)->forceFill([
            'CLASS_COURSE_ID' => 100000,
            'CLASS_COURSE_OFFER_NBR' => 1,
            'CLASS_SECTION' => 10,
            'CLASS_SESSION_CD' => 'DD',
            'CLASS_TERM_CD' => 1232,
            'CLASSM_BLDG_CD' => 'C BLD',
            'CLASSM_CLASS_MTG_NBR' => 1,
            'CLASSM_DAYS_PATTERN' => 'MWF',
            'CLASSM_END_DATE' => '2023-04-08',
            'CLASSM_FACILITY_ID' => 'C_FAC_ID',
            'CLASSM_LOCATION' => 'COLL01',
            'CLASSM_MEETING_TIME_END' => '2023-03-25T09:00:00.000000Z',
            'CLASSM_MEETING_TIME_START' => '2023-03-25T09:50:00.000000Z',
            'CLASSM_ROOM' => 303,
            'CLASSM_START_DATE' => '2023-03-14',
            'CLASSM_COURSE_TOPIC_DESCR' => '',
            'CLASS_INSTITUTION_CD' => 'COLL01',
            ...$attributes,
        ]);
    }
}
