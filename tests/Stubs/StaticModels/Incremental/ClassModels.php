<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Incremental;

use Smorken\Sis\Contracts\Base\Klass\Klass;
use Smorken\Sis\Enums\Klass\EnrollmentStatuses;

class ClassModels
{
    public static function get(array $attributes = []): Klass
    {
        return (new \Smorken\Sis\Db\Incremental\Klass\Klass)->forceFill([
            'CASSC_GRADING_BASIS' => '',
            'CASSC_HONORS_YN' => false,
            'CASSC_UNITS_MINIMUM' => 3.0,
            'CLASS_ACAD_CAREER' => 'CRED',
            'CLASS_ACAD_ORG' => 'AO-1',
            'CLASS_ACAD_ORG_LDESC' => 'Org 1',
            'CLASS_ASSOCIATED_CLASS' => '9999',
            'CLASS_CAMPUS_CD' => 'CAMP01',
            'CLASS_CANCEL_DATE' => null,
            'CLASS_CATALOG_NBR' => ' 101',
            'CLASS_CLASS_NAME' => 'ABC101',
            'CLASS_CLASS_NBR' => 10000,
            'CLASS_CLASS_STAT' => 'A',
            'CLASS_COMBINED_MAJOR_YN' => false,
            'CLASS_COMPONENT_CD' => 'LEC',
            'CLASS_COURSE_ID' => 100000,
            'CLASS_COURSE_OFFER_NBR' => 1,
            'CLASS_DAY_EVE' => 'D',
            'CLASS_DESCR' => 'ABC101 Class',
            'CLASS_END_DATE' => '2023-04-24T00:00:00.000000Z',
            'CLASS_ENRL_CAP' => 30,
            'CLASS_ENRL_STAT' => EnrollmentStatuses::OPEN,
            'CLASS_ENRL_TOT' => 15,
            'CLASS_INSTITUTION_CD' => 'COLL01',
            'CLASS_INSTRUCTION_MODE' => 'P',
            'CLASS_LOCATION_CD' => 'PC Main',
            'CLASS_SECTION' => 10,
            'CLASS_SESSION_CD' => 'DD',
            'CLASS_START_DATE' => '2023-03-17T00:00:00.000000Z',
            'CLASS_SUBJECT_CD' => 'ABC',
            'CLASS_TERM_CD' => 1232,
            'CLASS_TYPE_CD' => 'E',
            ...$attributes,
        ]);
    }
}
