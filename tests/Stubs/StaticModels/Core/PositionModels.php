<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Instructor\Position;

class PositionModels
{
    public static function get(array $attributes = []): Position
    {
        return (new \Smorken\Sis\Db\Core\Instructor\Position)->forceFill([
            'EMPLID' => 30000000,
            'EMPL_RCD' => 3,
            'DEPTID' => 141,
            'JOBCODE' => 'JOB39',
            'POSITION_NBR' => 5,
            'DESCR' => 'et similique maxime',
            ...$attributes,
        ]);
    }
}
