<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Student\Group;

class GroupModels
{
    public static function get(array $attributes = []): Group
    {
        return (new \Smorken\Sis\Db\Core\Student\Group)->forceFill([
            'STDGR_INSTITUTION' => 'COLL01',
            'STDGR_EMPLID' => 30000000,
            'STDGR_STUDENT_GROUP' => 'OHKQZ',
            'STDGR_STDNT_GROUP_LDESC' => 'Group description for OHKQZ',
            'STDGR_START_DATE' => '2022-08-27T00:00:00.000000Z',
            'STDGR_END_DATE' => '1986-08-12T00:00:00.000000Z',
            'STDGR_STUDENT_STATUS' => 'A',
            'STDGR_STUDENT_STATUS_LDESC' => 'Active',
            'STDGR_COMMENTS' => 'reprehenderit maiores ut quia quasi',
            ...$attributes,
        ]);
    }
}
