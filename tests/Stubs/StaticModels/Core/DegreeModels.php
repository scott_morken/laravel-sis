<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Student\Degree;

class DegreeModels
{
    public static function get(array $attributes = []): Degree
    {
        return (new \Smorken\Sis\Db\Core\Student\Degree)->forceFill([
            'INSTITUTION' => 'COLL01',
            'ACAD_PROG' => 'CRED',
            'ACAD_PLAN_TYPE' => 'DEG',
            'ACAD_PLAN' => 2707,
            'EFFDT' => '2021-10-12',
            'EFF_STATUS' => 'A',
            'FIRST_TERM_VALID' => 1012,
            'DESCR' => 'rerum ducimus distinctio',
            'DEGREE' => 'AAS',
            'DEGREE_DESCR' => 'Associate in Applied Science',
            'DIPLOMA_DESCR' => 'architecto quas quod',
            'TRNSCR_DESCR' => 'eius a nihil',
            'MIN_UNITS_REQD' => 60,
            'PLAN_DESCRIPTION' => 'Ea repudiandae assumenda',
            ...$attributes,
        ]);
    }
}
