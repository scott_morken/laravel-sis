<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Course\CourseOffer;

class CourseOfferModels
{
    public static function get(array $attributes = []): CourseOffer
    {
        return (new \Smorken\Sis\Db\Core\Course\CourseOffer)->forceFill([
            'COFFR_ACAD_CAREER' => 'CRED',
            'COFFR_ACAD_GROUP' => 'AO',
            'COFFR_ACAD_ORG' => 'AO-1',
            'COFFR_CAMPUS' => 'CAMP01',
            'COFFR_CATALOG_NBR' => '101',
            'COFFR_CATALOG_PRINT' => 'Y',
            'COFFR_CLASS_SID' => '1',
            'COFFR_COURSE_APPROVED' => 'A',
            'COFFR_CRSE_EFFDT' => '2022-03-25',
            'COFFR_CRSE_ID' => 100000,
            'COFFR_CRSE_OFFER_NBR' => 1,
            'COFFR_INSTITUTION' => 'COLL01',
            'COFFR_REC_KEY' => '100000+1+2022-03-25',
            'COFFR_SCHEDULE_PRINT' => 'Y',
            'COFFR_SUBJECT' => 'ABC',
            'CRSE_CATALOG_SID' => 5286,
            ...$attributes,
        ]);
    }
}
