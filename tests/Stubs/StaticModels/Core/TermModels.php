<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Term;

class TermModels
{
    public static function get(array $attributes = []): Term
    {
        return (new \Smorken\Sis\Db\Core\Term)->forceFill([
            'STRM' => 'T1234',
            'DESCR' => 'Term 1234',
            'ACAD_CAREER' => 'CRED',
            'TERM_BEGIN_DT' => '2022-01-01',
            'TERM_END_DT' => '2022-05-01',
            'INSTITUTION' => 'COLL01',
            ...$attributes,
        ]);
    }
}
