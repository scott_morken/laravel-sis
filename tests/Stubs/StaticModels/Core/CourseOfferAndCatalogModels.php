<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Course\CourseOffer;

class CourseOfferAndCatalogModels
{
    public static function get(array $attributes = []): CourseOffer
    {
        $m = (new \Smorken\Sis\Db\Core\Course\CourseOffer)->forceFill([
            'COFFR_ACAD_CAREER' => 'CRED',
            'COFFR_ACAD_GROUP' => 'AO',
            'COFFR_ACAD_ORG' => 'AO-1',
            'COFFR_CAMPUS' => 'CAMP01',
            'COFFR_CATALOG_NBR' => '101',
            'COFFR_CATALOG_PRINT' => 'Y',
            'COFFR_CLASS_SID' => '1',
            'COFFR_COURSE_APPROVED' => 'A',
            'COFFR_CRSE_EFFDT' => '2022-03-25',
            'COFFR_CRSE_ID' => 100000,
            'COFFR_CRSE_OFFER_NBR' => 1,
            'COFFR_INSTITUTION' => 'COLL01',
            'COFFR_REC_KEY' => '100000+1+2022-03-25',
            'COFFR_SCHEDULE_PRINT' => 'Y',
            'COFFR_SUBJECT' => 'ABC',
            'CRSE_CATALOG_SID' => 5286,
            ...$attributes,
        ]);
        $cat = CourseCatalogModels::fromCourseOffer($m);
        $m->setRelation('catalog', $cat);

        return $m;
    }

    public static function getAsJoin(array $attributes = []): CourseOffer
    {
        return (new \Smorken\Sis\Db\Core\Course\CourseOffer)->forceFill([
            'COFFR_ACAD_CAREER' => 'CRED',
            'COFFR_ACAD_GROUP' => 'AO',
            'COFFR_ACAD_ORG' => 'AO-1',
            'COFFR_CAMPUS' => 'CAMP01',
            'COFFR_CATALOG_NBR' => '101',
            'COFFR_CATALOG_PRINT' => 'Y',
            'COFFR_CLASS_SID' => '1',
            'COFFR_COURSE_APPROVED' => 'A',
            'COFFR_CRSE_EFFDT' => '2022-03-25',
            'COFFR_CRSE_ID' => 100000,
            'COFFR_CRSE_OFFER_NBR' => 1,
            'COFFR_INSTITUTION' => 'COLL01',
            'COFFR_REC_KEY' => '100000+1+2022-03-25',
            'COFFR_SCHEDULE_PRINT' => 'Y',
            'COFFR_SUBJECT' => 'ABC',
            'CRSE_CATALOG_SID' => 5286,
            'CRSE_REC_KEY' => '100000+1+2022-03-25',
            'CRSE_CRSE_ID' => 100000,
            'CRSE_DESCR' => 'Course 100000',
            'CRSE_CONSENT' => 'N',
            'CRSE_UNITS_MINIMUM' => 3.0,
            'CRSE_UNITS_MAXIMUM' => 3.0,
            'CRSE_GRADING_BASIS' => 'GRD',
            'CRSE_COMPONENT' => 'LEC',
            'CRSE_COURSE_NOTES' => '',
            'CRSE_GOV_BD_APPROVAL_DATE' => '2022-03-25',
            'CRSE_EFFDT' => '2022-03-25',
            ...$attributes,
        ]);
    }
}
