<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Student\ServiceIndicator;

class ServiceIndicatorModels
{
    public static function get(array $attributes = []): ServiceIndicator
    {
        return (new \Smorken\Sis\Db\Core\Student\ServiceIndicator)->forceFill([
            'SRVC_EMPLID' => 30000000,
            'SRVC_IND_DATETIME' => '2022-10-14T04:52:12.000000Z',
            'SRVC_OPRID' => 'HCS0000000',
            'SRVC_INSTITUTION' => 'COLL01',
            'SRVC_SERVICE_IND_CD' => 'ADM',
            'SRVC_SERVICE_IND_LDESC' => 'Administrative Hold',
            'SRVC_SERVICE_IND_REASON' => 'RADM',
            'SRVC_SERVICE_IND_REASON_LDESC' => 'R: Administrative Hold',
            'SRVC_SERVICE_IND_ACT_TERM' => 1232,
            'SRVC_SERVICE_IND_ACTIVE_DT' => '2023-01-04',
            'SRVC_POSITIVE_SRVC_INDICATOR' => 'N',
            'SRVC_AMOUNT' => 91.68,
            'SRVC_COMM_COMMENTS' => 'enim illum minima animi dolorem',
            'SRVC_CONTACT' => 'Phyllis Olson',
            'SRVC_DEPARTMENT' => 'D800',
            'SRVC_DEPARTMENT_LDESC' => 'COLL01 Department',
            ...$attributes,
        ]);
    }
}
