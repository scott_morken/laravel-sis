<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Course\CourseCatalog;
use Smorken\Sis\Contracts\Base\Course\CourseOffer;

class CourseCatalogModels
{
    public static function fromCourseOffer(CourseOffer $offer): CourseCatalog
    {
        return self::get([
            'CRSE_REC_KEY' => $offer->COFFR_REC_KEY,
            'CRSE_CATALOG_SID' => $offer->CRSE_CATALOG_SID,
            'CRSE_CRSE_ID' => $offer->COFFR_CRSE_ID,
            'CRSE_EFFDT' => $offer->COFFR_CRSE_EFFDT,
        ]);
    }

    public static function get(array $attributes = []): CourseCatalog
    {
        return (new \Smorken\Sis\Db\Core\Course\CourseCatalog)->forceFill([
            'CRSE_REC_KEY' => '100000+1+2022-03-25',
            'CRSE_CATALOG_SID' => 1,
            'CRSE_CRSE_ID' => 100000,
            'CRSE_DESCR' => 'Course 100000',
            'CRSE_CONSENT' => 'N',
            'CRSE_UNITS_MINIMUM' => 3.0,
            'CRSE_UNITS_MAXIMUM' => 3.0,
            'CRSE_GRADING_BASIS' => 'GRD',
            'CRSE_COMPONENT' => 'LEC',
            'CRSE_COURSE_NOTES' => '',
            'CRSE_GOV_BD_APPROVAL_DATE' => '2022-03-25',
            'CRSE_EFFDT' => '2022-03-25',
            ...$attributes,
        ]);
    }
}
