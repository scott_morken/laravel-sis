<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Klass\Klass;
use Smorken\Sis\Contracts\Base\Person\Person;
use Smorken\Sis\Contracts\Base\Student\Enrollment;
use Smorken\Sis\Enums\Shared\GradingBases;

class EnrollmentModels
{
    public static function fromClass(Klass $class, array $attributes = []): Enrollment
    {
        return self::get([
            'ENRL_TERM_CD' => $class->CLASS_TERM_CD,
            'ENRL_CLASS_NBR' => $class->CLASS_CLASS_NBR,
            ...$attributes,
        ]);
    }

    public static function fromPerson(Person $person, array $attributes = []): Enrollment
    {
        return self::get([
            'EMPLID' => $person->PERS_EMPLID ?? $person->EMPLID,
            ...$attributes,
        ]);
    }

    public static function get(array $attributes = []): Enrollment
    {
        return (new \Smorken\Sis\Db\Core\Student\Enrollment)->forceFill([
            'CLASS_SID' => 175848596,
            'EMPLID' => 30000000,
            'ENRL_INSTITUTION_CD' => 'COLL01',
            'ENRL_TERM_CD' => 1232,
            'ENRL_CLASS_NBR' => 10000,
            'ENRL_ACAD_CAREER' => 'CRED',
            'ENRL_GRADING_BASIS' => GradingBases::GRADED,
            'ENRL_GRADING_BASIS_LDESC' => GradingBases::toArray()[GradingBases::GRADED],
            'ENRL_OFFICIAL_GRADE' => 'A',
            'ENRL_GRADE_DATE' => null,
            'ENRL_GRADE_POINTS' => 4.0,
            'ENRL_UNITS_TAKEN' => 3.0,
            'ENRL_EARNED_CREDIT' => 'Y',
            'ENRL_STATUS_REASON_CD' => 'ENRL',
            'ENRL_STATUS_REASON_LDESC' => 'Enrolled',
            'ENRL_ACTN_RSN_LAST_CD' => null,
            'ENRL_ACTN_RSN_LAST_LDESC' => null,
            'ENRL_INCLUDE_IN_GPA' => 'Y',
            'ENRL_OEE_START_DATE' => '2023-03-25',
            'ENRL_OEE_END_DATE' => '2023-03-25',
            ...$attributes,
        ]);
    }
}
