<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Person\Email;
use Smorken\Sis\Contracts\Base\Person\Person;

class EmailModels
{
    public static function fromPerson(Person $person): Email
    {
        $email = $person->PERS_OPRID.'@email.edu';

        return self::get([
            'EMAIL_REC_KEY' => '1-'.$email,
            'EMAIL_EMPLID' => $person->PERS_EMPLID ?? $person->EMPLID,
            'EMAIL_EMAIL_ADDR' => $email,
        ]);
    }

    public static function get(array $attributes = []): Email
    {
        return (new \Smorken\Sis\Db\Core\Person\Email)->forceFill([
            'EMAIL_REC_KEY' => '1-ABC1234567@email.edu',
            'EMAIL_EMPLID' => 30000000,
            'EMAIL_ADDR_TYPE' => 'MCCD',
            'EMAIL_ADDR_TYPE_LDESC' => 'MCCD Official',
            'EMAIL_PREF_EMAIL_FLAG' => 'Y',
            'EMAIL_EMAIL_ADDR' => 'ABC1234567@email.edu',
        ]);
    }
}
