<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Fee;

class FeeModels
{
    public static function get(array $attributes = []): Fee
    {
        return (new \Smorken\Sis\Db\Core\Klass\Fee)->forceFill([
            'CLSF_SID' => 51655,
            'CLASS_SID' => '123210000',
            'CLSBF_SETID' => 'COLL01',
            'CLSBF_CRSE_ID' => 100000,
            'CLSBF_CRSE_OFFER_NBR' => 1,
            'CLSBF_STRM' => 1232,
            'CLSBF_SESSION_CODE' => 'DD',
            'CLSBF_CLASS_SECTION' => 10,
            'CLSBF_COMPONENT' => 'LEC',
            'CLSBF_ACCOUNT_TYPE_SF' => 'ACT',
            'CLSBF_ITEM_TYPE' => '10000001111111',
            'CLSBF_ITEM_TYPE_SDESC' => 16933502,
            'CLSBF_ITEM_TYPE_LDESC' => 'Fee for 16933502',
            'CLSBF_FLAT_AMT' => '84.00',
            'CLSBF_CURRENCY_CD' => 'USD',
            ...$attributes,
        ]);
    }
}
