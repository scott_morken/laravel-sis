<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Person\Person;
use Smorken\Sis\Contracts\Base\Student\Athlete;

class AthleteModels
{
    public static function get(array $attributes = []): Athlete
    {
        return (new \Smorken\Sis\Db\Core\Student\Athlete)->forceFill([
            'ATHL_INSTITUTION' => 'COLL01',
            'ATHL_EMPLID' => 30000000,
            'ATHL_SPORT' => 'BSK',
            'ATHL_SPORT_LDESC' => 'Dicta in soluta similique aut.',
            'ATHL_ATHLETIC_PARTIC_CD' => 'ACTIV',
            'ATHL_ATHLETIC_PARTIC_LDESC' => 'Active',
            'ATHL_NCAA_ELIGIBLE' => 'Y',
            'ATHL_CURRENT_PARTICIPANT' => 'Y',
            'ATHL_START_DATE' => '2023-02-23',
            'ATHL_END_DATE' => null,
            ...$attributes,
        ]);
    }

    public static function fromPerson(Person $person): Athlete
    {
        return self::get([
            'ATHL_EMPLID' => $person->PERS_EMPLID ?? $person->EMPLID,
        ]);
    }
}
