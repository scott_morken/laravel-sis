<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Person\Email;
use Smorken\Sis\Contracts\Base\Person\Person;
use Smorken\Sis\Oci\Constants\Person\AddressType;

class AddressModels
{
    public static function fromPerson(Person $person): Email
    {
        $email = $person->PERS_OPRID.'@email.edu';

        return self::get([
            'ADDR_REC_KEY' => '1-'.$email,
            'ADDR_EMPLID' => $person->PERS_EMPLID ?? $person->EMPLID,
            'ADDR_EMAIL_ADDR' => $email,
        ]);
    }

    public static function get(array $attributes = []): Email
    {
        return (new \Smorken\Sis\Db\Core\Person\Email)->forceFill([
            'ADDR_REC_KEY' => '1-30000000',
            'ADDR_EMPLID' => 30000000,
            'ADDR_ADDRESS_TYPE' => AddressType::HOME,
        ]);
    }
}
