<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Physical\Facility;

class FacilityModels
{
    public static function get(array $attributes = []): Facility
    {
        return (new \Smorken\Sis\Db\Core\Physical\Facility)->forceFill([
            'FACILITY_SETID' => 'PCC01',
            'FACILITY_ID' => 'N207',
            'FACILITY_EFF_STATUS' => 'A',
            'FACILITY_BLDG_CD' => 'A N BLDG',
            'FACILITY_ROOM' => 207,
            'FACILITY_DESCR' => 'N207 Room',
            'FACILITY_DESCRSHORT' => 'N207',
            'FACILITY_GROUP' => 'CLAS',
            'FACILITY_LOCATION' => 'PC MAIN',
            'FACILITY_CAPACITY' => 25,
            'FACILITY_ACAD_ORG' => 'AO-1',
            ...$attributes,
        ]);
    }
}
