<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Student\StudentTerm;

class StudentTermModels
{
    public static function get(array $attributes = []): StudentTerm
    {
        return (new \Smorken\Sis\Db\Core\Student\StudentTerm)->forceFill([
            'EMPLID' => 30000000,
            'CTERM_TERM_CD' => 1232,
            'CTERM_TERM_SDESC' => 'Term 1232',
            'CTERM_INSTITUTION_CODE' => 'COLL01',
            'CTERM_ACAD_CAREER' => 'CRED',
            'STFACT_CUM_GPA' => 2.71,
            'STFACT_TERM_GPA' => 1.57,
            'STFACT_MCCD_CUM_AHRS' => 34.0,
            'STFACT_MCCD_CUM_EHRS' => 11.0,
            'STFACT_MCCD_CUM_QHRS' => 22.0,
            'STFACT_MCCD_CUM_QPTS' => 1.48,
            'STFACT_MCCD_CUM_CLOCK_HRS' => 52.48,
            'STFACT_MCCD_TERM_AHRS' => 13.0,
            'STFACT_MCCD_TERM_EHRS' => 8.0,
            'STFACT_MCCD_TERM_QHRS' => 7.0,
            'STFACT_MCCD_TERM_QPTS' => 3.83,
            'STFACT_MCCD_TERM_CLOCK_HRS' => 12.52,
            'CTERM_ACAD_STNDNG_ACTN' => 'ACPT',
            'CTERM_ACAD_STDNG_LDESC' => 'Acceptable',
            'CTERM_ACAD_LOAD_CODE' => 'F',
            'CTERM_ACAD_LOAD_LDESC' => 'Enrolled Full-Time',
            'STFACT_TOT_INPROG_GPA' => 2.9,
            'STFACT_TOT_INPROG_NOGPA' => 1.1,
            ...$attributes,
        ]);
    }
}
