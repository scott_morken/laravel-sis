<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Klass\Material;

class MaterialModels
{
    public static function get(array $attributes = []): Material
    {
        return (new \Smorken\Sis\Db\Core\Klass\Material)->forceFill([
            'TBMAT_SID' => '123210000',
            'TBMAT_SEQ_NBR' => 1,
            'TBMAT_TYPE_CD' => 'TEXTBOOK',
            'TBMAT_TYPE_CD_LDESC' => 'Textbook',
            'TBMAT_STATUS_CD' => 'REQ',
            'TBMAT_STATUS_LDESC' => 'Required',
            'TBMAT_TITLE' => 'placeat vitae aliquam',
            'TBMAT_ISBN_NBR' => '9793808992950',
            'TBMAT_AUTHOR' => 'Gonzalo Zulauf',
            'TBMAT_PUBLISHER' => 'Considine-Conroy',
            'TBMAT_EDITION' => 6,
            'TBMAT_YEAR_PUBLISHED' => '1977',
            'TBMAT_PRICE' => 14.14,
            'TBMAT_NOTES' => 'excepturi iste cupiditate',
            ...$attributes,
        ]);
    }
}
