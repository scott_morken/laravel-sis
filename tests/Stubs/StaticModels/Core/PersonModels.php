<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Person\Person;

class PersonModels
{
    public static function get(array $attributes = []): Person
    {
        return (new \Smorken\Sis\Db\Core\Person\Person)->forceFill([
            'EMPLID' => 30000000,
            'PERS_OPRID' => '2QDGF00000',
            'PERS_PREFERRED_FIRST_NAME' => '',
            'PERS_PREFERRED_LAST_NAME' => '',
            'PERS_PRIMARY_FIRST_NAME' => 'Louisa',
            'PERS_PRIMARY_LAST_NAME' => 'Wuckert',
            'EMAIL_EMAIL_ADDR' => 'xlowe@example.org',
            'PERS_HOME_PHONE_NBR' => '+1.847.640.3470',
            'PERS_BIRTHDATE' => '1987-04-01',
            'HRMS_EMPLID' => 10000000,
            ...$attributes,
        ]);
    }
}
