<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Student\Program;

class ProgramModels
{
    public static function get(array $attributes = []): Program
    {
        return (new \Smorken\Sis\Db\Core\Student\Program)->forceFill([
            'STRUC_EMPLID' => 30000000,
            'STRUC_INSTITUTION' => 'COLL01',
            'STRUC_ACAD_PROG_LDESC' => 'Degree and Cert Programs',
            'STRUC_ACAD_PROG_STATUS' => 'AC',
            'STRUC_ACAD_PROG_STATUS_LDESC' => 'Active',
            'STRUC_PROG_REASON_LDESC' => 'Active in Program',
            'STRUC_PROG_ADMIT_TERM' => 232,
            'STRUC_ACAD_PLAN_LDESC' => 'Associate in FooBar',
            'STRUC_PLAN_TYPE_LDESC' => 'Degree',
            'STRUC_ACAD_PLAN_DEGREE' => 'AAS',
            'STRUC_EXP_GRAD_TERM' => 4232,
            'STRUC_ACAD_INTEREST' => null,
            'STRUC_ACAD_INTEREST_LDESC' => null,
            'STRUC_DEGR_CHECKOUT_STAT' => 'AW',
            'STRUC_DEGR_CHECKOUT_LDESC' => 'Awarded',
            ...$attributes,
        ]);
    }
}
