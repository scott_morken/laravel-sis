<?php

namespace Tests\Smorken\Sis\Stubs\StaticModels\Core;

use Smorken\Sis\Contracts\Base\Klass\Klass;
use Smorken\Sis\Enums\Klass\EnrollmentStatuses;
use Smorken\Sis\Enums\Klass\SessionCodes;
use Smorken\Sis\Enums\Klass\TypeCodes;
use Smorken\Sis\Enums\Shared\GradingBases;

class ClassModels
{
    public static function get(array $attributes = []): Klass
    {
        return (new \Smorken\Sis\Db\Core\Klass\Klass)->forceFill([
            'CASSC_GRADING_BASIS' => GradingBases::PASS_FAIL,
            'CASSC_GRADING_BASIS_LDESC' => GradingBases::toArray()[GradingBases::PASS_FAIL],
            'CASSC_HONORS_YN' => 'N',
            'CASSC_UNITS_MINIMUM' => 3.0,
            'CLASS_ACAD_CAREER' => 'CRED',
            'CLASS_ACAD_ORG' => 'AO-1',
            'CLASS_ACAD_ORG_LDESC' => 'Org 1',
            'CLASS_ASSOCIATED_CLASS' => '9999',
            'CLASS_CAMPUS_CD' => 'CAMP01',
            'CLASS_CAMPUS_LDESC' => 'Campus 1',
            'CLASS_CANCEL_DATE' => null,
            'CLASS_CATALOG_NBR' => '101',
            'CLASS_CLASS_NAME' => 'ABC101',
            'CLASS_CLASS_NBR' => 10000,
            'CLASS_CLASS_ROLL_YN' => 'Y',
            'CLASS_CLASS_STAT' => 'A',
            'CLASS_CLASS_STAT_LDESC' => 'Active',
            'CLASS_COMBINED_ID' => '0010',
            'CLASS_COMBINED_DESCR' => 'ABC102-11111, 103',
            'CLASS_COMBINED_MAJOR_YN' => 'N',
            'CLASS_COMBINED_SECTION' => 'N',
            'CLASS_COMPONENT_CD' => 'LEC',
            'CLASS_COMPONENT_LDESC' => 'Lecture',
            'CLASS_CONSENT' => 'N',
            'CLASS_CONSENT_LDESC' => 'No Special Consent Required',
            'CLASS_COURSE_ID' => 100000,
            'CLASS_COURSE_OFFER_NBR' => 1,
            'CLASS_DAY_EVE' => 'D',
            'CLASS_DAY_EVE_LDESC' => 'Day',
            'CLASS_DEPTID_EXPENSE_CD' => '100-12345',
            'CLASS_DESCR' => 'ABC101 Class',
            'CLASS_ENRL_CAP' => 30,
            'CLASS_ENRL_STAT' => EnrollmentStatuses::OPEN,
            'CLASS_ENRL_STAT_LDESC' => EnrollmentStatuses::toArray()[EnrollmentStatuses::OPEN],
            'CLASS_ENRL_TOT' => 15,
            'CLASS_HS_DUAL_ENROLL_YN' => 'N',
            'CLASS_HS_DUAL_SPONSOR' => '',
            'CLASS_HIGH_SCHOOL_LDESC' => '',
            'CLASS_HIGH_SCHOOL_SDESC' => '',
            'CLASS_HONORS' => 'N',
            'CLASS_INSTITUTION_CD' => 'COLL01',
            'CLASS_INSTRUCTION_MODE' => 'P',
            'CLASS_INSTRUCTION_MODE_LDESC' => 'In Person',
            'CLASS_INSTRUCTOR_EMPLID' => 30000000,
            'CLASS_INSTRUCTOR_NAME' => 'Last,First M',
            'CLASS_LOAD_CALC_CODE' => 'S',
            'CLASS_LOAD_CALC_SDESC' => 'Standard',
            'CLASS_LOCATION_CD' => 'PC Main',
            'CLASS_LOCATION_LDESC' => 'Phoenix College',
            'CLASS_MAJOR_CLASS_NAME' => 'ABC101',
            'CLASS_MAJOR_CLASS_NBR' => '12344',
            'CLASS_SCHEDULE_PRINT' => 'Y',
            'CLASS_SECTION' => 10,
            'CLASS_SESSION_CD' => SessionCodes::DYNAMIC_DATED,
            'CLASS_SESSION_LDESC' => SessionCodes::toArray()[SessionCodes::DYNAMIC_DATED],
            'CLASS_SID' => '123210000',
            'CLASS_SUBJECT_CD' => 'ABC',
            'CLASS_TERM_CD' => 1232,
            'CLASS_TYPE_CD' => TypeCodes::E,
            'CLASS_TYPE_LDESC' => TypeCodes::toArray()[TypeCodes::E],
            'COFFR_CLASS_SID' => 7279,
            'COMP_WEEK_WORKLOAD_HRS' => 2.4,
            'CLASS_TOT_WORKLOAD_HRS' => 2.4,
            'CLASS_START_DATE' => '2023-03-17T00:00:00.000000Z',
            'CLASS_END_DATE' => '2023-04-24T00:00:00.000000Z',
            ...$attributes,
        ]);
    }
}
