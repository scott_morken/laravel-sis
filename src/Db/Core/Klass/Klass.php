<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:03 PM
 */

namespace Smorken\Sis\Db\Core\Klass;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\ClassAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Core\ClassModifiers;
use Smorken\Sis\Builders\Core\ClassBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasClassRelations;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Db\Core\Student\Enrollment;
use Smorken\Sis\Db\Core\Term;
use Smorken\Sis\Db\Incremental\Course\CourseNote;
use Smorken\Sis\Db\Incremental\Klass\ClassInstructor;
use Smorken\Sis\Db\Incremental\Klass\ClassNote;
use Smorken\Sis\Db\Incremental\Klass\Meeting;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new ClassModifiers), MapAttributeNames(new ClassAttributeMap)]
class Klass extends Base implements \Smorken\Sis\Contracts\Base\Klass\Klass
{
    use Compoships, HasAcademicOrgIdAttribute, HasClassRelations, HasCollegeIdAttribute, HasStartEndDateAttributes, HasTermIdAttribute;

    protected array $attributeNames = [
        'academicOrgId' => 'CLASS_ACAD_ORG',
        'collegeId' => 'CLASS_INSTITUTION_CD',
        'endDate' => 'CLASS_END_DATE',
        'startDate' => 'CLASS_START_DATE',
        'termId' => 'CLASS_TERM_CD',
    ];

    protected array $addAttributeMutatedAttributes = ['major_class', 'combined_classes_string'];

    protected static string $builder = ClassBuilder::class;

    protected $casts = [
        'CLASS_END_DATE' => 'date',
        'CLASS_START_DATE' => 'date',
        'CLASS_CANCEL_DATE' => 'date',
        'CLASS_ENRL_CAP' => 'integer',
        'CLASS_ENRL_TOT' => 'integer',
        'CASSC_UNITS_MINIMUM' => 'float',
        'COMP_WEEK_WORKLOAD_HRS' => 'float',
        'CLASS_TOT_WORKLOAD_HRS' => 'float',
    ];

    protected ?Collection $combinedSections = null;

    protected array $defaultColumns = [
        'CASSC_GRADING_BASIS',
        'CASSC_GRADING_BASIS_LDESC',
        'CASSC_HONORS_YN',
        'CASSC_UNITS_MINIMUM',
        'CLASS_ACAD_CAREER',
        'CLASS_ACAD_ORG',
        'CLASS_ACAD_ORG_LDESC',
        'CLASS_ASSOCIATED_CLASS',
        'CLASS_CAMPUS_CD',
        'CLASS_CAMPUS_LDESC',
        'CLASS_CANCEL_DATE',
        'CLASS_CATALOG_NBR',
        'CLASS_CLASS_NAME',
        'CLASS_CLASS_NBR',
        'CLASS_CLASS_ROLL_YN',
        'CLASS_CLASS_STAT',
        'CLASS_CLASS_STAT_LDESC',
        'CLASS_COMBINED_ID',
        'CLASS_COMBINED_DESCR',
        'CLASS_COMBINED_MAJOR_YN',
        'CLASS_COMBINED_SECTION',
        'CLASS_COMPONENT_CD',
        'CLASS_COMPONENT_LDESC',
        'CLASS_CONSENT',
        'CLASS_CONSENT_LDESC',
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_DAY_EVE',
        'CLASS_DAY_EVE_LDESC',
        'CLASS_DEPTID_EXPENSE_CD',
        'CLASS_DESCR',
        'CLASS_END_DATE',
        'CLASS_ENRL_CAP',
        'CLASS_ENRL_STAT',
        'CLASS_ENRL_STAT_LDESC',
        'CLASS_ENRL_TOT',
        'CLASS_HS_DUAL_ENROLL_YN',
        'CLASS_HS_DUAL_SPONSOR',
        'CLASS_HIGH_SCHOOL_LDESC',
        'CLASS_HIGH_SCHOOL_SDESC',
        'CLASS_HONORS',
        'CLASS_INSTITUTION_CD',
        'CLASS_INSTRUCTION_MODE',
        'CLASS_INSTRUCTION_MODE_LDESC',
        'CLASS_INSTRUCTOR_EMPLID',
        'CLASS_INSTRUCTOR_NAME',
        'CLASS_LOAD_CALC_CODE',
        'CLASS_LOAD_CALC_SDESC',
        'CLASS_LOCATION_CD',
        'CLASS_LOCATION_LDESC',
        'CLASS_MAJOR_CLASS_NAME',
        'CLASS_MAJOR_CLASS_NBR',
        'CLASS_SCHEDULE_PRINT',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_SESSION_LDESC',
        'CLASS_SID',
        'CLASS_START_DATE',
        'CLASS_SUBJECT_CD',
        'CLASS_TERM_CD',
        'CLASS_TYPE_CD',
        'CLASS_TYPE_LDESC',
        'COFFR_CLASS_SID',
        'COMP_WEEK_WORKLOAD_HRS',
        'CLASS_TOT_WORKLOAD_HRS',
    ];

    protected $primaryKey = 'CLASS_SID';

    protected array $relationMap = [
        Relations::MATERIALS => Material::class,
        Relations::ENROLLMENTS => Enrollment::class,
        Relations::MEETINGS => Meeting::class,
        Relations::CLASS_INSTRUCTORS => ClassInstructor::class,
        Relations::TERM => Term::class,
        Relations::CLASS_NOTES => ClassNote::class,
        Relations::COURSE_NOTES => CourseNote::class,
        Relations::FEES => Fee::class,
        Relations::OTHER => \Smorken\Sis\Db\Incremental\Klass\Klass::class,
        Relations::COMBINED_SECTIONS => Klass::class,
    ];

    protected $table = 'RDS_CLASS_VW';

    public function combinedId(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value): ?string => $this->CLASS_COMBINED_ID
        );
    }

    public function enrollments(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany($rel, 'CLASS_SID', 'CLASS_SID')
            ->defaultColumns();
    }

    public function fees(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany($rel, 'CLASS_SID', 'CLASS_SID')
            ->defaultColumns();
    }

    public function materials(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany($rel, 'TBMAT_SID', 'CLASS_SID')
            ->defaultColumns();
    }

    public function mergeOther(array $withs = []): \Smorken\Sis\Contracts\Base\Klass\Klass
    {
        /** @var \Smorken\Sis\Db\Incremental\Klass\Klass $other */
        $other = $this->other;
        if ($other && $withs) {
            $withs = array_filter($withs, fn (string $relation) => ! $other->relationLoaded($relation));
            $other->load($withs);
        }
        if ($other) {
            $this->mergeAttributesFromOther($other);
            $this->mergeRelationsFromOther($other);
        }

        return $this;
    }

    /**
     * @phpstan-param  \Smorken\Sis\Db\Core\Klass\Klass|\Smorken\Sis\Db\Incremental\Klass\Klass  $class
     */
    protected function mergeAttributesFromOther(\Smorken\Sis\Contracts\Base\Klass\Klass $class): void
    {
        foreach ($class->attributesToArray() as $k => $v) {
            $this->setAttribute($k, $v);
        }
    }

    /**
     * @phpstan-param  \Smorken\Sis\Db\Core\Klass\Klass|\Smorken\Sis\Db\Incremental\Klass\Klass  $class
     */
    protected function mergeRelationsFromOther(\Smorken\Sis\Contracts\Base\Klass\Klass $class): void
    {
        $relations = array_map(fn (string $v) => Str::snake($v),
            array_filter(array_keys($this->relationMap), fn (string $v) => ! in_array($v, ['other', 'materials'])));
        foreach ($relations as $relation) {
            if ($class->relationLoaded($relation)) {
                $this->setRelation($relation, $class->$relation);
            }
        }
    }
}
