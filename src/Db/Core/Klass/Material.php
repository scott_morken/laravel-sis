<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:50 PM
 */

namespace Smorken\Sis\Db\Core\Klass;

use Awobaz\Compoships\Compoships;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\MaterialAttributeMap;
use Smorken\Sis\Builders\Core\MaterialBuilder;
use Smorken\Sis\Db\Common\Base;

#[MapAttributeNames(new MaterialAttributeMap)]
class Material extends Base implements \Smorken\Sis\Contracts\Base\Klass\Material
{
    use Compoships;

    protected static string $builder = MaterialBuilder::class;

    protected $casts = ['TBMAT_PRICE' => 'float'];

    protected array $defaultColumns = [
        'TBMAT_SID',
        'TBMAT_SEQ_NBR',
        'TBMAT_TYPE_CD',
        'TBMAT_TYPE_CD_LDESC',
        'TBMAT_STATUS_CD',
        'TBMAT_STATUS_LDESC',
        'TBMAT_TITLE',
        'TBMAT_ISBN_NBR',
        'TBMAT_AUTHOR',
        'TBMAT_PUBLISHER',
        'TBMAT_EDITION',
        'TBMAT_YEAR_PUBLISHED',
        'TBMAT_PRICE',
        'TBMAT_NOTES',
    ];

    protected $primaryKey = 'TBMAT_SID';

    protected $table = 'REC_CLASS_MATERIALS_VW';
}
