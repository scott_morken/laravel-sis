<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/27/17
 * Time: 12:10 PM
 */

namespace Smorken\Sis\Db\Core\Klass;

use Awobaz\Compoships\Compoships;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\FeeAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Core\FeeModifiers;
use Smorken\Sis\Builders\Core\FeeBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;

#[ModifyValuesAttribute(new FeeModifiers), MapAttributeNames(new FeeAttributeMap)]
class Fee extends Base implements \Smorken\Sis\Contracts\Base\Fee
{
    use Compoships, HasCollegeIdAttribute, HasTermIdAttribute;

    protected array $attributeNames = [
        'collegeId' => 'CLSBF_SETID',
        'termId' => 'CLSBF_STRM',
    ];

    protected static string $builder = FeeBuilder::class;

    protected array $defaultColumns = [
        'CLSF_SID',
        'CLASS_SID',
        'CLSBF_SETID', //college id
        'CLSBF_CRSE_ID',
        'CLSBF_CRSE_OFFER_NBR',
        'CLSBF_STRM',
        'CLSBF_SESSION_CODE',
        'CLSBF_CLASS_SECTION',
        'CLSBF_COMPONENT',
        'CLSBF_ACCOUNT_TYPE_SF',
        'CLSBF_ITEM_TYPE',
        'CLSBF_ITEM_TYPE_SDESC',
        'CLSBF_ITEM_TYPE_LDESC',
        'CLSBF_FLAT_AMT',
        'CLSBF_CURRENCY_CD',
    ];

    protected $table = 'RDS_CLASS_SUBFEE_VW';
}
