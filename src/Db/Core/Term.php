<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:57 PM
 */

namespace Smorken\Sis\Db\Core;

use Awobaz\Compoships\Compoships;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\TermAttributeMap;
use Smorken\Sis\Builders\Core\TermBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;

#[MapAttributeNames(new TermAttributeMap)]
class Term extends Base implements \Smorken\Sis\Contracts\Base\Term
{
    use Compoships, HasAcademicCareerAttribute, HasCollegeIdAttribute, HasStartEndDateAttributes, HasTermIdAttribute;

    protected array $attributeNames = [
        'academicCareer' => 'ACAD_CAREER',
        'endDate' => 'TERM_END_DT',
        'startDate' => 'TERM_BEGIN_DT',
        'termId' => 'STRM',
        'collegeId' => 'INSTITUTION',
    ];

    protected static string $builder = TermBuilder::class;

    protected $casts = ['TERM_BEGIN_DT' => 'date', 'TERM_END_DT' => 'date'];

    protected array $defaultColumns = [
        'STRM',
        'DESCR',
        'ACAD_CAREER',
        'TERM_BEGIN_DT',
        'TERM_END_DT',
        'INSTITUTION',
    ];

    protected $primaryKey = 'STRM';

    protected $table = 'RDS_TERM_VW';
}
