<?php
declare(strict_types=1);

namespace Smorken\Sis\Db\Core\Person;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\AddressAttributeMap;
use Smorken\Sis\Builders\Core\AddressBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Oci\Constants\Person\AddressType;

#[MapAttributeNames(new AddressAttributeMap)]
class Address extends Base implements \Smorken\Sis\Contracts\Base\Person\Address
{
    /** @uses \Illuminate\Database\Eloquent\HasBuilder<AddressBuilder<static>> */
    use HasBuilder;
    /** @uses \Illuminate\Database\Eloquent\Factories\HasFactory<AddressFactory> */
    use HasFactory;
    use HasStudentIdAttribute;
    
    protected static string $builder = AddressBuilder::class;

    protected array $attributeNames = [
        'studentId' => 'ADDR_EMPLID',
    ];

    protected array $defaultColumns = [
        'ADDR_REC_KEY',
        'ADDR_EMPLID',
        'ADDR_ADDRESS_TYPE',
        'ADDR_COUNTRY',
        'ADDR_COUNTRY_LDESC',
        'ADDR_ADDRESS1',
        'ADDR_ADDRESS2',
        'ADDR_ADDRESS3',
        'ADDR_CITY',
        'ADDR_COUNTY',
        'ADDR_STATE',
        'ADDR_STATE_DESC',
        'ADDR_POSTAL',
        'ADDR_GEO_CODE',
        'ADDR_EMAIL_ADDR',
        'ADDR_PREF_EMAIL_FLAG',
        'ADDR_PHONE_COUNTRY_CD',
        'ADDR_PHONE_NUMBER',
        'ADDR_PHONE_EXTENSION',
    ];

    protected $primaryKey = 'ADDR_REC_KEY';

    protected $table = 'CC_ADDRESSES_VW';

    protected function casts(): array
    {
        return [
            'ADDR_ADDRESS_TYPE' => AddressType::class,
        ];
    }
}