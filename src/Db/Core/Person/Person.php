<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:07 PM
 */

namespace Smorken\Sis\Db\Core\Person;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\PersonAttributeMap;
use Smorken\Sis\Builders\Core\PersonBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasPersonNames;
use Smorken\Sis\Db\Common\Concerns\HasPersonRelations;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Core\Instructor\Position;
use Smorken\Sis\Db\Core\Student\Enrollment;
use Smorken\Sis\Db\Core\Student\Group;
use Smorken\Sis\Db\Core\Student\StudentTerm;
use Smorken\Sis\Db\Incremental\Klass\ClassInstructor;
use Smorken\Sis\Enums\Relations;

#[MapAttributeNames(new PersonAttributeMap)]
class Person extends Base implements \Smorken\Sis\Contracts\Base\Person\Person
{
    use HasPersonNames, HasPersonRelations, HasStudentIdAttribute;

    protected $appends = ['firstName', 'lastName'];

    protected array $attributeNames = [
        'studentId' => 'EMPLID',
    ];

    protected static string $builder = PersonBuilder::class;

    protected $casts = ['PERS_BIRTHDATE' => 'date'];

    protected array $defaultColumns = [
        'EMPLID',
        'PERS_OPRID',
        'PERS_PREFERRED_FIRST_NAME',
        'PERS_PREFERRED_LAST_NAME',
        'PERS_PRIMARY_FIRST_NAME',
        'PERS_PRIMARY_LAST_NAME',
        'EMAIL_EMAIL_ADDR',
        'PERS_HOME_PHONE_NBR',
        'PERS_BIRTHDATE',
        'HRMS_EMPLID',
    ];

    protected $primaryKey = 'EMPLID';

    protected array $relationMap = [
        Relations::HOME_ADDRESS => Address::class,
        Relations::INSTRUCTS_CLASSES => ClassInstructor::class,
        Relations::INSTRUCTOR_POSITIONS => Position::class,
        Relations::ENROLLMENTS => Enrollment::class,
        Relations::STUDENT_TERMS => StudentTerm::class,
        Relations::STUDENT_GROUPS => Group::class,
    ];

    protected $table = 'RDS_PERSON_VW';

    protected function getLocalStudentIdColumnName(): string
    {
        return 'EMPLID';
    }
}
