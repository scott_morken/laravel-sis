<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/2/18
 * Time: 10:12 AM
 */

namespace Smorken\Sis\Db\Core\Person;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\EmailAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Core\EmailModifiers;
use Smorken\Sis\Builders\Core\EmailBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;

#[ModifyValuesAttribute(new EmailModifiers), MapAttributeNames(new EmailAttributeMap)]
class Email extends Base implements \Smorken\Sis\Contracts\Base\Person\Email
{
    use HasStudentIdAttribute;

    protected array $attributeNames = [
        'studentId' => 'EMAIL_EMPLID',
    ];

    protected static string $builder = EmailBuilder::class;

    protected array $defaultColumns = [
        'EMAIL_REC_KEY',
        'EMAIL_EMPLID',
        'EMAIL_ADDR_TYPE',
        'EMAIL_ADDR_TYPE_LDESC',
        'EMAIL_PREF_EMAIL_FLAG',
        'EMAIL_EMAIL_ADDR',
    ];

    protected $primaryKey = 'EMAIL_REC_KEY';

    protected $table = 'CC_EMAIL_ADDRESSES_VW';
}
