<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/4/18
 * Time: 7:38 AM
 */

namespace Smorken\Sis\Db\Core\Instructor;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\PositionAttributeMap;
use Smorken\Sis\Builders\Core\PositionBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;

/**
 * SIS job to employee table
 */
#[MapAttributeNames(new PositionAttributeMap)]
class Position extends Base implements \Smorken\Sis\Contracts\Base\Instructor\Position
{
    use HasStudentIdAttribute;

    protected array $attributeNames = [
        'studentId' => 'EMPLID',
    ];

    protected static string $builder = PositionBuilder::class;

    protected array $defaultColumns = [
        'EMPLID',
        'EMPL_RCD',
        'DEPTID',
        'JOBCODE',
        'POSITION_NBR',
        'DESCR',
    ];

    protected $table = 'PS_MC_INSTR_POS_VW';
}
