<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:09 PM
 */

namespace Smorken\Sis\Db\Core\Physical;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\FacilityAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Core\FacilityModifiers;
use Smorken\Sis\Builders\Core\FacilityBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;

#[ModifyValuesAttribute(new FacilityModifiers), MapAttributeNames(new FacilityAttributeMap)]
class Facility extends Base implements \Smorken\Sis\Contracts\Base\Physical\Facility
{
    use HasAcademicOrgIdAttribute, HasCollegeIdAttribute;

    protected array $addAttributeMutatedAttributes = ['group_description', 'location_description'];

    protected array $attributeNames = [
        'academicOrgId' => 'FACILITY_ACAD_ORG',
        'collegeId' => 'FACILITY_SETID',
    ];

    protected static string $builder = FacilityBuilder::class;

    protected $casts = ['FACILITY_CAPACITY' => 'integer'];

    protected array $defaultColumns = [
        'FACILITY_SETID',
        'FACILITY_ID',
        'FACILITY_EFF_STATUS',
        'FACILITY_BLDG_CD',
        'FACILITY_ROOM',
        'FACILITY_DESCR',
        'FACILITY_DESCRSHORT',
        'FACILITY_GROUP',
        'FACILITY_LOCATION',
        'FACILITY_CAPACITY',
        'FACILITY_ACAD_ORG',
    ];

    protected $primaryKey = 'FACILITY_ID';

    protected $table = 'REC_FACILITY_VW';
}
