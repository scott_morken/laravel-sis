<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:01 PM
 */

namespace Smorken\Sis\Db\Core\Course;

use Awobaz\Compoships\Compoships;
use Awobaz\Compoships\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\CourseCatalogAttributeMap;
use Smorken\Sis\Builders\Core\CourseCatalogBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Enums\Relations;

#[MapAttributeNames(new CourseCatalogAttributeMap)]
class CourseCatalog extends Base implements \Smorken\Sis\Contracts\Base\Course\CourseCatalog
{
    use Compoships;

    protected static string $builder = CourseCatalogBuilder::class;

    protected $casts = [
        'CRSE_GOV_BD_APPROVAL_DATE' => 'date', 'CRSE_EFFDT' => 'date', 'CRSE_UNITS_MINIMUM' => 'float',
        'CRSE_UNITS_MAXIMUM' => 'float',
    ];

    protected array $defaultColumns = [
        'CRSE_REC_KEY',
        'CRSE_CATALOG_SID',
        'CRSE_CRSE_ID',
        'CRSE_DESCR',
        'CRSE_CONSENT',
        'CRSE_UNITS_MINIMUM', //credits
        'CRSE_UNITS_MAXIMUM',
        'CRSE_GRADING_BASIS',
        'CRSE_COMPONENT',
        'CRSE_COURSE_NOTES',
        'CRSE_GOV_BD_APPROVAL_DATE',
        'CRSE_EFFDT',
    ];

    protected $primaryKey = 'CRSE_REC_KEY';

    protected array $relationMap = [
        Relations::COURSE_OFFERINGS => CourseOffer::class,
    ];

    protected $table = 'REC_CRSE_CATALOG_VW';

    public function offerings(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            ['CRSE_CATALOG_SID', 'COFFR_CRSE_ID'],
            ['CRSE_CATALOG_SID', 'CRSE_CRSE_ID']
        )->defaultColumns();
    }
}
