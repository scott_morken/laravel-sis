<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:01 PM
 */

namespace Smorken\Sis\Db\Core\Course;

use Awobaz\Compoships\Compoships;
use Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo;
use Awobaz\Compoships\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\CourseOfferAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Core\CourseOfferModifiers;
use Smorken\Sis\Builders\Core\CourseOfferBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Incremental\Course\CourseNote;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new CourseOfferModifiers), MapAttributeNames(new CourseOfferAttributeMap)]
class CourseOffer extends Base implements \Smorken\Sis\Contracts\Base\Course\CourseOffer
{
    use Compoships, HasAcademicCareerAttribute, HasAcademicOrgIdAttribute, HasCollegeIdAttribute;

    public bool $liftedCatalogAttributes = false;

    protected array $attributeNames = [
        'academicCareer' => 'COFFR_ACAD_CAREER',
        'academicOrgId' => 'COFFR_ACAD_ORG',
        'collegeId' => 'COFFR_INSTITUTION',
    ];

    protected static string $builder = CourseOfferBuilder::class;

    protected $casts = ['COFFR_CRSE_EFFDT' => 'date'];

    protected array $defaultColumns = [
        'COFFR_ACAD_CAREER',
        'COFFR_ACAD_GROUP',
        'COFFR_ACAD_ORG',
        'COFFR_CAMPUS',
        'COFFR_CATALOG_NBR',
        'COFFR_CATALOG_PRINT',
        'COFFR_CLASS_SID',
        'COFFR_COURSE_APPROVED',
        'COFFR_CRSE_EFFDT',
        'COFFR_CRSE_ID',
        'COFFR_CRSE_OFFER_NBR',
        'COFFR_INSTITUTION',
        'COFFR_REC_KEY',
        'COFFR_SCHEDULE_PRINT',
        'COFFR_SUBJECT',
        'CRSE_CATALOG_SID',
    ];

    protected $primaryKey = 'COFFR_REC_KEY';

    protected array $relationMap = [
        Relations::COURSE_CATALOG => CourseCatalog::class,
        Relations::COURSE_NOTES => CourseNote::class,

    ];

    protected $table = 'REC_CRSE_OFFER_VW';

    public function catalog(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo(
            $rel,
            ['CRSE_CATALOG_SID', 'COFFR_CRSE_ID'],
            ['CRSE_CATALOG_SID', 'CRSE_CRSE_ID']
        )->defaultColumns();
    }

    public function courseNotes(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
            ],
            [
                'COFFR_CRSE_ID',
                'COFFR_CRSE_OFFER_NBR',
            ]
        )->defaultColumns();
    }
}
