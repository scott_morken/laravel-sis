<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:13 PM
 */

namespace Smorken\Sis\Db\Core\Student;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\ServiceIndicatorAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Core\ServiceIndicatorModifiers;
use Smorken\Sis\Builders\Core\ServiceIndicatorBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Db\Core\Person\Person;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new ServiceIndicatorModifiers), MapAttributeNames(new ServiceIndicatorAttributeMap)]
class ServiceIndicator extends Base implements \Smorken\Sis\Contracts\Base\Student\ServiceIndicator
{
    use HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute;

    protected array $attributeNames = [
        'collegeId' => 'SRVC_INSTITUTION',
        'studentId' => 'SRVC_EMPLID',
        'termId' => 'SRVC_SERVICE_IND_ACT_TERM',
    ];

    protected static string $builder = ServiceIndicatorBuilder::class;

    protected $casts = [
        'SRVC_IND_DATETIME' => 'datetime', 'SRVC_SERVICE_IND_ACTIVE_DT' => 'date', 'SRVC_EXT_DUE_DT' => 'datetime',
    ];

    protected array $defaultColumns = [
        'SRVC_EMPLID',
        'SRVC_IND_DATETIME',
        'SRVC_OPRID',
        'SRVC_INSTITUTION',
        'SRVC_SERVICE_IND_CD',
        'SRVC_SERVICE_IND_LDESC',
        'SRVC_SERVICE_IND_REASON',
        'SRVC_SERVICE_IND_REASON_LDESC',
        'SRVC_SERVICE_IND_ACT_TERM',
        'SRVC_SERVICE_IND_ACTIVE_DT',
        'SRVC_POSITIVE_SRVC_INDICATOR',
        'SRVC_AMOUNT',
        'SRVC_COMM_COMMENTS',
        'SRVC_CONTACT',
        'SRVC_DEPARTMENT',
        'SRVC_DEPARTMENT_LDESC',
        'SRVC_EXT_DUE_DT',
    ];

    protected array $relationMap = [
        Relations::STUDENT => Person::class,
    ];

    protected $table = 'RDS_SERV_IND_VW';

    public function student(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'SRVC_EMPLID', 'EMPLID')->defaultColumns();
    }
}
