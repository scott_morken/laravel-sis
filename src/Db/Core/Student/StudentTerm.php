<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:14 PM
 */

namespace Smorken\Sis\Db\Core\Student;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\StudentTermAttributeMap;
use Smorken\Sis\Builders\Core\StudentTermBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Db\Core\Person\Person;
use Smorken\Sis\Enums\Relations;

#[MapAttributeNames(new StudentTermAttributeMap)]
class StudentTerm extends Base implements \Smorken\Sis\Contracts\Base\Student\StudentTerm
{
    use HasAcademicCareerAttribute, HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute;

    protected array $attributeNames = [
        'academicCareer' => 'CTERM_ACAD_CAREER',
        'collegeId' => 'CTERM_INSTITUTION_CODE',
        'studentId' => 'EMPLID',
        'termId' => 'CTERM_TERM_CD',
    ];

    protected static string $builder = StudentTermBuilder::class;

    protected $casts = [
        'STFACT_MCCD_TERM_AHRS' => 'float',
        'STFACT_MCCD_TERM_EHRS' => 'float',
        'STFACT_MCCD_TERM_QHRS' => 'float',
        'STFACT_MCCD_TERM_QPTS' => 'float',
        'STFACT_MCCD_TERM_CLOCK_HRS' => 'float',
        'STFACT_MCCD_CUM_AHRS' => 'float',
        'STFACT_MCCD_CUM_EHRS' => 'float',
        'STFACT_MCCD_CUM_QHRS' => 'float',
        'STFACT_MCCD_CUM_QPTS' => 'float',
        'STFACT_MCCD_CUM_CLOCK_HRS' => 'float',
        'STFACT_TOT_INPROG_GPA' => 'float',
        'STFACT_TOT_INPROG_NOGPA' => 'float',
    ];

    protected array $defaultColumns = [
        'EMPLID',
        'CTERM_TERM_CD',
        'CTERM_INSTITUTION_CODE',
        'CTERM_ACAD_CAREER',
        'STFACT_CUM_GPA',
        'STFACT_TERM_GPA',
        'STFACT_MCCD_CUM_AHRS',
        'STFACT_MCCD_CUM_EHRS',
        'STFACT_MCCD_CUM_QHRS',
        'STFACT_MCCD_CUM_QPTS',
        'STFACT_MCCD_TERM_CLOCK_HRS',
        'STFACT_MCCD_TERM_AHRS',
        'STFACT_MCCD_TERM_EHRS',
        'STFACT_MCCD_TERM_QHRS',
        'STFACT_MCCD_TERM_QPTS',
        'STFACT_MCCD_CUM_CLOCK_HRS',
        'CTERM_ACAD_STDNG_LDESC',
        'CTERM_ACAD_LOAD_SDESC',
        'STFACT_TOT_INPROG_GPA',
        'STFACT_TOT_INPROG_NOGPA',
        'CTERM_ACAD_STNDNG_ACTN',
        'CTERM_ACAD_LOAD_CODE',
    ];

    protected array $relationMap = [
        Relations::STUDENT => Person::class,
    ];

    protected $table = 'RDS_STU_TERM_VW';

    public function student(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'EMPLID', 'EMPLID')->defaultColumns();
    }
}
