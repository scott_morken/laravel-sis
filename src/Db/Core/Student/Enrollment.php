<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:09 PM
 */

namespace Smorken\Sis\Db\Core\Student;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\EnrollmentAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Core\EnrollmentModifiers;
use Smorken\Sis\Builders\Core\EnrollmentBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Db\Core\Klass\Klass;
use Smorken\Sis\Db\Core\Klass\Material;
use Smorken\Sis\Db\Core\Person\Person;
use Smorken\Sis\Db\Core\Term;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new EnrollmentModifiers), MapAttributeNames(new EnrollmentAttributeMap)]
class Enrollment extends Base implements \Smorken\Sis\Contracts\Base\Student\Enrollment
{
    use Compoships, HasAcademicCareerAttribute, HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute;

    protected array $addAttributeMutatedAttributes = ['grading_basis_description'];

    protected array $attributeNames = [
        'academicCareer' => 'ENRL_ACAD_CAREER',
        'collegeId' => 'ENRL_INSTITUTION_CD',
        'studentId' => 'EMPLID',
        'termId' => 'ENRL_TERM_CD',
    ];

    protected static string $builder = EnrollmentBuilder::class;

    protected $casts = [
        'ENRL_GRADE_DATE' => 'date', 'ENRL_UNITS_TAKEN' => 'float',
        'ENRL_GRADE_POINTS' => 'float',
        'ENRL_OEE_START_DATE' => 'date', 'ENRL_OEE_END_DATE' => 'date',
    ];

    protected array $defaultColumns = [
        'CLASS_SID',
        'EMPLID',
        'ENRL_INSTITUTION_CD',
        'ENRL_TERM_CD',
        'ENRL_CLASS_NBR',
        'ENRL_ACAD_CAREER',
        'ENRL_GRADING_BASIS',
        'ENRL_OFFICIAL_GRADE',
        'ENRL_GRADE_DATE',
        'ENRL_GRADE_POINTS',
        'ENRL_UNITS_TAKEN',
        'ENRL_STATUS_REASON_CD',
        'ENRL_STATUS_REASON_LDESC',
        'ENRL_ACTN_RSN_LAST_CD',
        'ENRL_ACTN_RSN_LAST_LDESC',
        'ENRL_INCLUDE_IN_GPA',
        'ENRL_OEE_START_DATE',
        'ENRL_OEE_END_DATE',
    ];

    protected array $relationMap = [
        Relations::KLASS => Klass::class,
        Relations::STUDENT => Person::class,
        Relations::MATERIALS => Material::class,
        Relations::TERM => Term::class,
    ];

    protected $table = 'RDS_STU_CLASS_VW';

    public function class(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'CLASS_SID', 'CLASS_SID')
            ->defaultColumns();
    }

    public function materials(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany($rel, 'TBMAT_SID', 'CLASS_SID')
            ->defaultColumns()
            ->orderSequence();
    }

    public function student(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'EMPLID', 'EMPLID')
            ->defaultColumns();
    }

    public function term(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'ENRL_TERM_CD', 'STRM')
            ->defaultColumns();
    }
}
