<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/27/17
 * Time: 12:08 PM
 */

namespace Smorken\Sis\Db\Core\Student;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\AthleteAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Core\AthleteModifiers;
use Smorken\Sis\Builders\Core\AthleteBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Incremental\Person\Person;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new AthleteModifiers), MapAttributeNames(new AthleteAttributeMap)]
class Athlete extends Base implements \Smorken\Sis\Contracts\Base\Student\Athlete
{
    use HasCollegeIdAttribute, HasStartEndDateAttributes, HasStudentIdAttribute;

    protected array $attributeNames = [
        'collegeId' => 'ATHL_INSTITUTION',
        'endDate' => 'ATHL_END_DATE',
        'startDate' => 'ATHL_START_DATE',
        'studentId' => 'ATHL_EMPLID',
    ];

    protected static string $builder = AthleteBuilder::class;

    protected $casts = ['ATHL_START_DATE' => 'date', 'ATHL_END_DATE' => 'date'];

    protected array $defaultColumns = [
        'ATHL_INSTITUTION',
        'ATHL_EMPLID',
        'ATHL_SPORT',
        'ATHL_SPORT_LDESC',
        'ATHL_ATHLETIC_PARTIC_CD',
        'ATHL_ATHLETIC_PARTIC_LDESC',
        'ATHL_NCAA_ELIGIBLE',
        'ATHL_CURRENT_PARTICIPANT',
        'ATHL_START_DATE',
        'ATHL_END_DATE',
    ];

    protected array $relationMap = [
        Relations::STUDENT => Person::class,
        Relations::ENROLLMENTS => Enrollment::class,
    ];

    protected $table = 'REC_ATHLETIC_PARTIC_VW';

    public function enrollments(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany($rel, 'EMPLID', 'ATHL_EMPLID')->defaultColumns();
    }

    public function student(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'ATHL_EMPLID', 'PERS_EMPLID')->defaultColumns();
    }
}
