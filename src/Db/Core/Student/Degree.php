<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:11 PM
 */

namespace Smorken\Sis\Db\Core\Student;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\DegreeAttributeMap;
use Smorken\Sis\Builders\Core\DegreeBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;

#[MapAttributeNames(new DegreeAttributeMap)]
class Degree extends Base implements \Smorken\Sis\Contracts\Base\Student\Degree
{
    use HasAcademicCareerAttribute, HasCollegeIdAttribute;

    protected array $attributeNames = [
        'academicCareer' => 'ACAD_PROG',
        'collegeId' => 'INSTITUTION',
    ];

    protected static string $builder = DegreeBuilder::class;

    protected $casts = ['EFFDT' => 'date'];

    protected array $defaultColumns = [
        'INSTITUTION',
        'ACAD_PROG',
        'ACAD_PLAN_TYPE',
        'ACAD_PLAN',
        'EFFDT',
        'EFF_STATUS',
        'FIRST_TERM_VALID',
        'DESCR',
        'DEGREE',
        'DEGREE_DESCR',
        'DIPLOMA_DESCR',
        'TRNSCR_DESCR',
        'MIN_UNITS_REQD',
        'PLAN_DESCRIPTION',
    ];

    protected $table = 'PCC_REC_DEGREE_PLAN_VW';
}
