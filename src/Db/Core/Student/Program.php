<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:12 PM
 */

namespace Smorken\Sis\Db\Core\Student;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\ProgramAttributeMap;
use Smorken\Sis\Builders\Core\ProgramBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Core\Person\Person;
use Smorken\Sis\Enums\Relations;

#[MapAttributeNames(new ProgramAttributeMap)]
class Program extends Base implements \Smorken\Sis\Contracts\Base\Student\Program
{
    use HasAcademicCareerAttribute, HasCollegeIdAttribute, HasStudentIdAttribute;

    protected array $attributeNames = [
        'academicCareer' => 'STRUC_ACAD_CAREER',
        'collegeId' => 'STRUC_INSTITUTION',
        'studentId' => 'STRUC_EMPLID',
    ];

    protected static string $builder = ProgramBuilder::class;

    protected array $defaultColumns = [
        'STRUC_EMPLID',
        'STRUC_ACAD_CAREER',
        'STRUC_STDNT_CAR_NBR',
        'STRUC_INSTITUTION',
        'STRUC_ACAD_PROG',
        'STRUC_ACAD_PROG_LDESC',
        'STRUC_ACAD_PROG_STATUS',
        'STRUC_ACAD_PROG_STATUS_LDESC',
        'STRUC_PROG_ACTION',
        'STRUC_PROG_ACTION_LDESC',
        'STRUC_PROG_REASON',
        'STRUC_PROG_REASON_LDESC',
        'STRUC_PROG_ADMIT_TERM',
        'STRUC_ACAD_PLAN',
        'STRUC_ACAD_PLAN_LDESC',
        'STRUC_PLAN_TYPE',
        'STRUC_PLAN_TYPE_LDESC',
        'STRUC_ACAD_PLAN_DEGREE',
        'STRUC_EXP_GRAD_TERM',
        'STRUC_ACAD_INTEREST',
        'STRUC_ACAD_INTEREST_LDESC',
        'STRUC_DEGR_CHECKOUT_STAT',
        'STRUC_DEGR_CHECKOUT_LDESC',
        'STRUC_ACAD_SUB_PLAN',
        'STRUC_ACAD_SUB_PLAN_LDESC',
        'STRUC_ACAD_SUBPLAN_TYPE',
        'STRUC_ACAD_SUBPLAN_TYPE_LDESC',
    ];

    protected array $relationMap = [
        Relations::STUDENT => Person::class,
    ];

    protected $table = 'RDS_PROG_PLAN_VW';

    public function student(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'STRUC_EMPLID', 'EMPLID')->defaultColumns();
    }
}
