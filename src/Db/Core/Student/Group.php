<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 2:11 PM
 */

namespace Smorken\Sis\Db\Core\Student;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Attributes\AttributeNames\Core\Camel\GroupAttributeMap;
use Smorken\Sis\Builders\Core\GroupBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;

#[MapAttributeNames(new GroupAttributeMap)]
class Group extends Base implements \Smorken\Sis\Contracts\Base\Student\Group
{
    use HasCollegeIdAttribute, HasStudentIdAttribute;

    protected array $attributeNames = [
        'collegeId' => 'STDGR_INSTITUTION',
        'studentId' => 'STDGR_EMPLID',
    ];

    protected static string $builder = GroupBuilder::class;

    protected $casts = ['STDGR_START_DATE' => 'date', 'STDGR_END_DATE' => 'date'];

    protected array $defaultColumns = [
        'STDGR_EMPLID',
        'STDGR_INSTITUTION',
        'STDGR_STUDENT_GROUP',
        'STDGR_STDNT_GROUP_LDESC',
        'STDGR_START_DATE',
        'STDGR_END_DATE',
        'STDGR_STUDENT_STATUS',
        'STDGR_STUDENT_STATUS_LDESC',
        'STDGR_COMMENTS',
    ];

    protected $table = 'RDS_STU_GROUPS_VW';
}
