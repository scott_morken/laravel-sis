<?php

namespace Smorken\Sis\Db\Common\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * @property object|\Smorken\Model\Contracts\Model $model
 */
trait HasAcademicOrgIdAttribute
{
    public function getAcademicOrgIdAttributeName(): string
    {
        if (is_a($this, Builder::class) && method_exists($this->model, 'getAcademicOrgIdAttributeName')) {
            return $this->model->getAcademicOrgIdAttributeName();
        }
        if (property_exists($this, 'attributeNames')) {
            return $this->attributeNames['academicOrgId'] ?? 'ACAD_ORG';
        }

        return 'ACAD_ORG';
    }
}
