<?php

namespace Smorken\Sis\Db\Common\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * @property object|\Smorken\Model\Contracts\Model $model
 */
trait HasStartEndDateAttributes
{
    public function getEndDateAttributeName(): string
    {
        if (is_a($this, Builder::class) && method_exists($this->model, 'getEndDateAttributeName')) {
            return $this->model->getEndDateAttributeName();
        }
        if (property_exists($this, 'attributeNames')) {
            return $this->attributeNames['endDate'] ?? 'END_DT';
        }

        return 'END_DT';
    }

    public function getStartDateAttributeName(): string
    {
        if (is_a($this, Builder::class) && method_exists($this->model, 'getStartDateAttributeName')) {
            return $this->model->getStartDateAttributeName();
        }
        if (property_exists($this, 'attributeNames')) {
            return $this->attributeNames['startDate'] ?? 'START_DT';
        }

        return 'START_DT';
    }
}
