<?php

namespace Smorken\Sis\Db\Common\Concerns;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Smorken\Sis\Oci\Constants\Person\AddressType;

trait HasPersonRelations
{
    abstract protected function getLocalStudentIdColumnName(): string;

    public function emails(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany($rel, 'EMAIL_EMPLID', $this->getLocalStudentIdColumnName())
            ->defaultColumns();
    }

    public function enrollments(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            'ENRL_EMPLID',
            $this->getLocalStudentIdColumnName()
        )
            ->defaultColumns();
    }

    public function homeAddress(): HasOne
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasOne($rel, 'ADDR_EMPLID', $this->getLocalStudentIdColumnName())
            ->typeIs(AddressType::HOME)
            ->defaultColumns();
    }

    public function instructsClasses(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany($rel, 'CLASSM_INSTRUCTOR_EMPLID', $this->getLocalStudentIdColumnName());
    }

    public function positions(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany($rel, 'EMPLID', $this->getLocalStudentIdColumnName())
            ->defaultColumns();
    }

    public function studentGroups(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            'STDGR_EMPLID',
            $this->getLocalStudentIdColumnName()
        )
            ->defaultColumns()
            ->active();
    }

    public function studentTerms(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            'CTERM_EMPLID',
            $this->getLocalStudentIdColumnName()
        )
            ->defaultColumns();
    }
}
