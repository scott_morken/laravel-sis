<?php

namespace Smorken\Sis\Db\Common\Concerns;

trait HasPersonNames
{
    public function fullName(): string
    {
        return $this->getFullNameAttribute();
    }

    public function getFirstName(): string
    {
        $name = trim($this->PERS_PREFERRED_FIRST_NAME ?? '');
        if (! $name) {
            $name = trim($this->PERS_PRIMARY_FIRST_NAME ?? '');
        }

        return $name;
    }

    public function getFullNameAttribute(): string
    {
        return implode(' ', array_filter([$this->getFirstName(), $this->getLastName()]));
    }

    public function getLastName(): string
    {
        $name = trim($this->PERS_PREFERRED_LAST_NAME ?? '');
        if (! $name) {
            $name = trim($this->PERS_PRIMARY_LAST_NAME ?? '');
        }

        return $name;
    }

    public function getFirstNameAttribute(): string
    {
        return $this->getFirstName();
    }

    public function getLastNameAttribute(): string
    {
        return $this->getLastName();
    }

    public function getShortNameAttribute(): string
    {
        return implode('. ', array_filter([substr($this->getFirstName(), 0, 1) ?: null, $this->getLastName()]));
    }

    public function shortName(): string
    {
        return $this->getShortNameAttribute();
    }
}
