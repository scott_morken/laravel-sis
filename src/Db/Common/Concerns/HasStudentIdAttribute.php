<?php

namespace Smorken\Sis\Db\Common\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * @property object|\Smorken\Model\Contracts\Model $model
 */
trait HasStudentIdAttribute
{
    public function getStudentIdAttributeName(): string
    {
        if (is_a($this, Builder::class) && method_exists($this->model, 'getStudentIdAttributeName')) {
            return $this->model->getStudentIdAttributeName();
        }
        if (property_exists($this, 'attributeNames')) {
            return $this->attributeNames['studentId'] ?? 'EMPLID';
        }

        return 'EMPLID';
    }
}
