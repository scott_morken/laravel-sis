<?php

namespace Smorken\Sis\Db\Common\Concerns;

use Awobaz\Compoships\Database\Eloquent\Relations\HasMany;
use Awobaz\Compoships\Database\Eloquent\Relations\HasOne;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasClassRelations
{
    public function classInstructors(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
            ->defaultColumns();
    }

    public function classNotes(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
            ->defaultColumns();
    }

    public function combinedSections(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'CLASS_TERM_CD',
                'CLASS_INSTITUTION_CD',
                'CLASS_SESSION_CD',
                'CLASS_COMBINED_ID',
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_INSTITUTION_CD',
                'CLASS_SESSION_CD',
                'CLASS_COMBINED_ID',
            ]
        )->where(function (Builder $query) {
            // @phpstan-ignore method.notFound
            $query->whereNotNull('CLASS_COMBINED_ID')
                ->where('CLASS_COMBINED_ID', '<>', '')
                ->notCancelled();
        })
            ->defaultColumns();
    }

    public function courseNotes(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
            ]
        )
            ->defaultColumns();
    }

    public function fees(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'CLSBF_CRSE_ID',
                'CLSBF_CRSE_OFFER_NBR',
                'CLSBF_STRM',
                'CLSBF_SESSION_CODE',
                'CLSBF_CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
            ->defaultColumns();
    }

    public function meetings(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
            ->defaultColumns();
    }

    public function other(): HasOne
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasOne(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
            ->defaultColumns();
    }

    public function term(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'CLASS_TERM_CD', 'STRM');
    }
}
