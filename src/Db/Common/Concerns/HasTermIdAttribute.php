<?php

namespace Smorken\Sis\Db\Common\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * @property object|\Smorken\Model\Contracts\Model $model
 */
trait HasTermIdAttribute
{
    public function getTermIdAttributeName(): string
    {
        if (is_a($this, Builder::class) && method_exists($this->model, 'getTermIdAttributeName')) {
            return $this->model->getTermIdAttributeName();
        }
        if (property_exists($this, 'attributeNames')) {
            return $this->attributeNames['termId'] ?? 'CLASS_TERM_CD';
        }

        return 'CLASS_TERM_CD';
    }
}
