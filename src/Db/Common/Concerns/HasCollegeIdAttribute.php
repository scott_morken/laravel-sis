<?php

namespace Smorken\Sis\Db\Common\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * @property object|\Smorken\Model\Contracts\Model $model
 */
trait HasCollegeIdAttribute
{
    public function getCollegeIdAttributeName(): string
    {
        if (is_a($this, Builder::class) && method_exists($this->model, 'getCollegeIdAttributeName')) {
            return $this->model->getCollegeIdAttributeName();
        }
        if (property_exists($this, 'attributeNames')) {
            return $this->attributeNames['collegeId'] ?? 'INSTITUTION_CD';
        }

        return 'INSTITUTION_CD';
    }
}
