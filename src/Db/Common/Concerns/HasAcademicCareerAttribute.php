<?php

namespace Smorken\Sis\Db\Common\Concerns;

use Illuminate\Contracts\Database\Eloquent\Builder;

/**
 * @property object|\Smorken\Model\Contracts\Model $model
 */
trait HasAcademicCareerAttribute
{
    public function getAcademicCareerAttributeName(): string
    {
        if (is_a($this, Builder::class) && method_exists($this->model, 'getAcademicCareerAttributeName')) {
            return $this->model->getAcademicCareerAttributeName();
        }
        if (property_exists($this, 'attributeNames')) {
            return $this->attributeNames['academicCareer'] ?? 'ACAD_CAREER';
        }

        return 'ACAD_CAREER';
    }
}
