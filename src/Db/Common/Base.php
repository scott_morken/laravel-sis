<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:32 PM
 */

namespace Smorken\Sis\Db\Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Smorken\Model\Concerns\WithDefaultColumns;
use Smorken\Model\Concerns\WithRelationMap;
use Smorken\Model\Eloquent;
use Smorken\Model\QueryBuilders\Concerns\Scopes\HasMultiKeyJoin;

#[\AllowDynamicProperties]
abstract class Base extends Eloquent
{
    use HasFactory, HasMultiKeyJoin, WithDefaultColumns, WithRelationMap;

    public $incrementing = false;

    protected $connection = 'sis';
}
