<?php

declare(strict_types=1);

namespace Smorken\Sis\Db\Common\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class LiftCourseCatalogOnCourseOfferScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        // @phpstan-ignore method.notFound
        if ($model->shouldLiftCatalogAttributes()) {
            // @phpstan-ignore method.notFound
            $builder->joinCourseCatalog();
        }
    }
}
