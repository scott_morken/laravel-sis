<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:48 PM
 */

namespace Smorken\Sis\Db\Incremental\Klass;

use Awobaz\Compoships\Compoships;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\ClassNoteAttributeMap;
use Smorken\Sis\Attributes\Modifiers\NoteModifiers;
use Smorken\Sis\Builders\Common\NoteBuilder;
use Smorken\Sis\Contracts\Base\Note;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new NoteModifiers), MapAttributeNames(new ClassNoteAttributeMap)]
class ClassNote extends Base implements Note
{
    use Compoships, HasCollegeIdAttribute, HasTermIdAttribute;

    protected array $attributeNames = [
        'collegeId' => 'CLASS_INSTITUTION_CD',
        'termId' => 'CLASS_TERM_CD',
    ];

    protected static string $builder = NoteBuilder::class;

    protected array $defaultColumns = [
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_TERM_CD',
        'NOTES_SEQ_NBR',
        'NOTES_NOTE_NBR',
        'NOTES_TYPE_PUBL_STAF',
        'NOTES_DESCR',
        'NOTES_NOTE_TEXT',
        'CLASS_INSTITUTION_CD',
    ];

    protected array $relationMap = [
        Relations::KLASS => Klass::class,
    ];

    protected $table = 'CDS_CLASS_NOTES_TBL';
}
