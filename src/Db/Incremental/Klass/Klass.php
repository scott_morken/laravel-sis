<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:35 PM
 */

namespace Smorken\Sis\Db\Incremental\Klass;

use Awobaz\Compoships\Compoships;
use Awobaz\Compoships\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Collection;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\ClassAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Incremental\ClassModifiers;
use Smorken\Sis\Builders\Incremental\ClassBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasClassRelations;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Db\Core\Klass\Fee;
use Smorken\Sis\Db\Core\Klass\Material;
use Smorken\Sis\Db\Core\Term;
use Smorken\Sis\Db\Incremental\Course\CourseNote;
use Smorken\Sis\Db\Incremental\Student\Enrollment;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new ClassModifiers), MapAttributeNames(new ClassAttributeMap)]
class Klass extends Base implements \Smorken\Sis\Contracts\Base\Klass\Klass
{
    use Compoships, HasAcademicOrgIdAttribute, HasClassRelations, HasCollegeIdAttribute, HasStartEndDateAttributes, HasTermIdAttribute;

    protected array $addAttributeMutatedAttributes = [
        'component_description',
        'day_evening_description',
        'status_description',
        'enrollment_status_description',
        'instruction_mode_description',
        'grading_basis_description',
        'type_description',
        'session_description',
    ];

    protected array $attributeNames = [
        'academicOrgId' => 'CLASS_ACAD_ORG',
        'collegeId' => 'CLASS_INSTITUTION_CD',
        'endDate' => 'CLASS_END_DATE',
        'startDate' => 'CLASS_START_DATE',
        'termId' => 'CLASS_TERM_CD',
    ];

    protected static string $builder = ClassBuilder::class;

    protected $casts = [
        'CLASS_END_DATE' => 'date',
        'CLASS_START_DATE' => 'date',
        'CLASS_CANCEL_DATE' => 'date',
        'CLASS_ENRL_CAP' => 'integer',
        'CLASS_ENRL_TOT' => 'integer',
        'CASSC_UNITS_MINIMUM' => 'float',
    ];

    protected ?string $combinedIdValue = null;

    protected ?Collection $combinedSections = null;

    protected array $defaultColumns = [
        'CASSC_GRADING_BASIS',
        'CASSC_HONORS_YN',
        'CASSC_UNITS_MINIMUM',
        'CLASS_ACAD_CAREER',
        'CLASS_ACAD_ORG',
        'CLASS_ACAD_ORG_LDESC',
        'CLASS_ASSOCIATED_CLASS',
        'CLASS_CANCEL_DATE',
        'CLASS_CATALOG_NBR',
        'CLASS_CLASS_NAME',
        'CLASS_CLASS_NBR',
        'CLASS_CLASS_STAT',
        'CLASS_COMBINED_MAJOR_YN',
        'CLASS_COMPONENT_CD',
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_DAY_EVE',
        'CLASS_DESCR',
        'CLASS_END_DATE',
        'CLASS_ENRL_CAP',
        'CLASS_ENRL_STAT',
        'CLASS_ENRL_TOT',
        'CLASS_INSTITUTION_CD',
        'CLASS_INSTRUCTION_MODE',
        'CLASS_LOCATION_CD',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_START_DATE',
        'CLASS_SUBJECT_CD',
        'CLASS_TERM_CD',
        'CLASS_TYPE_CD',
    ];

    protected array $relationMap = [
        Relations::MATERIALS => Material::class,
        Relations::ENROLLMENTS => Enrollment::class,
        Relations::MEETINGS => Meeting::class,
        Relations::CLASS_INSTRUCTORS => ClassInstructor::class,
        Relations::TERM => Term::class,
        Relations::CLASS_NOTES => ClassNote::class,
        Relations::COURSE_NOTES => CourseNote::class,
        Relations::FEES => Fee::class,
        Relations::OTHER => \Smorken\Sis\Db\Core\Klass\Klass::class,
        Relations::COMBINED_SECTIONS => \Smorken\Sis\Db\Core\Klass\Klass::class,
    ];

    protected $table = 'CDS_CLASS_TBL';

    public function combinedId(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value): string {
                if ($this->combinedIdValue === null) {
                    $this->combinedIdValue = $this->queryCombinedIdValue();
                }

                return $this->combinedIdValue;
            }
        );
    }

    public function enrollments(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'ENRL_TERM_CD',
                'ENRL_CLASS_NBR',
            ],
            [
                'CLASS_TERM_CD',
                'CLASS_CLASS_NBR',
            ]
        )
            ->defaultColumns();
    }

    public function getMaterialsAttribute(): Collection
    {
        return $this->other->materials;
    }

    public function mergeOther(): \Smorken\Sis\Contracts\Base\Klass\Klass
    {
        // @phpstan-ignore method.notFound
        $this->other->setRelation('other', $this);

        return $this->other->mergeOther();
    }

    protected function queryCombinedIdValue(): string
    {
        return $this->other?->combined_id ?? '';
    }
}
