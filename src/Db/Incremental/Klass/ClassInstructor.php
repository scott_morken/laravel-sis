<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:43 PM
 */

namespace Smorken\Sis\Db\Incremental\Klass;

use Awobaz\Compoships\Compoships;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\ClassInstructorAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Incremental\ClassInstructorModifiers;
use Smorken\Sis\Builders\Incremental\ClassInstructorBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Core\Instructor\Position;
use Smorken\Sis\Db\Incremental\Person\Person;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new ClassInstructorModifiers), MapAttributeNames(new ClassInstructorAttributeMap)]
class ClassInstructor extends Base implements \Smorken\Sis\Contracts\Base\Klass\ClassInstructor
{
    use Compoships, HasCollegeIdAttribute, HasStudentIdAttribute;

    protected array $addAttributeMutatedAttributes = ['role_description', 'type_description'];

    protected array $attributeNames = [
        'collegeId' => 'CLASS_INSTITUTION_CD',
        'studentId' => 'CLASSM_INSTRUCTOR_EMPLID',
    ];

    protected static string $builder = ClassInstructorBuilder::class;

    protected $casts = ['CLASSM_INSTR_LOAD_FACTOR' => 'float', 'CLASSM_WEEK_WORKLOAD_HRS' => 'float'];

    protected array $defaultColumns = [
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_INSTITUTION_CD',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_TERM_CD',
        'CLASSM_ASSIGN_TYPE',
        'CLASSM_CLASS_MTG_NBR',
        'CLASSM_INSTR_ASSIGN_SEQ',
        'CLASSM_INSTR_LOAD_FACTOR',
        'CLASSM_WEEK_WORKLOAD_HRS',
        'CLASSM_INSTR_NAME_FLAST',
        'CLASSM_INSTR_TYPE',
        'CLASSM_INSTRUCTOR_EMPLID',
        'CLASSM_INSTRUCTOR_NAME',
        'CLASSM_INSTRUCTOR_ROLE',
    ];

    protected array $relationMap = [
        Relations::KLASS => Klass::class,
        Relations::MEETINGS => Meeting::class,
        Relations::INSTRUCTOR => Person::class,
        Relations::INSTRUCTOR_POSITIONS => Position::class,
    ];

    protected $table = 'CDS_CLASS_INSTR_TBL';

    public function class(): \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
            ->defaultColumns();
    }

    public function instructor(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'CLASSM_INSTRUCTOR_EMPLID', 'PERS_EMPLID')
            ->defaultColumns();
    }

    public function meetings(): \Awobaz\Compoships\Database\Eloquent\Relations\HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
                'CLASSM_CLASS_MTG_NBR',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
                'CLASSM_CLASS_MTG_NBR',
            ]
        )
            ->defaultColumns();
    }

    public function positions(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany($rel, 'EMPLID', 'PERS_EMPLID')
            ->defaultColumns();
    }
}
