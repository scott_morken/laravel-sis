<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:42 PM
 */

namespace Smorken\Sis\Db\Incremental\Klass;

use Awobaz\Compoships\Compoships;
use Awobaz\Compoships\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\MeetingAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Incremental\MeetingModifiers;
use Smorken\Sis\Builders\Incremental\MeetingBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Db\Core\Physical\Facility;
use Smorken\Sis\Enums\Instructor\Roles;
use Smorken\Sis\Enums\Relations;
use Smorken\Sis\VO\Days;

#[ModifyValuesAttribute(new MeetingModifiers), MapAttributeNames(new MeetingAttributeMap)]
class Meeting extends Base implements \Smorken\Sis\Contracts\Base\Klass\Meeting
{
    use Compoships, HasCollegeIdAttribute, HasStartEndDateAttributes, HasTermIdAttribute;

    protected array $addAttributeMutatedAttributes = [
        'days_to_array',
        'days_to_array_raw',
        'days_to_string',
    ];

    protected array $attributeNames = [
        'collegeId' => 'CLASS_INSTITUTION_CD',
        'endDate' => 'CLASSM_END_DATE',
        'startDate' => 'CLASSM_START_DATE',
        'termId' => 'CLASS_TERM_CD',
    ];

    protected static string $builder = MeetingBuilder::class;

    protected $casts = [
        'CLASSM_START_DATE' => 'date', 'CLASSM_END_DATE' => 'date', 'CLASSM_MEETING_TIME_END' => 'datetime',
        'CLASSM_MEETING_TIME_START' => 'datetime',
    ];

    protected array $defaultColumns = [
        'CLASS_COURSE_ID',
        'CLASS_COURSE_OFFER_NBR',
        'CLASS_SECTION',
        'CLASS_SESSION_CD',
        'CLASS_TERM_CD',
        'CLASS_INSTITUTION_CD',
        'CLASSM_BLDG_CD',
        'CLASSM_CLASS_MTG_NBR',
        'CLASSM_DAYS_PATTERN',
        'CLASSM_END_DATE',
        'CLASSM_FACILITY_ID',
        'CLASSM_LOCATION',
        'CLASSM_MEETING_TIME_END',
        'CLASSM_MEETING_TIME_START',
        'CLASSM_ROOM',
        'CLASSM_START_DATE',
        'CLASSM_COURSE_TOPIC_DESCR',
    ];

    protected array $relationMap = [
        Relations::KLASS => Klass::class,
        Relations::CLASS_INSTRUCTORS => ClassInstructor::class,
        Relations::FACILITY => Facility::class,
    ];

    protected $table = 'CDS_CLASS_MTG_TBL';

    public function class(): \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
            ]
        )
            ->defaultColumns();
    }

    public function classInstructors(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
                'CLASSM_CLASS_MTG_NBR',
            ],
            [
                'CLASS_COURSE_ID',
                'CLASS_COURSE_OFFER_NBR',
                'CLASS_TERM_CD',
                'CLASS_SESSION_CD',
                'CLASS_SECTION',
                'CLASSM_CLASS_MTG_NBR',
            ]
        )
            ->defaultColumns();
    }

    public function daysVO(): Attribute
    {
        return Attribute::make(
            get: fn (): Days => new Days($this->CLASSM_DAYS_PATTERN ?? ''),
        );
    }

    public function facility(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo($rel, 'CLASSM_FACILITY_ID', 'FACILITY_ID')->defaultColumns();
    }

    public function primaryClassInstructor(): Attribute
    {
        return Attribute::make(
            get: fn (): ?\Smorken\Sis\Contracts\Base\Klass\ClassInstructor => $this->primaryInstructor(),
        );
    }

    public function primaryInstructor(): ?\Smorken\Sis\Contracts\Base\Klass\ClassInstructor
    {
        $primary = null;
        foreach ($this->classInstructors as $inst) {
            if ($primary === null) {
                $primary = $inst;
            }
            if ($inst->CLASSM_INSTRUCTOR_ROLE === Roles::PRIMARY && $primary->CLASSM_INSTRUCTOR_ROLE !== Roles::PRIMARY) {
                $primary = $inst;
            }
            if ($inst->CLASSM_INSTR_LOAD_FACTOR > $primary->CLASSM_INSTR_LOAD_FACTOR && $inst->CLASSM_INSTRUCTOR_ROLE === Roles::PRIMARY) {
                $primary = $inst;
            }
        }

        return $primary;
    }
}
