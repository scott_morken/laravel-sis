<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:55 PM
 */

namespace Smorken\Sis\Db\Incremental\Student;

use Awobaz\Compoships\Compoships;
use Awobaz\Compoships\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\StudentTermAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Incremental\StudentTermModifiers;
use Smorken\Sis\Builders\Incremental\StudentTermBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Db\Core\Term;
use Smorken\Sis\Db\Incremental\Person\Person;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new StudentTermModifiers), MapAttributeNames(new StudentTermAttributeMap)]
class StudentTerm extends Base implements \Smorken\Sis\Contracts\Base\Student\StudentTerm
{
    use Compoships, HasAcademicCareerAttribute, HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute;

    protected array $addAttributeMutatedAttributes = [
        'cumulative_gpa', 'term_gpa', 'academic_standing_description', 'load_description',
    ];

    protected array $attributeNames = [
        'academicCareer' => 'CTERM_ACAD_CAREER',
        'collegeId' => 'CTERM_INSTITUTION_CODE',
        'studentId' => 'CTERM_EMPLID',
        'termId' => 'CTERM_TERM_CD',
    ];

    protected static string $builder = StudentTermBuilder::class;

    protected $casts = [
        'STFACT_MCCD_TERM_AHRS' => 'float',
        'STFACT_MCCD_TERM_EHRS' => 'float',
        'STFACT_MCCD_TERM_QHRS' => 'float',
        'STFACT_MCCD_TERM_QPTS' => 'float',
        'STFACT_MCCD_TERM_CLOCK_HRS' => 'float',
        'STFACT_MCCD_CUM_AHRS' => 'float',
        'STFACT_MCCD_CUM_EHRS' => 'float',
        'STFACT_MCCD_CUM_QHRS' => 'float',
        'STFACT_MCCD_CUM_QPTS' => 'float',
        'STFACT_MCCD_CUM_CLOCK_HRS' => 'float',
    ];

    protected array $defaultColumns = [
        'CTERM_EMPLID',
        'CTERM_TERM_CD',
        'CTERM_INSTITUTION_CODE',
        'CTERM_ACAD_CAREER',
        'STFACT_MCCD_TERM_AHRS',
        'STFACT_MCCD_TERM_EHRS',
        'STFACT_MCCD_TERM_QHRS',
        'STFACT_MCCD_TERM_QPTS',
        'STFACT_MCCD_TERM_CLOCK_HRS',
        'STFACT_MCCD_CUM_AHRS',
        'STFACT_MCCD_CUM_EHRS',
        'STFACT_MCCD_CUM_QHRS',
        'STFACT_MCCD_CUM_QPTS',
        'STFACT_MCCD_CUM_CLOCK_HRS',
        'CTERM_ACAD_STNDNG_ACTN',
        'CTERM_ACAD_LOAD_CODE',
    ];

    protected array $relationMap = [
        Relations::STUDENT => Person::class,
        Relations::TERM => Term::class,
        Relations::ENROLLMENTS => Enrollment::class,
    ];

    protected $table = 'CDS_STU_TERM_TBL';

    public function enrollments(): HasMany
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasMany(
            $rel,
            ['ENRL_TERM_CD', 'ENRL_INSTITUTION_CD', 'ENRL_ACAD_CAREER', 'ENRL_EMPLID'],
            ['CTERM_TERM_CD', 'CTERM_INSTITUTION_CODE', 'CTERM_ACAD_CAREER', 'CTERM_EMPLID']
        )
            ->defaultColumns();
    }

    public function student(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo(
            $rel,
            'PERS_EMPLID',
            'CTERM_EMPLID'
        )
            ->defaultColumns();
    }

    public function term(): \Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo(
            $rel,
            ['CTERM_TERM_CD', 'CTERM_INSTITUTION_CODE', 'CTERM_ACAD_CAREER'],
            ['STRM', 'INSTITUTION', 'ACAD_CAREER']
        )
            ->defaultColumns();
    }
}
