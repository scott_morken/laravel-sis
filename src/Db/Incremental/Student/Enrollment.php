<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:54 PM
 */

namespace Smorken\Sis\Db\Incremental\Student;

use Awobaz\Compoships\Compoships;
use Awobaz\Compoships\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Model\Attributes\ModifyValuesAttribute;
use Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\EnrollmentAttributeMap;
use Smorken\Sis\Attributes\Modifiers\Incremental\EnrollmentModifiers;
use Smorken\Sis\Builders\Incremental\EnrollmentBuilder;
use Smorken\Sis\Db\Common\Base;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Db\Core\Term;
use Smorken\Sis\Db\Incremental\Klass\Klass;
use Smorken\Sis\Db\Incremental\Person\Person;
use Smorken\Sis\Enums\Relations;

#[ModifyValuesAttribute(new EnrollmentModifiers), MapAttributeNames(new EnrollmentAttributeMap)]
class Enrollment extends Base implements \Smorken\Sis\Contracts\Base\Student\Enrollment
{
    use Compoships, HasAcademicCareerAttribute, HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute;

    protected array $addAttributeMutatedAttributes = ['grading_basis_description', 'status_description'];

    protected array $attributeNames = [
        'academicCareer' => 'ENRL_ACAD_CAREER',
        'collegeId' => 'ENRL_INSTITUTION_CD',
        'studentId' => 'ENRL_EMPLID',
        'termId' => 'ENRL_TERM_CD',
    ];

    protected static string $builder = EnrollmentBuilder::class;

    protected $casts = [
        'ENRL_GRADE_DATE' => 'date', 'ENRL_UNITS_TAKEN' => 'float',
        'ENRL_GRADE_POINTS' => 'float',
        'ENRL_OEE_START_DATE' => 'date', 'ENRL_OEE_END_DATE' => 'date',
    ];

    protected array $defaultColumns = [
        'ENRL_ACAD_CAREER',
        'ENRL_ACTN_RSN_LAST_CD',
        'ENRL_ACTN_RSN_LAST_LDESC',
        'ENRL_CLASS_NBR',
        'ENRL_EMPLID',
        'ENRL_EARNED_CREDIT',
        'ENRL_GRADE_DATE',
        'ENRL_GRADE_POINTS',
        'ENRL_GRADING_BASIS',
        'ENRL_INCLUDE_IN_GPA',
        'ENRL_INSTITUTION_CD',
        'ENRL_OFFICIAL_GRADE',
        'ENRL_STATUS_REASON_CD',
        'ENRL_TERM_CD',
        'ENRL_UNITS_TAKEN',
        'ENRL_OEE_START_DATE',
        'ENRL_OEE_END_DATE',
    ];

    protected array $relationMap = [
        Relations::KLASS => Klass::class,
        Relations::STUDENT => Person::class,
        Relations::TERM => Term::class,
    ];

    protected $table = 'CDS_STU_CLASS_TBL';

    public function class(): BelongsTo
    {
        $klass = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo(
            $klass,
            ['ENRL_CLASS_NBR', 'ENRL_TERM_CD'],
            ['CLASS_CLASS_NBR', 'CLASS_TERM_CD']
        )
            ->defaultColumns();
    }

    public function student(): HasOne
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->hasOne($rel, 'PERS_EMPLID', 'ENRL_EMPLID')
            ->defaultColumns();
    }

    public function term(): BelongsTo
    {
        $rel = $this->getRelationClass(__FUNCTION__);

        return $this->belongsTo(
            $rel,
            ['ENRL_TERM_CD', 'ENRL_INSTITUTION_CD', 'ENRL_ACAD_CAREER'],
            ['STRM', 'INSTITUTION', 'ACAD_CAREER']
        )
            ->defaultColumns();
    }
}
