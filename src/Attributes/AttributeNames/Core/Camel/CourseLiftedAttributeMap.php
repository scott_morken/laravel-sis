<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class CourseLiftedAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'academicCareer' => 'COFFR_ACAD_CAREER',
            'academicGroup' => 'COFFR_ACAD_GROUP',
            'academicOrgId' => 'COFFR_ACAD_ORG',
            'campusCode' => 'COFFR_CAMPUS',
            'catalogNumber' => 'COFFR_CATALOG_NBR',
            'catalogPrint' => 'COFFR_CATALOG_PRINT',
            'classId' => 'COFFR_CLASS_SID',
            'approved' => 'COFFR_COURSE_APPROVED',
            'effectiveDate' => 'COFFR_CRSE_EFFDT',
            'courseId' => 'COFFR_CRSE_ID',
            'offeringNumber' => 'COFFR_CRSE_OFFER_NBR',
            'collegeId' => 'COFFR_INSTITUTION',
            'id' => 'COFFR_REC_KEY',
            'schedulePrint' => 'COFFR_SCHEDULE_PRINT',
            'subject' => 'COFFR_SUBJECT',
            'catalogId' => 'CRSE_CATALOG_SID',
            // from catalog
            'description' => 'CRSE_DESCR',
            'consentCode' => 'CRSE_CONSENT',
            'componentCode' => 'CRSE_COMPONENT',
            'units' => 'CRSE_UNITS_MINIMUM',
            'unitsMax' => 'CRSE_UNITS_MAXIMUM',
            'gradingBasisCode' => 'CRSE_GRADING_BASIS',
            'notes' => 'CRSE_COURSE_NOTES',
        ];
    }
}
