<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class EmailAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'EMAIL_REC_KEY',
            'studentId' => 'EMAIL_EMPLID',
            'typeCode' => 'EMAIL_ADDR_TYPE',
            'typeDescription' => 'EMAIL_ADDR_TYPE_LDESC',
            'preferred' => 'EMAIL_PREF_EMAIL_FLAG',
            'email' => 'EMAIL_EMAIL_ADDR',
        ];
    }
}
