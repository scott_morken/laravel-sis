<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class MaterialAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'TBMAT_SID',
            'sequenceNumber' => 'TBMAT_SEQ_NBR',
            'typeCode' => 'TBMAT_TYPE_CD',
            'typeDescription' => 'TBMAT_TYPE_CD_LDESC',
            'statusCode' => 'TBMAT_STATUS_CD',
            'statusDescription' => 'TBMAT_STATUS_LDESC',
            'title' => 'TBMAT_TITLE',
            'isbn' => 'TBMAT_ISBN_NBR',
            'author' => 'TBMAT_AUTHOR',
            'publisher' => 'TBMAT_PUBLISHER',
            'edition' => 'TBMAT_EDITION',
            'yearPublished' => 'TBMAT_YEAR_PUBLISHED',
            'price' => 'TBMAT_PRICE',
            'notes' => 'TBMAT_NOTES',
        ];
    }
}
