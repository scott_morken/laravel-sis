<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class FeeAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'CLSF_SID',
            'classId' => 'CLASS_SID',
            'collegeId' => 'CLSBF_SETID', //college id
            'courseId' => 'CLSBF_CRSE_ID',
            'offeringNumber' => 'CLSBF_CRSE_OFFER_NBR',
            'termId' => 'CLSBF_STRM',
            'sessionCode' => 'CLSBF_SESSION_CODE',
            'section' => 'CLSBF_CLASS_SECTION',
            'component' => 'CLSBF_COMPONENT',
            'accountType' => 'CLSBF_ACCOUNT_TYPE_SF',
            'itemType' => 'CLSBF_ITEM_TYPE',
            'shortDescription' => 'CLSBF_ITEM_TYPE_SDESC',
            'description' => 'CLSBF_ITEM_TYPE_LDESC',
            'amount' => 'CLSBF_FLAT_AMT',
            'currency' => 'CLSBF_CURRENCY_CD',
        ];
    }
}
