<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class DegreeAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'collegeId' => 'INSTITUTION',
            'academicProgramCode' => 'ACAD_PROG',
            'planTypeCode' => 'ACAD_PLAN_TYPE',
            'planCode' => 'ACAD_PLAN',
            'effectiveDate' => 'EFFDT',
            'statusCode' => 'EFF_STATUS',
            'termActive' => 'FIRST_TERM_VALID',
            'description' => 'DESCR',
            'degreeCode' => 'DEGREE',
            'degreeDescription' => 'DEGREE_DESCR',
            'diplomaDescription' => 'DIPLOMA_DESCR',
            'friendlyDescription' => 'TRNSCR_DESCR',
            'creditsRequired' => 'MIN_UNITS_REQD',
            'planDescription' => 'PLAN_DESCRIPTION',
        ];
    }
}
