<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

class PersonAttributeMap extends \Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\PersonAttributeMap
{
    protected function getMapArray(): array
    {
        return [...parent::getMapArray(), 'id' => 'EMPLID', 'email' => 'EMAIL_EMAIL_ADDR'];
    }
}
