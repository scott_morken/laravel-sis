<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ProgramAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'studentId' => 'STRUC_EMPLID',
            'academicCareer' => 'STRUC_ACAD_CAREER',
            'sequenceNumber' => 'STRUC_STDNT_CAR_NBR',
            'collegeId' => 'STRUC_INSTITUTION',
            'programCode' => 'STRUC_ACAD_PROG',
            'programDescription' => 'STRUC_ACAD_PROG_LDESC',
            'programStatusCode' => 'STRUC_ACAD_PROG_STATUS',
            'programStatusDescription' => 'STRUC_ACAD_PROG_STATUS_LDESC',
            'lastActionCode' => 'STRUC_PROG_ACTION',
            'lastActionDescription' => 'STRUC_PROG_ACTION_LDESC',
            'lastReasonCode' => 'STRUC_PROG_REASON',
            'lastReasonDescription' => 'STRUC_PROG_REASON_LDESC',
            'admitTerm' => 'STRUC_PROG_ADMIT_TERM',
            'planCode' => 'STRUC_ACAD_PLAN',
            'planDescription' => 'STRUC_ACAD_PLAN_LDESC',
            'planTypeCode' => 'STRUC_PLAN_TYPE',
            'planTypeDescription' => 'STRUC_PLAN_TYPE_LDESC',
            'awardTypeCode' => 'STRUC_ACAD_PLAN_DEGREE',
            'expectedCheckoutTerm' => 'STRUC_EXP_GRAD_TERM',
            'interestCode' => 'STRUC_ACAD_INTEREST',
            'interestDescription' => 'STRUC_ACAD_INTEREST_LDESC',
            'checkoutCode' => 'STRUC_DEGR_CHECKOUT_STAT',
            'checkoutDescription' => 'STRUC_DEGR_CHECKOUT_LDESC',
            'subPlanCode' => 'STRUC_ACAD_SUB_PLAN',
            'subPlanDescription' => 'STRUC_ACAD_SUB_PLAN_LDESC',
            'subPlanTypeCode' => 'STRUC_ACAD_SUBPLAN_TYPE',
            'subPlanTypeDescription' => 'STRUC_ACAD_SUBPLAN_TYPE_LDESC',
        ];
    }
}
