<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

class StudentTermAttributeMap extends \Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\StudentTermAttributeMap
{
    protected function getMapArray(): array
    {
        return [
            ...parent::getMapArray(),
            'studentId' => 'EMPLID',
            'cumulativeGpa' => 'STFACT_CUM_GPA',
            'termGpa' => 'STFACT_TERM_GPA',
            'academicStandingDescription' => 'CTERM_ACAD_STDNG_LDESC',
            'loadDescription' => 'CTERM_ACAD_LOAD_LDESC',
            'inProgressGpa' => 'STFACT_TOT_INPROG_GPA',
            'inProgressNoGpa' => 'STFACT_TOT_INPROG_NOGPA',
        ];
    }
}
