<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class CourseCatalogAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'CRSE_REC_KEY',
            'catalogId' => 'CRSE_CATALOG_SID',
            'courseId' => 'CRSE_CRSE_ID',
            'description' => 'CRSE_DESCR',
            'consentCode' => 'CRSE_CONSENT',
            'units' => 'CRSE_UNITS_MINIMUM', //credits
            'unitsMax' => 'CRSE_UNITS_MAXIMUM',
            'gradingBasisCode' => 'CRSE_GRADING_BASIS',
            'componentCode' => 'CRSE_COMPONENT',
            'notes' => 'CRSE_COURSE_NOTES',
            'approvalDate' => 'CRSE_GOV_BD_APPROVAL_DATE',
            'effectiveDate' => 'CRSE_EFFDT',
        ];
    }
}
