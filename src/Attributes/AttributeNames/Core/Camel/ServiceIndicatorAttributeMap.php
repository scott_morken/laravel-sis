<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ServiceIndicatorAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'studentId' => 'SRVC_EMPLID',
            'createdAt' => 'SRVC_IND_DATETIME',
            'createdBy' => 'SRVC_OPRID',
            'collegeId' => 'SRVC_INSTITUTION',
            'code' => 'SRVC_SERVICE_IND_CD',
            'description' => 'SRVC_SERVICE_IND_LDESC',
            'reasonCode' => 'SRVC_SERVICE_IND_REASON',
            'reasonDescription' => 'SRVC_SERVICE_IND_REASON_LDESC',
            'activeTermId' => 'SRVC_SERVICE_IND_ACT_TERM',
            'activeDate' => 'SRVC_SERVICE_IND_ACTIVE_DT',
            'positive' => 'SRVC_POSITIVE_SRVC_INDICATOR',
            'amount' => 'SRVC_AMOUNT',
            'comments' => 'SRVC_COMM_COMMENTS',
            'contact' => 'SRVC_CONTACT',
            'departmentCode' => 'SRVC_DEPARTMENT',
            'departmentDescription' => 'SRVC_DEPARTMENT_LDESC',
            'dueDate' => 'SRVC_EXT_DUE_DT',
        ];
    }
}
