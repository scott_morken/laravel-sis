<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ClassAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'gradingBasisCode' => 'CASSC_GRADING_BASIS',
            'honors' => 'CASSC_HONORS_YN',
            'units' => 'CASSC_UNITS_MINIMUM',
            'academicCareer' => 'CLASS_ACAD_CAREER',
            'academicOrgId' => 'CLASS_ACAD_ORG',
            'academicOrgDescription' => 'CLASS_ACAD_ORG_LDESC',
            'associatedClassNumber' => 'CLASS_ASSOCIATED_CLASS',
            'campusCode' => 'CLASS_CAMPUS_CD',
            'campusDescription' => 'CLASS_CAMPUS_LDESC',
            'cancelDate' => 'CLASS_CANCEL_DATE',
            'catalogNumber' => 'CLASS_CATALOG_NBR',
            'className' => 'CLASS_CLASS_NAME',
            'classNumber' => 'CLASS_CLASS_NBR',
            'statusCode' => 'CLASS_CLASS_STAT',
            'combinedId' => 'CLASS_COMBINED_ID',
            'combinedMajor' => 'CLASS_COMBINED_MAJOR_YN',
            'combinedSection' => 'CLASS_COMBINED_SECTION',
            'componentCode' => 'CLASS_COMPONENT_CD',
            'courseId' => 'CLASS_COURSE_ID',
            'offeringNumber' => 'CLASS_COURSE_OFFER_NBR',
            'dayEveningCode' => 'CLASS_DAY_EVE',
            'friendlyDescription' => 'CLASS_DESCR',
            'description' => 'CLASS_DESCR',
            'endDate' => 'CLASS_END_DATE',
            'enrolledCap' => 'CLASS_ENRL_CAP',
            'enrollmentStatusCode' => 'CLASS_ENRL_STAT',
            'enrolled' => 'CLASS_ENRL_TOT',
            'collegeId' => 'CLASS_INSTITUTION_CD',
            'instructionModeCode' => 'CLASS_INSTRUCTION_MODE',
            'locationCode' => 'CLASS_LOCATION_CD',
            'section' => 'CLASS_SECTION',
            'sessionCode' => 'CLASS_SESSION_CD',
            'startDate' => 'CLASS_START_DATE',
            'subject' => 'CLASS_SUBJECT_CD',
            'termId' => 'CLASS_TERM_CD',
            'typeCode' => 'CLASS_TYPE_CD',
            'componentDescription' => 'CLASS_COMPONENT_LDESC',
            'dayEveningDescription' => 'CLASS_DAY_EVE_LDESC',
            'statusDescription' => 'CLASS_CLASS_STAT_LDESC',
            'enrollmentStatusDescription' => 'CLASS_ENRL_STAT_LDESC',
            'instructionModeDescription' => 'CLASS_INSTRUCTION_MODE_LDESC',
            'gradingBasisDescription' => 'CASSC_GRADING_BASIS_LDESC',
            'typeDescription' => 'CLASS_TYPE_LDESC',
            'sessionDescription' => 'CLASS_SESSION_LDESC',
            'consentCode' => 'CLASS_CONSENT',
            'consentDescription' => 'CLASS_CONSENT_LDESC',
            'departmentId' => 'CLASS_DEPTID_EXPENSE_CD',
            'dualEnroll' => 'CLASS_HS_DUAL_ENROLL_YN',
            'dualEnrollSponsorCode' => 'CLASS_HS_DUAL_SPONSOR',
            'dualEnrollSponsorDescription' => 'CLASS_HIGH_SCHOOL_LDESC',
            'dualEnrollSponsorShortDescription' => 'CLASS_HIGH_SCHOOL_SDESC',
            'loadCalculationCode' => 'CLASS_LOAD_CALC_CODE',
            'loadCalculationDescription' => 'CLASS_LOAD_CALC_SDESC',
            'locationDescription' => 'CLASS_LOCATION_LDESC',
            'majorClassName' => 'CLASS_MAJOR_CLASS_NAME',
            'majorClassNumber' => 'CLASS_MAJOR_CLASS_NBR',
            'primaryInstructorId' => 'CLASS_INSTRUCTOR_EMPLID',
            'primaryInstructorName' => 'CLASS_INSTRUCTOR_NAME',
            'rollover' => 'CLASS_CLASS_ROLL_YN',
            'schedulePrint' => 'CLASS_SCHEDULE_PRINT',
            'weekWorkloadHours' => 'COMP_WEEK_WORKLOAD_HRS',
            'totalWorkloadHours' => 'CLASS_TOT_WORKLOAD_HRS',
            'majorClass' => 'major_class',
            'combinedClassesString' => 'combined_classes_string',
        ];
    }
}
