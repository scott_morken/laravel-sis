<?php
declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class AddressAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'ADDR_REC_KEY',
            'studentId' => 'ADDR_EMPLID',
            'type' => 'ADDR_ADDRESS_TYPE',
            'country' => 'ADDR_COUNTRY',
            'countryDescription' => 'ADDR_COUNTRY_LDESC',
            'address1' => 'ADDR_ADDRESS1',
            'address2' => 'ADDR_ADDRESS2',
            'address3' => 'ADDR_ADDRESS3',
            'city' => 'ADDR_CITY',
            'county' => 'ADDR_COUNTY',
            'state' => 'ADDR_STATE',
            'stateDescription' => 'ADDR_STATE_DESC',
            'postal' => 'ADDR_POSTAL',
            'geoCode' => 'ADDR_GEO_CODE',
            'email' => 'ADDR_EMAIL_ADDR',
            'emailFlag' => 'ADDR_PREF_EMAIL_FLAG',
            'phoneCountryCode' => 'ADDR_PHONE_COUNTRY_CD',
            'phone' => 'ADDR_PHONE_NUMBER',
            'phoneExtension' => 'ADDR_PHONE_EXTENSION',
        ];
    }

}