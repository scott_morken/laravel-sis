<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

class EnrollmentAttributeMap extends \Smorken\Sis\Attributes\AttributeNames\Incremental\Camel\EnrollmentAttributeMap
{
    protected function getMapArray(): array
    {
        return [
            ...parent::getMapArray(),
            'studentId' => 'EMPLID',
            'earnedCredit' => 'ENRL_EARNED_CREDIT',
            'statusDescription' => 'ENRL_STATUS_REASON_LDESC',
        ];
    }
}
