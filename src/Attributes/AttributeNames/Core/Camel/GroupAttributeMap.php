<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class GroupAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'studentId' => 'STDGR_EMPLID',
            'collegeId' => 'STDGR_INSTITUTION',
            'id' => 'STDGR_STUDENT_GROUP',
            'description' => 'STDGR_STDNT_GROUP_LDESC',
            'startDate' => 'STDGR_START_DATE',
            'endDate' => 'STDGR_END_DATE',
            'statusCode' => 'STDGR_STUDENT_STATUS',
            'statusDescription' => 'STDGR_STUDENT_STATUS_LDESC',
            'comments' => 'STDGR_COMMENTS',
        ];
    }
}
