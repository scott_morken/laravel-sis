<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class TermAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'STRM',
            'description' => 'DESCR',
            'academicCareer' => 'ACAD_CAREER',
            'startDate' => 'TERM_BEGIN_DT',
            'endDate' => 'TERM_END_DT',
            'collegeId' => 'INSTITUTION',
        ];
    }
}
