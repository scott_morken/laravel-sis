<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class FacilityAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'collegeId' => 'FACILITY_SETID',
            'id' => 'FACILITY_ID',
            'status' => 'FACILITY_EFF_STATUS',
            'buildingCode' => 'FACILITY_BLDG_CD',
            'room' => 'FACILITY_ROOM',
            'description' => 'FACILITY_DESCR',
            'shortDescription' => 'FACILITY_DESCRSHORT',
            'groupCode' => 'FACILITY_GROUP',
            'locationCode' => 'FACILITY_LOCATION',
            'capacity' => 'FACILITY_CAPACITY',
            'academicOrgId' => 'FACILITY_ACAD_ORG',
            'groupDescription' => 'group_description',
            'locationDescription' => 'location_description',
        ];
    }
}
