<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class AthleteAttributeMap extends MapToArray
{
    public function getMapArray(): array
    {
        return [
            'collegeId' => 'ATHL_INSTITUTION',
            'studentId' => 'ATHL_EMPLID',
            'sportCode' => 'ATHL_SPORT',
            'sportDescription' => 'ATHL_SPORT_LDESC',
            'statusCode' => 'ATHL_ATHLETIC_PARTIC_CD',
            'statusDescription' => 'ATHL_ATHLETIC_PARTIC_LDESC',
            'ncaaEligible' => 'ATHL_NCAA_ELIGIBLE',
            'currentParticipant' => 'ATHL_CURRENT_PARTICIPANT',
            'startDate' => 'ATHL_START_DATE',
            'endDate' => 'ATHL_END_DATE',
        ];
    }
}
