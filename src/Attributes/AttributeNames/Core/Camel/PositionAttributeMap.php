<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class PositionAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'studentId' => 'EMPLID',
            'recordNumber' => 'EMPL_RCD',
            'departmentId' => 'DEPTID',
            'jobCode' => 'JOBCODE',
            'positionNumber' => 'POSITION_NBR',
            'positionDescription' => 'DESCR',
        ];
    }
}
