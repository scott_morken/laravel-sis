<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ProgramAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'student_id' => 'STRUC_EMPLID',
            'college_id' => 'STRUC_INSTITUTION',
            'program_type' => 'STRUC_ACAD_PROG_LDESC',
            'program_status_code' => 'STRUC_ACAD_PROG_STATUS',
            'program_status' => 'STRUC_ACAD_PROG_STATUS_LDESC',
            'program_last_action' => 'STRUC_PROG_REASON_LDESC',
            'admit_term' => 'STRUC_PROG_ADMIT_TERM',
            'plan_name' => 'STRUC_ACAD_PLAN_LDESC',
            'plan_type' => 'STRUC_PLAN_TYPE_LDESC',
            'award_type' => 'STRUC_ACAD_PLAN_DEGREE',
            'grad_term' => 'STRUC_EXP_GRAD_TERM',
            'grad_status_code' => 'STRUC_DEGR_CHECKOUT_STAT',
            'grad_status_desc' => 'STRUC_DEGR_CHECKOUT_LDESC',
            'interest' => 'STRUC_ACAD_INTEREST_LDESC',
        ];
    }
}
