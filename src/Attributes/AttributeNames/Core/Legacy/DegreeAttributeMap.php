<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class DegreeAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'college_id' => 'INSTITUTION',
            'acad_prog_code' => 'ACAD_PROG',
            'plan_type_code' => 'ACAD_PLAN_TYPE',
            'plan_code' => 'ACAD_PLAN',
            'effective_date' => 'EFFDT',
            'status' => 'EFF_STATUS',
            'term_active' => 'FIRST_TERM_VALID',
            'short_desc' => 'DESCR',
            'degree_code' => 'DEGREE',
            'degree_desc' => 'DEGREE_DESCR',
            'diploma_desc' => 'DIPLOMA_DESCR',
            'transcript_desc' => 'TRNSCR_DESCR',
            'credits_required' => 'MIN_UNITS_REQD',
            'long_desc' => 'PLAN_DESCRIPTION',
        ];
    }
}
