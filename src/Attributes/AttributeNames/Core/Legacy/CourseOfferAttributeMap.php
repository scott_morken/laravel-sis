<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class CourseOfferAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'acad_career' => 'COFFR_ACAD_CAREER',
            'acad_group' => 'COFFR_ACAD_GROUP',
            'acad_org' => 'COFFR_ACAD_ORG',
            'approved' => 'COFFR_COURSE_APPROVED',
            'campus' => 'COFFR_CAMPUS',
            'catalog_id' => 'CRSE_CATALOG_SID',
            'catalog_number' => 'COFFR_CATALOG_NBR',
            'class_id' => 'COFFR_CLASS_SID',
            'college_id' => 'COFFR_INSTITUTION',
            'course_id' => 'COFFR_CRSE_ID',
            'effective_date' => 'COFFR_CRSE_EFFDT',
            'id' => 'COFFR_REC_KEY',
            'offer_number' => 'COFFR_CRSE_OFFER_NBR',
            'subject' => 'COFFR_SUBJECT',
            'catalog_print' => 'COFFR_CATALOG_PRINT',
            'schedule_print' => 'COFFR_SCHEDULE_PRINT',
        ];
    }
}
