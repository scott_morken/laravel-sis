<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class FacilityAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'college_id' => 'FACILITY_SETID',
            'id' => 'FACILITY_ID',
            'status' => 'FACILITY_EFF_STATUS',
            'building_code' => 'FACILITY_BLDG_CD',
            'room' => 'FACILITY_ROOM',
            'description' => 'FACILITY_DESCR',
            'short_descr' => 'FACILITY_DESCRSHORT',
            'group' => 'FACILITY_GROUP',
            'location_code' => 'FACILITY_LOCATION',
            'capacity' => 'FACILITY_CAPACITY',
            'acad_org' => 'FACILITY_ACAD_ORG',
        ];
    }
}
