<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class FeeAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'CLSF_SID',
            'class_id' => 'CLASS_SID',
            'college_id' => 'CLSBF_SETID', //college id
            'course_id' => 'CLSBF_CRSE_ID',
            'course_offer_number' => 'CLSBF_CRSE_OFFER_NBR',
            'term_id' => 'CLSBF_STRM',
            'session_code' => 'CLSBF_SESSION_CODE',
            'section' => 'CLSBF_CLASS_SECTION',
            'component' => 'CLSBF_COMPONENT',
            'account_type' => 'CLSBF_ACCOUNT_TYPE_SF',
            'item_type' => 'CLSBF_ITEM_TYPE',
            'short_desc' => 'CLSBF_ITEM_TYPE_SDESC',
            'desc' => 'CLSBF_ITEM_TYPE_LDESC',
            'amount' => 'CLSBF_FLAT_AMT',
            'currency' => 'CLSBF_CURRENCY_CD',
        ];
    }
}
