<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class PersonAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'EMPLID',
            'alt_id' => 'PERS_OPRID',
            'first_name' => 'firstName',
            'last_name' => 'lastName',
            'email' => 'EMAIL_EMAIL_ADDR',
            'dob' => 'PERS_BIRTHDATE',
            'hrms_id' => 'HRMS_EMPLID',
        ];
    }
}
