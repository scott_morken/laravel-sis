<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ServiceIndicatorAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'student_id' => 'SRVC_EMPLID',
            'svc_datetime' => 'SRVC_IND_DATETIME',
            'entered_by' => 'SRVC_OPRID',
            'college_id' => 'SRVC_INSTITUTION',
            'code' => 'SRVC_SERVICE_IND_CD',
            'description' => 'SRVC_SERVICE_IND_LDESC',
            'reason_code' => 'SRVC_SERVICE_IND_REASON',
            'reason_desc' => 'SRVC_SERVICE_IND_REASON_LDESC',
            'term_id' => 'SRVC_SERVICE_IND_ACT_TERM',
            'active_date' => 'SRVC_SERVICE_IND_ACTIVE_DT',
            'positive' => 'SRVC_POSITIVE_SRVC_INDICATOR',
            'amount' => 'SRVC_AMOUNT',
            'comments' => 'SRVC_COMM_COMMENTS',
            'contact' => 'SRVC_CONTACT',
            'department' => 'SRVC_DEPARTMENT_LDESC',
            'due_date' => 'SRVC_EXT_DUE_DT',
        ];
    }
}
