<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class PositionAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'student_id' => 'EMPLID',
            'record_number' => 'EMPL_RCD',
            'department_id' => 'DEPTID',
            'job_code' => 'JOBCODE',
            'position_number' => 'POSITION_NBR',
            'position_desc' => 'DESCR',
        ];
    }
}
