<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class CourseCatalogAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'CRSE_REC_KEY',
            'catalog_id' => 'CRSE_CATALOG_SID',
            'course_id' => 'CRSE_CRSE_ID',
            'description' => 'CRSE_DESCR',
            'consent' => 'CRSE_CONSENT',
            'units' => 'CRSE_UNITS_MINIMUM', //credits
            'units_max' => 'CRSE_UNITS_MAXIMUM',
            'grading_basis' => 'CRSE_GRADING_BASIS',
            'component_code' => 'CRSE_COMPONENT',
            'notes' => 'CRSE_COURSE_NOTES',
            'approval_date' => 'CRSE_GOV_BD_APPROVAL_DATE',
            'effective_date' => 'CRSE_EFFDT',
        ];
    }
}
