<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class EnrollmentAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'student_id' => 'EMPLID',
            'college_id' => 'ENRL_INSTITUTION_CD',
            'term_id' => 'ENRL_TERM_CD',
            'class_number' => 'ENRL_CLASS_NBR',
            'acad_career' => 'ENRL_ACAD_CAREER',
            'grading_basis' => 'ENRL_GRADING_BASIS',
            'grade' => 'ENRL_OFFICIAL_GRADE',
            'grade_date' => 'ENRL_GRADE_DATE',
            'units_taken' => 'ENRL_UNITS_TAKEN',
            'grade_points' => 'ENRL_GRADE_POINTS',
            'status_code' => 'ENRL_STATUS_REASON_CD',
            'status_desc' => 'ENRL_STATUS_REASON_LDESC',
            'last_action_code' => 'ENRL_ACTN_RSN_LAST_CD',
            'last_action_desc' => 'ENRL_ACTN_RSN_LAST_LDESC',
            'include_in_gpa' => 'ENRL_INCLUDE_IN_GPA',
            'oee_start_date' => 'ENRL_OEE_START_DATE',
            'oee_end_date' => 'ENRL_OEE_END_DATE',
        ];
    }
}
