<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class GroupAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'student_id' => 'STDGR_EMPLID',
            'college_id' => 'STDGR_INSTITUTION',
            'id' => 'STDGR_STUDENT_GROUP',
            'description' => 'STDGR_STDNT_GROUP_LDESC',
            'start_date' => 'STDGR_START_DATE',
            'end_date' => 'STDGR_END_DATE',
            'status_code' => 'STDGR_STUDENT_STATUS',
            'status_desc' => 'STDGR_STUDENT_STATUS_LDESC',
            'comments' => 'STDGR_COMMENTS',
        ];
    }
}
