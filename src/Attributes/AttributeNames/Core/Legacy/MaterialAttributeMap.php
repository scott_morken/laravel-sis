<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class MaterialAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'sequence_number' => 'TBMAT_SEQ_NBR',
            'type_code' => 'TBMAT_TYPE_CD',
            'type_desc' => 'TBMAT_TYPE_CD_LDESC',
            'status_code' => 'TBMAT_STATUS_CD',
            'status_desc' => 'TBMAT_STATUS_LDESC',
            'title' => 'TBMAT_TITLE',
            'isbn' => 'TBMAT_ISBN_NBR',
            'author' => 'TBMAT_AUTHOR',
            'publisher' => 'TBMAT_PUBLISHER',
            'edition' => 'TBMAT_EDITION',
            'year_published' => 'TBMAT_YEAR_PUBLISHED',
            'price' => 'TBMAT_PRICE',
            'notes' => 'TBMAT_NOTES',
        ];
    }
}
