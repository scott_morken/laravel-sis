<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class EmailAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'EMAIL_REC_KEY',
            'student_id' => 'EMAIL_EMPLID',
            'type' => 'EMAIL_ADDR_TYPE',
            'type_description' => 'EMAIL_ADDR_TYPE_LDESC',
            'preferred' => 'EMAIL_PREF_EMAIL_FLAG',
            'email' => 'EMAIL_EMAIL_ADDR',
        ];
    }
}
