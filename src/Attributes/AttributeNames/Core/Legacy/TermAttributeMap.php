<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class TermAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'STRM',
            'full_id' => 'STRM',
            'description' => 'DESCR',
            'acad_career' => 'ACAD_CAREER',
            'start_date' => 'TERM_BEGIN_DT',
            'end_date' => 'TERM_END_DT',
            'college_id' => 'INSTITUTION',
        ];
    }
}
