<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ClassAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'acad_career' => 'CLASS_ACAD_CAREER',
            'acad_org' => 'CLASS_ACAD_ORG',
            'acad_org_desc' => 'CLASS_ACAD_ORG_LDESC',
            'associated_class_number' => 'CLASS_ASSOCIATED_CLASS',
            'cancel_date' => 'CLASS_CANCEL_DATE',
            'catalog_number' => 'CLASS_CATALOG_NBR',
            'class_name' => 'CLASS_CLASS_NAME',
            'class_number' => 'CLASS_CLASS_NBR',
            'college_id' => 'CLASS_INSTITUTION_CD',
            'component_code' => 'CLASS_COMPONENT_CD',
            'component_desc' => 'CLASS_COMPONENT_LDESC',
            'consent' => 'CLASS_CONSENT',
            'consent_desc' => 'CLASS_CONSENT_LDESC',
            'course_offer_number' => 'CLASS_COURSE_OFFER_NBR',
            'day_evening_code' => 'CLASS_DAY_EVE',
            'day_evening_desc' => 'CLASS_DAY_EVE_LDESC',
            'department_id' => 'CLASS_DEPTID_EXPENSE_CD',
            'description' => 'CLASS_DESCR',
            'friendly_description' => 'CLASS_DESCR',
            'dual_enroll_sponsor' => 'CLASS_HS_DUAL_SPONSOR',
            'dual_enroll_descr' => 'CLASS_HIGH_SCHOOL_LDESC',
            'dual_enroll_descr_short' => 'CLASS_HIGH_SCHOOL_SDESC',
            'end_date' => 'CLASS_END_DATE',
            'enrolled' => 'CLASS_ENRL_TOT',
            'enrolled_cap' => 'CLASS_ENRL_CAP',
            'enrollment_status' => 'CLASS_ENRL_STAT',
            'enrollment_status_desc' => 'CLASS_ENRL_STAT_LDESC',
            'grading_basis' => 'CASSC_GRADING_BASIS',
            'instruction_mode' => 'CLASS_INSTRUCTION_MODE',
            'instruction_mode_desc' => 'CLASS_INSTRUCTION_MODE_LDESC',
            'load_calc_code' => 'CLASS_LOAD_CALC_CODE',
            'load_calc_desc' => 'CLASS_LOAD_CALC_SDESC',
            'location_code' => 'CLASS_LOCATION_CD',
            'location_desc' => 'CLASS_LOCATION_LDESC',
            'major_class_name' => 'CLASS_MAJOR_CLASS_NAME',
            'major_class_number' => 'CLASS_MAJOR_CLASS_NBR',
            'primary_instructor_id' => 'CLASS_INSTRUCTOR_EMPLID',
            'primary_instructor_name' => 'CLASS_INSTRUCTOR_NAME',
            'section' => 'CLASS_SECTION',
            'session_code' => 'CLASS_SESSION_CD',
            'session_type' => 'CLASS_SESSION_CD',
            'start_date' => 'CLASS_START_DATE',
            'status' => 'CLASS_CLASS_STAT',
            'status_desc' => 'CLASS_CLASS_STAT_LDESC',
            'subject' => 'CLASS_SUBJECT_CD',
            'term_id' => 'CLASS_TERM_CD',
            'units' => 'CASSC_UNITS_MINIMUM',
            'week_workload_hours' => 'COMP_WEEK_WORKLOAD_HRS',
            'total_workload_hours' => 'CLASS_TOT_WORKLOAD_HRS',
            'combined_id' => 'CLASS_COMBINED_ID',
            'combined_major' => 'CLASS_COMBINED_MAJOR_YN',
            'combined_section' => 'CLASS_COMBINED_SECTION',
            'combined_list' => 'combined_classes_string',
            'dual_enroll' => 'CLASS_HS_DUAL_ENROLL_YN',
            'honors' => 'CLASS_HONORS',
            'rollover' => 'CLASS_CLASS_ROLL_YN',
            'schedule_print' => 'CLASS_SCHEDULE_PRINT',
        ];
    }
}
