<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Core\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class AthleteAttributeMap extends MapToArray
{
    public function getMapArray(): array
    {
        return [
            'college_id' => 'ATHL_INSTITUTION',
            'student_id' => 'ATHL_EMPLID',
            'sport_code' => 'ATHL_SPORT',
            'sport_desc' => 'ATHL_SPORT_LDESC',
            'status_code' => 'ATHL_ATHLETIC_PARTIC_CD',
            'status_desc' => 'ATHL_ATHLETIC_PARTIC_LDESC',
            'ncaa_eligible' => 'ATHL_NCAA_ELIGIBLE',
            'current_participant' => 'ATHL_CURRENT_PARTICIPANT',
            'start_date' => 'ATHL_START_DATE',
            'end_date' => 'ATHL_END_DATE',
        ];
    }
}
