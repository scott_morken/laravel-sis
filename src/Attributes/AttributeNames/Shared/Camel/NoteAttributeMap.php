<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Shared\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class NoteAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'courseId' => 'CLASS_COURSE_ID',
            'offeringNumber' => 'CLASS_COURSE_OFFER_NBR',
            'termId' => 'CLASS_TERM_CD',
            'sequenceNumber' => 'NOTES_SEQ_NBR',
            'noteNumber' => 'NOTES_NOTE_NBR',
            'type' => 'NOTES_TYPE_PUBL_STAF',
            'description' => 'NOTES_DESCR',
            'text' => 'NOTES_NOTE_TEXT',
            'collegeId' => 'CLASS_INSTITUTION_CD',
        ];
    }
}
