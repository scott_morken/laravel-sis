<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ClassInstructorAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'courseId' => 'CLASS_COURSE_ID',
            'offeringNumber' => 'CLASS_COURSE_OFFER_NBR',
            'collegeId' => 'CLASS_INSTITUTION_CD',
            'section' => 'CLASS_SECTION',
            'sessionCode' => 'CLASS_SESSION_CD',
            'termId' => 'CLASS_TERM_CD',
            'assignmentTypeCode' => 'CLASSM_ASSIGN_TYPE',
            'meetingSequenceNumber' => 'CLASSM_CLASS_MTG_NBR',
            'sequenceNumber' => 'CLASSM_INSTR_ASSIGN_SEQ',
            'loadFactor' => 'CLASSM_INSTR_LOAD_FACTOR',
            'workloadHours' => 'CLASSM_WEEK_WORKLOAD_HRS',
            'shortName' => 'CLASSM_INSTR_NAME_FLAST',
            'typeCode' => 'CLASSM_INSTR_TYPE',
            'typeDescription' => 'type_description',
            'instructorId' => 'CLASSM_INSTRUCTOR_EMPLID',
            'fullName' => 'CLASSM_INSTRUCTOR_NAME',
            'roleCode' => 'CLASSM_INSTRUCTOR_ROLE',
            'roleDescription' => 'role_description',
        ];
    }
}
