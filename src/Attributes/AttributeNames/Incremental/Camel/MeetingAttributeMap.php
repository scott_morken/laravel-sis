<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class MeetingAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'courseId' => 'CLASS_COURSE_ID',
            'offeringNumber' => 'CLASS_COURSE_OFFER_NBR',
            'section' => 'CLASS_SECTION',
            'sessionCode' => 'CLASS_SESSION_CD',
            'termId' => 'CLASS_TERM_CD',
            'collegeId' => 'CLASS_INSTITUTION_CD',
            'buildingCode' => 'CLASSM_BLDG_CD',
            'sequenceNumber' => 'CLASSM_CLASS_MTG_NBR',
            'daysPattern' => 'CLASSM_DAYS_PATTERN',
            'endDate' => 'CLASSM_END_DATE',
            'facilityId' => 'CLASSM_FACILITY_ID',
            'locationCode' => 'CLASSM_LOCATION',
            'endTime' => 'CLASSM_MEETING_TIME_END',
            'startTime' => 'CLASSM_MEETING_TIME_START',
            'room' => 'CLASSM_ROOM',
            'startDate' => 'CLASSM_START_DATE',
            'freeFormatTopic' => 'CLASSM_COURSE_TOPIC_DESCR',
            'days' => 'days_to_array',
            'daysRaw' => 'days_to_array_raw',
            'daysToString' => 'days_to_string',
        ];
    }
}
