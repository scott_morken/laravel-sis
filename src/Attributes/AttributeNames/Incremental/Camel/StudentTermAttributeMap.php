<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class StudentTermAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'studentId' => 'CTERM_EMPLID',
            'termId' => 'CTERM_TERM_CD',
            'collegeId' => 'CTERM_INSTITUTION_CODE',
            'academicCareer' => 'CTERM_ACAD_CAREER',
            'termAttemptedHours' => 'STFACT_MCCD_TERM_AHRS',
            'termEarnedHours' => 'STFACT_MCCD_TERM_EHRS',
            'termGpaHours' => 'STFACT_MCCD_TERM_QHRS',
            'termGpaPoints' => 'STFACT_MCCD_TERM_QPTS',
            'termClockHours' => 'STFACT_MCCD_TERM_CLOCK_HRS',
            'cumulativeAttemptedHours' => 'STFACT_MCCD_CUM_AHRS',
            'cumulativeEarnedHours' => 'STFACT_MCCD_CUM_EHRS',
            'cumulativeGpaHours' => 'STFACT_MCCD_CUM_QHRS',
            'cumulativeGpaPoints' => 'STFACT_MCCD_CUM_QPTS',
            'cumulativeClockHours' => 'STFACT_MCCD_CUM_CLOCK_HRS',
            'academicStandingCode' => 'CTERM_ACAD_STNDNG_ACTN',
            'loadCode' => 'CTERM_ACAD_LOAD_CODE',
            'cumulativeGpa' => 'cumulative_gpa',
            'termGpa' => 'term_gpa',
            'academicStandingDescription' => 'academic_standing_description',
            'loadDescription' => 'load_description',
        ];
    }
}
