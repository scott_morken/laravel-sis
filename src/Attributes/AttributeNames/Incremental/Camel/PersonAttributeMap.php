<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class PersonAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'PERS_EMPLID',
            'altId' => 'PERS_OPRID',
            'email' => 'PERS_EMAIL_ADDR',
            'phone' => 'PERS_HOME_PHONE_NBR',
            'dob' => 'PERS_BIRTHDATE',
            'hcmId' => 'HRMS_EMPLID',
        ];
    }
}
