<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Camel;

use Smorken\Sis\Attributes\AttributeNames\Shared\Camel\NoteAttributeMap;

class ClassNoteAttributeMap extends NoteAttributeMap
{
    protected function getMapArray(): array
    {
        return [
            ...parent::getMapArray(),
            'section' => 'CLASS_SECTION',
            'sessionCode' => 'CLASS_SESSION_CD',
        ];
    }
}
