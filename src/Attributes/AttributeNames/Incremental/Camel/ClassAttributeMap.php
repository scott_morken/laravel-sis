<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ClassAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'gradingBasisCode' => 'CASSC_GRADING_BASIS',
            'honors' => 'CASSC_HONORS_YN',
            'units' => 'CASSC_UNITS_MINIMUM',
            'academicCareer' => 'CLASS_ACAD_CAREER',
            'academicOrgId' => 'CLASS_ACAD_ORG',
            'academicOrgDescription' => 'CLASS_ACAD_ORG_LDESC',
            'associatedClassNumber' => 'CLASS_ASSOCIATED_CLASS',
            'campusCode' => 'CLASS_CAMPUS_CD',
            'cancelDate' => 'CLASS_CANCEL_DATE',
            'catalogNumber' => 'CLASS_CATALOG_NBR',
            'className' => 'CLASS_CLASS_NAME',
            'classNumber' => 'CLASS_CLASS_NBR',
            'statusCode' => 'CLASS_CLASS_STAT',
            'combinedMajor' => 'CLASS_COMBINED_MAJOR_YN',
            'componentCode' => 'CLASS_COMPONENT_CD',
            'courseId' => 'CLASS_COURSE_ID',
            'offeringNumber' => 'CLASS_COURSE_OFFER_NBR',
            'dayEveningCode' => 'CLASS_DAY_EVE',
            'friendlyDescription' => 'CLASS_DESCR',
            'description' => 'CLASS_DESCR',
            'endDate' => 'CLASS_END_DATE',
            'enrolledCap' => 'CLASS_ENRL_CAP',
            'enrollmentStatusCode' => 'CLASS_ENRL_STAT',
            'enrolled' => 'CLASS_ENRL_TOT',
            'collegeId' => 'CLASS_INSTITUTION_CD',
            'instructionModeCode' => 'CLASS_INSTRUCTION_MODE',
            'locationCode' => 'CLASS_LOCATION_CD',
            'section' => 'CLASS_SECTION',
            'sessionCode' => 'CLASS_SESSION_CD',
            'startDate' => 'CLASS_START_DATE',
            'subject' => 'CLASS_SUBJECT_CD',
            'termId' => 'CLASS_TERM_CD',
            'typeCode' => 'CLASS_TYPE_CD',
            'componentDescription' => 'component_description',
            'dayEveningDescription' => 'day_evening_description',
            'statusDescription' => 'status_description',
            'enrollmentStatusDescription' => 'enrollment_status_description',
            'instructionModeDescription' => 'instruction_mode_description',
            'gradingBasisDescription' => 'grading_basis_description',
            'typeDescription' => 'type_description',
            'sessionDescription' => 'session_description',
        ];
    }
}
