<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Camel;

use Smorken\Sis\Attributes\AttributeNames\Shared\Camel\NoteAttributeMap;

class CourseNoteAttributeMap extends NoteAttributeMap {}
