<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class EnrollmentAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'academicCareer' => 'ENRL_ACAD_CAREER',
            'lastActionCode' => 'ENRL_ACTN_RSN_LAST_CD',
            'lastActionDescription' => 'ENRL_ACTN_RSN_LAST_LDESC',
            'classNumber' => 'ENRL_CLASS_NBR',
            'studentId' => 'ENRL_EMPLID',
            'earnedCredit' => 'ENRL_EARNED_CREDIT',
            'gradeDate' => 'ENRL_GRADE_DATE',
            'gradePoints' => 'ENRL_GRADE_POINTS',
            'gradingBasisCode' => 'ENRL_GRADING_BASIS',
            'gradingBasisDescription' => 'grading_basis_description',
            'includeInGpa' => 'ENRL_INCLUDE_IN_GPA',
            'collegeId' => 'ENRL_INSTITUTION_CD',
            'grade' => 'ENRL_OFFICIAL_GRADE',
            'statusCode' => 'ENRL_STATUS_REASON_CD',
            'statusDescription' => 'status_description',
            'termId' => 'ENRL_TERM_CD',
            'unitsTaken' => 'ENRL_UNITS_TAKEN',
            'oeeStartDate' => 'ENRL_OEE_START_DATE',
            'oeeEndDate' => 'ENRL_OEE_END_DATE',
        ];
    }
}
