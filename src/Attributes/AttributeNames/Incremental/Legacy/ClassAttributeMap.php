<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ClassAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'acad_career' => 'CLASS_ACAD_CAREER',
            'acad_org' => 'CLASS_ACAD_ORG',
            'acad_org_desc' => 'CLASS_ACAD_ORG_LDESC',
            'associated_class_number' => 'CLASS_ASSOCIATED_CLASS',
            'campus_code' => 'CLASS_CAMPUS_CD',
            'cancel_date' => 'CLASS_CANCEL_DATE',
            'catalog_number' => 'CLASS_CATALOG_NBR',
            'class_name' => 'CLASS_CLASS_NAME',
            'class_number' => 'CLASS_CLASS_NBR',
            'college_id' => 'CLASS_INSTITUTION_CD',
            'component_code' => 'CLASS_COMPONENT_CD',
            'course_id' => 'CLASS_COURSE_ID',
            'course_offer_number' => 'CLASS_COURSE_OFFER_NBR',
            'day_evening_code' => 'CLASS_DAY_EVE',
            'description' => 'CLASS_DESCR',
            'friendly_description' => 'CLASS_DESCR',
            'end_date' => 'CLASS_END_DATE',
            'enrolled' => 'CLASS_ENRL_TOT',
            'enrolled_cap' => 'CLASS_ENRL_CAP',
            'enrollment_status' => 'CLASS_ENRL_STAT',
            'grading_basis' => 'CASSC_GRADING_BASIS',
            'instruction_mode' => 'CLASS_INSTRUCTION_MODE',
            'location_code' => 'CLASS_LOCATION_CD',
            'section' => 'CLASS_SECTION',
            'session_code' => 'CLASS_SESSION_CD',
            'start_date' => 'CLASS_START_DATE',
            'status' => 'CLASS_CLASS_STAT',
            'subject' => 'CLASS_SUBJECT_CD',
            'term_id' => 'CLASS_TERM_CD',
            'units' => 'CASSC_UNITS_MINIMUM',
            'type_code' => 'CLASS_TYPE_CD',
            'combined_major' => 'CLASS_COMBINED_MAJOR_YN',
            'component_desc' => 'component_description',
            'day_evening_desc' => 'day_evening_description',
            'enrollment_status_desc' => 'enrollment_status_description',
            'honors_section' => 'CASSC_HONORS_YN',
            'instruction_mode_desc' => 'instruction_mode_description',
            'status_desc' => 'status_description',
        ];
    }
}
