<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class MeetingAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'meeting_id' => 'CLASSM_CLASS_MTG_NBR',
            'college_id' => 'CLASS_INSTITUTION_CD',
            'building_code' => 'CLASSM_BLDG_CD',
            'location_code' => 'CLASSM_LOCATION',
            'room' => 'CLASSM_FACILITY_ID',
            'free_format_topic' => 'CLASSM_COURSE_TOPIC_DESCR',
            'start_date' => 'CLASSM_START_DATE',
            'end_date' => 'CLASSM_END_DATE',
            'start_time' => 'CLASSM_MEETING_TIME_START',
            'end_time' => 'CLASSM_MEETING_TIME_END',
            'days_pattern' => 'CLASSM_DAYS_PATTERN',
            'days' => 'days_to_array',
            'days_raw' => 'days_to_array_raw',
        ];
    }
}
