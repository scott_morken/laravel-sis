<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ClassInstructorAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'sequence_number' => 'CLASSM_INSTR_ASSIGN_SEQ',
            'instructor_id' => 'CLASSM_INSTRUCTOR_EMPLID',
            'full_name' => 'CLASSM_INSTRUCTOR_NAME',
            'short_name' => 'CLASSM_INSTR_NAME_FLAST',
            'type_code' => 'CLASSM_INSTR_TYPE',
            'type_desc' => 'type_description',
            'role_code' => 'CLASSM_INSTRUCTOR_ROLE',
            'role_desc' => 'role_description',
            'load' => 'CLASSM_INSTR_LOAD_FACTOR',
            'workload' => 'CLASSM_WEEK_WORKLOAD_HRS',
            'assignment_type_code' => 'CLASSM_ASSIGN_TYPE',
            'college_id' => 'CLASS_INSTITUTION_CD',
        ];
    }
}
