<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class StudentTermAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'student_id' => 'CTERM_EMPLID',
            'term_id' => 'CTERM_TERM_CD',
            'college_id' => 'CTERM_INSTITUTION_CODE',
            'acad_career' => 'CTERM_ACAD_CAREER',
            'cumu_gpa' => 'cumulative_gpa',
            'cumu_attempted_hours' => 'STFACT_MCCD_CUM_AHRS',
            'cumu_earned_hours' => 'STFACT_MCCD_CUM_EHRS',
            'cumu_gpa_hours' => 'STFACT_MCCD_CUM_QHRS',
            'cumu_gpa_points' => 'STFACT_MCCD_CUM_QPTS',
            'term_attempted_hours' => 'STFACT_MCCD_TERM_AHRS',
            'term_earned_hours' => 'STFACT_MCCD_TERM_EHRS',
            'term_gpa_hours' => 'STFACT_MCCD_TERM_QHRS',
            'term_gpa_points' => 'STFACT_MCCD_TERM_QPTS',
            'academic_standing_code' => 'CTERM_ACAD_STNDNG_ACTN',
            'academic_standing_desc' => 'academic_standing_description',
            'load_code' => 'CTERM_ACAD_LOAD_CODE',
            'load_desc' => 'load_description',
        ];
    }
}
