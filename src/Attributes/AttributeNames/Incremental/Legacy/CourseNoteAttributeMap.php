<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class CourseNoteAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'sequence_number' => 'NOTES_SEQ_NBR',
            'note_number' => 'NOTES_NOTE_NBR',
            'college_id' => 'CLASS_INSTITUTION_CD',
            'type' => 'NOTES_TYPE_PUBL_STAF',
            'description' => 'NOTES_DESCR',
            'text' => 'NOTES_NOTE_TEXT',
        ];
    }
}
