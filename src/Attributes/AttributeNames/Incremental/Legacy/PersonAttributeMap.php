<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Legacy;

use Smorken\Model\Attributes\Mappers\MapToArray;

class PersonAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'PERS_EMPLID',
            'alt_id' => 'PERS_OPRID',
            'first_name' => 'firstName',
            'last_name' => 'lastName',
            'email' => 'PERS_EMAIL_ADDR',
            'phone' => 'PERS_HOME_PHONE_NBR',
            'dob' => 'PERS_BIRTHDATE',
            'hrms_id' => 'HRMS_EMPLID',
        ];
    }
}
