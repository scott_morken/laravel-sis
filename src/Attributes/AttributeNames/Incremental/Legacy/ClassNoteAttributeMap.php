<?php

declare(strict_types=1);

namespace Smorken\Sis\Attributes\AttributeNames\Incremental\Legacy;

class ClassNoteAttributeMap extends CourseNoteAttributeMap {}
