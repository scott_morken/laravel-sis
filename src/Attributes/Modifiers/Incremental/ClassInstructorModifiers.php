<?php

namespace Smorken\Sis\Attributes\Modifiers\Incremental;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Sis\Contracts\Base\Klass\ClassInstructor;
use Smorken\Sis\Enums\Instructor\Roles;
use Smorken\Sis\Enums\Instructor\Types;

class ClassInstructorModifiers extends Modifiers
{
    protected function getRole(ClassInstructor $model): string
    {
        return Roles::toArray()[$model->CLASSM_INSTRUCTOR_ROLE] ?? ($model->CLASSM_INSTRUCTOR_ROLE ?? '');
    }

    protected function getType(ClassInstructor $model): string
    {
        return Types::toArray()[$model->CLASSM_INSTR_TYPE] ?? ($model->CLASSM_INSTR_TYPE ?? '');
    }

    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'role_description' => fn (mixed $v, ClassInstructor $model) => $this->getRole($model),
                'type_description' => fn (mixed $v, ClassInstructor $model) => $this->getType($model),
            ],
            'set' => [],
        ];
    }
}
