<?php

namespace Smorken\Sis\Attributes\Modifiers\Incremental;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Sis\Contracts\Base\Student\StudentTerm;
use Smorken\Sis\Enums\Person\AcademicLoadCodes;
use Smorken\Sis\Enums\Person\AcademicStandingActions;

class StudentTermModifiers extends Modifiers
{
    protected function getAcademicStanding(StudentTerm $model): string
    {
        return AcademicStandingActions::toArray()[$model->CTERM_ACAD_STNDNG_ACTN] ?? $model->CTERM_ACAD_STNDNG_ACTN;
    }

    protected function getCumulativeGpa(StudentTerm $model): float
    {
        return $this->getGpa($model->STFACT_MCCD_CUM_QHRS ?? 0, $model->STFACT_MCCD_CUM_QPTS ?? 0);
    }

    protected function getGpa(float $hours, float $points): float
    {
        if ($hours > 0) {
            return (float) number_format($points / $hours, 3);
        }

        return 0;
    }

    protected function getLoad(StudentTerm $model): string
    {
        return AcademicLoadCodes::toArray()[$model->CTERM_ACAD_LOAD_CODE] ?? $model->CTERM_ACAD_LOAD_CODE;
    }

    protected function getTermGpa(StudentTerm $model): float
    {
        return $this->getGpa($model->STFACT_MCCD_TERM_QHRS ?? 0, $model->STFACT_MCCD_TERM_QPTS ?? 0);
    }

    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'cumulative_gpa' => fn (mixed $v, StudentTerm $model): float => $this->getCumulativeGpa($model),
                'term_gpa' => fn (mixed $v, StudentTerm $model): float => $this->getTermGpa($model),
                'academic_standing_description' => fn (
                    mixed $v,
                    StudentTerm $model
                ): string => $this->getAcademicStanding($model),
                'load_description' => fn (mixed $v, StudentTerm $model): string => $this->getLoad($model),
            ],
            'set' => [],
        ];
    }
}
