<?php

namespace Smorken\Sis\Attributes\Modifiers\Incremental;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\TrimModifier;
use Smorken\Sis\Contracts\Base\Klass\Klass;
use Smorken\Sis\Enums\Klass\ComponentCodes;
use Smorken\Sis\Enums\Klass\EnrollmentStatuses;
use Smorken\Sis\Enums\Klass\InstructionModes;
use Smorken\Sis\Enums\Klass\SessionCodes;
use Smorken\Sis\Enums\Klass\Statuses;
use Smorken\Sis\Enums\Klass\TimeOfDays;
use Smorken\Sis\Enums\Klass\TypeCodes;
use Smorken\Sis\Enums\Shared\GradingBases;

class ClassModifiers extends Modifiers
{
    protected function getComponentDescription(Klass $model): string
    {
        return ComponentCodes::toArray()[$model->CLASS_COMPONENT_CD] ?? $model->CLASS_COMPONENT_CD;
    }

    protected function getDayEveningDescription(Klass $model): string
    {
        return TimeOfDays::toArray()[$model->CLASS_DAY_EVE] ?? $model->CLASS_DAY_EVE;
    }

    protected function getEnrollmentStatusDescription(Klass $model): string
    {
        return EnrollmentStatuses::toArray()[$model->CLASS_ENRL_STAT] ?? $model->CLASS_ENRL_STAT;
    }

    protected function getGradingBasisDescription(Klass $model): ?string
    {
        return GradingBases::toArray()[$model->CASSC_GRADING_BASIS] ?? $model->CASSC_GRADING_BASIS;
    }

    protected function getInstructionModeDescription(Klass $model): string
    {
        return InstructionModes::toArray()[$model->CLASS_INSTRUCTION_MODE] ?? $model->CLASS_INSTRUCTION_MODE;
    }

    protected function getSessionDescription(Klass $model): string
    {
        return SessionCodes::toArray()[$model->CLASS_SESSION_CD] ?? $model->CLASS_SESSION_CD;
    }

    protected function getStatusDescription(Klass $model): string
    {
        return Statuses::toArray()[$model->CLASS_CLASS_STAT] ?? $model->CLASS_CLASS_STAT;
    }

    protected function getTypeDescription(Klass $model): string
    {
        return TypeCodes::toArray()[$model->CLASS_TYPE_CD] ?? $model->CLASS_TYPE_CD;
    }

    protected function isYes(mixed $v): bool
    {
        return $v === 'Y';
    }

    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'CLASS_CATALOG_NBR' => new TrimModifier,
                'CLASS_COMBINED_MAJOR_YN' => fn (mixed $v, Klass $model): bool => $this->isYes($v),
                'CASSC_HONORS_YN' => fn (mixed $v, Klass $model): bool => $this->isYes($v),
                'component_description' => fn (mixed $v, Klass $model): string => $this->getComponentDescription($model),
                'day_evening_description' => fn (
                    mixed $v,
                    Klass $model
                ): string => $this->getDayEveningDescription($model),
                'status_description' => fn (mixed $v, Klass $model): string => $this->getStatusDescription($model),
                'enrollment_status_description' => fn (
                    mixed $v,
                    Klass $model
                ): string => $this->getEnrollmentStatusDescription($model),
                'instruction_mode_description' => fn (
                    mixed $v,
                    Klass $model
                ): string => $this->getInstructionModeDescription($model),
                'grading_basis_description' => fn (
                    mixed $v,
                    Klass $model
                ): ?string => $this->getGradingBasisDescription($model),
                'session_description' => fn (
                    mixed $v,
                    Klass $model
                ): string => $this->getSessionDescription($model),
                'type_description' => fn (
                    mixed $v,
                    Klass $model
                ): string => $this->getTypeDescription($model),
            ],
            'set' => [],
        ];
    }
}
