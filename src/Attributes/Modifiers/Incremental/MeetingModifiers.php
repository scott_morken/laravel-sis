<?php

namespace Smorken\Sis\Attributes\Modifiers\Incremental;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\TrimModifier;
use Smorken\Sis\Contracts\Base\Klass\Meeting;

class MeetingModifiers extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'CLASSM_COURSE_TOPIC_DESCR' => new TrimModifier,
                'days_to_array' => fn (mixed $v, Meeting $model): array => $model->daysVO->days(),
                'days_to_array_raw' => fn (mixed $v, Meeting $model): array => $model->daysVO->daysRaw(),
                'days_to_string' => fn (mixed $v, Meeting $model): string => $model->daysVO->daysToString(),
            ],
            'set' => [],
        ];
    }
}
