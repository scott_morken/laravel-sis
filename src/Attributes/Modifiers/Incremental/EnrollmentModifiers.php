<?php

namespace Smorken\Sis\Attributes\Modifiers\Incremental;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Sis\Contracts\Base\Student\Enrollment;
use Smorken\Sis\Enums\Shared\GradingBases;
use Smorken\Sis\Enums\Student\EnrollmentStatuses;
use Smorken\Sis\Enums\YesNo;

class EnrollmentModifiers extends Modifiers
{
    protected function getGradingBasis(Enrollment $model): string
    {
        return GradingBases::toArray()[$model->ENRL_GRADING_BASIS] ?? $model->ENRL_GRADING_BASIS;
    }

    protected function getStatus(Enrollment $model): string
    {
        return EnrollmentStatuses::toArray()[$model->ENRL_STATUS_REASON_CD] ?? $model->ENRL_STATUS_REASON_CD;
    }

    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'ENRL_EARNED_CREDIT' => static fn (mixed $v, Enrollment $model): bool => $v === YesNo::YES,
                'ENRL_INCLUDE_IN_GPA' => static fn (mixed $v, Enrollment $model): bool => $v === YesNo::YES,
                'grading_basis_description' => fn (
                    mixed $v,
                    Enrollment $model
                ): string => $this->getGradingBasis($model),
                'status_description' => fn (mixed $v, Enrollment $model): string => $this->getStatus($model),
            ],
            'set' => [],
        ];
    }
}
