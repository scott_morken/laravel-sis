<?php

namespace Smorken\Sis\Attributes\Modifiers\Core;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\TrimModifier;
use Smorken\Sis\Contracts\Base\Klass\Klass;

class ClassModifiers extends Modifiers
{
    protected function getCombinedString(Klass $model): string
    {
        return str_replace('-', ' ', $model->CLASS_COMBINED_DESCR ?? '');
    }

    protected function getMajorClassName(Klass $model): ?string
    {
        $parts = [$model->CLASS_MAJOR_CLASS_NAME, $model->CLASS_MAJOR_CLASS_NBR];

        return implode(' ', array_filter($parts));
    }

    protected function isCombinedSection(mixed $v): bool
    {
        return $v === 'C';
    }

    protected function isYes(mixed $v): bool
    {
        return $v === 'Y';
    }

    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'CLASS_CATALOG_NBR' => new TrimModifier,
                'CLASS_COMBINED_ID' => new TrimModifier,
                'CLASS_COMBINED_MAJOR_YN' => fn (mixed $v, Klass $model): bool => $this->isYes($v),
                'CLASS_COMBINED_SECTION' => fn (mixed $v, Klass $model): bool => $this->isCombinedSection($v),
                'CLASS_HS_DUAL_ENROLL_YN' => fn (mixed $v, Klass $model): bool => $this->isYes($v),
                'CASSC_HONORS_YN' => fn (mixed $v, Klass $model): bool => $this->isYes($v),
                'CLASS_CLASS_ROLL_YN' => fn (mixed $v, Klass $model): bool => $this->isYes($v),
                'CLASS_SCHEDULE_PRINT' => fn (mixed $v, Klass $model): bool => $this->isYes($v),
                'major_class' => fn (mixed $v, Klass $model): ?string => $this->getMajorClassName($model),
                'combined_classes_string' => fn (mixed $v, Klass $model): string => $this->getCombinedString($model),
            ],
            'set' => [],
        ];
    }
}
