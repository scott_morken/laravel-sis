<?php

namespace Smorken\Sis\Attributes\Modifiers\Core;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\ViaCallbackModifier;
use Smorken\Sis\Contracts\Base\Person\Person;

class PersonModifiers extends Modifiers
{
    protected function getFirstName(Person $model): string
    {
        $name = trim($model->PERS_PREFERRED_FIRST_NAME ?? '');
        if (! $name) {
            $name = trim($model->PERS_PRIMARY_FIRST_NAME ?? '');
        }

        return $name;
    }

    protected function getLastName(Person $model): string
    {
        $name = trim($model->PERS_PREFERRED_LAST_NAME ?? '');
        if (! $name) {
            $name = trim($model->PERS_PRIMARY_LAST_NAME ?? '');
        }

        return $name;
    }

    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'first_name' => new ViaCallbackModifier(fn (mixed $value, Person $model) => $this->getFirstName($model)),
                'last_name' => new ViaCallbackModifier(fn (mixed $value, Person $model) => $this->getLastName($model)),
            ],
            'set' => [],
        ];
    }
}
