<?php

namespace Smorken\Sis\Attributes\Modifiers\Core;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\TrimModifier;
use Smorken\Model\Attributes\Modifiers\ViaCallbackModifier;
use Smorken\Model\Contracts\Model;

class CourseOfferModifiers extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'COFFR_CATALOG_NBR' => new TrimModifier,
                'COFFR_CATALOG_PRINT' => new ViaCallbackModifier(fn (
                    mixed $v,
                    Model $model
                ) => $this->stringYToBool($v)),
                'COFFR_SCHEDULE_PRINT' => new ViaCallbackModifier(fn (
                    mixed $v,
                    Model $model
                ) => $this->stringYToBool($v)),
            ],
            'set' => [],
        ];
    }

    protected function stringYToBool(mixed $v): bool
    {
        return $v === 'Y';
    }
}
