<?php

namespace Smorken\Sis\Attributes\Modifiers\Core;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Sis\Contracts\Base\Person\Email;
use Smorken\Sis\Enums\YesNo;

class EmailModifiers extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'EMAIL_PREF_EMAIL_FLAG' => static fn (mixed $v, Email $model): bool => $v === YesNo::YES,
            ],
            'set' => [],
        ];
    }
}
