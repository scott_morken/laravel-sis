<?php

namespace Smorken\Sis\Attributes\Modifiers\Core;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Sis\Contracts\Base\Student\Athlete;
use Smorken\Sis\Enums\YesNo;

class AthleteModifiers extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'ATHL_NCAA_ELIGIBLE' => static fn (mixed $v, Athlete $model): bool => $v === YesNo::YES,
                'ATHL_CURRENT_PARTICIPANT' => static fn (mixed $v, Athlete $model): bool => $v === YesNo::YES,
            ],
            'set' => [],
        ];
    }
}
