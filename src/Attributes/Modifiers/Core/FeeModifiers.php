<?php

namespace Smorken\Sis\Attributes\Modifiers\Core;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Sis\Contracts\Base\Fee;

class FeeModifiers extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'CLSBF_FLAT_AMT' => static fn (mixed $v, Fee $model): float => (float) number_format((float) ($v ?? 0), 2),
            ],
            'set' => [],
        ];
    }
}
