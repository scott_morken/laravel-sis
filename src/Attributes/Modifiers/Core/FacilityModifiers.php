<?php

namespace Smorken\Sis\Attributes\Modifiers\Core;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Sis\Contracts\Base\Physical\Facility;
use Smorken\Sis\Enums\Physical\FacilityGroups;
use Smorken\Sis\Enums\Physical\FacilityLocationCodes;

class FacilityModifiers extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'location_description' => static fn (
                    mixed $v,
                    Facility $model
                ) => FacilityLocationCodes::toArray()[$model->FACILITY_LOCATION] ?? $model->FACILITY_LOCATION,
                'group_description' => static fn (
                    mixed $v,
                    Facility $model
                ) => FacilityGroups::toArray()[$model->FACILITY_GROUP] ?? $model->FACILITY_GROUP,
            ],
            'set' => [],
        ];
    }
}
