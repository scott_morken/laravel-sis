<?php

namespace Smorken\Sis\Attributes\Modifiers\Core;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\TrimModifier;
use Smorken\Sis\Contracts\Base\Student\ServiceIndicator;
use Smorken\Sis\Enums\YesNo;

class ServiceIndicatorModifiers extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'SRVC_POSITIVE_SRVC_INDICATOR' => static fn (
                    mixed $v,
                    ServiceIndicator $model
                ): bool => $v === YesNo::YES,
                'SRVC_CONTACT' => new TrimModifier,
            ],
            'set' => [],
        ];
    }
}
