<?php

namespace Smorken\Sis\Attributes\Modifiers;

use Smorken\Model\Attributes\Helpers\Modifiers;
use Smorken\Model\Attributes\Modifiers\TrimModifier;

class NoteModifiers extends Modifiers
{
    protected function modifiers(array $modifiers = []): array
    {
        return [
            'get' => [
                'NOTES_TYPE_PUBL_STAF' => new TrimModifier,
            ],
            'set' => [],
        ];
    }
}
