<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models\Klass;

use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Builders\ClassBuilder;
use Smorken\Sis\Db\Common\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasClassRelations;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;

class Klass extends Base implements \Smorken\Sis\Contracts\Base\Klass\Klass
{
    use HasAcademicOrgIdAttribute, HasClassRelations, HasCollegeIdAttribute, HasStartEndDateAttributes, HasTermIdAttribute;

    protected array $attributeNames = [
        'academicOrgId' => 'acad_org',
        'collegeId' => 'institution',
        'endDate' => 'start_dt',
        'startDate' => 'end_dt',
        'termId' => 'strm',
    ];

    protected static string $builder = ClassBuilder::class;

    protected $casts = [
        'timestamp' => 'datetime', 'effdt' => 'date',
        'start_dt' => 'date', 'end_dt' => 'date',
        'cancel_dt' => 'date',
    ];

    protected $table = 'ps_class_tbl';

    public function mergeOther(): \Smorken\Sis\Contracts\Base\Klass\Klass
    {
        return $this;
    }
}
