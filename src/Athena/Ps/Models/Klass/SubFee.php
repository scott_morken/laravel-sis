<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models\Klass;

use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Builders\SubFeeBuilder;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;

class SubFee extends Base implements \Smorken\Sis\Contracts\Athena\Klass\SubFee
{
    use HasCollegeIdAttribute, HasTermIdAttribute;

    protected array $attributeNames = [
        'termId' => 'strm',
        'collegeId' => 'setid',
    ];

    protected static string $builder = SubFeeBuilder::class;

    protected $table = 'ps_class_sbfee_tbl';
}
