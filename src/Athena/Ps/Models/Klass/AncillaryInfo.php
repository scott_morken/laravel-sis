<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models\Klass;

use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Builders\ClassAncillaryInfoBuilder;

/**
 * @property \Carbon\Carbon $timestamp
 * @property string $crse_id
 * @property int $crse_offer_nbr
 * @property string $strm
 * @property string $session_code
 * @property string $class_section
 * @property string $mc_sr_day_eve
 * @property string $mc_sr_ftse_yn
 * @property string $mc_sr_vocat_fund
 * @property string $mc_sr_short_term
 * @property \Carbon\Carbon $mc_sr_add_dt
 * @property string $mc_sr_hs_dual_enroll
 * @property string $mc_sr_hs_sponsor
 * @property string $mc_sr_crs_ld_calc
 * @property string $mc_sr_joint_inst
 * @property int $mc_sr_nbr_days
 * @property string $mc_sr_legacy_sect
 * @property string $mc_sr_cmbnd_major
 * @property string $mc_sr_class_roll
 * @property string $mc_sr_honors
 * @property string $mc_sr_dept_cd_exp
 * @property string $mc_sr_acct_cd
 * @property float $mc_sr_contract_amt
 * @property string $mc_sr_maint_dt
 * @property string $mc_sr_maint_oprid
 * @property string $class_enrl_type
 * @property int $class_nbr
 * @property string $email_notification
 */
class AncillaryInfo extends Base implements \Smorken\Sis\Contracts\Athena\Klass\AncillaryInfo
{
    protected static string $builder = ClassAncillaryInfoBuilder::class;

    protected $casts = [
        'timestamp' => 'datetime',
        'mc_sr_add_dt' => 'date',
        'mc_sr_nbr_days' => 'integer',
        'mc_sr_contract_amt' => 'float',
        'class_nbr' => 'integer',
    ];

    protected $table = 'ps_mc_sr_class_tbl';
}
