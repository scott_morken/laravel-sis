<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models;

use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Builders\TranslateItemBuilder;

class TranslateItem extends Base implements \Smorken\Sis\Contracts\Athena\TranslateItem
{
    protected static string $builder = TranslateItemBuilder::class;

    protected $casts = ['timestamp' => 'datetime', 'effdt' => 'date'];

    protected $table = 'psxlatitem';
}
