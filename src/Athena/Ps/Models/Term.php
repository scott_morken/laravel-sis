<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel\TermAttributeMap;
use Smorken\Sis\Athena\Ps\Builders\TermBuilder;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;

/**
 * @property \Carbon\Carbon $timestamp
 * @property string $institution
 * @property string $acad_career
 * @property string $strm
 * @property string $descr
 * @property string $descrshort
 * @property \Carbon\Carbon $term_begin_dt
 * @property \Carbon\Carbon $term_end_dt
 * @property string $session_code
 * @property float $weeks_of_instruct
 * @property string $term_category
 * @property string $acad_year
 */
#[MapAttributeNames(new TermAttributeMap)]
class Term extends Base implements \Smorken\Sis\Contracts\Base\Term
{
    use HasAcademicCareerAttribute, HasCollegeIdAttribute, HasStartEndDateAttributes, HasTermIdAttribute;

    protected array $attributeNames = [
        'academicCareer' => 'acad_career',
        'endDate' => 'term_end_dt',
        'startDate' => 'term_begin_dt',
        'termId' => 'strm',
        'collegeId' => 'institution',
    ];

    protected static string $builder = TermBuilder::class;

    protected $casts = [
        'term_end_dt' => 'date', 'term_begin_dt' => 'date', 'weeks_of_instruct' => 'float', 'timestamp' => 'datetime',
    ];

    protected $table = 'ps_term_tbl';
}
