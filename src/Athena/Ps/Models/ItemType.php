<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel\ItemTypeAttributeMap;
use Smorken\Sis\Athena\Ps\Builders\ItemTypeBuilder;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;

/**
 * @property \Carbon\Carbon $timestamp
 * @property string $setid //college id
 * @property string $item_type
 * @property \Carbon\Carbon $effdt
 * @property string $eff_status
 * @property string $descr
 * @property string $descrshort
 * @property string $currency_cd
 * @property float $minimum_amt
 * @property float $maximum_amt
 * @property float $default_amt
 * @property string $item_type_cd
 * @property string $keyword1
 * @property string $keyword2
 * @property string $keyword3
 */
#[MapAttributeNames(new ItemTypeAttributeMap)]
class ItemType extends Base implements \Smorken\Sis\Contracts\Athena\ItemType
{
    use HasCollegeIdAttribute;

    protected array $attributeNames = [
        'collegeId' => 'setid',
    ];

    protected static string $builder = ItemTypeBuilder::class;

    protected $casts = [
        'timestamp' => 'datetime', 'effdt' => 'date', 'minimum_amt' => 'float', 'maximum_amt' => 'float',
        'default_amt' => 'float',
    ];

    protected $table = 'ps_item_type_tbl';
}
