<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models\Physical;

use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Builders\ExternalOrgBuilder;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;

/**
 * @property \Carbon\Carbon $timestamp
 * @property string $ext_org_id
 * @property \Carbon\Carbon $effdt
 * @property string $eff_status
 * @property string $oth_name_sort_srch
 * @property string $ext_org_type
 * @property string $descr
 * @property string $descr50
 * @property string $descrshort
 * @property string $org_contact
 * @property string $org_location
 * @property string $org_department
 * @property string $proprietorship
 * @property string $setid
 * @property string $vendor_id
 * @property string $taxpayer_id_no
 * @property string $lastupddttm
 * @property string $lastupdoprid
 */
class ExternalOrg extends Base implements \Smorken\Sis\Contracts\Athena\Physical\ExternalOrg
{
    use HasCollegeIdAttribute;

    protected array $attributeNames = [
        'collegeId' => 'setid',
    ];

    protected static string $builder = ExternalOrgBuilder::class;

    protected $casts = [
        'timestamp' => 'datetime', 'effdt' => 'date', 'org_contact' => 'integer', 'org_location' => 'integer',
        'org_department' => 'integer', 'lastupddttm' => 'datetime',
    ];

    protected $table = 'ps_ext_org_tbl';
}
