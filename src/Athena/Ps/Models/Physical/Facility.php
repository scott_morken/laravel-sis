<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models\Physical;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel\FacilityAttributeMap;
use Smorken\Sis\Athena\Ps\Builders\FacilityBuilder;
use Smorken\Sis\Db\Common\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;

#[MapAttributeNames(new FacilityAttributeMap)]
class Facility extends Base implements \Smorken\Sis\Contracts\Base\Physical\Facility
{
    use HasAcademicOrgIdAttribute, HasCollegeIdAttribute;

    protected array $attributeNames = [
        'academicOrgId' => 'acad_org',
        'collegeId' => 'setid',
    ];

    protected static string $builder = FacilityBuilder::class;

    protected $casts = ['timestamp' => 'datetime', 'effdt' => 'date', 'room_capacity' => 'integer'];

    protected $table = 'ps_facility_tbl';
}
