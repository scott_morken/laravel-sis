<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel\AcademicOrgAttributeMap;
use Smorken\Sis\Athena\Ps\Builders\AcademicOrgBuilder;
use Smorken\Sis\Db\Common\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Enums\AcademicOrg\EffectiveStatuses;

/**
 * @property \Carbon\Carbon $timestamp
 * @property string $acad_org
 * @property \Carbon\Carbon $effdt
 * @property string $eff_status
 * @property string $descr
 * @property string $descshort
 * @property string $descrformal
 * @property string $institution
 * @property string $campus
 * @property string $manager_id
 * @property string $instr_edit
 * @property string $campus_edit
 * @property string $subject_edit
 * @property string $course_edit
 *
 * Virtual
 * @property bool $isActive
 */
#[MapAttributeNames(new AcademicOrgAttributeMap)]
class AcademicOrg extends Base implements \Smorken\Sis\Contracts\Base\AcademicOrg
{
    use HasAcademicOrgIdAttribute, HasCollegeIdAttribute;

    protected array $attributeNames = [
        'academicOrgId' => 'acad_org',
        'collegeId' => 'institution',
    ];

    protected static string $builder = AcademicOrgBuilder::class;

    protected $casts = ['timestamp' => 'datetime', 'effdt' => 'date'];

    protected $table = 'ps_acad_org_tbl';

    public function isActive(): Attribute
    {
        return Attribute::make(
            get: function () {
                $now = Carbon::now();

                return $this->eff_status === EffectiveStatuses::ACTIVE &&
                    $this->effdt <= $now;
            }
        );
    }
}
