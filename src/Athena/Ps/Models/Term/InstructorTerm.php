<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models\Term;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel\InstructorTermAttributeMap;
use Smorken\Sis\Athena\Ps\Builders\InstructorTermBuilder;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;

#[MapAttributeNames(new InstructorTermAttributeMap)]
class InstructorTerm extends Base implements \Smorken\Sis\Contracts\Athena\Term\InstructorTerm
{
    use HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute;

    protected array $attributeNames = [
        'studentId' => 'emplid',
        'collegeId' => 'institution',
        'termId' => 'strm',
    ];

    protected static string $builder = InstructorTermBuilder::class;

    protected $casts = ['timestamp' => 'datetime', 'effdt' => 'date', 'assigned_fte_pct' => 'float', 'instr_multiplier' => 'int'];

    protected $table = 'ps_instructor_term';
}
