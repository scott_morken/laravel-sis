<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models;

use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel\NoteAttributeMap;
use Smorken\Sis\Athena\Ps\Builders\NoteBuilder;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;

/**
 * @property \Carbon\Carbon $timestamp
 * @property string $institution
 * @property string $class_note_nbr
 * @property \Carbon\Carbon $effdt
 * @property string $eff_status
 * @property string $mc_sr_note_code
 * @property string $descr
 * @property string $descrlong
 */
#[MapAttributeNames(new NoteAttributeMap)]
class Note extends Base implements \Smorken\Sis\Contracts\Athena\Note
{
    use HasCollegeIdAttribute;

    protected array $attributeNames = [
        'collegeId' => 'institution',
    ];

    protected static string $builder = NoteBuilder::class;

    protected $casts = ['timestamp' => 'datetime', 'effdt' => 'date'];

    protected $table = 'ps_class_notes_tbl';
}
