<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models\Person;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Smorken\Model\Attributes\MapAttributeNames;
use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel\PersonNameAttributeMap;
use Smorken\Sis\Athena\Ps\Builders\PersonNameBuilder;
use Smorken\Sis\Db\Common\Concerns\HasPersonNames;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;

#[MapAttributeNames(new PersonNameAttributeMap)]
class Name extends Base implements \Smorken\Sis\Contracts\Athena\Person\Name
{
    use HasPersonNames, HasStudentIdAttribute;

    protected array $attributeNames = [
        'studentId' => 'emplid',
    ];

    protected static string $builder = PersonNameBuilder::class;

    protected $casts = [
        'timestamp' => 'datetime', 'effdt' => 'date',
    ];

    protected $table = 'ps_names';

    public function firstName(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->getFirstName(),
        );
    }

    public function fullName(): string
    {
        return $this->getFullNameAttribute();
    }

    public function getFirstName(): string
    {
        $name = trim($this->pref_first_name ?? '');
        if (! $name) {
            $name = trim($this->attributes['first_name'] ?? '');
        }

        return $name;
    }

    public function getFullNameAttribute(): string
    {
        return implode(' ', array_filter([$this->getFirstName(), $this->getLastName()]));
    }

    public function getLastName(): string
    {
        $name = trim($this->pref_last_name ?? '');
        if (! $name) {
            $name = trim($this->attributes['last_name'] ?? '');
        }

        return $name;
    }

    public function getShortNameAttribute(): string
    {
        return implode('. ', array_filter([substr($this->getFirstName(), 0, 1) ?: null, $this->getLastName()]));
    }

    public function lastName(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->getLastName(),
        );
    }

    public function shortName(): string
    {
        return $this->getShortNameAttribute();
    }
}
