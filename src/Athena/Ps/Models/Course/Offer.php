<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models\Course;

use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Builders\CourseOfferBuilder;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Db\Common\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;

class Offer extends Base implements \Smorken\Sis\Contracts\Athena\Course\Offer
{
    use HasAcademicCareerAttribute, HasAcademicOrgIdAttribute, HasCollegeIdAttribute;

    protected array $attributeNames = [
        'academicCareer' => 'acad_career',
        'academicOrgId' => 'acad_org',
        'collegeId' => 'institution',
    ];

    protected static string $builder = CourseOfferBuilder::class;

    protected $casts = ['timestamp' => 'datetime', 'effdt' => 'date'];

    protected $table = 'ps_crse_offer';
}
