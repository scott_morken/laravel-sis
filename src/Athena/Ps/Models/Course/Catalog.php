<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Models\Course;

use Smorken\Sis\Athena\Common\Models\Base;
use Smorken\Sis\Athena\Ps\Builders\CourseCatalogBuilder;

class Catalog extends Base implements \Smorken\Sis\Contracts\Athena\Course\Catalog
{
    protected static string $builder = CourseCatalogBuilder::class;

    protected $casts = ['timestamp' => 'datetime', 'effdt' => 'date'];

    protected $table = 'ps_crse_catalog';
}
