<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class InstructorTermAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'collegeId' => 'institution',
            'studentId' => 'emplid',
            'termId' => 'strm',
            'instructorClass' => 'instructor_class',
            'applyLoadCalculation' => 'load_calc_apply',
            'applyWorkloadLimit' => 'workld_limit_apply',
            'assignedPercentFTE' => 'assigned_fte_percent',
            'multiplier' => 'instr_multiplier',
        ];
    }
}
