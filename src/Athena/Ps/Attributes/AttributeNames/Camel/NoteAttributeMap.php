<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class NoteAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'collegeId' => 'institution',
            'id' => 'class_note_nbr',
            'status' => 'eff_status',
            'description' => 'descr',
            'text' => 'descrlong',
        ];
    }
}
