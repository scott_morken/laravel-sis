<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class ItemTypeAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'collegeId' => 'setid',
            'id' => 'item_type',
            'status' => 'eff_status',
            'description' => 'descr',
            'shortDescription' => 'descrshort',
            'currency' => 'currency_cd',
            'minimumAmount' => 'mimimum_amt',
            'maximumAmount' => 'maximum_amt',
            'defaultAmount' => 'default_amt',
            'itemTypeCode' => 'item_type_cd',
        ];
    }
}
