<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class PersonNameAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'timestamp' => 'timestamp',
            'studentId' => 'emplid',
            'nameType' => 'name_type',
            'effectiveDate' => 'effdt',
            'status' => 'eff_status',
            'prefix' => 'name_prefix',
            'suffix' => 'name_suffix',
            'title' => 'name_title',
            'primaryLastName' => 'last_name',
            'primaryFirstName' => 'first_name',
            'primaryMiddleName' => 'middle_name',
            'secondLastName' => 'second_last_name',
            'preferredFirstName' => 'pref_first_name',
            'preferredLastName' => 'pref_last_name',
        ];
    }
}
