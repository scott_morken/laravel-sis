<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class TermAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'id' => 'strm',
            'description' => 'descr',
            'academicCareer' => 'acad_career',
            'startDate' => 'term_begin_dt',
            'endDate' => 'term_end_dt',
            'collegeId' => 'institution',
        ];
    }
}
