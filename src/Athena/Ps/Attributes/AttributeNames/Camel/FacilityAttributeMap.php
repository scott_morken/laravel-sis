<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class FacilityAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'collegeId' => 'setid',
            'id' => 'facility_id',
            'status' => 'eff_status',
            'buildingCode' => 'bldg_cd',
            'room' => 'room',
            'description' => 'descr',
            'shortDescription' => 'descrshort',
            'groupCode' => 'facility_type',
            'locationCode' => 'location',
            'capacity' => 'room_capacity',
            'academicOrgId' => 'acad_org',
        ];
    }
}
