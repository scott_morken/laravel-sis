<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Attributes\AttributeNames\Camel;

use Smorken\Model\Attributes\Mappers\MapToArray;

class AcademicOrgAttributeMap extends MapToArray
{
    protected function getMapArray(): array
    {
        return [
            'updatedAt' => 'timestamp',
            'id' => 'acad_org',
            'description' => 'descr',
            'shortDescription' => 'descrshort',
            'effectiveDate' => 'effdt',
            'status' => 'eff_status',
            'collegeId' => 'institution',
            'campusCode' => 'campus',
        ];
    }
}
