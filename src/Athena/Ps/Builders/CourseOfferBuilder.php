<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\JoinClause;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasEffectiveStatusScope;
use Smorken\Sis\Athena\Ps\Models\Course\Catalog;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicOrgIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;

class CourseOfferBuilder extends Builder
{
    use HasAcademicCareerScopes, HasAcademicOrgIdScopes, HasCollegeIdScopes, HasEffectiveStatusScope;

    protected string $effectiveStatusColumn = 'course_approved';

    public function joinMaxEffectiveCatalog(?Carbon $lessThan = null): EloquentBuilder
    {
        $table = $this->getModel()->getTable();

        return $this->joinSub($this->getMaxEffectiveCatalogSubquery($lessThan), 'cat',
            function (JoinClause $join) use ($table) {
                $join->on($table.'.crse_id', '=', 'cat.crse_id');
            });
    }

    protected function getMaxEffectiveCatalogSubquery(?Carbon $lessThan): EloquentBuilder
    {
        // @phpstan-ignore method.notFound
        $table = $this->getCatalogModel()->getTable();

        // @phpstan-ignore method.notFound
        return $this->getCatalogModel()->newQuery()
            ->select($table.'.crse_id', $table.'.descr', $table.'.descrlong')
            ->joinOnMaxEffectiveDateSubquery($lessThan);
    }

    protected function getCatalogModel(): \Smorken\Sis\Contracts\Athena\Course\Catalog
    {
        return new Catalog;
    }

    protected function getSubqueryIdentifierColumns(): array
    {
        return ['subject', 'catalog_nbr', $this->getAcademicOrgIdAttributeName(), $this->getCollegeIdAttributeName()];
    }
}
