<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasStartEndDateScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;

class TermBuilder extends Builder
{
    use HasAcademicCareerScopes, HasCollegeIdScopes, HasStartEndDateScopes, HasTermIdAttribute;

    public function active(): EloquentBuilder
    {
        $now = Carbon::now();

        return $this->betweenStartAndEndDates($now);
    }

    public function activeAndUpcoming(): EloquentBuilder
    {
        $now = Carbon::now();

        return $this->onOrAfterEndDate($now);
    }
}
