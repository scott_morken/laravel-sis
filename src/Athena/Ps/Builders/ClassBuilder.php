<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicOrgIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Enums\Klass\Statuses;

class ClassBuilder extends Builder
{
    use HasAcademicCareerScopes, HasAcademicOrgIdScopes, HasCollegeIdScopes;

    public function notCancelled(): EloquentBuilder
    {
        return $this->whereNull('cancel_dt')
            ->where('class_stat', '<>', Statuses::CANCELLED);
    }
}
