<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasEffectiveStatusScope;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;

class PersonNameBuilder extends Builder
{
    use HasEffectiveStatusScope, HasStudentIdScopes;

    public function nameTypeIs(string $nameType): EloquentBuilder
    {
        return $this->where('name_type', '=', $nameType);
    }

    public function primaryNameType(): EloquentBuilder
    {
        return $this->nameTypeIs('PRI');
    }

    protected function getSubqueryIdentifierColumns(): array
    {
        return ['emplid'];
    }
}
