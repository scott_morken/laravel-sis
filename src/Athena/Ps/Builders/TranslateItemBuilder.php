<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasEffectiveStatusScope;

class TranslateItemBuilder extends Builder
{
    use HasEffectiveStatusScope;

    public function fieldNameIs(string $fieldname): EloquentBuilder
    {
        return $this->where($this->getModel()->getTable().'.fieldname', '=', strtoupper($fieldname));
    }

    protected function getSubqueryIdentifierColumns(): array
    {
        return ['fieldname', 'fieldvalue'];
    }
}
