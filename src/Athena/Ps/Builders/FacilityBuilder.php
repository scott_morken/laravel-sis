<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasEffectiveStatusScope;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicOrgIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;

class FacilityBuilder extends Builder
{
    use HasAcademicOrgIdScopes, HasCollegeIdScopes, HasEffectiveStatusScope;

    protected function getSubqueryIdentifierColumns(): array
    {
        return ['facility_id', $this->getCollegeIdAttributeName()];
    }
}
