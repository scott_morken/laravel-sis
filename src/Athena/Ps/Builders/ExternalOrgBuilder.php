<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasEffectiveStatusScope;
use Smorken\Sis\Athena\Ps\Models\Klass\AncillaryInfo;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;

class ExternalOrgBuilder extends Builder
{
    use HasCollegeIdScopes, HasEffectiveStatusScope;

    public function hasSponsorGTETermId(string $termId): EloquentBuilder
    {
        return $this->whereExists($this->getHasClassesGTETermIdSubquery($termId));
    }

    protected function getAncillaryInfoModel(): \Smorken\Sis\Contracts\Athena\Klass\AncillaryInfo
    {
        return new AncillaryInfo;
    }

    protected function getHasClassesGTETermIdSubquery(string $termId): EloquentBuilder
    {
        // @phpstan-ignore method.notFound
        $ancillaryTable = $this->getAncillaryInfoModel()->getTable();
        $table = $this->getModel()->getTable();

        // @phpstan-ignore method.notFound
        return $this->getAncillaryInfoModel()->newQuery()
            ->selectRaw('1')
            ->joinClass()
            ->joinedClassIsNotCancelled()
            ->joinedClassTermIdGTE($termId)
            ->whereColumn($table.'.ext_org_id', '=', $ancillaryTable.'.mc_sr_hs_sponsor');
    }

    protected function getSubqueryIdentifierColumns(): array
    {
        return ['ext_org_id', $this->getCollegeIdAttributeName()];
    }
}
