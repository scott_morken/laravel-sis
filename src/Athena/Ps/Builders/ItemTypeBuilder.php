<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\JoinClause;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasEffectiveStatusScope;
use Smorken\Sis\Athena\Ps\Models\Klass\SubFee;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Enums\ItemType\Codes;

class ItemTypeBuilder extends Builder
{
    use HasCollegeIdScopes, HasEffectiveStatusScope;

    public function isNotObsolete(): EloquentBuilder
    {
        return $this->where('keyword2', '<>', '*OBSOLETE*');
    }

    public function isUSD(): EloquentBuilder
    {
        return $this->where('currency_cd', '=', 'USD');
    }

    /**
     * @phpstan-param  Codes::*  $itemTypeCode
     */
    public function itemTypeCodeIs(string $itemTypeCode
    ): EloquentBuilder {
        return $this->where('item_type_cd', '=', $itemTypeCode);
    }

    public function joinCommonAmountFromSubFees(int $limitTo = 50): EloquentBuilder
    {
        $table = $this->getModel()->getTable();
        // @phpstan-ignore method.notFound
        $subquery = ($this->getSubFeeModel())->newQuery()->commonFlatAmount($limitTo);
        $this->leftJoinSub($subquery, 'cfa', function (JoinClause $join) use ($table) {
            $join->on($table.'.item_type', '=', 'cfa.item_type')
                ->on($table.'.setid', '=', 'cfa.setid');
        });

        return $this->select($table.'.*')->addSelect('cfa.common_amt');
    }

    protected function getSubFeeModel(): \Smorken\Sis\Contracts\Athena\Klass\SubFee
    {
        return new SubFee;
    }

    protected function getSubqueryIdentifierColumns(): array
    {
        return ['item_type', $this->getCollegeIdAttributeName()];
    }
}
