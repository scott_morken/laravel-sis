<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\JoinClause;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasEffectiveStatusScope;
use Smorken\Sis\Athena\Ps\Models\Course\Offer;

class CourseCatalogBuilder extends Builder
{
    use HasEffectiveStatusScope;

    public function joinMaxEffectiveOffer(?Carbon $lessThan = null): EloquentBuilder
    {
        $table = $this->getModel()->getTable();

        return $this->joinSub($this->getMaxEffectiveOfferSubquery($lessThan), 'co',
            function (JoinClause $join) use ($table) {
                $join->on($table.'.crse_id', '=', 'co.crse_id');
            });
    }

    protected function getMaxEffectiveOfferSubquery(?Carbon $lessThan): EloquentBuilder
    {
        // @phpstan-ignore method.notFound
        $table = $this->getOfferModel()->getTable();

        // @phpstan-ignore method.notFound
        return $this->getOfferModel()->newQuery()
            ->select($table.'.crse_id', $table.'.acad_org', $table.'.institution', $table.'.crse_offer_nbr',
                $table.'.subject', $table.'.catalog_nbr')
            ->joinOnMaxEffectiveDateSubquery($lessThan);
    }

    protected function getOfferModel(): \Smorken\Sis\Contracts\Athena\Course\Offer
    {
        return new Offer;
    }

    protected function getSubqueryIdentifierColumns(): array
    {
        return ['crse_id'];
    }
}
