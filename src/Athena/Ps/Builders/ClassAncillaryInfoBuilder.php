<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\JoinClause;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Athena\Support\WrapString;
use Smorken\Sis\Athena\Ps\Models\Klass\Klass;
use Smorken\Sis\Athena\Support\ClassJoinHelper;
use Smorken\Sis\Enums\Klass\Statuses;

class ClassAncillaryInfoBuilder extends Builder
{
    protected bool $classIsJoined = false;

    protected ?string $classTableName = null;

    public function joinClass(): EloquentBuilder
    {
        if ($this->classIsJoined) {
            return $this;
        }
        $classTable = $this->getClassTableName();
        $table = $this->getModel()->getTable();
        $this->classIsJoined = true;

        return $this->join($classTable, function (JoinClause $join) use ($classTable, $table) {
            $helper = new ClassJoinHelper($table, $classTable);
            $helper->usingJoin($join);
        });
    }

    public function joinedClassCollegeIdIs(string $collegeId): EloquentBuilder
    {
        $this->joinClass();
        $classTableName = $this->getClassTableName();

        return $this->where($classTableName.'.institution', '=', $collegeId);
    }

    public function joinedClassIsNotCancelled(): EloquentBuilder
    {
        $this->joinClass();
        $classTableName = $this->getClassTableName();

        return $this->whereNull($classTableName.'.cancel_dt')
            ->where($classTableName.'.class_stat', '<>', Statuses::CANCELLED);
    }

    public function joinedClassTermIdGTE(string $termId): EloquentBuilder
    {
        $this->joinClass();
        $classTableName = $this->getClassTableName();

        $termId = WrapString::wrap($termId);

        return $this->where($classTableName.'.strm', '>=', $termId);
    }

    protected function getClassTableName(): string
    {
        if (! $this->classTableName) {
            $this->classTableName = (new Klass)->getTable();
        }

        return $this->classTableName;
    }
}
