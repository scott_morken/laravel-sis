<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasEffectiveStatusScope;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicOrgIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;

class AcademicOrgBuilder extends Builder
{
    use HasAcademicOrgIdScopes, HasCollegeIdScopes, HasEffectiveStatusScope;

    public function defaultOrder(): EloquentBuilder
    {
        return $this->orderBy($this->model->getTable().'.'.$this->getAcademicOrgIdAttributeName());
    }

    protected function getSubqueryIdentifierColumns(): array
    {
        return [$this->getAcademicOrgIdAttributeName(), $this->getCollegeIdAttributeName()];
    }
}
