<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\JoinClause;
use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Models\Klass\Klass;
use Smorken\Sis\Athena\Support\ClassJoinHelper;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;

class SubFeeBuilder extends Builder
{
    use HasCollegeIdScopes, HasTermIdScopes;

    public function commonFlatAmount(int $maxCount = 50): EloquentBuilder
    {
        $table = $this->getModel()->getTable();
        $fromSub = $this->getModel()->newQuery()
            ->selectRaw($table.'.*, row_number() over (partition by setid, item_type order by strm desc) as rn');

        return $this->select('item_type', 'setid', 'flat_amt as common_amt')
            ->selectRaw('count(*) as fee_count')
            ->from($fromSub, 'cax')
            ->where('cax.rn', '<=', (string) $maxCount)
            ->groupBy('cax.flat_amt', 'cax.item_type', 'cax.setid')
            ->orderBy('fee_count', 'desc');
    }

    public function descriptionLike(string $search): EloquentBuilder
    {
        return $this->where('descr', 'LIKE', '%'.$search.'%');
    }

    public function joinClass(): EloquentBuilder
    {
        $classTable = $this->classTableName();
        $table = $this->getModel()->getTable();

        return $this->join($classTable, function (JoinClause $join) use ($classTable, $table) {
            $helper = new ClassJoinHelper($table, $classTable);
            $helper->usingJoin($join);
        });
    }

    protected function classTableName(): string
    {
        return (new Klass)->getTable();
    }
}
