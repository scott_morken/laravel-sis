<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;

class InstructorTermBuilder extends Builder
{
    use HasCollegeIdScopes, HasStudentIdScopes, HasTermIdScopes;
}
