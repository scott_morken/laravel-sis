<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders\Concerns;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;

trait HasStartEndDateScopes
{
    use \Smorken\Sis\Builders\Concerns\Scopes\HasStartEndDateScopes;

    public function scopeAfterDate(Builder $query, string $column, Carbon|string $date): Builder
    {
        $date = $this->valueToCarbonDate($date)->endOfDay();

        return $query->whereTimestamp($column, '>', $date);
    }

    public function scopeBeforeDate(Builder $query, string $column, Carbon|string $date): Builder
    {
        $date = $this->valueToCarbonDate($date)->startOfDay();

        return $query->whereTimestamp($column, '<', $date);
    }

    public function scopeOnOrAfterDate(Builder $query, string $column, Carbon|string $date): Builder
    {
        $date = $this->valueToCarbonDate($date)->startOfDay();

        return $query->whereTimestamp($column, '>=', $date);
    }

    public function scopeOnOrBeforeDate(Builder $query, string $column, Carbon|string $date): Builder
    {
        $date = $this->valueToCarbonDate($date)->endOfDay();

        return $query->whereTimestamp($column, '<=', $date);
    }
}
