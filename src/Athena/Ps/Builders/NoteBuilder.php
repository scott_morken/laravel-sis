<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Ps\Builders;

use Smorken\Athena\Database\Query\Builder;
use Smorken\Sis\Athena\Ps\Builders\Concerns\HasEffectiveStatusScope;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;

class NoteBuilder extends Builder
{
    use HasCollegeIdScopes, HasEffectiveStatusScope;

    protected function getSubqueryIdentifierColumns(): array
    {
        return ['class_note_nbr', $this->getCollegeIdAttributeName()];
    }
}
