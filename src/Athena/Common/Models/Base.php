<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Common\Models;

use Smorken\Athena\Database\Eloquent;
use Smorken\Model\Concerns\WithDefaultColumns;
use Smorken\Model\Concerns\WithRelationMap;
use Smorken\Model\QueryBuilders\Concerns\Scopes\HasMultiKeyJoin;

#[\AllowDynamicProperties]
class Base extends Eloquent
{
    use HasMultiKeyJoin, WithDefaultColumns, WithRelationMap;
}
