<?php

declare(strict_types=1);

namespace Smorken\Sis\Athena\Support;

use Illuminate\Database\Query\JoinClause;

class ClassJoinHelper
{
    protected array $columns = ['crse_id', 'crse_offer_nbr', 'strm', 'session_code', 'class_section'];

    public function __construct(protected string $parentTable, protected string $relatedTable) {}

    public function toArray(): array
    {
        $joins = [];
        foreach ($this->columns as $column) {
            $joins[$this->parentTable.'.'.$column] = $this->relatedTable.'.'.$column;
        }

        return $joins;
    }

    public function usingJoin(JoinClause $join): JoinClause
    {
        foreach ($this->toArray() as $parent => $related) {
            $join->on($parent, '=', $related);
        }

        return $join;
    }
}
