<?php

declare(strict_types=1);

namespace Smorken\Sis\VO;

use Carbon\CarbonInterface;
use Smorken\Sis\Enums\Meeting\DayOutput;

class Days
{
    protected static DayOutput $dayOutput = DayOutput::ONE_CHAR;

    public function __construct(protected string $daysPattern) {}

    public static function setDayOutputType(DayOutput $output
    ): void {
        self::$dayOutput = $output;
    }

    public function daysPattern(): string
    {
        return $this->daysPattern;
    }

    public function days(): array
    {
        $conversion = [
            'U' => CarbonInterface::SUNDAY,
            'M' => CarbonInterface::MONDAY,
            'T' => CarbonInterface::TUESDAY,
            'W' => CarbonInterface::WEDNESDAY,
            'R' => CarbonInterface::THURSDAY,
            'F' => CarbonInterface::FRIDAY,
            'S' => CarbonInterface::SATURDAY,
        ];
        $raw = $this->daysRaw();

        return array_filter(
            array_map(fn (string $day): ?int => $conversion[$day] ?? null, $raw)
        );
    }

    public function daysRaw(): array
    {
        return array_filter(str_split(trim($this->daysPattern ?? '')), static fn (string $v): bool => (bool) $v);
    }

    public function daysToString(?DayOutput $output = null): string
    {
        $output ??= self::$dayOutput;
        $key = match ($output) {
            DayOutput::ONE_CHAR => 1,
            DayOutput::TWO_CHARS => 2,
            DayOutput::THREE_CHARS => 0,
        };
        $conv = [
            CarbonInterface::SUNDAY => ['Sun', 'U', 'Su'],
            CarbonInterface::MONDAY => ['Mon', 'M', 'Mo'],
            CarbonInterface::TUESDAY => ['Tue', 'T', 'Tu'],
            CarbonInterface::WEDNESDAY => ['Wed', 'W', 'We'],
            CarbonInterface::THURSDAY => ['Thu', 'R', 'Th'],
            CarbonInterface::FRIDAY => ['Fri', 'F', 'Fr'],
            CarbonInterface::SATURDAY => ['Sat', 'S', 'Sa'],
        ];

        return implode('', array_filter(
            array_map(fn (string $day): ?string => $conv[$day][$key] ?? null, $this->days())
        ));
    }
}
