<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:40 PM
 */

namespace Smorken\Sis\VO;

use Smorken\Model\VO;

#[\AllowDynamicProperties]
abstract class Base extends VO {}
