<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:40 PM
 */

namespace Smorken\Sis\VO\Physical;

use Smorken\Sis\VO\Base;

class College extends Base implements \Smorken\Sis\Contracts\Base\Physical\College {}
