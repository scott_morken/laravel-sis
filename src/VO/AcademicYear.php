<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:41 PM
 */

namespace Smorken\Sis\VO;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class AcademicYear extends Base implements \Smorken\Sis\Contracts\Base\AcademicYear
{
    protected string $end_day = '30';

    protected string $end_month = '04';

    public function endingAfter(string $date, int $limit = 2): Collection
    {
        $point_year = $this->getPointYear($date);
        $collection = new Collection;
        for ($i = 0; $i < $limit; $i++) {
            $collection->push($this->createModelForYear($point_year));
            $point_year += 1;
        }

        return $collection;
    }

    public function find(string|int $id): static
    {
        return $this->createModelForYear($id);
    }

    public function setEndMonthAndDay(string $month, string $day): void
    {
        $this->end_day = $day;
        $this->end_month = $month;
    }

    protected function createModelForYear(string|int $year): static
    {
        $start = Carbon::create($year - 1, (int) $this->end_month, (int) $this->end_day, 00, 00, 00)->addDay();
        $end = $start->copy()->addYear()->subSecond();
        $data = [
            'id' => $year,
            'description' => sprintf('%s - %s', $start->year, $end->year),
            'description_long' => sprintf('Fall %s - Spring %s', $start->year, $end->year),
            'start_date' => $start,
            'end_date' => $end,
        ];

        return $this->newInstance($data);
    }

    protected function getPointYear(string $date): int
    {
        $point = Carbon::parse($date);
        $point_year = $point->year;
        $start = Carbon::create($point_year, (int) $this->end_month, (int) $this->end_day, 00, 00, 00)->addDay();
        if ($point->lt($start)) {
            return $point_year;
        }

        return $point_year + 1;
    }
}
