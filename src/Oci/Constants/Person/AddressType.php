<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Constants\Person;

use Smorken\Support\Constants\Attributes\Concerns\EnumHasLabel;
use Smorken\Support\Constants\Attributes\EnumLabel;

enum AddressType: string
{
    use EnumHasLabel;

    #[EnumLabel('Billing')]
    case BILL = 'BILL';
    #[EnumLabel('Business')]
    case BUSN = 'BUSN';
    #[EnumLabel('Campus')]
    case CAMP = 'CAMP';
    #[EnumLabel('Dormitory')]
    case DORM = 'DORM';
    #[EnumLabel('Home')]
    case HOME = 'HOME';
    #[EnumLabel('ISIR')]
    case ISIR = 'ISIR';
    #[EnumLabel('Legal')]
    case LEGL = 'LEGL';
    #[EnumLabel('Mailing')]
    case MAIL = 'MAIL';
    #[EnumLabel('Other')]
    case OTHR = 'OTHR';
    #[EnumLabel('Other 1')]
    case OTH1 = 'OTH1';
    #[EnumLabel('Other 2')]
    case OTH2 = 'OTH2';
    #[EnumLabel('Other 2')]
    case OTR2 = 'OTR2';
    #[EnumLabel('Permanent')]
    case PERM = 'PERM';
    #[EnumLabel('Preferred')]
    case PREF = 'PREF';
    #[EnumLabel('Veteran')]
    case VTRN = 'VTRN';
    #[EnumLabel('Work')]
    case WORK = 'WORK';
}
