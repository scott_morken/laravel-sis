<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Constants;

use Smorken\Support\Constants\Attributes\Concerns\EnumHasLabel;
use Smorken\Support\Constants\Attributes\EnumLabel;

enum EffectiveStatus: string
{
    use EnumHasLabel;

    #[EnumLabel('Active')]
    case ACTIVE = 'A';
    #[EnumLabel('Inactive')]
    case INACTIVE = 'I';
}
