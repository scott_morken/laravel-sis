<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Models\Base\Person;

use Illuminate\Database\Eloquent\HasBuilder;
use Smorken\Sis\Oci\Builders\Base\Person\AddressBuilder;
use Smorken\Sis\Oci\Constants\EffectiveStatus;
use Smorken\Sis\Oci\Constants\Person\AddressType;
use Smorken\Sis\Oci\Models\BaseModel;

class Address extends BaseModel
{
    /** @uses \Illuminate\Database\Eloquent\HasBuilder<AddressBuilder<static>> */
    use HasBuilder;
    
    protected static string $builder = AddressBuilder::class;

    protected array $defaultColumns = [
        'EMPLID',
        'ADDRESS_TYPE',
        'EFFDT',
        'EFF_STATUS',
        'COUNTRY',
        'ADDRESS1',
        'ADDRESS2',
        'ADDRESS3',
        'ADDRESS4',
        'CITY',
        'STATE',
        'POSTAL',
        'LASTUPDDTTM',
        'LASTUPDOPRID',
    ];

    protected $table = 'BIB_ADDRESSES_VW';

    protected function casts(): array
    {
        return [
            'ADDRESS_TYPE' => AddressType::class,
            'EFFDT' => 'date',
            'EFF_STATUS' => EffectiveStatus::class,
            'LASTUPDDTTM' => 'datetime',
        ];
    }
}