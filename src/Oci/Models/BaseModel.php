<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Models;

use Smorken\Sis\Db\Common\Base;

abstract class BaseModel extends Base
{

}