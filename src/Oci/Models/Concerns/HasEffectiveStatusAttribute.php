<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Models\Concerns;

trait HasEffectiveStatusAttribute
{
    use HasAttributeName;

    public function getEffectiveStatusAttributeName(): string
    {
        return $this->getAttributeNameByFunctionOrKey(__FUNCTION__, 'effectiveStatus', 'EFF_STATUS');
    }
}