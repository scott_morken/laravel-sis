<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Models\Concerns;

trait HasEffectiveDateAndStatusAttributes
{
    use HasEffectiveStatusAttribute, HasEffectiveDateAttribute;
}