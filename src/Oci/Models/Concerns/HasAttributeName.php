<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Models\Concerns;

use Smorken\Model\QueryBuilders\Builder;

trait HasAttributeName
{

    protected function getAttributeNameByFunctionOrKey(string $functionName, string $key, string $defaultName): string
    {
        $fromFunction = $this->getAttributeNameFromModelIfBuilder($functionName);
        if ($fromFunction) {
            return $fromFunction;
        }

        return $this->getAttributeNameByKey($key, $defaultName);
    }

    protected function getAttributeNameByKey(string $key, string $defaultName): string
    {
        if (property_exists($this, 'attributeNames')) {
            return $this->attributeNames[$key] ?? $defaultName;
        }

        return $defaultName;
    }

    protected function getAttributeNameFromModelIfBuilder(string $functionName): ?string
    {
        if (is_a($this, Builder::class) && method_exists($this->model, $functionName)) {
            return $this->model->$functionName();
        }

        return null;
    }
}