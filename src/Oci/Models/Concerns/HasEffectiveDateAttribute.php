<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Models\Concerns;

trait HasEffectiveDateAttribute
{
    use HasAttributeName;

    public function getEffectiveDateAttributeName(): string
    {
        return $this->getAttributeNameByFunctionOrKey(__FUNCTION__, 'effectiveDate', 'EFFDT');
    }
}