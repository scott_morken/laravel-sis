<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Builders\Base\Person;

use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Oci\Builders\Concerns\HasEffectiveDateAndStatusScopes;

/**
 * @template TModel of \Smorken\Sis\Oci\Models\Base\Person\Address
 * @extends Builder<TModel>
 */
class AddressBuilder extends Builder
{
    use HasEffectiveDateAndStatusScopes;

    protected function getEffectiveDateSubqueryIdentifierColumns(): array
    {
        return ['EMPLID', 'ADDRESS_TYPE'];
    }
}