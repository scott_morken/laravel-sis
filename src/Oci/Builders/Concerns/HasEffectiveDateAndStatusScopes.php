<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Builders\Concerns;

use Carbon\Carbon;
use Illuminate\Database\Query\JoinClause;
use Smorken\Sis\Oci\Constants\EffectiveStatus;
use Smorken\Sis\Oci\Models\Concerns\HasEffectiveDateAttribute;
use Smorken\Sis\Oci\Models\Concerns\HasEffectiveStatusAttribute;

trait HasEffectiveDateAndStatusScopes
{
    use HasEffectiveDateAttribute, HasEffectiveStatusAttribute;

    abstract protected function getEffectiveDateSubqueryIdentifierColumns(): array;

    public function effectiveStatusIs(string|EffectiveStatus $status): self
    {
        if (is_a($status, EffectiveStatus::class)) {
            $status = $status->value;
        }

        return $this->where($this->getEffectiveStatusAttributeName(), '=', $status);
    }

    public function effectiveStatusIsActive(): self
    {
        return $this->effectiveStatusIs(EffectiveStatus::ACTIVE);
    }

    public function effectiveStatusIsInactive(): self
    {
        return $this->effectiveStatusIs(EffectiveStatus::INACTIVE);
    }

    public function joinOnMaxEffectiveDate(?Carbon $lessThan = null): self
    {
        $table = $this->getTableName();

        return $this->joinSub(
            $this->maxEffectiveDateSubquery($lessThan),
            'sub_eff_date',
            function (JoinClause $join) use ($table) {
                $columns = $this->getEffectiveDateSubqueryIdentifierColumns();
                $firstColumn = array_shift($columns);
                $joinQuery = $join->on(
                    $table.'.'.$firstColumn,
                    '=',
                    'sub_eff_date.'.$firstColumn
                )
                    ->whereColumn($table.'.'.$this->getEffectiveDateAttributeName(), '=', 'sub_eff_date.max_date');
                foreach ($columns as $column) {
                    $joinQuery->whereColumn($table.'.'.$column, '=', 'sub_eff_date.'.$column);
                }
            }
        );
    }

    public function maxEffectiveDateSubquery(?Carbon $lessThan = null): self
    {
        $columns = $this->getEffectiveDateSubqueryIdentifierColumns();
        $q = $this->model->newQuery()
            ->selectRaw(
                implode(', ', $columns).', MAX('.$this->getEffectiveDateAttributeName().') as max_date'
            )
            ->effectiveStatusIsActive()
            ->groupBy($columns);
        if ($lessThan) {
            $q->where($this->getEffectiveDateAttributeName(), '<=', $lessThan);
        }

        return $q;
    }

    protected function getTableName(): string
    {
        return $this->model->table();
    }
}