<?php
declare(strict_types=1);

namespace Smorken\Sis\Oci\Contracts\Models\Base\Person;

use Smorken\Model\Contracts\Model;

/**
 * Person addresses
 * Keys: EMPLID, ADDRESS_TYPE
 * @property int $studentId
 * @property int $EMPLID
 * @property \Smorken\Sis\Oci\Constants\Person\AddressType $addressType
 * @property \Smorken\Sis\Oci\Constants\Person\AddressType $ADDRESS_TYPE
 * @property \Carbon\Carbon $effectiveDate
 * @property \Carbon\Carbon $EFFDT
 * @property \Smorken\Sis\Oci\Constants\EffectiveStatus $effectiveStatus
 * @property \Smorken\Sis\Oci\Constants\EffectiveStatus $EFF_STATUS
 * @property string $country
 * @property string $COUNTRY
 * @property string $address1
 * @property string $ADDRESS1
 * @property string $address2
 * @property string $ADDRESS2
 * @property string $address3
 * @property string $ADDRESS3
 * @property string $address4
 * @property string $ADDRESS4
 * @property string $city
 * @property string $CITY
 * @property string $NUM1 // not used
 * @property string $NUM2 // not used
 * @property string $HOUSE_TYPE // not used
 * @property string $ADDR_FIELD1 // not used
 * @property string $ADDR_FIELD2 // not used
 * @property string $ADDR_FIELD3 // not used
 * @property string $COUNTY // not used
 * @property string $state
 * @property string $STATE
 * @property string $postal
 * @property string $POSTAL
 * @property string $GEO_CODE // not used
 * @property string $IN_CITY_LIMIT // not used
 * @property string $ADDRESS1_AC // not used
 * @property string $ADDRESS2_AC // not used
 * @property string $ADDRESS3_AC // not used
 * @property string $CITY_AC // not used
 * @property string $REG_REGION // not used
 * @property \Carbon\Carbon $lastUpdated
 * @property string $LASTUPDDTTM
 * @property string $LASTUPDOPRID
 */
interface Address extends Model
{

}