<?php

namespace Smorken\Sis\Builders\Concerns\Scopes;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Sis\Db\Common\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Enums\Shared\AcademicCareers;

trait HasAcademicCareerScopes
{
    use HasAcademicCareerAttribute;

    /**
     * @phpstan-param  AcademicCareers::*  $academicCareer
     */
    public function academicCareerIs(string $academicCareer
    ): Builder {
        return $this->scopeAcademicCareerIs($this, $academicCareer);
    }

    /**
     * @phpstan-param  AcademicCareers::*  $academicCareer
     */
    public function academicCareerIsNot(
        string $academicCareer
    ): Builder {
        return $this->scopeAcademicCareerIsNot($this, $academicCareer);
    }

    public function credit(): Builder
    {
        return $this->scopeCredit($this);
    }

    /**
     * @phpstan-param  AcademicCareers::*  $academicCareer
     */
    public function scopeAcademicCareerIs(
        Builder $query,
        string $academicCareer
    ): Builder {
        return $query->where($this->getAcademicCareerAttributeName(), '=', $academicCareer);
    }

    /**
     * @phpstan-param  AcademicCareers::*  $academicCareer
     */
    public function scopeAcademicCareerIsNot(
        Builder $query,
        string $academicCareer
    ): Builder {
        return $query->where($this->getAcademicCareerAttributeName(), '<>', $academicCareer);
    }

    public function scopeCredit(Builder $query): Builder
    {
        return $this->scopeAcademicCareerIsNot($query, AcademicCareers::NONCREDIT);
    }
}
