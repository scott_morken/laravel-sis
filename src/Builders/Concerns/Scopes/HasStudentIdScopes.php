<?php

namespace Smorken\Sis\Builders\Concerns\Scopes;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Sis\Db\Common\Concerns\HasStudentIdAttribute;

trait HasStudentIdScopes
{
    use HasStudentIdAttribute;

    public function scopeStudentIdIs(
        Builder $query,
        string|int $studentId
    ): Builder {
        return $query->where($this->getStudentIdAttributeName(), '=', $studentId);
    }

    public function studentIdIn(array $studentIds): Builder
    {
        if (empty($studentIds)) {
            return $this;
        }

        return $this->whereIn($this->getStudentIdAttributeName(), $studentIds);
    }

    public function studentIdIs(string|int $studentId): Builder
    {
        return $this->scopestudentIdIs($this, $studentId);
    }
}
