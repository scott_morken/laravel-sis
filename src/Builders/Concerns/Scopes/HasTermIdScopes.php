<?php

namespace Smorken\Sis\Builders\Concerns\Scopes;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Constants\OrderDirection;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;

trait HasTermIdScopes
{
    use HasTermIdAttribute;

    public function greaterThanOrEqualToTerm(string $termId): Builder
    {
        return $this->where($this->getTermIdAttributeName(), '>=', $termId);
    }

    public function greaterThanTerm(string $termId): Builder
    {
        return $this->where($this->getTermIdAttributeName(), '>', $termId);
    }

    public function lessThanOrEqualToTerm(string $termId): Builder
    {
        return $this->where($this->getTermIdAttributeName(), '<=', $termId);
    }

    public function lessThanTerm(string $termId): Builder
    {
        return $this->where($this->getTermIdAttributeName(), '<', $termId);
    }

    public function orderByTerm(OrderDirection $direction = OrderDirection::DESCENDING): Builder
    {
        return $this->scopeOrderByTerm($this, $direction);
    }

    public function scopeOrderByTerm(Builder $query, OrderDirection $direction = OrderDirection::DESCENDING): Builder
    {
        return $query->orderBy($this->getTermIdAttributeName(), $direction->value);
    }

    public function scopeTermIdIn(Builder $query, array $termIds): Builder
    {
        if (empty($termIds)) {
            return $query;
        }

        return $query->whereIn($this->getTermIdAttributeName(), $termIds);
    }

    public function scopeTermIdIs(
        Builder $query,
        string $termId
    ): Builder {
        return $query->where($this->getTermIdAttributeName(), '=', $termId);
    }

    public function termIdIn(array $termIds): Builder
    {
        return $this->scopeTermIdIn($this, $termIds);
    }

    public function termIdIs(string $termId): Builder
    {
        return $this->scopeTermIdIs($this, $termId);
    }
}
