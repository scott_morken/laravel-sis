<?php

namespace Smorken\Sis\Builders\Concerns\Scopes;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Sis\Db\Common\Concerns\HasAcademicOrgIdAttribute;

trait HasAcademicOrgIdScopes
{
    use HasAcademicOrgIdAttribute;

    public function academicOrgIdIs(string $academicOrgId): Builder
    {
        return $this->scopeAcademicOrgIdIs($this, $academicOrgId);
    }

    public function scopeAcademicOrgIdIs(
        Builder $query,
        string $academicOrgId
    ): Builder {
        return $query->where($this->getAcademicOrgIdAttributeName(), '=', $academicOrgId);
    }
}
