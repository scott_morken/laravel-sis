<?php

declare(strict_types=1);

namespace Smorken\Sis\Builders\Concerns\Scopes;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\JoinClause;
use Smorken\Sis\Enums\Shared\EffectiveStatuses;

trait HasEffectiveStatusScope
{
    abstract protected function getSubqueryIdentifierColumns(): array;

    public function effectiveStatusIs(string $effectiveStatus = EffectiveStatuses::ACTIVE): EloquentBuilder
    {
        return $this->where($this->getEffectiveStatusColumn(), '=', $effectiveStatus);
    }

    public function joinOnMaxEffectiveDateSubquery(?Carbon $lessThan = null): EloquentBuilder
    {
        $table = $this->getModel()->getTable();

        return $this->joinSub($this->maxEffectiveDateSubquery($lessThan), 'sub_eff_date',
            function (JoinClause $join) use ($table) {
                $columns = $this->getSubqueryIdentifierColumns();
                $firstColumn = array_shift($columns);
                $jq = $join->on(
                    $table.'.'.$firstColumn,
                    '=',
                    'sub_eff_date.'.$firstColumn
                )
                    ->whereColumn($table.'.'.$this->getEffectiveDateColumn(), '=', 'sub_eff_date.max_date');
                foreach ($columns as $column) {
                    $jq->whereColumn($table.'.'.$column, '=', 'sub_eff_date.'.$column);
                }
                $this->modifyJoinOnMaxEffectiveDateSubquery($jq);
            });
    }

    public function maxEffectiveDateSubquery(?Carbon $lessThan = null): EloquentBuilder
    {
        // @phpstan-ignore method.notFound
        $q = $this->getModel()
            ->newQuery()
            ->selectRaw(implode(', ',
                $this->getSubqueryIdentifierColumns()).', MAX('.$this->getEffectiveDateColumn().') as max_date')
            ->effectiveStatusIs()
            ->groupBy($this->getSubqueryIdentifierColumns());
        if ($lessThan) {
            $q->where($this->getEffectiveDateColumn(), '<=', $lessThan);
        }

        return $this->modifyEffectiveDateSubquery($q);
    }

    protected function getEffectiveDateColumn(): string
    {
        if (property_exists($this, 'effectiveDateColumn')) {
            return $this->effectiveDateColumn;
        }

        return 'EFFDT';
    }

    protected function getEffectiveStatusColumn(): string
    {
        if (property_exists($this, 'effectiveStatusColumn')) {
            return $this->effectiveStatusColumn;
        }

        return 'EFF_STATUS';
    }

    protected function modifyEffectiveDateSubquery(EloquentBuilder $query): EloquentBuilder
    {
        return $query;
    }

    protected function modifyJoinOnMaxEffectiveDateSubquery(JoinClause $join): JoinClause
    {
        return $join;
    }
}
