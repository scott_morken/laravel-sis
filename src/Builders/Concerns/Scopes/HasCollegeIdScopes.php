<?php

namespace Smorken\Sis\Builders\Concerns\Scopes;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Sis\Db\Common\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Enums\College\Ids;

trait HasCollegeIdScopes
{
    use HasCollegeIdAttribute;

    /**
     * @phpstan-param  Ids::*  $collegeId
     */
    public function collegeIdIs(string $collegeId): Builder
    {
        return $this->scopeCollegeIdIs($this, $collegeId);
    }

    /**
     * @phpstan-param  Ids::*  $collegeId
     */
    public function scopeCollegeIdIs(
        Builder $query,
        string $collegeId
    ): Builder {
        return $query->where($this->getCollegeIdAttributeName(), '=', $collegeId);
    }
}
