<?php

namespace Smorken\Sis\Builders\Concerns\Scopes;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Concerns\WithCarbonDateConversion;
use Smorken\Sis\Db\Common\Concerns\HasStartEndDateAttributes;

trait HasStartEndDateScopes
{
    use HasStartEndDateAttributes, WithCarbonDateConversion;

    public function afterDate(string $column, Carbon|string $date): Builder
    {
        return $this->scopeAfterDate($this->getBuilderForQuery(), $column, $date);
    }

    public function afterEndDate(Carbon|string $date): Builder
    {
        return $this->scopeAfterDate($this->getBuilderForQuery(), $this->getEndDateAttributeName(), $date);
    }

    public function afterStartDate(Carbon|string $date): Builder
    {
        return $this->scopeAfterDate($this->getBuilderForQuery(), $this->getStartDateAttributeName(), $date);
    }

    public function beforeDate(string $column, Carbon|string $date): Builder
    {
        return $this->scopeBeforeDate($this->getBuilderForQuery(), $column, $date);
    }

    public function beforeEndDate(Carbon|string $date): Builder
    {
        return $this->scopeBeforeDate($this->getBuilderForQuery(), $this->getEndDateAttributeName(), $date);
    }

    public function beforeStartDate(Carbon|string $date): Builder
    {
        return $this->scopeBeforeDate($this->getBuilderForQuery(), $this->getStartDateAttributeName(), $date);
    }

    public function betweenStartAndEndDates(Carbon|string $date, bool $inclusive = true): Builder
    {
        return $this->getBuilderForQuery()->where(function (Builder $query) use ($date, $inclusive): void {
            if ($inclusive) {
                $this->scopeOnOrBeforeDate($query, $this->getStartDateAttributeName(), $date);
                $this->scopeOnOrAfterDate($query, $this->getEndDateAttributeName(), $date);
            } else {
                $this->scopeBeforeDate($query, $this->getStartDateAttributeName(), $date);
                $this->scopeAfterDate($query, $this->getEndDateAttributeName(), $date);
            }
        });
    }

    public function onOrAfterDate(string $column, Carbon|string $date): Builder
    {
        return $this->scopeOnOrAfterDate($this->getBuilderForQuery(), $column, $date);
    }

    public function onOrAfterEndDate(Carbon|string $date): Builder
    {
        return $this->scopeOnOrAfterDate($this->getBuilderForQuery(), $this->getEndDateAttributeName(), $date);
    }

    public function onOrAfterStartDate(Carbon|string $date): Builder
    {
        return $this->scopeOnOrAfterDate($this->getBuilderForQuery(), $this->getStartDateAttributeName(), $date);
    }

    public function onOrBeforeDate(string $column, Carbon|string $date): Builder
    {
        return $this->scopeOnOrAfterDate($this->getBuilderForQuery(), $column, $date);
    }

    public function onOrBeforeEndDate(Carbon|string $date): Builder
    {
        return $this->scopeOnOrBeforeDate($this->getBuilderForQuery(), $this->getEndDateAttributeName(), $date);
    }

    public function onOrBeforeStartDate(Carbon|string $date): Builder
    {
        return $this->scopeOnOrBeforeDate($this->getBuilderForQuery(), $this->getStartDateAttributeName(), $date);
    }

    public function scopeAfterDate(Builder $query, string $column, Carbon|string $date): Builder
    {
        $date = $this->valueToCarbonDate($date)->endOfDay();

        return $query->where($column, '>', $date);
    }

    public function scopeBeforeDate(Builder $query, string $column, Carbon|string $date): Builder
    {
        $date = $this->valueToCarbonDate($date)->startOfDay();

        return $query->where($column, '<', $date);
    }

    public function scopeOnOrAfterDate(Builder $query, string $column, Carbon|string $date): Builder
    {
        $date = $this->valueToCarbonDate($date)->startOfDay();

        return $query->where($column, '>=', $date);
    }

    public function scopeOnOrBeforeDate(Builder $query, string $column, Carbon|string $date): Builder
    {
        $date = $this->valueToCarbonDate($date)->endOfDay();

        return $query->where($column, '<=', $date);
    }

    protected function getBuilderForQuery(): Builder
    {
        if ($this instanceof Builder) {
            return $this;
        }

        return $this->newQuery();
    }
}
