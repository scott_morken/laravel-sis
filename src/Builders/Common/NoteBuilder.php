<?php

namespace Smorken\Sis\Builders\Common;

use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;

class NoteBuilder extends Builder
{
    use HasCollegeIdScopes, HasTermIdScopes;
}
