<?php

namespace Smorken\Sis\Builders\Common;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicOrgIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStartEndDateScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;
use Smorken\Sis\Enums\Klass\EnrollmentStatuses;
use Smorken\Sis\Enums\Klass\Statuses;

abstract class ClassBuilder extends Builder
{
    use HasAcademicOrgIdScopes, HasCollegeIdScopes, HasStartEndDateScopes, HasTermIdScopes;

    public function catalogNumberIs(string|int $catalogNumber): EloquentBuilder
    {
        return $this->where('CLASS_CATALOG_NBR', '=', $catalogNumber);
    }

    public function classNameIs(string $className): EloquentBuilder
    {
        return $this->where('CLASS_CLASS_NAME', '=', $className);
    }

    public function classNameLike(string $className): EloquentBuilder
    {
        return $this->where('CLASS_CLASS_NAME', 'LIKE', $className.'%');
    }

    public function classNumberIs(string $classNumber): EloquentBuilder
    {
        return $this->where('CLASS_CLASS_NBR', '=', $classNumber);
    }

    public function courseIs(string $courseName): EloquentBuilder
    {
        return $this->classNameIs($courseName);
    }

    public function endAfter(Carbon|string $date): EloquentBuilder
    {
        return $this->afterEndDate($date);
    }

    public function identifierIs(string $identifier, string $separator = ':'): EloquentBuilder
    {
        if ($separator) {
            [$termId, $classNumber] = explode($separator, $identifier);
        } else {
            $termId = substr($identifier, 0, 4);
            $classNumber = substr($identifier, 4);
        }

        // @phpstan-ignore method.notFound
        return $this->termIdIs($termId)
            ->classNumberIs($classNumber);
    }

    public function inSession(): EloquentBuilder
    {
        $date = Carbon::now();

        return $this->betweenStartAndEndDates($date);
    }

    public function notCancelled(): EloquentBuilder
    {
        return $this->whereNull('CLASS_CANCEL_DATE')
            ->where('CLASS_CLASS_STAT', '<>', Statuses::CANCELLED);
    }

    public function open(): EloquentBuilder
    {
        return $this->where('CLASS_ENRL_STAT', '=', EnrollmentStatuses::OPEN);
    }

    public function orderByName(): EloquentBuilder
    {
        return $this->orderBy('CLASS_CLASS_NAME')
            ->orderBy('CLASS_CLASS_NBR');
    }

    public function orderByTerm(): EloquentBuilder
    {
        return $this->orderBy('CLASS_INSTITUTION_CD')
            ->orderBy('CLASS_TERM_CD');
    }

    public function startAfter(Carbon|string $date): EloquentBuilder
    {
        return $this->onOrAfterStartDate($date);
    }

    public function startBefore(Carbon|string $date): EloquentBuilder
    {
        return $this->onOrBeforeStartDate($date);
    }

    public function subjectCatalogNumberIs(string $subjectCatalogNumber): EloquentBuilder
    {
        return $this->classNameIs($subjectCatalogNumber);
    }

    public function subjectCatalogNumberLike(string $subjectCatalogNumber): EloquentBuilder
    {
        return $this->classNameLike($subjectCatalogNumber);
    }

    public function subjectIs(string $subject): EloquentBuilder
    {
        return $this->where('CLASS_SUBJECT_CD', '=', $subject);
    }
}
