<?php

namespace Smorken\Sis\Builders\Incremental;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Enums\Relations;

class PersonBuilder extends Builder
{
    use HasStudentIdScopes;

    public function altIdIs(string $altId): EloquentBuilder
    {
        return $this->meidIs($altId);
    }

    public function defaultOrder(): EloquentBuilder
    {
        return $this->orderBy('PERS_PRIMARY_LAST_NAME')
            ->orderBy('PERS_PRIMARY_FIRST_NAME');
    }

    public function emplidIs(string|int $emplid): EloquentBuilder
    {
        return $this->studentIdIs($emplid);
    }

    public function firstNameLike(string $first): EloquentBuilder
    {
        $first = $first.'%';

        return $this->where(static function ($q) use ($first): void {
            $q->where('PERS_PRIMARY_FIRST_NAME', 'LIKE', $first)
                ->orWhere('PERS_PREFERRED_FIRST_NAME', 'LIKE', $first);
        });
    }

    public function idIs(string|int $id): EloquentBuilder
    {
        return $this->studentIdIs($id);
    }

    public function isInstructor(): EloquentBuilder
    {
        return $this->whereHas(Relations::INSTRUCTS_CLASSES);
    }

    public function isStudent(): EloquentBuilder
    {
        return $this->whereHas(Relations::ENROLLMENTS);
    }

    public function lastNameLike(string $last): EloquentBuilder
    {
        return $this->where('PERS_PRIMARY_LAST_NAME', 'LIKE', $last.'%');
    }

    public function meidIs(string $meid): EloquentBuilder
    {
        return $this->where('PERS_OPRID', '=', $meid);
    }

    public function nameLike(?string $last, ?string $first): EloquentBuilder
    {
        if ($last) {
            $this->lastNameLike($last);
        }
        if ($first) {
            $this->firstNameLike($first);
        }

        return $this;
    }
}
