<?php

namespace Smorken\Sis\Builders\Incremental;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;
use Smorken\Sis\Enums\Relations;
use Smorken\Sis\Enums\Student\EnrollmentStatuses;
use Smorken\Sis\Enums\YesNo;

class EnrollmentBuilder extends Builder
{
    use HasAcademicCareerScopes, HasCollegeIdScopes, HasStudentIdScopes, HasTermIdScopes;

    public function activeByClass(): EloquentBuilder
    {
        return $this->whereHas(Relations::KLASS, static function (EloquentBuilder $query): void {
            // @phpstan-ignore method.notFound
            $query->notCancelled();
        });
    }

    public function byClassName(string $className): EloquentBuilder
    {
        return $this->whereHas(Relations::KLASS, static function (EloquentBuilder $query) use ($className): void {
            // @phpstan-ignore method.notFound
            $query->classNameIs($className);
        });
    }

    public function byTermEndingAfter(Carbon|string $date = '-3 years'): EloquentBuilder
    {
        return $this->whereHas(Relations::TERM, static function (EloquentBuilder $query) use ($date): void {
            // @phpstan-ignore method.notFound
            $query->onOrAfterEndDate($date);
        });
    }

    public function classNumberIs(string|int $classNumber): EloquentBuilder
    {
        return $this->where('ENRL_CLASS_NBR', $classNumber);
    }

    public function currentAndUpcomingByTerm(): EloquentBuilder
    {
        return $this->byTermEndingAfter(Carbon::now());
    }

    public function enrolled(): EloquentBuilder
    {
        return $this->statusCodeIn([EnrollmentStatuses::ENROLLED, EnrollmentStatuses::ENROLLED_WAITLIST]);
    }

    public function enrolledAndWaitlist(): EloquentBuilder
    {
        return $this->statusCodeIn([
            EnrollmentStatuses::ENROLLED, EnrollmentStatuses::ENROLLED_WAITLIST, EnrollmentStatuses::FULL,
        ]);
    }

    public function graded(): EloquentBuilder
    {
        return $this->whereNotNull('ENRL_GRADE_DATE');
    }

    public function includeInGpa(): EloquentBuilder
    {
        return $this->where('ENRL_INCLUDE_IN_GPA', '=', YesNo::YES);
    }

    public function orderByCollegeTermDescReasonDesc(): EloquentBuilder
    {
        return $this->orderBy($this->getCollegeIdAttributeName())
            ->orderBy($this->getTermIdAttributeName(), 'desc')
            ->orderBy('ENRL_STATUS_REASON_CD', 'desc');
    }

    public function orderByCollegeTermDescStudentId(): EloquentBuilder
    {
        return $this->orderBy($this->getCollegeIdAttributeName())
            ->orderBy($this->getTermIdAttributeName(), 'desc')
            ->orderBy($this->getStudentIdAttributeName());
    }

    public function orderByCollegeTermNumber(): EloquentBuilder
    {
        return $this->orderBy($this->getCollegeIdAttributeName())
            ->orderBy($this->getTermIdAttributeName())
            ->orderBy('ENRL_CLASS_NBR');
    }

    /**
     * @param  array<string>  $codes
     */
    public function statusCodeIn(array $codes): EloquentBuilder
    {
        if (! $codes) {
            return $this;
        }

        return $this->whereIn('ENRL_STATUS_REASON_CD', $codes);
    }

    /**
     * @phpstan-param  EnrollmentStatuses::*  $code
     */
    public function statusCodeIs(string $code
    ): EloquentBuilder {
        return $this->where('ENRL_STATUS_REASON_CD', '=', $code);
    }
}
