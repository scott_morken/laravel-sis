<?php

namespace Smorken\Sis\Builders\Incremental;

use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStartEndDateScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;

class MeetingBuilder extends Builder
{
    use HasCollegeIdScopes, HasStartEndDateScopes, HasTermIdScopes;
}
