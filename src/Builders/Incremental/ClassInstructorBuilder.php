<?php

namespace Smorken\Sis\Builders\Incremental;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Enums\Instructor\Roles;
use Smorken\Sis\Enums\Instructor\Types;

class ClassInstructorBuilder extends Builder
{
    use HasCollegeIdScopes, HasStudentIdScopes;

    public function adjunct(): EloquentBuilder
    {
        return $this->typeIs(Types::ADJUNCT);
    }

    public function primaryRole(): EloquentBuilder
    {
        return $this->roleIs(Roles::PRIMARY);
    }

    public function residential(): EloquentBuilder
    {
        return $this->typeIs(Types::RESIDENTIAL);
    }

    /**
     * @phpstan-param  Roles::*  $role
     */
    public function roleIs(string $role = Roles::PRIMARY
    ): EloquentBuilder {
        return $this->where('CLASSM_INSTRUCTOR_ROLE', '=', $role);
    }

    public function secondaryRole(): EloquentBuilder
    {
        return $this->roleIs(Roles::SECONDARY);
    }

    /**
     * @phpstan-param  Types::*  $type
     */
    public function typeIs(string $type = Types::RESIDENTIAL
    ): EloquentBuilder {
        return $this->where('CLASSM_INSTR_TYPE', '=', $type);
    }
}
