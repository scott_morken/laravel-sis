<?php

namespace Smorken\Sis\Builders\Incremental;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Expression;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;
use Smorken\Sis\Enums\Person\AcademicLoadCodes;

class StudentTermBuilder extends Builder
{
    use HasAcademicCareerScopes, HasCollegeIdScopes, HasStudentIdScopes, HasTermIdScopes;

    public function activeTerm(): EloquentBuilder
    {
        return $this->where('STFACT_MCCD_TERM_AHRS', '>', 0);
    }

    public function completed(): EloquentBuilder
    {
        return $this->whereHas('enrollments', static function (EloquentBuilder $query): void {
            // @phpstan-ignore method.notFound
            $query->graded()
                ->enrolled();
        });
    }

    public function graded(): EloquentBuilder
    {
        return $this->where(static function (EloquentBuilder $query): void {
            $query->where('STFACT_MCCD_TERM_EHRS', '>', 0)
                ->orWhere('STFACT_MCCD_TERM_QHRS', '>', 0);
        });
    }

    public function greaterThanOrEqualToCredits(float $credits): EloquentBuilder
    {
        return $this->where('STFACT_MCCD_CUM_EHRS', '>=', $credits);
    }

    /**
     * @phpstan-param  AcademicLoadCodes::*  $load
     */
    public function greaterThanOrEqualToLoad(
        string $load = AcademicLoadCodes::LESS_THAN_HALF
    ): EloquentBuilder {
        return $this->loadCodeIn($this->getLoadMixes()[$load] ?? []);
    }

    public function hasAcademicStanding(): EloquentBuilder
    {
        return $this->whereNotNull('CTERM_ACAD_STNDNG_ACTN');
    }

    public function hasLoad(): EloquentBuilder
    {
        return $this->loadCodeIsNot(AcademicLoadCodes::NONE);
    }

    public function hasLoadAndIsCredit(): EloquentBuilder
    {
        // @phpstan-ignore method.notFound
        return $this->credit()->hasLoad();
    }

    public function loadCodeIn(array $codes): EloquentBuilder
    {
        if (! $codes) {
            return $this;
        }

        return $this->whereIn('CTERM_ACAD_LOAD_CODE', $codes);
    }

    /**
     * @phpstan-param  AcademicLoadCodes::*  $code
     */
    public function loadCodeIs(string $code
    ): EloquentBuilder {
        return $this->where('CTERM_ACAD_LOAD_CODE', '=', $code);
    }

    /**
     * @phpstan-param  AcademicLoadCodes::*  $code
     */
    public function loadCodeIsNot(string $code
    ): EloquentBuilder {
        return $this->where('CTERM_ACAD_LOAD_CODE', '<>', $code);
    }

    public function maxByHours(int $hours = 41): EloquentBuilder
    {
        // @phpstan-ignore method.notFound
        return $this->maxTerm()->greaterThanOrEqualToCredits($hours);
    }

    public function maxTerm(): EloquentBuilder
    {
        $subquery = $this->model->newQuery()
            ->selectRaw(
                'MAX(CTERM_TERM_CD) AS MTERM_CD,
                            EMPLID AS MEMPLID, CTERM_INSTITUTION_CODE as MINSTITUTION_CODE,
                            CTERM_ACAD_CAREER AS MACAD_CAREER'
            )
            ->groupBy('EMPLID', 'CTERM_INSTITUTION_CODE', 'CTERM_ACAD_CAREER');

        return $this->join(
            new Expression(sprintf('(%s) stm', $subquery->toSql())),
            static function ($j): void {
                $j->on('stm.MTERM_CD', '=', new Expression('RDS_STU_TERM_VW.CTERM_TERM_CD'))
                    ->on('stm.MEMPLID', '=', new Expression('RDS_STU_TERM_VW.EMPLID'))
                    ->on('stm.MINSTITUTION_CODE', '=', new Expression('RDS_STU_TERM_VW.CTERM_INSTITUTION_CODE'))
                    ->on('stm.MACAD_CAREER', '=', new Expression('RDS_STU_TERM_VW.CTERM_ACAD_CAREER'));
            }
        );
    }

    public function orderByCollegeTermAndStudentId(): EloquentBuilder
    {
        return $this->orderBy($this->getCollegeIdAttributeName())
            ->orderBy($this->getTermIdAttributeName())
            ->orderBy($this->getStudentIdAttributeName());
    }

    public function orderByCollegeTermDescAndStudentId(): EloquentBuilder
    {
        return $this->orderBy($this->getCollegeIdAttributeName())
            ->orderBy($this->getTermIdAttributeName(), 'desc')
            ->orderBy($this->getStudentIdAttributeName());
    }

    public function orderByTermDesc(): EloquentBuilder
    {
        return $this->orderBy($this->getTermIdAttributeName(), 'desc');
    }

    protected function getLoadMixes(): array
    {
        return [
            AcademicLoadCodes::NONE => [
                AcademicLoadCodes::NONE,
                AcademicLoadCodes::LESS_THAN_HALF,
                AcademicLoadCodes::HALF,
                AcademicLoadCodes::THREE_QUARTERS,
                AcademicLoadCodes::FULL,
            ],
            AcademicLoadCodes::LESS_THAN_HALF => [
                AcademicLoadCodes::LESS_THAN_HALF,
                AcademicLoadCodes::HALF,
                AcademicLoadCodes::THREE_QUARTERS,
                AcademicLoadCodes::FULL,
            ],
            AcademicLoadCodes::HALF => [
                AcademicLoadCodes::HALF,
                AcademicLoadCodes::THREE_QUARTERS,
                AcademicLoadCodes::FULL,
            ],
            AcademicLoadCodes::THREE_QUARTERS => [
                AcademicLoadCodes::THREE_QUARTERS,
                AcademicLoadCodes::FULL,
            ],
            AcademicLoadCodes::FULL => [
                AcademicLoadCodes::FULL,
            ],
        ];
    }
}
