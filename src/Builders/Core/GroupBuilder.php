<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Enums\Student\GroupStatuses;

class GroupBuilder extends Builder
{
    use HasCollegeIdScopes, HasStudentIdScopes;

    public function active(): EloquentBuilder
    {
        $this->endDateIsNull();

        return $this->statusCodeIs(GroupStatuses::ACTIVE);
    }

    public function endDateIsNull(): EloquentBuilder
    {
        return $this->whereNull('STDGR_END_DATE');
    }

    /**
     * @phpstan-param GroupStatuses::* $code
     */
    public function statusCodeIs(string $code): EloquentBuilder
    {
        return $this->where('STDGR_STUDENT_STATUS', '=', $code);
    }
}
