<?php

namespace Smorken\Sis\Builders\Core;

use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;

class PositionBuilder extends Builder
{
    use HasStudentIdScopes;
}
