<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;

class ClassBuilder extends \Smorken\Sis\Builders\Common\ClassBuilder
{
    public function departmentIdIn(array|string $deptIds): EloquentBuilder
    {
        if (is_string($deptIds)) {
            return $this->departmentIdIs($deptIds);
        }
        if (($deptIds[0] ?? null) === '*') {
            return $this;
        }

        return $this->whereIn('CLASS_DEPTID_EXPENSE_CD', $deptIds);
    }

    public function departmentIdIs(string $deptId): EloquentBuilder
    {
        return $this->departmentIdIs($deptId);
    }
}
