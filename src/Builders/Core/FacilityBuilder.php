<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicOrgIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Enums\Physical\FacilityLocationCodes;
use Smorken\Sis\Enums\Physical\FacilityStatuses;

class FacilityBuilder extends Builder
{
    use HasAcademicOrgIdScopes, HasCollegeIdScopes;

    public function active(): EloquentBuilder
    {
        return $this->where('FACILITY_EFF_STATUS', '=', FacilityStatuses::ACTIVE);
    }

    public function descriptionLike(string $search): EloquentBuilder
    {
        $search = '%'.$search.'%';

        return $this->where('FACILITY_DESCR', 'LIKE', $search);
    }

    public function idIs(string $id): EloquentBuilder
    {
        return $this->where($this->model->getKeyName(), '=', $id);
    }

    /**
     * @phpstan-param  FacilityLocationCodes::*  $locationCode
     */
    public function locationCodeIs(string $locationCode
    ): EloquentBuilder {
        return $this->where('FACILITY_LOCATION', '=', $locationCode);
    }

    public function orderByDescription(): EloquentBuilder
    {
        return $this->orderBy('FACILITY_DESCR');
    }
}
