<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;

class MaterialBuilder extends Builder
{
    public function orderBySequence(): EloquentBuilder
    {
        return $this->orderBy('TBMAT_SEQ_NBR');
    }
}
