<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Enums\Student\AcademicPlanTypes;
use Smorken\Sis\Enums\Student\DegreeCheckoutStatuses;
use Smorken\Sis\Enums\Student\ProgramStatuses;

class ProgramBuilder extends Builder
{
    use HasAcademicCareerScopes, HasCollegeIdScopes, HasStudentIdScopes;

    public function active(): EloquentBuilder
    {
        return $this->programStatusCodeIs(ProgramStatuses::ACTIVE);
    }

    public function activeDegree(): Builder
    {
        // @phpstan-ignore method.notFound
        return $this->active()->planTypeCodeIsNot(AcademicPlanTypes::NONE);
    }

    public function admitTermLessThanTerm(string $termId): EloquentBuilder
    {
        return $this->where('STRUC_PROG_ADMIT_TERM', '<', $termId);
    }

    /**
     * @phpstan-param  DegreeCheckoutStatuses::*  $code
     */
    public function checkoutStatusCodeIs(string $code
    ): EloquentBuilder {
        return $this->where('STRUC_DEGR_CHECKOUT_STAT', '=', $code);
    }

    public function degreeAwarded(): EloquentBuilder
    {
        return $this->checkoutStatusCodeIs(DegreeCheckoutStatuses::AWARDED);
    }

    public function noSubPlan(): EloquentBuilder
    {
        return $this->whereNull('STRUC_ACAD_SUB_PLAN');
    }

    /**
     * @phpstan-param AcademicPlanTypes::* $code
     */
    public function planTypeCodeIs(string $code): EloquentBuilder
    {
        return $this->where('STRUC_ACAD_PLAN_TYPE', '=', $code);
    }

    /**
     * @phpstan-param AcademicPlanTypes::* $code
     */
    public function planTypeCodeIsNot(string $code
    ): EloquentBuilder {
        return $this->where('STRUC_ACAD_PLAN_TYPE', '<>', $code);
    }

    /**
     * @phpstan-param ProgramStatuses::* $code
     */
    public function programStatusCodeIs(string $code
    ): EloquentBuilder {
        return $this->where('STRUC_ACAD_PROG_STATUS', '=', $code);
    }
}
