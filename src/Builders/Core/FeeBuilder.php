<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;

class FeeBuilder extends Builder
{
    use HasCollegeIdScopes, HasTermIdScopes;

    public function descriptionLike(string $search): EloquentBuilder
    {
        return $this->where('CLSBF_ITEM_TYPE_LDESC', 'LIKE', '%'.$search.'%');
    }
}
