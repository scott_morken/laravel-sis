<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Enums\Student\DegreeStatuses;

class DegreeBuilder extends Builder
{
    use HasAcademicCareerScopes, HasCollegeIdScopes;

    public function active(): EloquentBuilder
    {
        $this->effectiveDateNotNull();
        $this->isNotConversionRecord();

        return $this->statusCodeIs(DegreeStatuses::ACTIVE);
    }

    public function effectiveDateNotNull(): EloquentBuilder
    {
        return $this->whereNotNull('DEGREE_EFFDT');
    }

    public function isNotConversionRecord(): EloquentBuilder
    {
        return $this->where('DESCRSHORT', '<>', 'CONV');
    }

    public function orderDescription(): EloquentBuilder
    {
        return $this->orderBy($this->getCollegeIdAttributeName())
            ->orderBy('TRNSCR_DESCR')
            ->orderBy('DEGREE');
    }

    public function planCodeIs(string $planCode): EloquentBuilder
    {
        return $this->where('ACAD_PLAN', '=', $planCode);
    }

    /**
     * @phpstan-param  DegreeStatuses::*  $code
     */
    public function statusCodeIs(string $code
    ): EloquentBuilder {
        return $this->where('EFF_STATUS', '=', $code);
    }
}
