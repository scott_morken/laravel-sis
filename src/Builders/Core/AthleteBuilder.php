<?php

namespace Smorken\Sis\Builders\Core;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStartEndDateScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Db\Incremental\Person\Person;
use Smorken\Sis\Enums\Student\AthleteParticipationCodes;
use Smorken\Sis\Enums\YesNo;

class AthleteBuilder extends Builder
{
    use HasCollegeIdScopes, HasStartEndDateScopes, HasStudentIdScopes;

    public function active(): EloquentBuilder
    {
        $this->statusCodeIs(AthleteParticipationCodes::ACTIVE);

        return $this->isCurrentParticipant();
    }

    public function activeAfterDate(Carbon|string $date): EloquentBuilder
    {
        $this->noEndDate();

        return $this->onOrAfterStartDate($date);
    }

    public function isCurrentParticipant(): EloquentBuilder
    {
        return $this->where('ATHL_CURRENT_PARTICIPANT', '=', YesNo::YES);
    }

    public function joinStudentAndOrder(): EloquentBuilder
    {
        $s = new Person;

        // @phpstan-ignore method.notFound
        return $this->join(
            $s->getTable(),
            sprintf('%s.PERS_EMPLID', $s->getTable()),
            '=',
            sprintf('%s.ATHL_EMPLID', $this->model->getTable())
        )
            ->orderByCollege()
            ->orderBy(sprintf('%s.PERS_PRIMARY_LAST_NAME', $s->getTable()))
            ->orderBy(sprintf('%s.PERS_PRIMARY_FIRST_NAME', $s->getTable()));
    }

    /**
     * @phpstan-param  YesNo::*  $eligible
     */
    public function ncaaEligible(string $eligible = 'Y'
    ): EloquentBuilder {
        $eligible = strtoupper($eligible);

        return $this->where('ATHL_NCAA_ELIGIBLE', '=', $eligible);
    }

    public function noEndDate(): EloquentBuilder
    {
        return $this->whereNull($this->getEndDateAttributeName());
    }

    public function orderByCollege(): EloquentBuilder
    {
        return $this->orderBy($this->getCollegeIdAttributeName());
    }

    /**
     * @phpstan-param  AthleteParticipationCodes::*  $code
     */
    public function statusCodeIs(string $code
    ): EloquentBuilder {
        return $this->where('ATHL_ATHLETIC_PARTIC_CD', '=', $code);
    }
}
