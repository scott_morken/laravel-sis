<?php

namespace Smorken\Sis\Builders\Core;

use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;

class StudentTermBuilder extends \Smorken\Sis\Builders\Incremental\StudentTermBuilder
{
    use HasAcademicCareerScopes, HasCollegeIdScopes, HasStudentIdScopes, HasTermIdScopes;
}
