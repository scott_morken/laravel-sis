<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasTermIdScopes;
use Smorken\Sis\Enums\Student\ServiceIndicatorCodes;
use Smorken\Sis\Enums\YesNo;

class ServiceIndicatorBuilder extends Builder
{
    use HasCollegeIdScopes, HasStudentIdScopes, HasTermIdScopes;

    public function codeIn(array $codes): EloquentBuilder
    {
        if (! $codes) {
            return $this;
        }

        return $this->whereIn('SRVC_SERVICE_IND_CD', $codes);
    }

    /**
     * @phpstan-param ServiceIndicatorCodes::* $code
     */
    public function codeIs(string $code): EloquentBuilder
    {
        return $this->where('SRVC_SERVICE_IND_CD', '=', $code);
    }

    public function holds(): EloquentBuilder
    {
        $this->isNegative();

        return $this->codeIn([
            ServiceIndicatorCodes::HOLD_ADMIN_DISTRICTWIDE,
            ServiceIndicatorCodes::HOLD_ADMIN,
            ServiceIndicatorCodes::DEBT_OWED,
            ServiceIndicatorCodes::HOLD_PERKINS_LOAN,
            ServiceIndicatorCodes::HOLD_FINANCIAL_AID_DISBURSEMENT,
        ]);
    }

    public function isNegative(): EloquentBuilder
    {
        return $this->where('SRVC_POSITIVE_SRVC_INDICATOR', '=', YesNo::NO);
    }

    public function isPositive(): EloquentBuilder
    {
        return $this->where('SRVC_POSITIVE_SRVC_INDICATOR', '=', YesNo::YES);
    }
}
