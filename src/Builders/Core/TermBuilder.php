<?php

namespace Smorken\Sis\Builders\Core;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasStartEndDateScopes;
use Smorken\Sis\Db\Common\Concerns\HasTermIdAttribute;
use Smorken\Sis\Enums\College\Ids;
use Smorken\Sis\Enums\Shared\AcademicCareers;

class TermBuilder extends Builder
{
    use HasAcademicCareerScopes, HasCollegeIdScopes, HasStartEndDateScopes, HasTermIdAttribute;

    public function active(): EloquentBuilder
    {
        $now = Carbon::now();

        return $this->betweenStartAndEndDates($now);
    }

    public function activeAndUpcoming(): EloquentBuilder
    {
        $now = Carbon::now();

        return $this->onOrAfterEndDate($now);
    }

    public function idIs(string $id): EloquentBuilder
    {
        return $this->termIdIs($id);
    }

    public function lessThanEqualTermId(string $termId): EloquentBuilder
    {
        return $this->where($this->getTermIdAttributeName(), '<=', $termId);
    }

    public function orTermIdIs(string $termId): EloquentBuilder
    {
        return $this->orWhere($this->getTermIdAttributeName(), '=', $termId);
    }

    public function order(int $limit = 15): EloquentBuilder
    {
        $query = $this->orderBy($this->getTermIdAttributeName());
        if ($limit) {
            $query->limit($limit);
        }

        return $query;
    }

    public function orderReverse(int $limit = 15): EloquentBuilder
    {
        $query = $this->orderBy($this->getTermIdAttributeName(), 'desc');
        if ($limit) {
            $query->limit($limit);
        }

        return $query;
    }

    public function past(): EloquentBuilder
    {
        return $this->onOrBeforeEndDate(Carbon::now());
    }

    public function termIdIn(array $termIds): EloquentBuilder
    {
        if ($termIds) {
            return $this->whereIn($this->getTermIdAttributeName(), $termIds);
        }

        return $this;
    }

    public function termIdIs(string $termId): EloquentBuilder
    {
        return $this->where($this->getTermIdAttributeName(), '=', $termId);
    }

    /**
     * @phpstan-param  Ids::*  $collegeId
     * @phpstan-param  AcademicCareers::*  $academicCareer
     * @phpstan-param  'and'|'or' $type
     */
    public function termMatchGroup(
        string $termId,
        string $collegeId,
        string $academicCareer,
        string $type = 'and'
    ): EloquentBuilder {
        $method = $type === 'or' ? 'orWhere' : 'where';

        return $this->$method(static function (TermBuilder $q) use ($termId, $collegeId, $academicCareer): void {
            $q->termIdIs($termId);
            $q->collegeIdIs($collegeId);
            $q->academicCareerIs($academicCareer);
        });
    }
}
