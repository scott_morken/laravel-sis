<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Enums\Person\EmailTypes;
use Smorken\Sis\Enums\YesNo;

class EmailBuilder extends Builder
{
    use HasStudentIdScopes;

    public function isPreferred(): EloquentBuilder
    {
        return $this->where('EMAIL_PREF_EMAIL_FLAG', '=', YesNo::YES);
    }

    /**
     * @phpstan-param  EmailTypes::*  $type
     */
    public function typeIs(string $type): EloquentBuilder
    {
        return $this->where('EMAIL_ADDR_TYPE', '=', $type);
    }
}
