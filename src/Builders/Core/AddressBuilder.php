<?php

namespace Smorken\Sis\Builders\Core;

use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasStudentIdScopes;
use Smorken\Sis\Enums\YesNo;
use Smorken\Sis\Oci\Constants\Person\AddressType;

/**
 * @template TModel of \Smorken\Sis\Db\Core\Person\Address
 * @extends Builder<TModel>
 */
class AddressBuilder extends Builder
{
    use HasStudentIdScopes;

    public function isPreferredEmail(): self
    {
        return $this->where('ADDR_PREF_EMAIL_FLAG', '=', YesNo::YES);
    }

    public function typeIs(AddressType|string $type): self
    {
        if (is_a($type, AddressType::class)) {
            $type = $type->value;
        }
        
        return $this->where('ADDR_ADDRESS_TYPE', '=', $type);
    }
}
