<?php

namespace Smorken\Sis\Builders\Core;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\JoinClause;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicCareerScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasAcademicOrgIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasCollegeIdScopes;
use Smorken\Sis\Builders\Concerns\Scopes\HasEffectiveStatusScope;
use Smorken\Sis\Db\Core\Course\CourseCatalog;

class CourseOfferBuilder extends Builder
{
    use HasAcademicCareerScopes, HasAcademicOrgIdScopes, HasCollegeIdScopes, HasEffectiveStatusScope;

    protected string $effectiveDateColumn = 'COFFR_CRSE_EFFDT';

    protected string $effectiveStatusColumn = 'COFFR_COURSE_APPROVED';

    public function approved(): EloquentBuilder
    {
        return $this->where('COFFR_COURSE_APPROVED', '=', 'A');
    }

    public function catalogNumberIs(string $catalogNumber): EloquentBuilder
    {
        if (strlen($catalogNumber) === 4) {
            return $this->where('COFFR_CATALOG_NBR', '=', $catalogNumber);
        }

        return $this->where('COFFR_CATALOG_NBR', 'LIKE', '%'.$catalogNumber);
    }

    public function defaultOrder(): EloquentBuilder
    {
        return $this->orderSubjectCatalogNbr();
    }

    public function joinCourseCatalog(): EloquentBuilder
    {
        // @phpstan-ignore property.notFound
        $this->getModel()->liftedCatalogAttributes = true;
        $ccTable = (new CourseCatalog)->getTable();
        $table = $this->getModel()->getTable();
        $this->select(sprintf('%s.*', $table))
            ->addSelect(
                [
                    'CRSE_REC_KEY',
                    'CRSE_CRSE_ID',
                    'CRSE_DESCR',
                    'CRSE_CONSENT',
                    'CRSE_UNITS_MINIMUM',
                    'CRSE_UNITS_MAXIMUM',
                    'CRSE_GRADING_BASIS',
                    'CRSE_COMPONENT',
                    'CRSE_COURSE_NOTES',
                    'CRSE_GOV_BD_APPROVAL_DATE',
                    'CRSE_EFFDT',
                ]
            );

        return $this->join($ccTable, function (JoinClause $join) use ($ccTable, $table) {
            $join->on($ccTable.'.CRSE_CRSE_ID', '=', $table.'.COFFR_CRSE_ID')
                ->on($ccTable.'.CRSE_CATALOG_SID', '=', $table.'.CRSE_CATALOG_SID')
                ->on($ccTable.'.CRSE_EFFDT', '=', $table.'.COFFR_CRSE_EFFDT');
        });
    }

    public function orderSubjectCatalogNbr(): EloquentBuilder
    {
        return $this->orderBy('COFFR_SUBJECT')
            ->orderBy('COFFR_CATALOG_NBR');
    }

    public function subjectAndCatalogNbrIs(string $subject, string $catalogNumber): EloquentBuilder
    {
        // @phpstan-ignore method.notFound
        return $this->subjectIs($subject)
            ->catalogNumberIs($catalogNumber);
    }

    public function subjectIs(string $subject): EloquentBuilder
    {
        return $this->where('COFFR_SUBJECT', '=', $subject);
    }

    protected function getSubqueryIdentifierColumns(): array
    {
        return ['COFFR_CRSE_ID'];
    }
}
