<?php

namespace Smorken\Sis\Enums\Klass;

use Smorken\Sis\Contracts\Enums\Arrayable;

class TimeOfDays implements Arrayable
{
    public const DAY = 'D';

    public const EVENING = 'E';

    public static function toArray(): array
    {
        return [
            self::DAY => 'Day',
            self::EVENING => 'Evening',
        ];
    }
}
