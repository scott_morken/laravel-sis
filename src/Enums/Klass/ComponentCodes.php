<?php

namespace Smorken\Sis\Enums\Klass;

use Smorken\Sis\Contracts\Enums\Arrayable;

class ComponentCodes implements Arrayable
{
    public const LAB = 'LAB';

    public const LECTURE = 'LEC';

    public const LECTURE_LAB = 'L+L';

    public static function toArray(): array
    {
        return [
            self::LAB => 'Lab',
            self::LECTURE => 'Lecture',
            self::LECTURE_LAB => 'Lecture and Lab',
        ];
    }
}
