<?php

namespace Smorken\Sis\Enums\Klass;

use Smorken\Sis\Contracts\Enums\Arrayable;

class Statuses implements Arrayable
{
    public const ACTIVE = 'A';

    public const CANCELLED = 'X';

    public const STOPPED = 'S';

    public const TENTATIVE = 'T';

    public static function toArray(): array
    {
        return [
            self::ACTIVE => 'Active',
            self::CANCELLED => 'Cancelled Section',
            self::TENTATIVE => 'Tentative Section',
            self::STOPPED => 'Stop Further Enrollment',
        ];
    }
}
