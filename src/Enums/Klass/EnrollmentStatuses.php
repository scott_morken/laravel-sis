<?php

namespace Smorken\Sis\Enums\Klass;

use Smorken\Sis\Contracts\Enums\Arrayable;

class EnrollmentStatuses implements Arrayable
{
    public const CLOSED = 'C';

    public const OPEN = 'O';

    public static function toArray(): array
    {
        return [
            self::OPEN => 'Open',
            self::CLOSED => 'Closed (full)',
        ];
    }
}
