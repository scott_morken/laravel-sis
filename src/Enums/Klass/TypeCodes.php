<?php

namespace Smorken\Sis\Enums\Klass;

use Smorken\Sis\Contracts\Enums\Arrayable;

class TypeCodes implements Arrayable
{
    public const E = 'E';

    public const N = 'N';

    public static function toArray(): array
    {
        return [
            self::E => 'Enrollable',
            self::N => 'Non-enrollable (Lab)',
        ];
    }
}
