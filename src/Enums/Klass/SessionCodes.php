<?php

namespace Smorken\Sis\Enums\Klass;

use Smorken\Sis\Contracts\Enums\Arrayable;

class SessionCodes implements Arrayable
{
    public const DYNAMIC_DATED = 'DD';

    public const OPEN_ENTRY = 'OEE';

    public static function toArray(): array
    {
        return [
            self::DYNAMIC_DATED => 'Dynamic Dated (normal)',
            self::OPEN_ENTRY => 'Open Entry/Exit',
        ];
    }
}
