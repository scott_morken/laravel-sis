<?php

namespace Smorken\Sis\Enums\Klass;

use Smorken\Sis\Contracts\Enums\Arrayable;

class InstructionModes implements Arrayable
{
    public const FIELD_BASED = 'FB';

    public const HYBRID = 'HY';

    public const HYBRID_VIRTUAL = 'HV';

    public const INDEPENDENT_STUDY = 'IS';

    public const IN_PERSON = 'P';

    public const LIVE_ONLINE = 'LO';

    public const ONLINE = 'IN';

    public const PRIVATE_INSTRUCTION = 'PI';

    public const STUDY_ABROAD = 'SA';

    public static function toArray(): array
    {
        return [
            self::IN_PERSON => 'In Person',
            self::ONLINE => 'Online',
            self::HYBRID => 'Hybrid',
            self::HYBRID_VIRTUAL => 'Hybrid Virtual',
            self::LIVE_ONLINE => 'Live Online',
            self::FIELD_BASED => 'Field Based',
            self::INDEPENDENT_STUDY => 'Independent Study',
            self::PRIVATE_INSTRUCTION => 'Private Instruction',
            self::STUDY_ABROAD => 'Study Abroad',
        ];
    }
}
