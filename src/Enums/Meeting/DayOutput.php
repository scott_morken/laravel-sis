<?php

namespace Smorken\Sis\Enums\Meeting;

enum DayOutput
{
    case ONE_CHAR;

    case TWO_CHARS;

    case THREE_CHARS;
}
