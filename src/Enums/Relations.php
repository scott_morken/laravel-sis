<?php

namespace Smorken\Sis\Enums;

class Relations
{
    public const ADDRESS = 'address';

    public const ADDRESSES = 'addresses';

    public const CLASS_INSTRUCTORS = 'classInstructors';

    public const CLASS_NOTES = 'classNotes';

    public const COMBINED_SECTIONS = 'combinedSections';

    public const COURSE_CATALOG = 'catalog';

    public const COURSE_NOTES = 'courseNotes';

    public const COURSE_OFFERINGS = 'offerings';

    public const EMAILS = 'emails';

    public const ENROLLMENTS = 'enrollments';

    public const FACILITY = 'facility';

    public const FEES = 'fees';

    public const HOME_ADDRESS = 'homeAddress';

    public const INSTRUCTOR = 'instructor';

    public const INSTRUCTOR_POSITIONS = 'positions';

    public const INSTRUCTS_CLASSES = 'instructsClasses';

    public const KLASS = 'class';

    public const MATERIALS = 'materials';

    public const MEETINGS = 'meetings';

    public const OTHER = 'other';

    public const STUDENT = 'student';

    public const STUDENT_GROUPS = 'studentGroups';

    public const STUDENT_TERMS = 'studentTerms';

    public const TERM = 'term';

    public static function from(array $relations): string
    {
        return implode('.', $relations);
    }
}
