<?php

namespace Smorken\Sis\Enums\Shared;

use Smorken\Sis\Contracts\Enums\Arrayable;

class GradingBases implements Arrayable
{
    public const CLOCK = 'CLK';

    public const GRADED = 'GRD';

    public const HISTORY = 'HST';

    public const NON_GRADED_COMPONENT = 'NON';

    public const NOT_GRADED = 'NOG';

    public const NSO = 'NSO';

    public const PASS_FAIL = 'PZ';

    public static function toArray(): array
    {
        return [
            self::CLOCK => 'Clock',
            self::GRADED => 'Graded (standard)',
            self::HISTORY => 'Historical',
            self::NON_GRADED_COMPONENT => 'Non-Graded Component',
            self::NOT_GRADED => 'No Grade Associated',
            self::PASS_FAIL => 'Pass/Fail',
            self::NSO => 'NSO',
        ];
    }
}
