<?php

namespace Smorken\Sis\Enums\Shared;

use Smorken\Sis\Contracts\Enums\Arrayable;

class AcademicCareers implements Arrayable
{
    public const CREDIT = 'CRED';

    public const NONCREDIT = 'NC';

    public const SKILL_CENTER = 'CLK';

    public static function toArray(): array
    {
        return [
            self::CREDIT => 'Credit',
            self::NONCREDIT => 'Non-Credit',
            self::SKILL_CENTER => 'Skill Center/Clock',
        ];
    }
}
