<?php

declare(strict_types=1);

namespace Smorken\Sis\Enums\Shared;

use Smorken\Sis\Contracts\Enums\Arrayable;

class EffectiveStatuses implements Arrayable
{
    public const ACTIVE = 'A';

    public const INACTIVE = 'I';

    public static function toArray(): array
    {
        return [
            self::ACTIVE => 'Active',
            self::INACTIVE => 'Inactive',
        ];
    }
}
