<?php

namespace Smorken\Sis\Enums\Student;

use Smorken\Sis\Contracts\Enums\Arrayable;

class ProgramStatuses implements Arrayable
{
    public const ACTIVE = 'AC';

    public const ADMITTED = 'AD';

    public const APPLICANT = 'AP';

    public const CANCELLED = 'CN';

    public const COMPLETED = 'CM';

    public const DISCONTINUED = 'DC';

    public static function toArray(): array
    {
        return [
            self::ACTIVE => 'Active',
            self::ADMITTED => 'Admitted',
            self::APPLICANT => 'Applicant',
            self::CANCELLED => 'Cancelled',
            self::COMPLETED => 'Completed Program',
            self::DISCONTINUED => 'Discontinued',
        ];
    }
}
