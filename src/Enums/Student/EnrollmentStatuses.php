<?php

namespace Smorken\Sis\Enums\Student;

use Smorken\Sis\Contracts\Enums\Arrayable;

class EnrollmentStatuses implements Arrayable
{
    public const CANCEL_DROP = 'CANC';

    public const ENROLLED = 'ENRL';

    public const ENROLLED_DROP = 'DROP';

    public const ENROLLED_WAITLIST = 'EWAT';

    public const FULL = 'FULL';

    public const RELATED_COMPONENT = 'RCMP';

    public const WITHDRAWN = 'WDRW';

    public static function toArray(): array
    {
        return [
            self::CANCEL_DROP => 'Dropped (cancelled)',
            self::ENROLLED_DROP => 'Dropped (enrolled)',
            self::ENROLLED => 'Enrolled',
            self::ENROLLED_WAITLIST => 'Enrolled from wait list',
            self::FULL => 'Section full',
            self::RELATED_COMPONENT => 'Related component',
            self::WITHDRAWN => 'Withdrawn',
        ];
    }
}
