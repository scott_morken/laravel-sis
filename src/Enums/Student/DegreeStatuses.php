<?php

namespace Smorken\Sis\Enums\Student;

use Smorken\Sis\Contracts\Enums\Arrayable;

class DegreeStatuses implements Arrayable
{
    public const ACTIVE = 'A';

    public const INACTIVE = 'I';

    public static function toArray(): array
    {
        return [
            self::ACTIVE => 'Active',
            self::INACTIVE => 'Inactive',
        ];
    }
}
