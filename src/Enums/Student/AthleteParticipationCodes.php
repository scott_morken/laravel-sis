<?php

namespace Smorken\Sis\Enums\Student;

use Smorken\Sis\Contracts\Enums\Arrayable;

class AthleteParticipationCodes implements Arrayable
{
    public const ACTIVE = 'ACTIV';

    public const INACTIVE = 'INACT';

    public static function toArray(): array
    {
        return [
            self::ACTIVE => 'Active Participant',
            self::INACTIVE => 'Inactive',
        ];
    }
}
