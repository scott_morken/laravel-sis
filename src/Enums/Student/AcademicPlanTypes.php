<?php

namespace Smorken\Sis\Enums\Student;

use Smorken\Sis\Contracts\Enums\Arrayable;

class AcademicPlanTypes implements Arrayable
{
    public const AWARD = 'AWD';

    public const CERTIFICATE = 'CER';

    public const DEGREE = 'DEG';

    public const NONE = 'NON';

    public const PREPARATION = 'PRP';

    public static function toArray(): array
    {
        return [
            self::AWARD => 'Award',
            self::CERTIFICATE => 'Certificate',
            self::DEGREE => 'Degree',
            self::NONE => 'No Degree/Certificate',
            self::PREPARATION => 'Preparation',
        ];
    }
}
