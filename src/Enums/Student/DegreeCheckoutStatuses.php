<?php

namespace Smorken\Sis\Enums\Student;

use Smorken\Sis\Contracts\Enums\Arrayable;

class DegreeCheckoutStatuses implements Arrayable
{
    public const APPLIED = 'AG';

    public const APPROVED = 'AP';

    public const AWARDED = 'AW';

    public const DENIED = 'DN';

    public static function toArray(): array
    {
        return [
            self::APPLIED => 'Applied for Graduation',
            self::APPROVED => 'Approved',
            self::AWARDED => 'Degree Awarded',
            self::DENIED => 'Denied',
        ];
    }
}
