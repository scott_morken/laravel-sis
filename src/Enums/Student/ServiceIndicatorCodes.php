<?php

namespace Smorken\Sis\Enums\Student;

use Smorken\Sis\Contracts\Enums\Arrayable;

class ServiceIndicatorCodes implements Arrayable
{
    public const ATHLETIC_ELIGIBILITY = 'ATH';

    public const AZDOR_NOT_SENT = 'NAZ';

    public const BANKRUPTCY = 'BNK';

    public const BLOCKED_SERVICES = 'HLD';

    public const COMMENT = 'COM';

    public const COMMUNITY_LIBRARY_BORROWER = 'CLB';

    public const CONVERSION_ADJ_CHARGE = 'CAC';

    public const CORPORATE_THIRD_PARTY = 'CTP';

    public const CREDIT_HISTORY = 'CRD';

    public const DACA = 'DAC';

    public const DEBT_MASTER_HOLD = 'DMH';

    public const DEBT_OWED = 'DBT';

    public const DECEASED = 'DCS';

    public const DISTANCE_LEARNING_ELIGIBILITY = 'DLS';

    public const DUAL_ENROLLMENT_U18 = 'CD';

    public const DUPLICATE = 'DUP';

    public const F1_COENROLLED = 'F1C';

    public const F1_STUDENT = 'F1';

    public const FINANCIAL_AID_COURSE_APPLICABILITY = 'FCA';

    public const FINANCIAL_AID_FAFSA_TRACKER = 'TRK';

    public const FINANCIAL_AID_MANUAL_PACKAGING = 'FAP';

    public const FINANCIAL_AID_TITLE_IV_RETURNED = 'TIV';

    public const FOREIGN_NATIONAL_STUDENT = 'FNS';

    public const FPC_RECORD_REQUEST = 'REC';

    public const GE_DISCLOSURE = 'GE';

    public const HEERF_REVIEW = 'HRV';

    public const HIGH_SCHOOL_STUDENT = 'CHS';

    public const HOLD_ADMIN = 'ADM';

    public const HOLD_ADMIN_DISTRICTWIDE = 'ADX';

    public const HOLD_DISTRICT_LAWFUL_PRESENCE = 'IDX';

    public const HOLD_ENROLLMENT = 'ENH';

    public const HOLD_FINANCIAL_AID_DISBURSEMENT = 'FAD';

    public const HOLD_PERKINS_LOAN = 'PKH';

    public const HONORS = 'HON';

    public const IDENTIFY_THEFT_STRAW = 'IDS';

    public const IDENTITY_THEFT_OFFENDER = 'IDO';

    public const IDENTITY_THEFT_VICTIM = 'IDT';

    public const IN_COLLECTIONS = 'COL';

    public const LIMITED_SERVICES = 'LMT';

    public const MULTIPLE_ADDRESS_APPROVAL = 'MAA';

    public const NATIONAL_GUARD_ARMY = 'NGA';

    public const NO_COLLECTIONS = 'NCO';

    public const NO_DROP = 'NOD';

    public const NO_ENROLLMENT_CANCELLATIONS = 'NEC';

    public const NO_FINANCIAL_AID = 'FAA';

    public const NO_PERSONAL_CHECKS = 'NPC';

    public const NO_REFUND = 'REF';

    public const ON_VISA = 'VSA';

    public const PAYMENT_PLAN_COLLECTIONS = 'PPC';

    public const RED_FLAG = 'RED';

    public const RESTRICTED_TUITION_WAIVER = 'RTW';

    public const SPECIAL_ADMISSIONS = 'AGE';

    public const STATE_AUTHORIZATION_COMPLIANCE = 'SAC';

    public const STATE_PROGRAM_ELIGIBILITY = 'SPE';

    public const TRANSCRIPT_HARDCOPY = 'TRS';

    public const UNDER_18 = 'U18';

    public const US_AIR_FORCE = 'UAF';

    public const VETERANS_DEPARTMENT = 'VA';

    public const VETERAN_1606 = '606';

    public const VETERAN_BENEFITS_CERTIFIED = 'VET';

    public const VETERAN_CHAPT_33 = 'VB3';

    public const VETERAN_CHAPT_35 = 'VB5';

    public const VETERAN_DEPENDENT = 'DVB';

    public const VETERAN_DEPENDENT_GI_BILL = 'VDP';

    public const VETERAN_VOC_REHAB = 'VB1';

    public const WRITEOFF = 'WRO';

    public static function toArray(): array
    {
        return [];
    }
}
