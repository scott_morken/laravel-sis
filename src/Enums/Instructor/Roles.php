<?php

namespace Smorken\Sis\Enums\Instructor;

use Smorken\Sis\Contracts\Enums\Arrayable;

class Roles implements Arrayable
{
    public const PRIMARY = 'PI';

    public const SECONDARY = 'SI';

    public static function toArray(): array
    {
        return [
            self::PRIMARY => 'Primary',
            self::SECONDARY => 'Secondary',
        ];
    }
}
