<?php

namespace Smorken\Sis\Enums\Instructor;

use Smorken\Sis\Contracts\Enums\Arrayable;

class Types implements Arrayable
{
    public const ADJUNCT = 'ADJ';

    public const RESIDENTIAL = 'RES';

    public const RETIREE = 'RET';

    public const SERVICE = 'SRV';

    public static function toArray(): array
    {
        return [
            self::ADJUNCT => 'Adjunct',
            self::RESIDENTIAL => 'Residential',
            self::RETIREE => 'Retiree',
            self::SERVICE => 'Service',
        ];
    }
}
