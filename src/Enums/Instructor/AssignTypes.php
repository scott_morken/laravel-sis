<?php

namespace Smorken\Sis\Enums\Instructor;

use Smorken\Sis\Contracts\Enums\Arrayable;

class AssignTypes implements Arrayable
{
    public const BASELOAD = 'NON';

    public const NONEXEMPT = 'NEX';

    public const NOPAY = 'NPY';

    public const PAY = 'PAY';

    public const REASSIGN = 'RAS';

    public static function toArray(): array
    {
        return [
            self::BASELOAD => 'Base Load',
            self::NONEXEMPT => 'Non-Exempt',
            self::NOPAY => 'No Pay',
            self::PAY => 'Pay',
            self::REASSIGN => 'Reassign',
        ];
    }
}
