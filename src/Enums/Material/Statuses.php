<?php

namespace Smorken\Sis\Enums\Material;

use Smorken\Sis\Contracts\Enums\Arrayable;

class Statuses implements Arrayable
{
    public const CHOICE = 'CH';

    public const CHOICE_RECOMMENDED = 'CRM';

    public const RECOMMENDED = 'REC';

    public const REQUIRED = 'REQ';

    public static function toArray(): array
    {
        return [
            self::CHOICE => 'Choice',
            self::CHOICE_RECOMMENDED => 'Choice (recommended)',
            self::RECOMMENDED => 'Recommended',
            self::REQUIRED => 'Required',
        ];
    }
}
