<?php

namespace Smorken\Sis\Enums\Material;

use Smorken\Sis\Contracts\Enums\Arrayable;

class Types implements Arrayable
{
    public const COURSE_PACKET = 'CRS PACKET';

    public const EBOOK = 'EBOOK';

    public const PACKAGE = 'PACKAGE';

    public const SUPPLY = 'SUPPLY';

    public const TEXTBOOK = 'TEXTBOOK';

    public static function toArray(): array
    {
        return [
            self::COURSE_PACKET => 'Course Packet',
            self::EBOOK => 'eBook',
            self::PACKAGE => 'Package',
            self::SUPPLY => 'Supply',
            self::TEXTBOOK => 'Textbook',
        ];
    }
}
