<?php

namespace Smorken\Sis\Enums\Person;

use Smorken\Sis\Contracts\Enums\Arrayable;

class AcademicStandingCodes implements Arrayable
{
    public const GOOD = 'GST';

    public const PROBATION = 'PRO';

    public static function toArray(): array
    {
        return [
            self::GOOD => 'Good',
            self::PROBATION => 'Probation',
        ];
    }
}
