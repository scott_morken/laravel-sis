<?php

namespace Smorken\Sis\Enums\Person;

use Smorken\Sis\Contracts\Enums\Arrayable;

class AcademicStandingActions implements Arrayable
{
    public const ACCEPTABLE = 'ACPT';

    public const CONTINUE_PROBATION = 'CONT';

    public const PROBATION = 'PROB';

    public static function toArray(): array
    {
        return [
            self::ACCEPTABLE => 'Acceptable',
            self::PROBATION => 'Probation',
            self::CONTINUE_PROBATION => 'Continue Probation',
        ];
    }
}
