<?php

namespace Smorken\Sis\Enums\Person;

use Smorken\Sis\Contracts\Enums\Arrayable;

class AcademicLoadCodes implements Arrayable
{
    public const FULL = 'F';

    public const HALF = 'H';

    public const LESS_THAN_HALF = 'L';

    public const NONE = 'N';

    public const THREE_QUARTERS = 'T';

    public static function toArray(): array
    {
        return [
            self::NONE => 'None',
            self::LESS_THAN_HALF => 'Less than half time',
            self::HALF => 'Half time',
            self::THREE_QUARTERS => 'Three-quarter time',
            self::FULL => 'Full time',
        ];
    }
}
