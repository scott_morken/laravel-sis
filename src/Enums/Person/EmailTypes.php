<?php

namespace Smorken\Sis\Enums\Person;

use Smorken\Sis\Contracts\Enums\Arrayable;

class EmailTypes implements Arrayable
{
    public const HOME = 'HOME';

    public const MCCD = 'MCCD';

    public const OTHER = 'OTHR';

    public const STAFF = 'STAF';

    public const WORK = 'Work';

    public static function toArray(): array
    {
        return [
            self::HOME => 'Home',
            self::MCCD => 'MCCD Official',
            self::OTHER => 'Other',
            self::STAFF => 'Staff',
            self::WORK => 'Work',
        ];
    }
}
