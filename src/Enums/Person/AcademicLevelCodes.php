<?php

namespace Smorken\Sis\Enums\Person;

use Smorken\Sis\Contracts\Enums\Arrayable;

class AcademicLevelCodes implements Arrayable
{
    public const FRESHMAN = 10;

    public const JUNIOR = 30;

    public const SENIOR = 40;

    public const SOPHOMORE = 20;

    public static function toArray(): array
    {
        return [
            self::FRESHMAN => 'Freshman',
            self::SOPHOMORE => 'Sophomore',
        ];
    }
}
