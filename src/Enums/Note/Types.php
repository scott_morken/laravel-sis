<?php

namespace Smorken\Sis\Enums\Note;

use Smorken\Sis\Contracts\Enums\Arrayable;

class Types implements Arrayable
{
    public const PUBLIC = 'Public';

    public const STAFF = 'Staff';

    public static function toArray(): array
    {
        return [
            self::PUBLIC => 'Public',
            self::STAFF => 'Staff',
        ];
    }
}
