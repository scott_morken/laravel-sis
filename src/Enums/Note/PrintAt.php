<?php

declare(strict_types=1);

namespace Smorken\Sis\Enums\Note;

use Smorken\Sis\Contracts\Enums\Arrayable;

class PrintAt implements Arrayable
{
    public const PUBLIC = 'A';

    public const STAFF = 'B';

    public static function toArray(): array
    {
        return [
            self::PUBLIC => 'Public',
            self::STAFF => 'Staff',
        ];
    }
}
