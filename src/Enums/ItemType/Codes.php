<?php

declare(strict_types=1);

namespace Smorken\Sis\Enums\ItemType;

use Smorken\Sis\Contracts\Enums\Arrayable;

class Codes implements Arrayable
{
    public const CHARGE = 'C';

    public const PAYMENT = 'P';

    public const REFUND = 'R';

    public static function toArray(): array
    {
        return [
            self::CHARGE => 'Charge',
            self::PAYMENT => 'Payment',
            self::REFUND => 'Refund',
        ];
    }
}
