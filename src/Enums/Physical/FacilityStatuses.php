<?php

namespace Smorken\Sis\Enums\Physical;

use Smorken\Sis\Contracts\Enums\Arrayable;

class FacilityStatuses implements Arrayable
{
    public const ACTIVE = 'A';

    public static function toArray(): array
    {
        return [
            self::ACTIVE => 'Active',
        ];
    }
}
