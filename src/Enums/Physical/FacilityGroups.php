<?php

namespace Smorken\Sis\Enums\Physical;

class FacilityGroups
{
    public const AUDITORIUM = 'AUD';

    public const CABLE_TV = 'CBL';

    public const CLASSROOM = 'CLAS';

    public const COMPUTER_LAB = 'CMLB';

    public const CONFERENCE = 'CONR';

    public const CULINARY = 'CUL';

    public const DISTANCE = 'DIST';

    public const EXTERIOR = 'EXT';

    public const INTERACTIVE_TV = 'ITV';

    public const LAB = 'LAB';

    public const LEC = 'LCTR';

    public const MULTIPURPOSE = 'MULP';

    public const OFFICE = 'OFFC';

    public const OFFSITE = 'OFF';

    public const OTHER = 'OTH';

    public const RESEARCH = 'RSCH';

    public const SEMINAR = 'SMNR';

    public const SPORT = 'SPRT';

    public const STUDIO = 'STU';

    public static function toArray(): array
    {
        return [
            self::AUDITORIUM => 'Auditorium/Theatre',
            self::CLASSROOM => 'Classroom',
            self::CONFERENCE => 'Conference Room',
            self::CULINARY => 'Culinary',
            self::DISTANCE => 'Online',
            self::LAB => 'Lab',
            self::MULTIPURPOSE => 'Multipurpose',
            self::OFFICE => 'Office',
            self::OFFSITE => 'Offsite',
            self::OTHER => 'Other',
            self::SPORT => 'Sport/Gym/Field',
            self::STUDIO => 'Studio',
        ];
    }
}
