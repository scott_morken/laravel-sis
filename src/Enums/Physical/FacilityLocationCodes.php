<?php

namespace Smorken\Sis\Enums\Physical;

use Smorken\Sis\Contracts\Enums\Arrayable;

class FacilityLocationCodes implements Arrayable
{
    public const DOWNTOWN = 'PCDT';

    public const DUAL_ENROLLMENT = 'HS DUAL PC';

    public const HEALTH = 'PC HEALTH';

    public const MAIN = 'PC MAIN';

    public const NURSING = 'PC NUR';

    public const OFFSITE = 'PC OFFSITE';

    public static function toArray(): array
    {
        return [
            self::DUAL_ENROLLMENT => 'Dual Enrollment (high school)',
            self::HEALTH => 'Healthcare Center',
            self::DOWNTOWN => 'IT Institute',
            self::MAIN => 'Main Campus',
            self::NURSING => 'Nursing Center',
            self::OFFSITE => 'Offsite',
        ];
    }
}
