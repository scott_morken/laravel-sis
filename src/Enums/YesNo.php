<?php

namespace Smorken\Sis\Enums;

use Smorken\Sis\Contracts\Enums\Arrayable;

class YesNo implements Arrayable
{
    public const NO = 'N';

    public const YES = 'Y';

    public static function toArray(): array
    {
        return [
            self::YES => 'Yes',
            self::NO => 'No',
        ];
    }
}
