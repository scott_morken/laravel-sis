<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 7/13/17
 * Time: 6:52 AM
 */

namespace Smorken\Sis\Contracts\Base;

use Smorken\Model\Contracts\Model;

interface AidYear extends Model {}
