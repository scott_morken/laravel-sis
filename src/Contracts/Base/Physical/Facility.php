<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:10 PM
 */

namespace Smorken\Sis\Contracts\Base\Physical;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;

interface Facility extends HasAcademicOrgIdAttribute, HasCollegeIdAttribute, Model {}
