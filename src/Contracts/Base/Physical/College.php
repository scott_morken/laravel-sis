<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 12:50 PM
 */

namespace Smorken\Sis\Contracts\Base\Physical;

use Smorken\Model\Contracts\Model;

interface College extends Model {}
