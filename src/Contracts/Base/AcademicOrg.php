<?php

namespace Smorken\Sis\Contracts\Base;

use Smorken\Model\Contracts\Model;

interface AcademicOrg extends Model {}
