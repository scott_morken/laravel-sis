<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:16 PM
 */

namespace Smorken\Sis\Contracts\Base;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Contracts\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

interface Term extends HasAcademicCareerAttribute, HasStartEndDateAttributes, HasTermIdAttribute, Model {}
