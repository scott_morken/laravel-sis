<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:02 PM
 */

namespace Smorken\Sis\Contracts\Base\Klass;

use Illuminate\Support\Collection;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

/**
 * Relations
 *
 * @property \Smorken\Sis\VO\Days $daysVO
 * @property \Smorken\Sis\Contracts\Base\Klass\Klass $class
 * @property Collection<\Smorken\Sis\Contracts\Base\Klass\ClassInstructor> $classInstructors
 * @property \Smorken\Sis\Contracts\Base\Physical\Facility $facility
 */
interface Meeting extends HasCollegeIdAttribute, HasStartEndDateAttributes, HasTermIdAttribute, Model
{
    public function primaryInstructor(): ?ClassInstructor;
}
