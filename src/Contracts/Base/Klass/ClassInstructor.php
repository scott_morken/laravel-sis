<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:04 PM
 */

namespace Smorken\Sis\Contracts\Base\Klass;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;

/**
 * Relations
 *
 * @property \Smorken\Sis\Contracts\Base\Klass\Klass $class
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Klass\Meeting> $meetings
 * @property \Smorken\Sis\Contracts\Base\Person\Person $instructor
 */
interface ClassInstructor extends HasCollegeIdAttribute, HasStudentIdAttribute, Model {}
