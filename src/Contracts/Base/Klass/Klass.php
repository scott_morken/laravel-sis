<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 12:59 PM
 */

namespace Smorken\Sis\Contracts\Base\Klass;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

/**
 * @property string $combined_id
 *
 * Relations
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Klass\Klass> $combinedSections
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Student\Enrollment> $enrollments
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Klass\Material> $materials
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Klass\Meeting> $meetings
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Klass\ClassInstructor> $classInstructors
 * @property \Smorken\Sis\Contracts\Base\Term $term
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Note> $classNotes
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Note> $courseNotes
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Fee> $fees
 * @property \Smorken\Sis\Contracts\Base\Klass\Klass $other (either CDS or RDS)
 */
interface Klass extends HasAcademicOrgIdAttribute, HasCollegeIdAttribute, HasStartEndDateAttributes, HasTermIdAttribute, Model
{
    public function mergeOther(): Klass;
}
