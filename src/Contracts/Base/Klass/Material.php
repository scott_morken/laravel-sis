<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:07 PM
 */

namespace Smorken\Sis\Contracts\Base\Klass;

use Smorken\Model\Contracts\Model;

interface Material extends Model {}
