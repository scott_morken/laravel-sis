<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/2/18
 * Time: 10:10 AM
 */

namespace Smorken\Sis\Contracts\Base\Person;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;

interface Email extends HasStudentIdAttribute, Model {}
