<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 12:52 PM
 */

namespace Smorken\Sis\Contracts\Base\Person;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasPersonNames;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;

/**
 * Relations
 *
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Person\Email> $emails
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Klass\ClassInstructor> $instructsClasses
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Student\Enrollment> $enrollments
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Instructor\Position> $positions
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Student\StudentTerm> $studentTerms
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Student\Group> $studentGroups
 */
interface Person extends HasPersonNames, HasStudentIdAttribute, Model {}
