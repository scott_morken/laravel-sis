<?php
declare(strict_types=1);

namespace Smorken\Sis\Contracts\Base\Person;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;

/**
 * @property string $id
 * @property string $ADDR_REC_KEY
 * @property string $studentId
 * @property string $ADDR_EMPLID
 * @property \Smorken\Sis\Oci\Constants\Person\AddressType $type
 * @property string $ADDR_ADDRESS_TYPE
 * @property string $country
 * @property string $ADDR_COUNTRY
 * @property string $countryDescription
 * @property string $ADDR_COUNTRY_LDESC
 * @property string $address1
 * @property string $ADDR_ADDRESS1
 * @property ?string $address2
 * @property ?string $ADDR_ADDRESS2
 * @property ?string $address3
 * @property ?string $ADDR_ADDRESS3
 * @property string $city
 * @property string $ADDR_CITY
 * @property ?string $county
 * @property ?string $ADDR_COUNTY
 * @property string $state
 * @property string $ADDR_STATE
 * @property ?string $stateDescription
 * @property ?string $ADDR_STATE_DESC
 * @property string $postal
 * @property string $ADDR_POSTAL
 * @property ?string $geoCode
 * @property ?string $ADDR_GEO_CODE
 * @property ?string $email
 * @property ?string $ADDR_EMAIL_ADDR
 * @property ?string $emailFlag
 * @property ?string $ADDR_PREF_EMAIL_FLAG
 * @property ?string $phoneCountryCode
 * @property ?string $ADDR_PHONE_COUNTRY_CD
 * @property ?string $phone
 * @property ?string $ADDR_PHONE_NUMBER
 * @property ?string $phoneExtension
 * @property ?string $ADDR_PHONE_EXTENSION
 */
interface Address extends HasStudentIdAttribute, Model
{
}