<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 12:51 PM
 */

namespace Smorken\Sis\Contracts\Base;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

interface Note extends HasCollegeIdAttribute, HasTermIdAttribute, Model {}
