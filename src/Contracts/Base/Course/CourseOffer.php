<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 12:55 PM
 */

namespace Smorken\Sis\Contracts\Base\Course;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Contracts\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;

/**
 * Relations
 *
 * @property \Smorken\Sis\Contracts\Base\Course\CourseCatalog $catalog
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Note> $courseNotes
 */
interface CourseOffer extends HasAcademicCareerAttribute, HasAcademicOrgIdAttribute, HasCollegeIdAttribute, Model {}
