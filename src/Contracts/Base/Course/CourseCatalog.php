<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 12:54 PM
 */

namespace Smorken\Sis\Contracts\Base\Course;

use Smorken\Model\Contracts\Model;

/**
 * Relations
 *
 * @property \Illuminate\Support\Collection<\Smorken\Sis\Contracts\Base\Course\CourseOffer> $offerings
 */
interface CourseCatalog extends Model {}
