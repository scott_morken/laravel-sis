<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/4/18
 * Time: 7:33 AM
 */

namespace Smorken\Sis\Contracts\Base\Instructor;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;

interface Position extends HasStudentIdAttribute, Model {}
