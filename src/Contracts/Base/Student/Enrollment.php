<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 12:57 PM
 */

namespace Smorken\Sis\Contracts\Base\Student;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

/**
 * Relations
 *
 * @property \Smorken\Sis\Contracts\Base\Klass\Klass $class
 * @property \Smorken\Sis\Contracts\Base\Person\Person $student
 * @property \Smorken\Sis\Contracts\Base\Term $term
 */
interface Enrollment extends HasAcademicCareerAttribute, HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute, Model {}
