<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:09 PM
 */

namespace Smorken\Sis\Contracts\Base\Student;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;

interface Degree extends HasAcademicCareerAttribute, HasCollegeIdAttribute, Model {}
