<?php

namespace Smorken\Sis\Contracts\Base\Student;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

/**
 * @property \Smorken\Sis\Contracts\Base\Person\Person $student
 */
interface StudentTerm extends HasAcademicCareerAttribute, HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute, Model {}
