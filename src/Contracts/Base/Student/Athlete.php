<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 12:56 PM
 */

namespace Smorken\Sis\Contracts\Base\Student;

use Illuminate\Support\Collection;
use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasStartEndDateAttributes;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;

/**
 * @property \Smorken\Sis\Contracts\Base\Person\Person $student
 * @property Collection<\Smorken\Sis\Contracts\Base\Student\Enrollment> $enrollments
 */
interface Athlete extends HasCollegeIdAttribute, HasStartEndDateAttributes, HasStudentIdAttribute, Model {}
