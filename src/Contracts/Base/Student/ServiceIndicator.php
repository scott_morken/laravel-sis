<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:15 PM
 */

namespace Smorken\Sis\Contracts\Base\Student;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

/**
 * Relations
 *
 * @property \Smorken\Sis\Contracts\Base\Person\Person $student
 */
interface ServiceIndicator extends HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute, Model {}
