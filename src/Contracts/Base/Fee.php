<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:12 PM
 */

namespace Smorken\Sis\Contracts\Base;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

interface Fee extends HasCollegeIdAttribute, HasTermIdAttribute, Model {}
