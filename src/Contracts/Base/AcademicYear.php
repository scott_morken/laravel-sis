<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/20/17
 * Time: 1:18 PM
 */

namespace Smorken\Sis\Contracts\Base;

use Carbon\Carbon;
use Smorken\Model\Contracts\Model;

/**
 * @property string $id
 * @property string $description
 * @property string $descriptionLong
 * @property Carbon $startDate
 * @property Carbon $endDate
 */
interface AcademicYear extends Model {}
