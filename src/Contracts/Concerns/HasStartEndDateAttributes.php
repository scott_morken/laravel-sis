<?php

namespace Smorken\Sis\Contracts\Concerns;

/**
 * @internal $attributeNames  ->endDate and ->startDate keys
 */
interface HasStartEndDateAttributes
{
    public function getEndDateAttributeName(): string;

    public function getStartDateAttributeName(): string;
}
