<?php

namespace Smorken\Sis\Contracts\Concerns;

/**
 * @internal $attributeNames  ->termId key
 */
interface HasTermIdAttribute
{
    public function getTermIdAttributeName(): string;
}
