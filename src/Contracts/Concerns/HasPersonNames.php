<?php

namespace Smorken\Sis\Contracts\Concerns;

interface HasPersonNames
{
    public function getFirstName(): string;

    public function getLastName(): string;
}
