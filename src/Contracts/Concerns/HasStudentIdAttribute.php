<?php

namespace Smorken\Sis\Contracts\Concerns;

/**
 * @internal $attributeNames  ->studentId key
 */
interface HasStudentIdAttribute
{
    public function getStudentIdAttributeName(): string;
}
