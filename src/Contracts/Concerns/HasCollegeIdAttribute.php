<?php

namespace Smorken\Sis\Contracts\Concerns;

/**
 * @internal $attributeNames  ->collegeId key
 */
interface HasCollegeIdAttribute
{
    public function getCollegeIdAttributeName(): string;
}
