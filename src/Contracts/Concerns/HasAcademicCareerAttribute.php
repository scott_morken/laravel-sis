<?php

namespace Smorken\Sis\Contracts\Concerns;

/**
 * @internal $attributeNames  ->academicCareer key
 */
interface HasAcademicCareerAttribute
{
    public function getAcademicCareerAttributeName(): string;
}
