<?php

namespace Smorken\Sis\Contracts\Concerns;

/**
 * @internal $attributeNames  ->academicOrgId key
 */
interface HasAcademicOrgIdAttribute
{
    public function getAcademicOrgIdAttributeName(): string;
}
