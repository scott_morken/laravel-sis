<?php

namespace Smorken\Sis\Contracts\Enums;

interface Arrayable
{
    public static function toArray(): array;
}
