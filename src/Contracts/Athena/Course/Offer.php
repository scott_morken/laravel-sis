<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena\Course;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasAcademicCareerAttribute;
use Smorken\Sis\Contracts\Concerns\HasAcademicOrgIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;

interface Offer extends HasAcademicCareerAttribute, HasAcademicOrgIdAttribute, HasCollegeIdAttribute, Model {}
