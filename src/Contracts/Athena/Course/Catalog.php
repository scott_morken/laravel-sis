<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena\Course;

use Smorken\Model\Contracts\Model;

interface Catalog extends Model {}
