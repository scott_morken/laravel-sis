<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena\Physical;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;

interface ExternalOrg extends HasCollegeIdAttribute, Model {}
