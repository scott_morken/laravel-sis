<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;

interface ItemType extends HasCollegeIdAttribute, Model {}
