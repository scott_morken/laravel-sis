<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena\Term;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

/**
 * @property string $collegeId
 * @property string $studentId
 * @property string $termId
 * @property string $instructorClass
 * @property string $applyLoadCalculation
 * @property string $applyWorkloadLimit
 * @property float $assignedPercentFTE
 * @property int $multiplier
 */
interface InstructorTerm extends HasCollegeIdAttribute, HasStudentIdAttribute, HasTermIdAttribute, Model {}
