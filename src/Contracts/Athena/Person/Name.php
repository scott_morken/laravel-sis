<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena\Person;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasPersonNames;
use Smorken\Sis\Contracts\Concerns\HasStudentIdAttribute;

/**
 * @property string $timestamp
 * @property string $studentId
 * @property string $nameType
 * @property string $effectiveDate
 * @property string $status
 * @property ?string $prefix
 * @property ?string $suffix
 * @property ?string $title
 * @property string $lastName
 * @property string $firstName
 * @property string $middleName
 * @property ?string $secondLastName
 * @property ?string $preferredFirstName
 * @property ?string $preferredLastName
 * @property string $primaryFirstName
 * @property string $primaryLastName
 */
interface Name extends HasPersonNames, HasStudentIdAttribute, Model {}
