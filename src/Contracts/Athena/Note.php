<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;

interface Note extends HasCollegeIdAttribute, Model {}
