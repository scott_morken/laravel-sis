<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena;

use Smorken\Model\Contracts\Model;

interface TranslateItem extends Model {}
