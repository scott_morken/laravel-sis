<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena\Klass;

use Smorken\Model\Contracts\Model;

interface AncillaryInfo extends Model {}
