<?php

declare(strict_types=1);

namespace Smorken\Sis\Contracts\Athena\Klass;

use Smorken\Model\Contracts\Model;
use Smorken\Sis\Contracts\Concerns\HasCollegeIdAttribute;
use Smorken\Sis\Contracts\Concerns\HasTermIdAttribute;

interface SubFee extends HasCollegeIdAttribute, HasTermIdAttribute, Model {}
